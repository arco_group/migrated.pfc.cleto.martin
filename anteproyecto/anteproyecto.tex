
\section{Introducción}
\label{intro}

En la actualidad existen plataformas que permiten el desarrollo y
despliegue de programas sobre una red de computadores. Los usos de
este tipo de arquitecturas son numerosos: sistemas de vigilancia,
domótica, sistemas de monitorización, etc. El software que permite
esta clase de computación distribuida se conoce como \emph{middleware}
de comunicaciones.

De entre la gran variedad de \emph{middlewares} que existen nos
centraremos en aquellos que están basados en la invocación de métodos
de objetos remotos~\cite{CORBA, zeroc, minimumCORBA}.  Estas
plataformas permiten que los objetos de una aplicación estén
accesibles por toda una red de computadores, proporcionando los
servicios necesarios para que un objeto o aplicación pueda comunicarse
con el resto de forma transparente.

En muchas aplicaciones aparece la necesidad de crear objetos
distribuidos en dispositivos empotrados con capacidades de computación
mínimas y coste reducido~\cite{minimumCORBA, eORB, nORB, UORB, legORB,
  MQC, dynamicTAO, uicCORBA}.  Piénsese, por ejemplo, en una red de
sensores y actuadores. Cada nodo del sistema debe integrarse en el
\emph{middleware} correspondiente y proporcionar servicios. Si
utilizásemos las herramientas habituales en el desarrollo de
aplicaciones para los \emph{middlewares}, los dispositivos que forman
la red necesitarían capacidades de cómputo sensiblemente mayores (y,
por ende, mayor complejidad y coste).

En los últimos años, el laboratorio ARCO ha investigado sobre las
soluciones posibles a este problema~\cite{SENDA}.  Una propuesta que
resuelve éste y otros problemas de las redes de sensores/actuadores
son los \textbf{picoObjetos}~\cite{picoWCMC}.  Los \emph{picoObjetos}
son entidades hardware/software mínimas, diseñadas para arquitecturas
simples, que son fácilmente integrables en entornos distribuidos
orientados a objetos. En el ámbito de las redes de sensores, cada
\emph{picoObjeto} puede realizar una funcionalidad básica como
recolección de datos, transmisión de datos, enrutado\...

Actualmente los \emph{picoObjetos} se construyen en un lenguaje
intermedio llamado \emph{FSM}. El lenguaje \emph{FSM} es una
descripción de un autómata capaz de reconocer mensajes que llegan al
\emph{picoObjeto}. Dicho lenguaje se compone de un conjunto de
primitivas muy simples, de modo que un programa escrito en \emph{FSM}
tienen poca semántica.

Una vez se tiene un programa en código \emph{FSM}, se podría ejecutar
sobre una máquina virtual o, finalmente, traducirlo a código
máquina. Exiten varias implementaciones de la máquina virtual, en
varios lenguajes y para diferentes arquitecturas. También existe un
traductor que genera \emph{bytecode} para la máquina virtual a partir
del código \emph{FSM}. Estas herramientas han sido desarrolladas por
el grupo ARCO.

\section{Objetivos y Requisitos}

Según lo comentado en la sección~\ref{intro}, sería deseable tener una
herramienta que permitiera generar el código \emph{FSM} a partir de
una descripción más abstracta. Una herramienta que, en definitiva,
facilite las labores de diseño, desarrollo y pruebas al programador de
este tipo de sistemas.

Por ello, el proyecto IcePick es un compilador que permite al
programador de este tipo de dispositivos crear objetos de un
\emph{middleware} determinado desde un lenguaje de entrada de alto
nivel, facilitando las fases de desarrollo software.

Habrá que diseñar el lenguaje de entrada (llamado \emph{IPK}) que
deberá ser una ayuda real al programador: un lenguaje fácil de
aprender y abstracto, con el que se puedan modelar muchos de los
problemas habituales en el paradigma distribuidos. El lenguaje tiene
que ser lo suficientemente rico como para integrar la filosofía de la
computación distribuida y soportar operaciones entre objetos y demás
entidades que conforman un sistema de este tipo. Además, debe
incorporar estructuras sintácticas que permitan incluir ficheros
externos con especificaciones, implementaciones de métodos y demás
archivos necesarios para la construcción de objetos.

IcePick, por otro lado, tendrá que dar soporte específico a
\emph{ICE}, un \emph{middleware} desarrollado por \emph{ZeroC}. Sin
embargo, este requisito no debe tomarse como una restricción, ya que
será ampliable a otros middlewares orientados a objetos.

IcePick deberá dar soporte completo a la generación de código
\emph{FSM} (incluida las máquinas virtuales de \emph{FSM} ya
existente). Sin embargo, es posible que en el futuro se realicen más
máquinas virtuales o, simplemente, las necesidades de generación de
código cambien por lo que es necesario que dada una descripción en
alto nivel en \emph{IPK} se puedan obtener distintas salidas. Cuando
exista la necesidad de dar soporte a una arquitectura nueva, IcePick
tendrá que ser fácilmente extensible en este sentido.

Además, IcePick deberá proporcionar apoyo a los servicios alto nivel
desarrollados dentro del grupo ARCO~\cite{GPC07} para aplicaciones de
computación \emph{pervasiva}, inteligencia ambiental, domótica y
aplicaciones con necesidades similares.

\section{Método y Fases de Trabajo}

Primeramente, se realizará un estudio sobre las necesidades que debe
cubrir el lenguaje a diseñar y las relaciones que debe tener con el
\emph{middleware}. Será necesario un aprendizaje de éste. Esta fase de
análisis y diseño concluirá con la especificación de una arquitectura
para el compilador que satisfaga las necesidades de extensibilidad que
el proyecto requiere.

A continuación, se decidirá qué herramientas se utilizarán para la
implementación del proyecto. Al tratarse de un compilador, se
estudiarán las distintas herramientas de generación de procesadores de
lenguajes (\emph{JFlex}, \emph{Flex}~\cite{flex},
\emph{Bison}~\cite{bison}\...).  Se deberá decidir la arquitectura
final del compilador, qué lenguajes debe generar, qué interfaz debe
proveer para una futura ampliación, etc.

Gran parte de la fase de desarrollo estará orientada por pruebas. La
idea principal de este tipo de metodología es la creación de pruebas
automáticas que verifiquen los casos de uso del software. De esta
forma se consigue que trazabilidad de requisitos y se evitan riesgos
en la fase de construcción.

En el caso concreto del compilador, se realizarán pruebas automáticas
para todos los niveles de análisis del lenguaje~\cite{Muchnick}:
\begin{itemize}
\item Nivel léxico: deberán probar que los tokens encontrados son
  correctos y que, en caso de fallo, se actúa convenientemente.

\item Nivel sintáctico: asegurarán que la estructura del lenguaje de
  entrada es correcta y los errores encontrados son los esperados.
\item Nivel semántico: probarán aspectos de alto nivel como conflictos
  entre tipos, eliminación de ambigüedades, comprobación de límites,
  etc.
\item Código generado: estas pruebas chequearán que el código
  resultante es correcto y que el objeto funciona correctamente.
\end{itemize}

Una vez desarrollado se harán las pruebas finales con dispositivos
reales, proporcionados por ARCO, sobre un sistema distribuido ya
instalado. Se harán pruebas de integración así como de funcionalidad.

\section{Medios Disponibles}

Como herramientas para la creación del plan y organización del
proyecto se utilizará \emph{Planner}, un gestor de proyectos de
distribución libre. Y para la documentación del mismo se utilizará
\LaTeX.

En las fases de análisis y diseño se utilizarán herramientas de
modelado incluidas en el entorno de desarrollo \textbf{NetBeans} de
Sun Microsystems, y también de distribución libre.

El desarrollo e implementación del proyecto se realizará con GNU Emacs
y en el lenguaje escogido, en función de las herramientas
elegidas. Todo el desarrollo se realizará sobre un computador
(proporcionado por el laboratorio ARCO) con GNU/Linux como sistema
operativo.

Para las pruebas y la integración final se utilizarán dispositivos
proporcionados por el laboratorio y la máquina virtual \emph{FSM}
existente.

IcePick ha sido seleccionado para participar en el III Concurso
Universitario de Sofware Libre y RedIRIS proporciona un conjunto de
herramientas para asistir el desarrollo del proyecto, que incluyen un
bugtracker, una lista de correcto, un repositorio público de código
con control de versiones, etc.

\section{Propiedad Intelectual}
El software desarrollado en este proyecto será distribuido como
Software Libre, bajo los términos de la licencia GPL versión
3~\cite{gpl}.

Se tendrá en consideración que todas las herramientas y librerías se
utilicen a lo largo del proyecto sean compatibles con la licencia
citada anteriormente.
