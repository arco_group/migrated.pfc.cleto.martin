% -*- coding: utf-8 -*-

\chapter{Conclusiones}
\label{chap:conclusiones}
\fancyMiniToc

En este capítulo se muestran los objetivos alcanzados tras la
elaboración del proyecto. A continuación, en base a nuevas necesidades
futuras y mejoras planteadas a medio y largo plazo, se describen las
propuestas para el futuro.

\section{Objetivos alcanzados}
Después de analizar los resultados de los
capítulos~\ref{chap:lenguajes} y~\ref{chap:ipkc}, se puede decir que
el objetivo general del proyecto (véase
sección~\ref{sec:objetivos_generales}) se ha conseguido en buena
medida. Las aplicaciones y lenguajes desarrollados en el proyecto
proporcionan una gran mejora al proceso de desarrollo de picoObjetos
que había anteriormente. Los usuarios pueden programar dispositivos
desde un alto nivel de abstracción reduciéndose, de esta forma, costes
en términos de tiempo y recursos.

La etapa que consumía más tiempo, antes de la existencia del
compilador, era la especificación de la funcionalidad en \ac{FSM}
(véase sección~\ref{sec:picoObjetos}). En numerosas entrevistas
realizadas, los usuarios perciben que utilizando el compilador el
tiempo para esta etapa oscila entre un 10\% y un 20\% del tiempo total
(incluyendo depuración).

Además, el esquema regular y ortogonal de los lenguajes IcePick y
\ac{SIS} permite crear programas que generen picoObjetos de forma
automática, por ejemplo, para la programación de una red de sensores
completa. Todos los picoObjetos incluidos en activos experimentales
creados del proyecto Hesperia (véase sección~\ref{sec:hesperia}) han
sido generados utilizando el compilador. Esto demuestra que los
lenguajes proporcionan la semántica suficiente al programador para
crear aplicaciones relativamente complejas.

Por otro lado, la estructura modular y extensible del compilador ha
demostrado adaptarse a la aparición de nuevas necesidades. Una buena
prueba de ello es que se ha utilizado en un contexto de investigación,
donde los requisitos y las necesidades del cliente varían
constantemente. Además, la integración de objetos \ac{DUO} activos a
través del sistema de plugins demuestra que el compilador es
fácilmente adaptable a contextos específicos.

Cada una de las etapas principales (frontend y backend) detectan e
informan de los errores de programación al usuario, asegurándose la
generación de código correcto. Por otra parte, las optimizaciones
realizadas sobre el código generado han demostrado ser suficientes
para las necesidades actuales.

Finalmente, el modo de resaltado de sintaxis implementado para
\ac{GNU} Emacs (véase figura~\ref{fig:emacs}) y la carga automática en
el dispositivo son herramientas que, sin duda alguna, ayudan la labor
de desarrollo.

A modo de resumen, se listan las contribuciones de este proyecto:

\begin{variablelist}
\item [Lenguajes de entrada IcePick y \ac{SIS}] Ofrecen abstracción en
  la programación de objetos distribuidos diseñados para ser cargados
  en dispositivos con recursos muy limitados.

  Por un lado, IcePick puede verse como un lenguaje para modelar un
  sistema distribuido de cualquier tipo. \ac{SIS}, por su parte,
  permite integrar la implementación del sirviente. Ésta puede ser
  construida por el mismo programador o bien proporcionada por
  terceras partes.

\item [Compilador \ac{ipkc}] Divido en fases y extensible a través del
  sistema de plugins. Se ha construido frontend para los lenguajes
  \ac{Slice}, IcePick y \ac{SIS}, así como el backend
  \texttt{slice2fsm} que genera el código \ac{FSM} para crear objetos
  del middleware \ac{Ice}.

  Además, se ha desarrollado el backend \texttt{slice2fsm\_stat} para
  la generación de estadísticas de consumo de memoria, código
  generado, etc. Con ello se demuestra que es posible crear nuevos
  generadores sin necesidad de modificar el resto.

\item [Bindings de Python] Se han desarrollado para permitir la
  construcción de generadores en Python, ya que el frontend está
  construido en C++. No obstante, es posible escribirlos tanto en
  Python como en C++.

\item [Pruebas] Para cada uno de los requisitos funcionales se han
  creado pruebas que demuestran su cumplimiento. El plan
  mostrado en el apéndice~\ref{chap:plan} y la cantidad de código
  desarrollado (véase tabla~\ref{tab:sloc2}) demuestran que las
  pruebas han sido de vital importancia en el desarrollo.

\item [Objetos \ac{DUO} activos] Mediante la implementación de los
  plugins semánticos y de generación, se han integrado los objetos
  \ac{DUO} activos. Ello demuestra que la estructura del compilador se
  adapta bien a los cambios en las necesidades.

\item [Herramientas auxiliares] Se ha desarrollado un modo
  (\emph{major-mode}) para \ac{GNU} Emacs que realiza resaltado de
  sintaxis e indentación automática para el lenguaje IcePick. El
  compilador genera \texttt{Makefiles} para la carga automática en el
  dispositivo final. De esta forma, se han proporcionado utilidades
  que ayudan al programador durante el proceso de desarrollo.
\end{variablelist}

\section{Trabajo futuro}
A lo largo del proceso de desarrollo se plantearon algunos objetivos a
medio y largo plazo en proyectos donde se utilizaría el compilador de
forma habitual:

\begin{itemize}
\item \textbf{\texttt{slice2vhdl}}: la creación de un generador de
  código VHDL, se podrían conseguir implementaciones hardware de
  picoObjetos.
\item \textbf{\texttt{idl2fsm}}: utilizando \ac{CORBA} como
middleware de comunicaciones, este gene\-rador permitiría la generación
de picoObjetos para el protocolo \ac{GIOP}.
\item \textbf{\ac{FSM} v4}: está planificado a corto plazo un cambio
  sustancial en el repertorio de instrucciones de \ac{FSM}. El cambio
  supondrá, entre otras ventajas, una mejora en el aprovechamiento de
  los recursos.
\end{itemize}

Por otro lado, desde el proyecto IcePick se proponen posibles mejoras
en los lenguajes y en el compilador. Basadas en la experiencia
adquirida, se exponen algunas de estas ideas:

\begin{itemize}
\item \textbf{Soporte a más de un adaptador local}: en estos momentos,
  las diferentes implementaciones de las máquinas virtuales \ac{FSM}
  no ofrecen la posibilidad de que el nodo local tenga más de un
  adaptador asociado. No obstante, y en previsión de que este hecho es
  cuestión de tiempo, sería posible adaptar los lenguajes y el
  compilador para que fuera posible.

\item \textbf{Código intermedio}: la tabla de símbolos
  \texttt{Semantic::SymbolTable} es la que utilizan los generadores
  para obtener la información necesaria. Sin embargo, se hace
  interesante sustituir esta tabla por un código intermedio debido a
  que sería posible realizar optimizaciones independientes de la
  plataforma destino.

  El diseño de un código intermedio válido para cualquier plataforma
  es una tarea compleja. Los plazos de entrega marcados en el proyecto
  Hesperia hicieron que se tomase la tabla de símbolos como
  solución. No obstante, ha demostrado ser suficiente en vista de los
  resultados obtenidos en términos de funcionalidad.

\item \textbf{Optimizaciones más ambiciosas}: junto con la mejora de
  las optimizaciones realizadas hasta el momento (por ejemplo, el
  optimizador de memoria \acs{ROM}), se plantea la posibilidad de
  crear de un planificador de bloques \ac{FSM}. Debido a
  características propias del bytecode, el tamaño del código generado
  depende en gran medida de la cantidad de saltos entre bloques y la
  separación que haya entre ellos.

  Por ello, se plantea planificación basada en algoritmos de búsqueda
  de inteligencia artificial (como A*) que encuentren la solución
  óptima a la ordenación de bloques \ac{FSM}.
\end{itemize}


\section{Conclusiones personales}

Mi experencia como desarrollador en el proyecto IcePick puede
resumirse como la primera frase del prólogo de~\cite{jflex2}:

\begin{quote}
  \emph{La construcción de un compilador es una de las tareas más
    gratas con las que un informático puede encontrarse a lo largo de
    su carrera profesional.}
\end{quote}

Y es que son pocos los proyectos como IcePick, en los que se ponen a
prueba la gran mayoría de los conocimientos técnicos adquiridos
durante la carrera universitaria: procesamiento de lenguajes,
programación en alto y bajo nivel, redes, sistemas distribuidos, etc.

Sin embargo, el aspecto que más destacaría es que he aprendido a
valorar aún más la importancia de una metodología de desarrollo. Para
que un proyecto como éste, en el que los requisitos funcionales
cambian frecuentemente, es de vital importancia una metodología que
garantice la calidad del producto final. En cada una de las fases de
las iteraciones es necesario tener una actitud «sistemática», sobre
todo en la de diseño y desarrollo.

El plan de pruebas y su implementación es un ejemplo de esta actitud:
«nada funciona, a menos que haya una prueba que lo demuestre». Y,
efectivamente, desarrollar en base a un plan de pruebas significa que
los esfuerzos van dirigidos a cubrir los requisitos funcionales, ni
más ni menos.

Muchas de las modificaciones que he realizado al compilador a lo largo
del proyecto introducían efectos laterales en otros componentes.
Cuando un proyecto crece lo suficiente es muy difícil predecir y
localizar este tipo de errores. Ejecutar las \emph{pruebas después de
  cada modificación} me servía para saber \emph{que todo iba bien} y
que nada había dejado de funcionar. El ahorro de tiempo de depuración
que supone es evidente.


% Local variables:
%   ispell-local-dictionary: "castellano8"
% End:
