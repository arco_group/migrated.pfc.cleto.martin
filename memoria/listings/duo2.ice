#include <IceStorm/IceStorm.ice>

module DUO {
  module Active {
    interface R {
      Object* getCb();
      IceStorm::Topic* getTopic();
    };
    interface W {
      void setCbTopic(Object* publisher, IceStorm::Topic* topic);
    };
  };
};