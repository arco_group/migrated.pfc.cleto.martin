module Ejemplo {
  interface Caudalimetro {
    float caudal();
  };

  interface Log {
    void informar(float f);
  };

  interface Alarma {
    void activar();
    void parar();
  };

  interface Valvula {
    void abrir(byte pasos);
    void cerrar(byte pasos);
  };
};
