#include <Ice/Identity.ice>
#include <IceStorm/IceStorm.ice>

#include <Hesperia/DUO.ice>
#include <Hesperia/ASDF.ice>

module Device {
    interface Switch extends DUO::IBool::R, DUO::Active::W {};
  };
};

