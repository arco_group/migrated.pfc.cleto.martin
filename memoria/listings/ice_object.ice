module Ice {

  sequence<string> StringSeq;

  interface Object {

    ...

    string ice_id();
    StringSeq ice_ids();
    bool ice_isA(string iface);
    void ice_ping();

    ...

  };
};
