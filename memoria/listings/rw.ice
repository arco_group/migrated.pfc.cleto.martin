#include "Hesperia/DUO.ice"

module Devices {
  interface Door extends DUO::IBool::R, DUO::IBool::W {};
};