#include <boost/python.hpp>

#include "Adapter.h"

BOOST_PYTHON_MODULE(IcePyck)
{

  [...]

  class_<Semantic::Adapter>("Adapter", no_init)
  .def("name",            &Semantic::Adapter::getName)
  .def("endpoint",        &Semantic::Adapter::getEndpoint)
  .def("type",            &Semantic::Adapter::getType)
  .def("objects",         &Semantic::Adapter::getObjects)
  ;

  [...]

}
