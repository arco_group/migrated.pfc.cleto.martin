#include <Ice/BuiltinSequences.ice>
#include <Ice/Identity.ice>

[...]

module ASD  {
  interface Listener {
    void adv(Object* prx);
    void bye(Ice::Identity oid);
  };

  [...]

};
