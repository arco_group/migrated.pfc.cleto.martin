
\chapter{Desarrollo del proyecto}
\label{chap:planificacion}
\fancyMiniToc

En el presente capítulo se describe la planificación y las distintas
fases de la evolución del proyecto IcePick. Además, se describe el
plan de pruebas y que ha servido como base principal para validar el
cumplimiento de todos los requisitos planteados. El plan puede
encontrarse en el apéndice~\ref{chap:plan}.

Finalmente, se plantean los resultados sobre los recursos utilizados y
desarrollados, así como una valoración de los costes asociados al
proyecto.

\section{Evolución}
\label{sec:evolucion}

En base a la metodología de desarrollo descrita en la
sección~\ref{sec:metodologia}, el proyecto IcePick se ha dividido en
ocho iteraciones. El resultado final de cada una es un prototipo
evaluable por el cliente. En este caso, miembros del grupo \ac{ARCO}
han sido los que detectaban las carencias del prototipo o proponían
nuevos requisitos basados en la evolución del proyecto Hesperia.

A continuación, se describen cada una de las iteraciones del
proyecto. Aunque no aparezca explícitamente, la tarea de documentación
se ha llevado a cabo en todas ellas utilizando el sistema de gestión
de proyectos\footnote{Accesible a través de su web:
  \url{http://arco.esi.uclm.es}.} de \ac{ARCO}. Además, éste ha sido
uno de los medios de comunicación con el cliente y usuarios del
compilador.

\subsection*{Iteración 0: IcePick y \acs{SIS}}
En esta iteración se planteó como objetivo la creación del lenguaje
IcePick de forma que los programadores obtuvieran una herramienta que
realmente agilizase el proceso de desarrollo de picoObjetos.

Para ello, se realizaron las siguientes tareas:

\subsubsection{Estudio del estado del arte}
Para comprender las necesidades de los usuarios se estudió en
profundidad el contexto en el que trabajan habitualmente:

\begin{itemize}
\item \textbf{Middlewares}: se estudió en profundidad las plataformas
  de sistemas distribuidos orientados a objetos, especialmente
  \ac{Ice}.

\item \textbf{Redes de sensores}: utilizando las publicaciones
  proporcionadas por el grupo, se estudió el concepto de picoObjeto y
  la terminología asociada a las propuestas sobre redes de sensores
  que propone~\cite{pico1, pico2, pico3}.

\item \textbf{Hesperia}: el marco en el que se sitúa el proyecto
  IcePick influye directamente sobre él. Por este motivo, se
  estudiaron las distintas soluciones ya creadas por el grupo
  \ac{ARCO}, como el modelo de información \ac{DUO}~\ref{sec:duo} y el
  protocolo \ac{ASD}~\ref{sec:asd}.

\item \textbf{Proceso de desarrollo}: a través de entrevistas con los
  programadores de \ac{FSM} se obtuvo información sobre las carencias
  que sufría el proceso de desarrollo que utilizaban. Además, se
  programaron distintos picoObjetos funcionales directamente en
  lenguaje \ac{FSM} para conocer y comprender mejor las necesidades.
\end{itemize}

\subsubsection{Planificación}

En base a los requisitos descubiertos en la tarea anterior, se
planificaron las siguientes tareas:

\begin{itemize}
\item Diseño de los lenguajes.
\item Construcción preliminar de los analizadores utilizando \ac{Flex}
  y Bison.
\item Creación de un prototipo para analizar archivos \ac{Slice}.
\item Desarrollo del plan de pruebas para los analizadores léxicos y
  sintácticos.
\end{itemize}

Las tareas de creación de prototipos de analizadores fueron necesarias
para familiarizarse con las herramientas \ac{Flex}, Bison y
\texttt{libslice}, de forma que se minimizase el riesgo por
desconocimiento sobre la tecnología de desarrollo.

\subsubsection{Diseño}
En base a la especificación de requisitos de esta iteración, la
necesidad principal era construir picoObjetos puramente servidores, es
decir, aquellos que sólo reciben invocaciones externas. De esta forma,
la primera versión de IcePick no incluía las estructuras de
\emph{triggers}. Nótese que al tratarse de servidores los adaptadores
remotos carecen de sentido, por lo que tampoco estaban incluidos en
esta versión.

Una vez conocidas las estructuras básicas de IcePick y \ac{SIS}, se
especificó el plan de pruebas para los analizadores.

\subsubsection{Desarrollo}
Primero, se llevó a cabo la implementación del plan de pruebas
utilizando la herramienta \texttt{atheist}. En base a ellas, se
implementaron los analizadores léxicos y sintácticos en C++ utilizando
\ac{Flex} y Bison, respectivamente.

Por otro lado, se creo una prueba de concepto en C++ para el análisis
de archivos \ac{Slice} utilizando \texttt{libslice}.

\subsubsection{Entrega y nuevos requisitos}
Una vez creadas las versiones iniciales de los lenguajes, el cliente
recibió las especificaciones \ac{EBNF} correspondientes. Estuvo
conforme con la estructura general del lenguaje a excepción de algunos
tokens elegidos para el nivel léxico. Por ejemplo, en \ac{SIS}, los
tokens \texttt{local} y \texttt{remote} se diseñaron inicialmente como
\texttt{private} y \texttt{public}, respectivamente.

\subsection*{Iteración 1: Analizador semántico}
En esta iteración, el objetivo fue crear las etapas fundamentales del
frontend del compilador.

\subsubsection{Planificación}
En base a las correcciones propuestas por el cliente en la iteración
anterior y los analizadores léxicos y sintácticos creados, se
planificaron las siguientes tareas:

\begin{itemize}
\item Diseño de la estructura del analizador.
\item Actualización y ampliación del plan de pruebas.
\item Corrección de los errores encontrados.
\item Implementación del nivel semántico.
\end{itemize}

\subsubsection{Diseño}
En todo momento, se tuvo presente el requisito de modularidad del
compilador (véase sección~\ref{sec:ipkc_requisitos}), por lo que se
diseñó del analizador semántico de forma independiente y,
posteriormente, se unificó con el realizado para el nivel léxico y
sintáctico. Las tablas de símbolos de cada uno de los lenguajes fueron
diseñadas precisamente como nexo de unión entre el nivel sintáctico y
el semántico.

Por otro lado, se modificó el plan de pruebas inicial para contemplar
los cambios propuestos por el cliente. Seguidamente, se añadieron las
pruebas necesarias a partir del diseño del análisis semántico. Para
planificar estas pruebas fue necesario seguir una estrategia
sistemática a la hora de definir las pruebas ya que en el nivel
semántico existen muchos posibles casos de fallo que el compilador
debe detectar. Esta estrategia se describe en la siguiente sección.

Parte de las nuevas pruebas planificadas se centraban en comprobar la
unificación del nivel sintáctico con el semántico.

\subsubsection{Implementación}
Primeramente, se corrigieron las pruebas iniciales para adaptarse a
las nuevas necesidades del cliente. Para el caso de los tokens no
válidos, se modificaron los respectivos analizadores léxicos.

A continuación se elaboraron las pruebas del nivel semántico y,
posteriormente, la implementación en C++ del analizador.

Debido a que el conocimiento sobre las tecnologías utilizadas
aumentaba con el tiempo, durante esta fase se realizaron cambios en el
código inicial para hacerlo más eficiente. En este punto, las pruebas
fueron fundamentales para evitar efectos colaterales en los cambios
que se realizaban.

Con vistas a la evaluación del prototipo por parte del cliente, se
creo una aplicación lanzadora en C++ que a partir de los ficheros de
entrada mostraba toda la información semántica contenida en ella.

\subsubsection{Entrega y nuevos requisitos}
A juicio del cliente las carencias en el prototipo fueron las
siguientes:

\begin{itemize}
\item Las opciones de la aplicación lanzadora no eran las
  esperadas. Se planteó la necesidad de incluir opciones como
  \texttt{-I}, \texttt{-d} y \texttt{-o} (véase
  sección~\ref{sec:ipkc_options}).
\item El nivel semántico cometía algunos errores en el análisis no
  contemplados en las pruebas. Por ejemplo, si un método no pertenecía
  a la interfaz más especializada, éste método se consideraba como
  erróneo. No obstante, es posible que lo herede de otra interfaz.
\end{itemize}

\subsection*{Iteración 2: Generador de \ac{FSM}}
En esta iteración, se creó la primera versión funcional del compilador
en su totalidad. Únicamente era posible crear objetos servidores pero
era suficiente para probarlos en una plataforma real. Las necesidades
y plazos que se marcaron en aquel momento en el proyecto Hesperia
hicieron necesario este prototipo funcional.

Las fases que se llevaron a cabo en esta iteración son las siguientes:

\subsubsection{Análisis}
La generación de código implica un conocimiento mayor sobre el
repertorio de instrucciones de \ac{FSM}. Por ello, fue necesario un
estudio más exhaustivo sobre dicho lenguaje utilizando ejemplos
concretos del proyecto Hesperia.

\subsubsection{Planificación}
En base al análisis anterior y los errores encontrados por el cliente
en la iteración anterior, se planificaron las siguientes tareas:

\begin{itemize}
\item Diseño del backend y de su integración con el frontend.
\item Actualización del plan de pruebas.
\item Corrección de los errores.
\item Implementación del backend \texttt{slice2fsm}.
\end{itemize}

\subsubsection{Diseño}
El primer problema que se resolvió fue la unión entre las fases del
frontend y el generador preservando los requisitos de modularidad
(véase sección~\ref{sec:ipkc_requisitos}). Como solución se diseñó la
tabla de símbolos del paquete \texttt{Semantic} junto con las
estructuras asociadas.

Posteriormente, se diseñó la estructura del backend. Éste era mucho
más simple que el mostrado en el capítulo~\ref{chap:ipkc} debido a que
sólo generaba código para objetos servidores. Además, el \ac{AFC} no
existía por lo que se generaba directamente en el archivo de salida.

En base a las estructuras diseñadas para el generador, se añadieron
nuevas pruebas al plan existente. Además, fue necesario incluir la
pruebas correspondientes a la tabla de símbolos, las del resto de
estructuras del paquete \texttt{Semantic} y aquellas que comprobaban
la integración del frontend con el backend. En la siguiente sección
se describen las características especiales de las pruebas del
generador de código.

Por otro lado, se añadieron pruebas del nivel semántico para
contemplar los errores encontrados por el cliente.


\subsubsection{Implementación}
Primeramente, se crearon y modificaron pruebas del analizador
semántico. Después se implementaron las soluciones a los distintos
errores del analizador.

Antes de implementar la tabla de símbolos y el generador en base al
diseño realizado se crearon las pruebas para cada una de estas
entidades. Seguidamente, se implementó la tabla de símbolos y el
generador en C++. Nótese que la implementación final de
\texttt{slice2fsm} está realizada en Python. Por aquel momento no
existía la necesidad de crear backends para otros lenguajes distintos
de C++.

Pese a que el generador de código era sencillo, la implementación en
C++ fue relativamente compleja debido a las pocas facilidades que el
lenguaje proporciona para la manipulación de cadenas (fundamental para
generar el código \ac{FSM} final).

Con vistas a la entrevista con el cliente, se elaboró un estudio sobre
una posible implementación del generador en Python, que ofrece una
gran variedad de utilidades para manipulación de cadenas (como la
clase \texttt{Template}).

\subsubsection{Entrega y nuevos requisitos}
Una vez evaluado el prototipo, el cliente no encontró errores
reseñables en el funcionamiento del compilador. Sin embargo, indicó
que sería necesario que se pudiera generar el bytecode utilizando
\texttt{fsm2data} de forma transparente. Hasta el momento, se debía
ejecutar esta aplicación de forma manual.

La propuesta de implementar el generador en Python fue aprobada por el
cliente siempre y cuando no impidiera la implementación de otros
generadores en C++.

\subsection*{Iteración 3: Bindings de Python}
Sin ningún nuevo requisito digno de mención, en esta iteración se creó
el binding para permitir construir backends en Python y se implementó
el generador \texttt{slice2fsm} y la aplicación lanzadora en éste
lenguaje. Además, se integró \texttt{fsm2data} (escrito en Python) con
la nueva implementación del generador.

Las fases realizadas en esta iteración fueron las siguientes:

\subsubsection{Planificación}

Las tareas programadas fueron las siguientes:

\begin{itemize}
\item Construcción del binding.
\item Construir el generador y la aplicación lanzadora en Python.
\item Integrar la funcionalidad del compilador con \texttt{fsm2data}.
\end{itemize}

\subsubsection{Diseño}
Para cada clase del paquete \texttt{Semantic} se decidió cuáles iban a
ser accesibles por los generadores. Las clases y estructuras de datos
que fueron diseñadas para el binding eran muy dependientes de la
librería \texttt{libboost}.

\subsubsection{Implementación}
En base al diseño anterior, se implementó el binding en lenguaje
C++. Posteriormente, y utilizando los mismos diseños de las
iteraciones anteriores, se construyó \texttt{slice2fsm} y la
aplicación lanzadora en Python. Las diferencias en términos de líneas
de código fueron sustanciales: el backend \texttt{slice2fsm} se redujo
alrededor de un 40\% del tamaño original.

Gracias a que atheist no depende del lenguaje de implementación del
sistema sometido a prueba, los tests construidos fueron
reutilizados. Además, la prueba de que el binding realizaba bien su
trabajo es que las mismas pruebas para el generador C++ validaban el
cumplimiento de los requisitos.

\subsubsection{Entrega y nuevos requisitos}
El cliente evaluó la nueva implementación y fue aceptada. Sin embargo,
propuso nuevas opciones para el compilador: \texttt{--no-ice-methods},
\texttt{--parse-only} y \texttt{--semantic-only} (véase
sección~\ref{sec:ipkc_requisitos}).

\subsection*{Iteración 4: Triggers}
Conforme avanzaba el proyecto Hesperia aparecieron nuevos requisitos
para el compilador. Ahora, los picoObjetos necesitaban realizar
invocaciones hacia el exterior de forma que pudieran comportarse como
clientes y servidores. Este fue uno de los cambios más importantes que
sufrió el compilador debido a que el impacto de los nuevos requisitos
recaía sobre todos los niveles. Sin embargo, el diseño modular del
compilador facilitó en gran medida la adaptación del prototipo.

Las fases que formaron parte de esta iteración fueron las siguientes:

\subsubsection{Análisis}
Para una mejor comprensión sobre la nueva funcionalidad requerida en
el compilador, se realizaron entrevistas con el cliente y un estudio
de la situación del proyecto Hesperia para conocer el origen de estos
requisitos.

Más tarde, se realizó (con la colaboración del cliente) un listado de
los requisitos concretos que el compilador debía cumplir. Debido al
gran número de necesidades nuevas, se acordó dejar para iteraciones
posteriores requisitos adicionales. Por ejemplo, para esta iteración,
sólo se consideraban invocaciones remotas con parámetros de longitud
fija. Las interfaces escalares de \ac{DUO} (véase
sección~\ref{sec:duo}) fueron tomadas como referencia.

\subsubsection{Planificación}
En base a los requisitos extraídos del análisis anterior, se determinó
que las tareas a realizar en esta iteración serían las siguientes:
\begin{itemize}
\item Diseño de las nuevas estructuras necesarias.
\item Integración de las estructuras diseñadas en el compilador.
\item Actualización del plan de pruebas.
\item Actualización de la gramática de los lenguajes de entrada para
  permitir realizar invocaciones.
\item Adecuación de los analizadores del frontend para estas
  estructuras.
\item Generación de invocaciones en \texttt{slice2fsm}.
\item Añadir las nuevas opciones propuestas por el cliente.
\end{itemize}

\subsubsection{Diseño}
Permitir invocaciones hacía necesario que existieran adaptadores
remotos (véase sección~\ref{sec:adaptadores}), hasta entonces
inexistentes. En base a la lista de requisitos obtenida se determinó
que la mejor manera de modelar el comportamiento de un picoObjeto es a
través de los triggers de IcePick.

Por otro lado, se añadieron al diseño las clases (\texttt{Trigger},
\texttt{Invocation}, etc.) a cada uno de los niveles del compilador
para representar a las nuevas entidades. Esta tarea se realizó
teniendo siempre presente la división en fases y que existiera
coherencia con el diseño del compilador. También fue necesario añadir
nuevas estructuras al binding.

El diseño del generador \texttt{slice2fsm}, originalmente bastante
sencillo, debía adaptarse a las nuevas estructuras lo que hacía
inviable mantener el diseño intacto. De esta necesidad surgió una
primera versión del \ac{AFC}.

Todo el diseño se realizó con la idea de que existía parte de
funcionalidad que no se iba a implementar en ese momento. Por ejemplo,
pese a que sólo se permitían invocaciones remotas, se diseñaron clases
como \texttt{Request} del generador dejando la puerta abierta a
distintos tipos de invocaciones.

Conociendo todas las estructuras de datos nuevas necesarias, se
actualizó el plan de pruebas a todos los niveles. El nivel en que más
se añadieron pruebas fue el semántico, debido a que las invocaciones
introducían muchas combinaciones válidas y erróneas.

\subsubsection{Implementación}
La implementación se realizó por niveles. Partiendo de los
analizadores léxicos y sintácticos, se escribían las pruebas de los
requisitos funcionales de una fase concreta. Posteriormente, se
programaba el código correspondiente para satisfacer todas las
pruebas. Una vez actualizado y probado toda la fase, se procedía de la
misma forma con la siguiente en orden de compilación.

El mayor esfuerzo de implementación se centró en el analizador
semántico ya que era necesario satisfacer un gran número de pruebas
(véase apéndice~\ref{chap:plan}).

\subsubsection{Entrega y nuevos requisitos}
Tras la evaluación del prototipo, el cliente propuso los siguientes
requisitos:

\begin{itemize}
\item Las invocaciones debían poder ser también locales y permitir
  parámetros de longitud variable.
\item Los eventos asociados a los distintos \emph{triggers} debían
  poder ser activados, desactivados y ejecutados de alguna forma a lo
  largo del programa.
\item El compilador debería dar soporte a la programación directa de
  los dispositivos tras la compilación.
\end{itemize}

\subsection*{Iteración 5: Etiquetas y parámetros de longitud variable}
Como se dijo anteriormente, gran parte de los nuevos requisitos tienen
como origen las necesidades de la iteración anterior. En general, en
esta iteración se creó una versión estable del compilador que
satisfacía gran parte de los objetivos del proyecto.

Las fases fueron las siguientes:

\subsubsection{Análisis}
Permitir parámetros de longitud variable entraba en conflicto con las
posibilidades que ofrecía el repertorio de instrucciones \ac{FSM} en
su versión 3. Por ello, se realizó una entrevista con el cliente para
valorar si era posible la modificación de \ac{FSM}. La adaptación
requería que fuera posible reservar memoria de forma dinámica.

En dicha entrevista se descartó tal adaptación debido a los plazos
marcados en el proyecto Hesperia ya que retrasaría demasiado los
activos experimentales. Sin embargo, se valoró diseñar una nueva
\ac{FSM} en el futuro de forma que se solventara problemas
sobrevenidos por el repertorio de instrucciones.

\subsubsection{Planificación}
Las tareas planificadas para esta iteración fueron las siguientes:
\begin{itemize}
\item Diseño de las nuevas estructuras asociadas a los nuevos requisitos.
\item Construcción del soporte a la programación de dispositivos.
\item Actualizar el plan de pruebas.
\end{itemize}

\subsubsection{Diseño}
Para proporcionar soporte a los parámetros de longitud variable se
amplió la gramática del lenguaje \ac{SIS}. Debido a que era un
lenguaje sencillo, el diseño de las nuevas estructuras y su
integración fue relativamente sencillo.

Gracias a que el diseño de la iteración anterior fue lo
suficientemente general, la integración fue sencilla. Las invocaciones
locales podían ser modeladas con las clases ya diseñadas habituales en
el lado del frontend y una especialización de \texttt{Request} en el
lado del generador.

Para desactivar y activar eventos se diseñaron las etiquetas de los
triggers que permitían invocar métodos sobre ellos. Añadiendo
relaciones entre las clases del frontend, la integración de esta
funcionalidad fue trivial. Desde el punto de vista del analizador
semántico, un trigger es un objeto que puede recibir
invocaciones. Ello justifica la relación de herencia de la
figura~\ref{fig:semantico_objetos}.

Al final del proceso de compilación se incluyeron las funciones y
estructuras de datos para dar soporte a la programación de
dispositivos. Para ello, se añadieron las opciones como \texttt{-b} o
\texttt{--list-platforms} (véase sección~\ref{sec:ipkc_options}).

El plan de pruebas se actualizó para dar cobertura a la nueva
funcionalidad.

\subsubsection{Implementación}
En base a lo diseñado anteriormente, se implementaron las pruebas para
la nueva funcionalidad y, utilizándolas como guía, se implementó el
código C++ y Python para el frontend y el backend, respectivamente.

La herramienta para asistir en la carga del código generado en los
dispositivos se creo en Python. La idea fue generar archivos Makefile
de forma que fuera posible compilar con \ac{ipkc} y cargar
automáticamente el bytecode generado por \texttt{fsm2data} en el
dispositivo a través de reglas de \texttt{make}.

\subsubsection{Entrega y nuevos requisitos}
La evaluación del prototipo por parte del cliente fue positiva y las
carencias que encontró fueron de poca importancia a nivel funcional.

\subsection*{Iteración 6: Optimizaciones y Functions}
Conforme avanzada el proyecto Hesperia los picoObjetos que se
construían eran cada vez más complejos. Muchos de los triggers
contenían secuencias de invocaciones comunes y los problemas de
espacio en el picoObjeto pronto empezaron a aparecer.

En esta iteración se realizaron las siguientes fases:

\subsubsection{Análisis}
Para entender mejor las necesidades del cliente se realizó una
entrevista para determinar los problemas de recursos que presentaba el
código generado con el compilador. Se estudiaron ejemplos construidos
para el proyecto Hesperia que no era posible cargarlos en
dispositivos finales. En ellos, se detectó bloques \ac{FSM} que podían
ser eliminados ya que eran generados por el comportamiento general del
compilador.

En general, de la entrevista se concluyó que el no reutilizar
invocaciones y la no existencia de una planificación de la memoria
\ac{ROM} eran los principales problemas. Por ello, para esta iteración
se plantearon como requisitos construir un optimizador de memoria y
dar soporte a reutilización de invocaciones IcePick.

\subsubsection{Planificación}
Para satisfacer los requisitos se planificaron las siguientes tareas:

\begin{itemize}
\item Actualizar el plan de pruebas.
\item Modificar el lenguaje IcePick para que soportara reutilización
  de invocaciones.
\item Creación de los optimizadores necesarios.
\end{itemize}

\subsubsection{Diseño}
La estructura \texttt{function} de IcePick fue diseñada para
proporcionar reutilización de un conjunto de invocaciones. La
integración de esta nueva estructura se ideó de forma que mantuviera
la coherencia del lenguaje y, de esta forma, fuera fácilmente
integrable en el diseño de las distintas etapas del compilador.

Por otro lado, los optimizadores se diseñaron para que actuasen tras
la construcción del \ac{AFC}. Para satisfacer los requisitos, se
diseñó un optimizador de memoria \acs{ROM} y otro para la eliminación
de código muerto y de bloques innecesarios. Para mantener la
funcionalidad que el compilador venía ofreciendo, se añadió la opción
de optimización \texttt{-O} de modo que en caso de que no se
presentara no se realizarían las optimizaciones.

El plan de pruebas se actualizó con nuevas pruebas para los
generadores y las estructuras \texttt{function}. En los generadores se
consideró como prueba suficiente que el código generado fuera
igualmente funcional.

\subsubsection{Implementación}
Con el plan actualizado, se implementaron las pruebas destinadas a
comprobar el funcionamiento e integración de las estructuras
\texttt{function}. En base a éstas, se elaboró el código necesario
para su integración en el compilador.

Para el optimizador de memoria \acs{ROM} se estudiaron distintas
alternativas. La primera aproximación fue construir un algoritmo
óptimo para encontrar las cadenas comunes basado en la escritura T9 de
los dispositivos móviles. Este algoritmo se basa en la construcción de
un árbol léxico-gráfico en el que, en base a las cadenas introducidas,
se añaden nodos por cada carácter. Conforme se añaden cadenas al
algoritmo, aquellos caracteres que ocupan la misma posición y tienen
el mismo valor se unifican en un mismo nodo. De esta forma, el
resultado del algoritmo es un árbol con el número de nodos mínimo para
todas las cadenas de la memoria \acs{ROM}.

Otra alternativa no óptima pero más sencilla de implementar es la
detección de la subcadena común más larga entre pares de cadenas. Con
esta idea, se implementó el optimizador de memoria \acs{ROM}. Estudios
sobre las posibilidades de esta implementación revelaron unos buenos
resultados para el contexto del proyecto Hesperia. Sin embargo, en
caso de no ser suficiente para el cliente se plantearía la primera
aproximación.

\subsubsection{Entrega y nuevos requisitos}
El cliente valoró positivamente la implementación elegida para el
optimizador de memoria \acs{ROM}. Para los ejemplos que planteaban
problemas, las optimizaciones junto con las nuevas estructuras
\texttt{function} hacían posible la carga de los programas.

Por otro lado, el cliente propuso que el compilador soportarse el caso
especial de los objetos activos de \ac{DUO} para la siguiente
iteración.

\subsection*{Iteración 7: Objetos \ac{DUO} activos}
Como respuesta a nuevas necesidades en el proyecto Hesperia, se hizo
necesario el soporte para objetos \ac{DUO} activos. En esta iteración
se implementó el sistema de plugins.

\subsubsection{Análisis}
Debido a las características del modelo de objetos activos (véase
sección~\ref{sec:duo}), se vio que era necesario que los objetos
tuvieran los atributos \texttt{publisher} y \texttt{topic}. Además,
los adaptadores y las identidades de objetos podían ser desconocidas
en tiempo de compilación. Hecho que hasta ahora no se contemplaba.

De esta forma, se realizó una entrevista con el cliente para acordar
los resquisitos específicos. Uno de los requisitos más importantes
extraidos fue que la integración de los objetos activos no podía ser
una solución adhoc al problema ya que, en el futuro, se deberían poder
integrar otros modelos.

Por otro lado, el compilador debía dar soporte directo a la
generación, por lo que se suponía que el sirviente no implementaba los
métodos. Se observó que no era posible generar código para obtener los
parámetros de longitud variable de las invocaciones. Como solución, el
cliente estableció unos tamaños fijos para los proxies del observador
y del canal.

\subsubsection{Planificación}
En base a los requisitos, se planificaron las siguientes tareas:

\begin{itemize}
\item Diseño e integración de las entidades derivadas de los objetos
  activos.
\item Actualización del plan de pruebas.
\item Implementación de las nuevas estructuras.
\end{itemize}

\subsubsection{Diseño}
Como solución se propuso el sistema de plugins (véase
sección~\ref{sec:ipkc_plugins}) de forma que necesidades futuras
afectaran lo menos posible a la estructura del compilador.

Al plan de pruebas se añadieron las correspondientes a la carga e
integración de los plugins en la estructura, así como las que probaban
el funcionamiento correcto de objetos activos generados.

\subsubsection{Implementación}
Una vez implementadas las pruebas añadidas en esta iteración, se
implementó el sistema de plugins y un plugin para objetos activos. La
componente semántica del plugin fue implementada en C++ y la de
generación en Python.

\subsubsection{Entrega y nuevos requisitos}
Una vez entregado el prototipo, el cliente valoró positivamente su
funcionalidad.

\section{Pruebas}
\label{sec:pruebas}
En el desarrollo del proyecto se ha tenido siempre presente el plan de
pruebas (véase apéndice~\ref{chap:plan}). Para cada uno de los
requisitos funcionales definidos por el cliente se crearon pruebas
automáticas que respaldaban su cumplimiento. Como plataforma de
pruebas se ha utilizado atheist (véase
sección~\ref{sec:aplicaciones}).

En el disco adjunto (directorio \texttt{test}) pueden encontrarse
todas las pruebas durante el desarrollo del proyecto.

\subsection{Frontend}
El esquema que se ha seguido para la de la creación de las pruebas de
los analizadores léxicos y sintácticos ha sido el siguiente:

\begin{itemize}
\item Por cada estructura sintáctica se crearon ficheros de entrada.
\item Uno de estos ficheros contenía la estructura bien formada, el
  resto eran combinaciones erróneas en la construcción.
\item Por cada uno de estos ficheros, se creó una prueba que evaluaba
  el comportamiento
\end{itemize}

El nivel semántico introdujo una complejidad mayor en términos del
plan de pruebas. La estrategia anterior no bastaba para probar
todas las situaciones ya que el nivel semántico afecta al conjunto y
no sólo a cada estructura concreta.

Por ello, primeramente se crearon las pruebas semánticas que afectaban
a cada estructura siguiendo el esquema anterior. Por ejemplo, las
invocaciones pueden tener objetos que no están declarados, un número
de argumentos inválido, un método no válido para la interfaz
implementada, etc.

Seguidamente, se platearon las pruebas propias de cada lenguaje. Por
ejemplo, duplicación de objetos en IcePick y de identificadores de
eventos en \ac{SIS}. \ac{Slice} se distribuye con su propia batería de
pruebas, por lo que no fue necesario crear otra.

Finalmente, se diseñaron las pruebas semánticas que requerían consulta
de información de las tres tablas de símbolos. Un ejemplo es la
existencia de un determinado método local (\ac{SIS}) para un tipo
concreto (\ac{Slice}) de objeto IcePick.

\subsection{Backend}
Basándose en las interfaces de \ac{DUO} y \ac{ASD} se diseñaron
pruebas que incluían distintas combinaciones de objetos y
funcionalidad. Partiendo de objetos puramente servidores, se
plantearon pruebas aumentando la complejidad hasta llegar a tener
objetos activos en un adaptador local, pasando por pruebas específicas
para los triggers y \texttt{functions}.

Todo el código generado se cargaba en la implementación Python de la
máquina virtual \ac{FSM} y, aprovechando la salida de depuración de
ésta, se comprobaba que funcionaba correctamente. Para interactuar con
el picoObjeto fue necesario implementar clientes \ac{Ice} que enviaran
y recibieran invocaciones del picoObjeto.

\section{Recursos y costes}
\label{sec:recursos}

Hasta la última iteración, el proyecto ha durado 10 meses. Para el
desarrollo del proyecto se han utilizado los recursos mostrados en la
tabla~\ref{tab:costes}. Las especificaciones técnicas de los
dispositivos descritos puede verse en la sección~\ref{sec:hardware}.

\begin{table}[h!]
  \centering
  \input{tabs/costes}
  \caption{Resumen de costes del proyecto}
  \label{tab:costes}
\end{table}

\footnotetext{Sobre un salario de 15.840,49~\euro\ brutos por año.}

Por otro lado, la tabla~\ref{tab:sloc} muestra un resumen sobre el
número de líneas de código construidas en el proyecto por cada módulo
principal. Las líneas de código para las pruebas incluyen la
especificación de la prueba para atheist y los programas auxiliares
necesarios. Las pruebas de los plugins semánticos se cuentan como
parte de las del nivel semántico y aquellas referentes los plugins de
generación se incluyen en las pruebas del backend \texttt{slice2fsm}.

\begin{table}[h!]
  \centering
  \input{tabs/sloc}
  \caption{Líneas de código por módulo}
  \label{tab:sloc}
\end{table}

A modo de resumen, la tabla~\ref{tab:sloc2} muestra el número de
archivos y líneas totales para cada uno de los lenguajes de
programación.

\begin{table}[h!]
  \centering
  \input{tabs/sloc2}
  \caption{Número de archivos y líneas de código por lenguaje}
  \label{tab:sloc2}
\end{table}


% Local variables:
%   ispell-local-dictionary: "castellano8"
% End:
