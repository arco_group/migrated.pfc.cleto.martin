% -*- coding: utf-8 -*-

\chapter{Métodología y herramientas}
\label{chap:metodo}
\fancyMiniToc

A continuación, por un lado se describe la metodología de desarrollo
escogida que, como se verá, está condicionada por el contexto del
proyecto Hesperia.

Por otro, se listan las herramientas utilizadas, así como una breve
descripción de cada una de ellas. Algunas ya han sido mencionadas en
el capítulo~\ref{chap:antecedentes} como antecedentes.

\section{Metodología de desarrollo}
\label{sec:metodologia}

IcePick es parte de un proyecto de investigación llamado Hesperia en
el que participan varias universidades y empresas a nivel nacional. El
compilador y los lenguajes diseñados han cambiado con frecuencia
debido a nuevos requisitos de funcionalidad en los picoObjetos,
diversidad en las tecnologías utilizadas, etc.

La propiedad de extensibilidad del compilador reduce el impacto de
cambios en los requisitos sobre el proceso de desarrollo. Si se
requiere una funcionalidad específica nueva (nuevo backend, añadir
soporte para otros middlewares, etc.) sólo es necesario aportar el
nuevo componente.

Sin embargo, si el diseño de los lenguajes de entrada cambia (debido a
una mejora en \ac{FSM}, nuevas necesidades del programador, etc.)
afecta a gran parte del compilador. Añadir, eliminar o modificar la
sintaxis afecta, en el peor de los casos, a todos los niveles del
compilador. No obstante, su diseño modular permite realizar los
cambios necesarios de forma localizada.

Todo lo anterior no implica que de los objetivos principales del
proyecto (capítulo~\ref{chap:objetivos}) hayan cambiado durante su
elaboración sino que, debido a la propia naturaleza de
Hesperia, varios requisitos funcionales han variado a lo largo del
proceso de desarrollo.

\subsection{Prototipado evolutivo}
\label{sec:prototipado_evolutivo}
Para subsanar las posibles consecuencias de los problemas anteriores,
la metodología que se ha utilizado es \textbf{prototipado
  evolutivo}~\cite{ingenieria_software}, pensado para proyectos de
tamaño pequeño y medio (no más de 500\,000 líneas de código) donde los
requisitos cambian a lo largo del tiempo o no están perfectamente
definidos de antemano. El esquema general del modelo de prototipado
evolutivo se muestra en la figura~\ref{fig:prototipado}.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.75\linewidth]{prototipado.pdf}
  \caption{Modelo de desarrollo de prototipado evolutivo}
  \label{fig:prototipado}
\end{figure}

El prototipado evolutivo necesita que los objetivos generales sean
previamente conocidos, pero los requisitos funcionales específicos
pueden ser parcialmente conocidos al comienzo. Por ello, partiendo de
una especificación no completa de los requisitos funcionales, se
comienza la elaboración de un prototipo que cumple con todos los
requerimientos elaborados en las etapas de análisis y diseño.

Una vez que se verifica que el prototipo es completamente funcional y
satisface todos los requisitos, se entrega al cliente para que sea
utilizado. Mediante el uso del sistema prototipo, el cliente puede
detectar carencias debidas a requisitos nuevos o no contemplados
inicialmente. Si es así, se realiza una nueva captura de requisitos y
el proceso se repite mientras el cliente encuentre
nuevos requisitos.

Cada vuelta en el modelo de prototipado evolutivo es una iteración que
añade funcionalidad al prototipo. A diferencia de la metodología de
prototipo desechable, el prototipado evolutivo conserva la versión
inicial en términos de requisitos para todas las iteraciones
siguientes.

No obstante, esta metodología tiene una serie de
inconvenientes~\cite{ingenieria_software}:
\begin{itemize}
\item Necesita una alta participación del usuario. No obstante, en el
  marco del proyecto Hesperia es posible obtener el \emph{feedback}
  necesario para realizar cada iteración.
\item Suele ser complicado redactar el contrato entre el usuario final
  y los desarrolladores ya que, en la mayoría de los casos, los
  contratos se establecen en términos de requisitos. Sin embargo, en
  el proyecto Hesperia se establecen objetivos y no requisitos
  funcionales (que los puede establecer el propio equipo de
  desarrollo).
\item Pueden existir problemas de mantenimiento debido al modelo de
  prototipo ya que cambia continuamente. Existe el riesgo, por tanto,
  de que los desarrolladores pueden especializar demasiado el
  prototipo y hacer imposible el mantenimiento del proyecto por parte
  de personal no familiarizado.

  Para mitigar los posibles problemas de mantenimiento derivados de lo
  anterior, se ha optado por un desarrollo dirigido por
  pruebas~\cite{xp} en las que cada requisito funcional está
  respaldado por una o más pruebas. En la siguiente sección se explica
  en detalle éste método de desarrollo.
\end{itemize}

Claramente, este modelo de desarrollo necesita de la participación
activa por parte del usuario final, que en el marco del proyecto de
investigación Hesperia tiene sentido.

\subsection{Desarrollo dirigido por pruebas}
\label{sec:desarrollo_pruebas}
La fase de construcción del proyecto se ha basado en las pruebas de
forma que cada una de ellas avala el cumplimiento de un requisito
funcional concreto.

La propia naturaleza de un compilador facilita la labor de la creación
de pruebas ya que la salida puede conocerse de antemano. Sin embargo,
una simple comparación entre el código esperado y el código resultante
de la compilación no es suficiente para la gran mayoría de las
pruebas. Por ello, casi todas las pruebas implementadas ejecutan el
código generado y se somete a verificación el sistema en
funcionamiento.

El desarrollo basado en pruebas puede verse como parte de la
metodología de programación extrema~\cite{xp}. Bajo esta filosofía, es
necesario crear las pruebas antes que el código de la aplicación. Las
ventajas de esta metodología sobre el proyecto IcePick son numerosas:

\begin{itemize}
\item Cualquier modificación que tenga efectos laterales sobre
  componentes ya probados es fácilmente detectable. En un sistema en
  los que los requisitos cambian con el tiempo este aspecto es clave
  para mantener la estabilidad del prototipo a lo largo del ciclo de
  vida.

\item Cada requisito funcional está reflejado en una prueba
  que lo avala. De esta forma, el usuario final puede comprobar que se
  cumple el comportamiento general esperado por él mismo.

\item La productividad aumenta considerablemente porque se invierte
  mucho menos tiempo en la depuración. Los errores son fácilmente
  localizables.
\end{itemize}

En base al análisis y diseño realizado a partir de los requisitos
parciales, en la etapa de construcción (véase
figura~\ref{fig:prototipado}) las pruebas del sistema se realizan
antes de la codificación. Cada prueba verifica un caso de uso esperado
por el usuario.


\section{Herramientas}
\label{sec:herramientas}

En el proyecto se han utilizado varios tipos de herramientas de
desarrollo. Además, los prototipos se han probado sobre plataformas
hardware reales proporcionadas por el grupo \ac{ARCO}. Muchos de los
dispositivos programados incorporan sensores, actuadores, conectividad
inalámbrica, funcionamiento con baterías, etc.

A continuación, se muestra una lista pormenorizada del software y
hardware utilizado en el proyecto.

\subsection{Aplicaciones de desarrollo}
\label{sec:aplicaciones}

\begin{description}
\item[\textbf{Flex}] Se trata de un generador de analizadores léxicos
  (véase sección~\ref{sec:flex}). El nivel léxico de los lenguajes
  diseñados en este proyecto están formados por analizadores generados
  con Flex~\cite{flex}.

\item[\textbf{Bison}] Los analizadores sintácticos de los lenguajes
  anteriores han sido creados con \ac{GNU} Bison~\cite{bison}, de tal
  forma, que los niveles sintácticos del compilador están descritos
  como gramáticas libres de contexto que representan su estructura. En
  la sección~\ref{sec:flex} se describe en profundidad la herramienta.

\item[\textbf{\textbf{ZeroC \ac{Ice}}}] middleware de comunicaciones
  orientado a objetos fabricado por la empresa ZeroC~\cite{ice}.

  La integración de \ac{Slice} se ha realizado utilizando librerías
  que ofrece \ac{Ice}.

\item[\textbf{\texttt{libboost}}] librería de utilidades para C++. En
  el proyecto se ha utilizado \texttt{libboost}~\cite{libboost} para
  la creación de un \emph{binding} de Python para C++ con el objetivo
  de escribir generadores de código en este lenguaje.

\item[\textbf{\texttt{fsm2data}}] traductor de código \ac{FSM} a
  bytecode desarrollado por el grupo \ac{ARCO}. El compilador integra
  esta herramienta.

\item[\textbf{Implementaciones de picoObjetos}] el grupo \ac{ARCO} ha
  desarrollado numerosas máquinas virtuales para distintas plataformas
  y utilizando distintas tecnologías:
  \begin{itemize}
  \item Implementación en Python para propósitos de depuración.
  \item Implementación en Java, tanto para la \ac{JVM} de Sun
    Microsystems como para \ac{leJOS}~\cite{lejos}.
  \item Implementación tinyOS para dispositivos CrossBow.
  \item Implementación en esamblador para PIC.
  \end{itemize}

\item[\textbf{Endpoint \ac{XBow}}] algunos dispositivos utilizados en
  este proyecto utilizan como protocolo de comunicación \ac{XBow} para
  dispositivos fabricados por la empresa CrossBow. Debido a que
  \ac{Ice} no soporta por defecto este protocolo, el grupo \ac{ARCO}
  implementó el soporte necesario para la integración de \ac{XBow}
  dentro de \ac{Ice}.

\item[\textbf{atheist}] plataforma de pruebas. Desarrollado por el
  grupo \ac{ARCO}, se trata de un entorno que ofrece la siguiente
  funcionalidad:

  \begin{itemize}
  \item \textbf{Creación de pruebas de caja blanca}: De esta forma, se
    probaron las distintas clases y estructuras definidas en el
    proyecto.
  \item \textbf{Creación de pruebas de caja negra}: Los objetos
    generados fueron probados en tiempo de ejecución utilizando la
    implementación Python de la máquina virtual de \ac{FSM}.

  \item \textbf{Especificación declarativa}: Las pruebas se describen
    en términos declarativos de forma que la ejecución de las mismas
    es responsabilidad de atheist y no del programador. En definitiva,
    en las pruebas sólo se indica qué entidades se quieren probar y no
    la implementación de la prueba en sí misma. Para ello, atheist
    ofrece diferentes estructuras de datos.

  \item \textbf{Información sobre los resultados}: Una vez ejecutadas,
    se muestra información útil y concreta sobre pruebas superadas y
    fallidas.
\end{itemize}

En este proyecto, se ha utilizado para la elaboración de la batería de
pruebas que satisfacen los requisitos del proyecto.

\item[\textbf{\ac{GNU} \ac{GCC}}] compilador de GNU para
  C/C++~\cite{gcc}.

\item[\textbf{\texttt{make}}] herramienta para construcción y
  compilación automática de aplicaciones~\cite{make}. El proceso de
  compilación del proyecto está automatizado con esta herramienta
  (incluída su documentación).

  Además, se ha utilizado para dar soporte automático a la carga del
  código generado en los dispositivos finales. Mediante la generación
  de archivos \emph{Makefile}, el proceso de carga de software se ha
  integrado en el compilador.

\item[\textbf{\ac{GNU} Emacs}] entorno de desarrollo de
  \ac{GNU}. Emacs~\cite{emacs} ha sido utilizado tanto en el proceso
  de construcción del compilador como en las tareas de documentación.

\item[\textbf{Subversion}] control de versiones
  centralizado. Utilizado para la gestión de la documentación del
  proyecto.

\item[\textbf{Mercurial}] control de versiones distribuido que ha sido
  utilizado para la construcción del compilador.

\end{description}

\subsection{Lenguajes de programación}
\begin{description}
\item[\textbf{Python}] es un lenguaje de alto nivel, interpretado y
  orientado a objetos~\cite{python-reference}. De entre sus mayores
  ventajas cabe resaltar la simplicidad del código de los programas
  resultantes y la potencia de su librería estándar.

  Python ha sido utilizado para implementar gran parte del compilador:
  los generadores de código y las extensiones de éstos.

\item[\textbf{C++}] creado por Bjarne Stroustrup~\cite{c++}, C++
  proporciona mecanismos de orientación a objetos y compatibilidad con
  C. Es un lenguaje compilado que ofrece distintos niveles de
  abstracción al programador.

  \ac{Ice} proporciona librerías para la manipulación de ficheros
  \ac{Slice}. Dicha librería se encuentra implementada en C++ y,
  además, Bison y Flex pueden generar código C++. Debido a esto, y no
  habiendo restricciones al respecto, se optó por este lenguaje para
  la implementación del frontend del compilador.

\item[\textbf{\ac{GNU} \acs{ELISP}}] \ac{GNU} \ac{ELISP} es la versión
  de \ac{LISP} utilizada para la programación de modos de
  Emacs~\cite{elisp}. Se ha utilizado para la creación de un entorno
  de desarrollo donde el resaltado de sintaxis y la indentación
  automática del código fuente IcePick ayudan al programador
  utilizando \ac{GNU} Emacs como editor.
\end{description}


\subsection{Documentación}

\begin{description}

\item[\textbf{Inkscape}] editor de imágenes vectoriales. Utilizado en
  la creación de la gran parte de las figuras del presente documento.

\item[\textbf{Dia}] editor de diagramas. Se ha utilizado para
  construir los diagramas de clases, secuencia, etc.

\item[\textbf{Gimp}] software de edición de imágenes y retoque
  fotográfico. Utilizado en la maquetación de fotografías e
  ilustraciones.

\item[\textbf{\LaTeX}] la documentación del proyecto ha sido maquetada
  utilizando esta herramienta~\cite{latex}.
\end{description}

\subsection{Sistemas operativos}

\begin{description}
\item[\textbf{Debian \ac{GNU}/Linux}] distribución del sistema
  operativo \ac{GNU} con núcleo Linux. La versión utilizada es la
  compilada para máquinas con arquitecutra PC compatible (versión
  i386).

\item[\textbf{tinyOS}] sistema operativo diseñado para dispositivos
  que forman parte de una red de sensores
  inalámbrica~\cite{tinyos}. Las motas CrossBow utilizan este sistema.
\end{description}

\subsection{Hardware}
\label{sec:hardware}
En el proyecto IcePick se han utilizado dispositivos en los que se han
cargado picoObjetos generados por el compilador y se ha probado su
funcionalidad en sistemas distribuidos existentes.

Uno de estos componentes hardware es el kit de robótica NXT fabricado
por Lego Mindstorms. Consta de una unidad central de procesamiento,
mostrada en la figura~\ref{fig:NXT-brick}, a la cual pueden añadirse
sensores y actuadores. En la figura~\ref{fig:NXT-sensors} se muestran
algunos de ellos.

\begin{figure}[h!]
  \centering
  \subfigure[Unidad de procesamiento NXT] {
    \includegraphics[width=0.45\linewidth]{NXT-brick.jpg}
    \label{fig:NXT-brick}
  }
  \subfigure[Sensores y actuadores NXT] {
    \centering
    \includegraphics[width=0.45\linewidth]{NXT-sensors.jpg}
    \label{fig:NXT-sensors}
  }
  \caption{Kit Lego Mindstorm}
\end{figure}

Lego NXT~\cite{nxt} utiliza una implementación de la máquina virtual
de Java llamada \ac{leJOS}. Las características hardware más
reseñables de este producto son:

\begin{itemize}
\item Procesador ARM de 32 bits a 48 MHz.
\item 256 KB de memoria flash y 64 KB de \ac{RAM}
\item 4 puertos de entrada para sensores.
\item 3 puertos de salida para actuadores.
\item Interfaz de conexión Bluetooth y \ac{USB}.
\end{itemize}

Gracias a la implementación existente del picoObjeto para \ac{leJOS},
se han realizado pruebas del código generado por el compilador sobre
el NXT.

Otros dispositivos utilizados han sido los fabricados por CrossBow
(figura~\ref{fig:xbow}). Se trata de pequeños dispositivos
(\emph{motas}) sobre los que se pueden montar placas de sensores y
actuadores para distintos propósitos.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.5\linewidth]{MICA2DOT.jpg}
  \caption{Mota MICA2DOT de CrossBow}
  \label{fig:xbow}
\end{figure}

Los dispositivos mostrados en la figura~\ref{fig:xbow}
y~\ref{fig:mota} tienen como características comunes:

\begin{itemize}
\item Microcontrolador de 8 bits.
\item 4 KiB de memoria \ac{EEPROM}.
\item 128 KiB de memoria \ac{RAM}.
\item Interfaz de comunicaciones radio (2.4 GHz).
\item Alimentación por baterías.
\item \ac{XBow} como protocolo de comunicaciones.
\end{itemize}

Junto con el endpoint \ac{XBow}, se ha utilizado la implementación en
C para tinyOS del picoObjeto para cargar código generado con el
compilador y probar su funcionalidad en los dispositivos reales.

Finalmente, la figura~\ref{fig:pic} muestra el prototipo ALSO1 creado
por el grupo \ac{ARCO} utilizando un microcontrolador PIC 16LF876A de
características aún más reducidas (128 bytes de RAM). Como
puede observarse, este dispositivo tiene una interfaz de conexión
ethernet.

\begin{figure}[h!]
  \centering
  \includegraphics[width=0.5\linewidth]{ALSO1.jpg}
  \caption{Mota ALSO1 del grupo \acs{ARCO}}
  \label{fig:pic}
\end{figure}

% Local variables:
%   ispell-local-dictionary: "castellano8"
% End:
