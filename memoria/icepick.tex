% -*- coding:utf-8 -*-

\chapter{Lenguajes}
\label{chap:lenguajes}
\fancyMiniToc

En este capítulo se muestra el diseño de los lenguajes de entrada para
el compilador. Se han diseñado dos lenguajes para satisfacer los
objetivos del proyecto (véase sección~\ref{sec:objetivos_especificos})
y se describen los requisitos de los lenguajes de entrada.

El lenguaje IcePick es la principal herramienta con la que cuenta el
programador de picoObjetos para definir el escenario distribuido que
desea construir.

\acs{SIS} (\acl{SIS}) es un pequeño lenguaje que define la interfaz
binaria entre la implementación de los métodos de los objetos y el
escenario distribuido. Este lenguaje se explicará en detalle más
adelante.

Por simplicidad, en este capítulo se tomará \ac{Slice} como lenguaje
de especificación de interfaces y los conceptos generales del
middleware \ac{Ice}. No obstante, todo lo que se expondrá es
fácilmente adaptable a \ac{CORBA} y otros middlewares similares.

\section{Requisitos}
\label{sec:requisitos_lenguajes}
A continuación, se listan los requisitos en términos de funcionalidad
que deben ofrecer los lenguajes de entrada al finalizar el
proyecto. Para obtener información sobre la evolución de estos
requisitos a lo largo del tiempo véase el
capítulo~\ref{chap:planificacion}.

\begin{variablelist}

\item[Ortogonalidad y regularidad] Los lenguajes diseñados deben ser
  ortogonales en términos sintácticos, es decir, estructuras del mismo
  tipo deben poderse describir de forma similar.

% FIXME Slice
\item [Integración de \textsc{Slice}] Integrar código \ac{Slice} para
  la definición de interfaces y métodos.

\item [Dominio del problema] Declarar los distintos adaptadores y
  objetos que se encuentran en el sistema distribuido. Uno de los
  adaptadores definidos será el punto de referencia del usuario en el
  sistema, el resto interactuarán con él.

  La dirección del picoObjeto puede no conocerse inicialmente y
  obtenerse de forma dinámica en tiempo de ejecución. El lenguaje debe
  permitir al programador especificar esta situación. Además,
  existirán aplicaciones en las que las identidades de objetos y el
  endpoint del adaptador donde residen no se conocen a priori.

\item [Implementación del sirviente independiente] La implementación
  de un sirviente concreto puede estar ya construida o provenir de
  terceras partes. El programador de picoObjetos puede conocer la
  interfaz de implementación, es decir, cómo utilizarla. No obstante,
  puede desconocer todos los demás detalles (lenguaje utilizado,
  direccionamiento, modo de acceso al hardware, etc.).

\item [Métodos locales del sirviente] Es posible que la implementación
  de los sirvientes proporcionada incluya métodos del sirviente que no
  sean accesibles a nodos remotos, pero sea posible ejecutarlos de
  forma local.

\item [Asociación de objetos y adaptadores] Relacionar objetos y
  adaptadores de tal forma que sea posible asociar objetos a distintos
  adaptadores.

\item [Invocaciones] Crear una descripción de las interacciones entre
  objetos a través de invocaciones similares a los lenguajes de alto
  nivel. Estas invocaciones pueden ser hacia objetos locales al
  picoObjeto o a objetos de adaptadores remotos.

  Es posible que la ejecución de invocaciones esté condicionada por
  valores umbrales. El programador debe poder especificar este tipo de
  situaciones.

  Finalmente, es necesario agrupar un conjunto de invocaciones para
  ofrecer mecanismos de modularidad y reutilización.

\item [Tipos de datos] Utilizar en las invocaciones (como argumentos
  o valores de retorno) los siguientes tipos de datos:
  \begin{itemize}
  \item Tipos básicos: \texttt{bool}, \texttt{byte}, \texttt{int},
    \texttt{float}, \texttt{double} y \texttt{string}.
  \item Diccionarios de la forma \texttt{clave:valor}.
  \item Identidades de objeto: en el caso de \ac{Ice}, el tipo
    \texttt{Ice::Identity}.
  \item Proxies a objetos: en el caso de \ac{Ice}, el tipo
    \texttt{Ice::ObjectPrx}.
  \end{itemize}

\item [Comportamiento asíncrono] En la sección~\ref{sec:picoObjetos}
  se explicó que el picoObjeto puede tener comportamiento reactivo o
  proactivo. Existen cuatro posibles eventos a los que cualquier
  picoObjeto responde.
  \begin{itemize}
  \item Arranque inicial del dispositivo.
  \item Señal periódica de temporizado. Es posible configurar el
    intervalo periódico.
  \item Eventos internos programables por el usuario.
    % FIXME dicuss
  \item Invocaciones sobre objetos del picoObjeto. En este caso, debe
    ser posible utilizar algún valor proveniente de la invocación como
    dato dentro del conjunto de invocaciones.
  \end{itemize}

  Debe ser posible asociar un conjunto de invocaciones (manejador) a
  cada uno de los tipos de eventos anteriormente descritos. Además, el
  programador puede habilitar y deshabilitar las señales que provocan
  la ejecución de los manejadores.

\end{variablelist}

\section{\acs{Slice}}

Es un lenguaje de especificación de interfaces construido por
ZeroC. No obstante, se trata de un lenguaje de entrada más para el
compilador por lo que se describirá brevemente en esta sección. En la
sección~\ref{sec:ipkc_frontend} se explica en detalle la integración
de los analizadores léxicos y sintácticos de este lenguaje en el
frontend del compilador.

\lstinputlisting[float,  language=Slice, caption={Especificación de
  interfaces en \acs{Slice}}, label=cod:switch_ice
]{listings/switch.ice}

El listado~\ref{cod:switch_ice} muestra un ejemplo que utiliza
interfaces del proyecto Hesperia. Teniendo en consideración lo
explicado en las secciones~\ref{sec:duo} y~\ref{sec:asd} referentes a
\ac{DUO} y \ac{ASD}, se define la interfaz \texttt{Device::Switch} que
representa a un objeto de tipo booleano:
\begin{itemize}
\item Su estado puede ser consultado a través del método
  \texttt{get()} de la interfaz \texttt{DUO::IBool::R}.
\item Es activo y, por tanto, notifica su estado a un publicador, y
  éste a un canal de eventos asociado. Tanto el publicador como el
  canal pueden modificarse en tiempo de ejecución utilizando el método
  \texttt{setCbTopic()} de \texttt{DUO::Active::W}.
\end{itemize}

\section{IcePick}
\label{sec:icepick}

IcePick es el lenguaje diseñado para describir un escenario
distribuido y la interacción entre los nodos y objetos del mismo.

El nombre de «IcePick» está inspirado en los nombres que ZeroC ha dado
a distintos servicios de \ac{Ice}. Todos los servicios de \ac{Ice}
tienen nombres referentes al hielo y evocan la funcionalidad de
éstos. Por ejemplo, el servicio de canales de eventos se llama
\emph{IceStorm} (tormenta de hielo) y su funcionalidad (transmitir
mensajes multidestino) recuerda al fenómeno de la tormenta de
hielo. \emph{IceBox} (cubito de hielo) es un servicio que permite
agrupar aplicaciones \ac{Ice} y gestionar su comportamiento de forma
remota (parada, arranque, etc.).

Cuando un picahielos (\emph{ice-pick}, en inglés) rompe un trozo de
hielo, los pequeños fragmentos que se obtienen representan a los
picoObjetos \ac{Ice}. Por ello, IcePick es una herramienta para crear
picoObjetos, inicialmente para el middleware \ac{Ice}.

Para satisfacer los requisitos marcados en la
sección~\ref{sec:requisitos_lenguajes}, se han definido tres partes
fundamentales:

\begin{enumerate}
\item \textbf{Archivos del lenguaje de especificación de interfaces}:
  directiva de inclusión de archivos que permita al programador
  utilizar las interfaces y tipos declarados en \ac{Slice}.

\item \textbf{Objetos y nodos}: estructuras que permitan al usuario
  describir los componentes de un sistema distribuido y términos que
  permitan diferenciar cuál es el nodo que se desea programar (el que
  representa al picoObjeto) y con cuáles interactúa.

\item \textbf{Comportamiento del picoObjeto}: mecanismos y estructuras
  basadas en invocaciones para poder definir con comodidad el
  comportamiento (reactivo o proactivo) que el usuario pretende
  programar.
\end{enumerate}

A continuación, se explican más detalladamente cada una de las partes
de un fichero IcePick.

\subsection{Archivos del lenguaje de especificación de interfaces}

El programador de picoObjetos necesita utilizar las interfaces y tipos
declarados en \ac{Slice} y, de esta forma, describir los objetos que
estarán presentes en el sistema distribuido.

Para permitir al programador construir un servidor de algún tipo de
los declarados, se ha diseñado la directiva \texttt{uses}. El
listado~\ref{cod:switch1} es un ejemplo de inclusión de ficheros
\ac{Slice} en el código IcePick.

\lstinputlisting[float,
language=IcePick,
lastline=3,
caption={Utilización de
  \texttt{switch.ice} en un fichero IcePick},
label=cod:switch1]{listings/switch.ipk}

Aunque no aparezca la definición de \texttt{Ice::Object} en el
ejemplo, está siempre implícita.

\subsection{Objetos y nodos}
\label{sec:adaptadores}
Conociendo la interfaz de los objetos que se desean incluir en el
sistema distribuido, el siguiente paso es describir los componentes
del mismo. Supongamos que se pretende construir un interruptor que
active y desactive objetos de tipo \texttt{DUO::IBool::W} (bombillas,
cerraduras electrónicas, etc.).

Los objetos que necesiten atender a los cambios de estado del
interruptor pueden recibirlos a través de un canal de eventos. Además,
el interruptor se anunciará utilizando un objeto
\texttt{ASD::Listener} para que otros puedan contactar con él (para
consultar su estado, por ejemplo). Por último, el interruptor podrá
ser deshabilitado para que deje de enviar información sobre su estado
y así evitar tráfico innecesario.

Para describir el escenario distribuido, se han incluido dos
estructuras fundamentales:

\begin{variablelist}
\item [Objetos] Los objetos IcePick deben ser de algún tipo declarado
  en el archivo \ac{Slice}. En los objetos es posible especificar el
  modo de acceso a través del atributo \texttt{mode} (cuyos valores
  posibles son: \texttt{oneway}, \texttt{twoway} o
  \texttt{datagram}). Por defecto, su valor es \texttt{twoway}.

  \lstinputlisting[float,
  language=IcePick,
  lastline=16,
  caption={Objetos en IcePick},
  label=cod:switch2] {listings/switch.ipk}

  Es necesario asignar un identificador a cada objeto declarado que
  servirá al programador como referencia a lo largo del programa. El
  listado~\ref{cod:switch2} muestra la declaración de cinco objetos:

  \begin{itemize}
  \item \texttt{pub}: el observador al que se enviará la información
    sobre el estado del interruptor.
  \item \texttt{tp}: el canal de eventos asociado al
    observador. Servirá a los objetos clientes que invoquen
    \texttt{getTopic()} para obtener directamente el canal de eventos.
  \item \texttt{service}: representa el servicio que proporciona el
    interruptor. A través del método \texttt{set()} puede ser
    habilitado o deshabilitado.
  \item \texttt{switch}: el interruptor que implementa
    \texttt{Device::Switch}. Inicialmente, el observador y el canal de
    eventos serán \texttt{pub} y \texttt{tp} respectivamente. Nótese
    que estos atributos han sido definidos por el programador. El
    lenguaje IcePick permite definir atributos de objetos siempre y
    cuando exista alguna forma de comprobar semánticamente dichos
    datos (véase sección~\ref{sec:ipkc_plugins}).
  \item \texttt{asda}: objeto que recibirá los anunciamientos del
    interruptor. El programador ha definido su modo de acceso como
    \texttt{oneway}, por lo que no se esperará respuesta de
    \texttt{asda} (aunque sus métodos retornaran algún valor). Nótese
    que el resto de objetos no han definido el modo de acceso, por lo
    que se supone \texttt{twoway}.
  \end{itemize}

\item [Adaptadores] En términos conceptuales, son similares a los
  adaptadores de objetos de los sistemas distribuidos (\acs{POA} en
  terminología \ac{CORBA}). En IcePick, un adaptador puede ser de dos
  tipos:
  \begin{description}
  \item [Local] representa el adaptador del picoObjeto que
    el usuario pretende construir. Se trata, por tanto, del «punto de
    vista» del programador en el sistema distribuido. Debe haber un
    único adaptador local por archivo.

  \item [Remoto] todos aquellos que no sean locales. En
    términos conceptuales, se trata de los nodos de interés para el
    picoObjeto y forman parte del entorno del sistema distribuido
    donde éste reside.
  \end{description}

  En los adaptadores de objetos también se pueden especificar
  atributos al igual que en los objetos. Sin embargo, existen dos
  atributos obligatorios en todo adaptador (\texttt{endpoint} y
  \texttt{objects}).  El listado~\ref{cod:switch3} amplía el ejemplo
  con la definición del interruptor declarando los adaptadores
  necesarios.

  \lstinputlisting[float,
  language=IcePick,
  lastline=32,
  caption={Adaptadores en IcePick},
  label=cod:switch3 ]{listings/switch.ipk}

  En el ejemplo, se han declarado tres adaptadores (uno de ellos local
  y dos remotos):
  \begin{itemize}
  \item \texttt{pico}: representa al adaptador de objetos donde se
    sitúa el punto de vista del programador en el sistema
    distribuido. Utilizando \texttt{0.0.0.0} en el adaptador se indica
    que se obtendrá la dirección en tiempo de ejecución (a través de
    \acs{DHCP} u otro protocolo similar).

    En dicho adaptador, estarán registrados los sirvientes de los
    objetos \texttt{switch} y \texttt{service}, cuyas identidades son
    \texttt{SWITCH} y \texttt{SWITCHSERVICE}, respectivamente.

  \item \texttt{node1}: adaptador remoto desde el punto de vista del
    picoObjeto, donde estará alojado el sirviente con proxy
    \texttt{ANNOUNCE -o:tcp -h 20.0.0.1 -p 9000}. Este objeto será
    utilizado para enviar anuncios al canal de eventos \texttt{ASDA}
    (véase sección~\ref{sec:asd}).

  \item \texttt{node2}: este adaptador tiene registrados los dos
    sirvientes encargados de recibir el estado del interruptor. Sin
    embargo, no se conoce en tiempo de compilación ni el endpoint del
    adaptador ni las identidades de los objetos.
  \end{itemize}
\end{variablelist}

\subsection{Comportamiento del picoObjeto}

Teniendo en consideración la forma en la que se dispara el
funcionamiento de un picoObjeto y los requisitos de la
sección~\ref{sec:requisitos_lenguajes}, se han incluido dos
estructuras básicas que permiten describir la funcionalidad:

\begin{variablelist}
\item [Invocaciones] Inspiradas en los lenguajes de alto nivel, las
  invocaciones tiene la habitual estructura sintáctica de
  \texttt{objeto.método(p1, p2,\...,pn)}. Las restricciones
  semánticas impuestas por los requisitos de la
  sección~\ref{sec:requisitos_lenguajes} son:

  \begin{itemize}
  \item \texttt{objeto} debe ser el nombre de un objeto especificado
    en el archivo.
  \item el método involucrado en la invocación debe ser válido para el
    tipo de \texttt{objeto}.
  \item Los parámetros pueden ser de cualquiera de los tipos básicos
    expuestos en la sección~\ref{sec:requisitos_lenguajes}. Para ello,
    el programador puede utilizar los tokens del lenguaje
    \texttt{bool}, \texttt{int}, \texttt{lit}, \texttt{id} mostrados
    en la próxima sección.
  \item También es posible utilizar como parámetro el nombre del
    objeto para referirse al proxy del sirviente de dicho objeto, o el
    atributo especial \texttt{.oid} que corresponde a la identidad
    (\texttt{Ice::Identity}) del sirviente.
  \item Como parámetros, es posible utilizar el valor de retorno de
    una invocación.
  \end{itemize}

  Las invocaciones de IcePick pueden ejecutarse o no en base a una
  condición asociada. Por ejemplo, supongamos que se pretende activar
  una alarma cuando un sensor alcanza un determinado umbral. Una
  \emph{invocación condicionada} resuelve este problema:

  \begin{center}
    \texttt{alarma.activar() if (sensor.valor() >=\ 100);}
  \end{center}

\item [\emph{Triggers}] Un \emph{trigger} está formado por la
  especificación de un evento y el manejador asociado. Cuando el
  evento tenga lugar, se realizará de forma secuencial las
  invocaciones indicadas en el manejador.

  Los triggers pueden ser etiquetados de forma que puedan
  utilizarse como un identificador de objeto IcePick e invocar métodos
  sobre él. Dependiendo del tipo de trigger se podrá etiquetar
  o no, y los métodos que ofrece dependerá del propio tipo de
  trigger.

  Los tipos de triggers disponibles son los siguientes:

  \begin{itemize}

  \item \texttt{\textbf{boot}}: tiene lugar en el arranque inicial del
    picoObjeto. No puede tener asociado ninguna etiqueta ya que sólo
    ocurrirá en la inicialización del picoObjeto.

  \item \texttt{\textbf{timer(n)}}: mediante este trigger es
    posible programar comportamiento periódico en el picoObjeto (cada
    \texttt{n} ticks). Si se etiqueta, ofrece la siguiente
    funcionalidad:

    \begin{itemize}
    \item \texttt{label\_timer.enable(bool v)}: activa/desactiva el
      temporizador.
    \end{itemize}

  \item \texttt{\textbf{event NOMBRE\_EVENTO do}}: dependiendo de la
    plataforma destino y de la implementación del sirviente, es
    posible que el programador de IcePick necesite utilizar eventos
    internos. Los triggers \texttt{event} permiten definir
    invocaciones que deben ejecutarse cuando el evento
    \texttt{NOMBRE\_EVENTO} ocurra.

  Si un trigger \texttt{event} es etiquetado, los métodos que pueden
  ser ejecutados son:

    \begin{itemize}
    \item \texttt{label\_event.enable(bool v)}: habilita/deshabilita
      el evento asociado, de tal forma que aunque éste se produzca no
      se ejecutarán las invocaciones asociadas.
    \item \texttt{label\_event.run()}: simula el evento asociado para
      provocar la ejecución de su manejador.
    \end{itemize}

  \item \texttt{\textbf{when INVOCACION do}}: ofrece la posibilidad de
    definir el comportamiento en términos de una invocación hacia un
    objeto local (añadido al adaptador local). \texttt{INVOCACION} se
    conoce como \textbf{invocación de disparo} del trigger
    \texttt{when}.

    El método aplicable a un trigger \texttt{when} etiquetado es
    el siguiente:
    \begin{itemize}
    \item \texttt{label\_when.enable(bool v)}: al igual que en el
      anterior, habilita o deshabilita la señal que produce la
      ejecución del trigger. En este caso, la señal esperada es
      la invocación de disparo.
    \end{itemize}
  \end{itemize}
\end{variablelist}

\lstinputlisting[float,
language=IcePick,
caption={Invocaciones y triggers en IcePick}, label=cod:switch4
]{listings/switch.ipk}

El listado~\ref{cod:switch4} muestra el ejemplo del interruptor
universal con invocaciones y triggers. Inicialmente (trigger
\texttt{boot}), el interruptor se inicia al estado de apagado
utilizando el método \texttt{set()}. Ciertamente, el objeto
\texttt{switch} (de tipo \texttt{Device::Switch}) no implementa
ninguna interfaz con este método. Esto es posible gracias al lenguaje
\acs{SIS} que permite definir métodos no accesibles remotamente. En la
siguiente sección, se explica en detalle.

Tras esta invocación, se informa al objeto publicador del estado
inicial del interruptor. El atributo \texttt{switch.oid} representa la
identidad identificador de objeto del interruptor en el adaptador
local. Cada 60 ticks, el interruptor se anunciará en el servicio
\texttt{asda}.

Además, se muestra un evento interno definido por el usuario que
modela la situación en la que se produce un cambio de estado debido al
movimiento de la palanca del interruptor. Dicho evento, activa un
método del sirviente que cambia efectivamente su estado, por lo que
carece de sentido cambiar el valor a través de una invocación local.

El trigger \texttt{event} está etiquetado como \texttt{change}
y se utiliza en el trigger \texttt{when}. En este último tienen
lugar las siguientes acciones:
\begin{enumerate}
\item Se realiza la invocación de disparo, es decir, el estado de
  \texttt{service} se cambia debido a ésta.
\item Utilizando el valor de \texttt{v}, se decide si se activa o no
  el trigger \texttt{change}. De esta forma, si el valor es
  \texttt{true}, cuando se produzca el evento \texttt{STATECHANGED} el
  picoObjeto ejecutará el comportamiento asociado. En otro caso, se
  deshabilita el bloque, por lo que se ignorará el cambio de estado en
  el interruptor.
\end{enumerate}

Una estructura similar a las anteriores es \texttt{function}. No es un
trigger porque no está asociado a ningún evento. Sin embargo, su
estructura sintáctica es similar ya que, al igual que los triggers,
permite agrupar invocaciones que representan una funcionalidad
concreta y es útil reutilizarla. \texttt{function} es una forma de
proporcionar mecanismos de modularidad y reutilización.

Debido a la naturaleza de esta estructura es \textbf{obligatorio} que
sea etiquetado por el usuario. El método que se puede utilizar para
\texttt{function} es:
\begin{itemize}
\item \texttt{label\_function.run()}: al igual que en \texttt{event}, se
  solicita la ejecución de las invocaciones asociadas al bloque.
\end{itemize}

En el apéndice~\ref{chap:icepick_ejemplos} pueden encontrarse más
ejemplos de código IcePick donde se muestran las invocaciones
condicionadas y otras características descritas que ofrece el
lenguaje. Además, en el listado~\ref{cod:switch_generado} se muestra
el código generado para el interruptor universal.

\subsection{\acs{EBNF} de IcePick}
\label{sec:ebnf_icepick}

El listado~\ref{cod:icepick_ebnf} muestra la gramática libre de
contexto en formato \ac{EBNF} del lenguaje IcePick. Los símbolos no
terminales se muestran en mayúscula y los símbolos terminales en
minúscula. Las comillas en los símbolos de la gramática se utilizan
para señalar que dicho símbolo pertenece a la gramática de IcePick y
no a la notación \ac{EBNF}.

\lstinputlisting[float,  caption={\acs{EBNF} de IcePick},
label=cod:icepick_ebnf ]{listings/IcePick.ebnf}

En el cuadro~\ref{tab:tokens_ipk} se describen los componentes léxicos
del lenguaje utilizados en la gramática. Las palabras reservadas del
lenguaje están resaltadas en negrita. Por simplicidad, en la
especificación de la gramática no se han utilizado los nombres de los
tokens más simples como \texttt{colon}, \texttt{semicolon}, etc. Se ha
utilizado, en su lugar, el símbolo que representan.

\begin{table}[htbp]
  \centering
  \input{tabs/IcePick.tokens.tex}
  \caption{Tokens de IcePick}
  \label{tab:tokens_ipk}
\end{table}

A partir de la especificación \ac{EBNF} es posible construir el
analizador léxico y sintáctico del lenguaje utilizando las
herramientas \ac{Flex} y Bison, respectivamente. En el capítulo
siguiente (véase sección~\ref{sec:ipkc_frontend}) se describe en
detalle el desarrollo e implementación de sendos analizadores ya que
forman parte del frontend del compilador.

\section{\acs{SIS}}
\label{sec:sis}

El lenguaje IcePick describe el escenario distribuido y el
comportamiento de forma abstracta de los objetos que formarán el
picoObjeto. Sin embargo, la implementación final de cada uno de los
métodos utilizados no ha sido especificada aún. Atendiendo a los
requisitos de la sección~\ref{sec:requisitos_lenguajes}, esta
implementación ya está disponible para el programador de picoObjetos
(bien por el propio desarrollador o por terceros).

La forma en que el picoObjeto se comporta a la hora de tratar los
eventos utilizando métodos del sirviente (véase
sección~\ref{sec:picoObjetos}) es clave para entender el propósito del
lenguaje \acs{SIS}.

\ac{SIS} es un pequeño lenguaje que permite especificar la interfaz
binaria de la implementación, es decir, una descripción que permite
unir el dominio de IcePick (modelado del escenario distribuido y sus
interacciones) con la implementación final de los sirvientes.


\subsection{Componentes}
\label{sec:sis_componentes}

En \ac{SIS} se definen dos estructuras básicas que permiten al
compilador completar la información que necesita para generar el
código necesario.

\begin{variablelist}
\item [Eventos] Como ya se explicó en la
  sección~\ref{sec:picoObjetos}, el picoObjeto necesita asignar a cada
  tipo de evento un identificador único. El compilador, por su parte,
  requiere conocer dicho identificador para generar el código de forma
  correcta.

  \lstinputlisting[float,
  language=SIS, caption={Eventos de
    \acs{SIS}}, label=cod:event_sis ]{listings/switch1.sis}

  El listado~\ref{cod:event_sis} muestra la declaración del evento
  \texttt{CHANGEDSTATE} en \ac{SIS}. Como se puede ver, el
  desarrollador debe conocer únicamente el identificador del evento
  que el programador de los sirvientes le asignó. En este caso, el
  identificador es \texttt{15}.

  Si el usuario no especifica lo contrario, los eventos básicos del
  picoObjeto (véase sección~\ref{sec:picoObjetos}) se
  identificarán con los valores indicados en los
  comentarios.

\item [Métodos] Cada método utilizado en IcePick debe estar
  implementado en el código de sirviente. Por ejemplo, si el
  interruptor recibe una invocación a través del método \texttt{get()}
  puede cargar su estado en un registro concreto y encender un
  \acs{LED}. El código correspondiente a estas acciones es el método
  del sirviente. Los métodos de \ac{SIS} permiten asignar
  identificadores de métodos del sirviente a cada uno de las
  invocaciones utilizadas en IcePick.

  \lstinputlisting[float,  language=SIS, caption={Métodos de \acs{SIS}},
  label=cod:metodos_sis ]{listings/switch2.sis}

  En el ejemplo de código~\ref{cod:metodos_sis} se declaran dos
  métodos remotos y uno local para el objetos del mismo tipo que el
  interruptor. Los métodos remotos son aquellos que pueden ser
  invocados por clientes del sistema distribuido donde se encuentra el
  picoObjeto. Por ejemplo, los objetos de tipo \texttt{DUO::IBool::R}
  que tienen un método \texttt{get()} ejecutarán el método número
  \texttt{10} cuando se produzca dicha invocación.

  Toda la información sobre los tipos y métodos declarados como
  remotos está disponible en el fichero \ac{Slice}, por que lo no es
  necesario especificar nada más.

  No obstante, y de acuerdo con los requisitos, es posible que el
  programador del sirviente ofrezca funcionalidad no accesible
  remotamente en el sirviente. En el ejemplo se declara la existencia
  estos métodos \emph{locales}.

  El método \texttt{set()} lo implementan aquellos objetos que
  implementen \texttt{Device::Switch} y tiene un argumento de entrada
  de tipo booleano. Este método, como se vio en el
  listado~\ref{cod:switch4}, se utiliza a lo largo del código IcePick
  al igual que los remotos.

\item [Parámetros de longitud variable] Las anteriores estructuras son
  válidas cuando se desea programar un picoObjeto cuyos métodos tienen
  como parámetros valores de tamaño fijo.

  Supóngase que el método local anteriormente declarado tiene otro
  parámetro de entrada: una cadena de caracteres. El
  código~\ref{cod:parvar_sis} muestra el código modificado y añadido
  para resolver esta situación.

  \lstinputlisting[float,  language=SIS, caption={Parámetros de tamaño
    variable de \acs{SIS} para picoObjetos},
  label=cod:parvar_sis ]{listings/switch3.sis}

  Como primer parámetro, \texttt{set\_state()} sigue teniendo el valor
  booleano inicial. Sin embargo, el segundo parámetro está compuesto
  por dos métodos locales más. A la vista del ejemplo, estos métodos
  locales los implementan todos los objetos \texttt{Ice::Object}, es
  decir, cualquiera.

  \texttt{get\_size()} obtiene el tamaño de la cadena de entrada y lo
  almacena en un registro especial del picoObjeto. De esta forma,
  cuando llegue el momento de leer los datos, \texttt{get\_data()} se
  encargará de realizar esta tarea y almacenar finalmente la cadena
  (en función del tamaño leído con anterioridad).

  No sólo es aplicable a métodos locales. También es posible utilizar
  parámetros de longitud variable en métodos remotos y como parámetros
  de retorno.

\end{variablelist}

Una vez mostradas las estructuras diseñadas para \ac{SIS}, a
continuación se lista la componente semántica:

\begin{itemize}
\item Los identificadores de eventos deben ser únicos, esto es, no se
  pueden repetirse.
\item Lo anterior es aplicable a los identificadores de los métodos
  del sirviente.
\item Los tipos y métodos remotos utilizados deben coincidir con los
  declarados en el archivo \ac{Slice}. En caso de que el tipo sea de
  tamaño variable, es posible utilizar métodos para que sea posible
  manejarlos.
\item Los métodos locales no pueden tener más de un parámetro de
  salida.
\end{itemize}

\subsection{\acs{EBNF} de \acs{SIS}}
\label{sec:sis_ebnf}

\begin{table}[htbp]
  \centering
  \input{tabs/SIS.tokens.tex}
  \caption{Tokens de \ac{SIS}}
  \label{tab:tokens_sis}
\end{table}

El cuadro~\ref{tab:tokens_sis} muestra el diseño del nivel léxico de
\ac{SIS} donde se describen los tokens y patrones utilizados. Las
palabras reservadas del lenguaje se muestran en negrita. Por otro
lado, el listado de código~\ref{cod:sis_ebnf} es la gramática libre de
contexto del lenguaje \ac{SIS} diseñada para el proyecto IcePick. Los
símbolos terminales de la gramática (escritos en minúscula) son los
tokens del cuadro~\ref{tab:tokens_sis}. Sin embargo, por cuestiones de
legibilidad, se ha utilizado en la mayoría de los casos el símbolo del
token en lugar de su identificador.

\lstinputlisting[float,  caption={\acs{EBNF} de \acs{SIS}},
label=cod:sis_ebnf]{listings/SIS.ebnf}

Al igual que los analizadores de IcePick, a partir de la
especificación \ac{EBNF} se ha construido los analizadores léxicos y
sintácticos utilizando las herramientas \ac{Flex} y Bison,
respectivamente. En la sección~\ref{sec:ipkc_frontend} se describe en
detalle el proceso de desarrollo.


% Local variables:
%   ispell-local-dictionary: "castellano8"
% End:


