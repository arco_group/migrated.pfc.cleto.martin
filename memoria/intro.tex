% -*- coding:utf-8 -*-

\chapter{Introducción}
\label{chap:intro}
\fancyMiniToc

Actualmente, en muchos sistemas de información están presentes
computadores de propósito específico con recursos de cómputo
reducidos. Es importante señalar que, en el contexto de este proyecto,
cuando se habla de sistemas con pocas prestaciones no se hace
referencia a dispositivos como una \ac{PDA} o un móvil, sino a
microcontroladores que poseen unos cuantos kilobytes de memoria de
programa, un procesador de pocos megahercios y, en ocasiones,
alimentación con baterías. En este tipo de dispositivos no es viable
el uso de sistemas operativos convencionales.

En el mercado existe una gran variedad de estos productos que pueden
utilizarse para múltiples fines (sistemas de monitorización, domótica,
medicina, defensa, etc.). En la mayoría de estas aplicaciones, estos
dispositivos miden magnitudes físicas o hacen las funciones de un
actuador simple. Normalmente, todos estos dispositivos están
controlados por un sistema más complejo (en términos de recursos) en
el cual reside la lógica fundamental de la aplicación.

Las redes de sensores y actuadores son, precisamente, aquellos
sistemas que utilizan nodos con recursos de cómputo limitados. En la
actualidad, existen diferentes líneas de investigación en torno a este
tipo de redes. Algunas propuestas utilizan el concepto de sistema
distribuido heterogéneo donde estas redes puedan ser integradas de
forma transparente al resto del sistema.

Hoy en día, es posible construir sistemas distribuidos heterogéneos
utilizando \emph{middlewares} de comunicaciones de propósito general,
esto es, plataformas que facilitan la implementación y despliegue de
aplicaciones distribuidas. Los middlewares orientados a objetos son
aquellos que llevan la abstracción de la POO a la programación de
sistemas distribuidos. \acs{CORBA} o ZeroC \acs{Ice} son ejemplos de
este tipo de middlewares.

Sin embargo, la integración de redes de sensores y actuadores en
sistemas distribuidos donde se utilizan middlewares de propósito
general tiene un problema fundamental: los recursos de los nodos son
muy limitados. La carga del software necesario para que sea funcional
bajo un middleware convencional no es viable.

Además, la creación de aplicaciones también es problemática ya que
suele ser de muy bajo nivel. La gran variedad de herramientas que
ofrecen los fabricantes de hardware no ayuda a que exista un proceso
uniforme en la creación de aplicaciones. Los problemas se agravan aún
más cuando, además de programar el dispositivo para un fin concreto,
es necesaria su integración en un sistema en red ya existente.

Como solución a estos problemas, el grupo de investigación \acs{ARCO}
propuso el concepto de \emph{picoObjeto}. Se trata de implementaciones
mínimas de objetos distribuidos para un middleware concreto
(picoCORBA, picoIce, etc.) de forma que pueda ser cargado en un
dispositivo empotrado.

En el marco de esta propuesta, el presente trabajo presenta una
solución completa para la programación de sistemas empotrados de bajo
coste y facilita su integración en un entorno distribuido. Además, se
propone un proceso de desarrollo uniforme que ayude al diseñador y
programador a lo largo del ciclo de vida del producto.

\section{Lenguajes y compiladores}
\label{sec:intro-compiladores}
Los procesadores de lenguajes en general, y los compiladores en
particular, son las herramientas básicas de las que dispone un
programador para crear aplicaciones que puedan ser ejecutadas en una
máquina concreta. Entendemos por procesador de lenguaje cualquier
programa que dado un texto escrito en un determinado \emph{lenguaje
  fuente} sea capaz de traducirlo a otro, llamado \emph{lenguaje
  objeto}.

En el ámbito de la programación, estos lenguajes pueden ofrecer más o
menos \textbf{nivel de abstracción}. De esta manera, Java o Python son
lenguajes de alto nivel porque ofrecen estructuras de datos y
mecanismos que permiten al programador resolver el problema en
términos conceptuales y no en base a una arquitectura hardware
concreta. Por su parte, el lenguaje ensamblador específico de un
computador es un ejemplo lenguaje de bajo nivel debido a que el
programador debe conocer muchos aspectos estructurales de la máquina
que pretende programar.

Una de las principales aportaciones de este proyecto es el lenguaje
IcePick. Se trata de un lenguaje de alto nivel diseñado para construir
aplicaciones distribuidas para nodos de una red de sensores y
actuadores.

Por otro lado, se ha construido el compilador \acs{ipkc} (\acl{ipkc})
que genera \emph{bytecode} interpretable por una máquina virtual
específica. Como se verá en el capítulo siguiente, existen diferentes
implementaciones de esta máquina virtual para distintas arquitecturas.

\section{Objetos distribuidos empotrados}
\label{sec:intro-middlewares}
Podemos pensar en los objetos distribuidos, de una manera informal,
como objetos tradicionales de la POO, pero desplegados y accesibles a
través de red. Es interesante llevar este concepto a las redes de
sensores y actuadores porque pueden representar perfectamente la
funcionalidad de muchos de tipos de nodos presentes en éstas.

Por ejemplo, una bombilla se puede modelar como un objeto
\texttt{bool} al que se le puede \emph{enviar} un valor \texttt{true}
(encender) o \texttt{false} (apagar). Piénsese en un sensor de
temperatura, el cual se puede ver como un objeto de tipo
\texttt{float} al que se le puede \emph{consultar} su estado.

El lenguaje IcePick está orientado a la definición y creación de
escenarios de objetos distribuidos que serán ejecutados en
dispositivos de bajo coste. Además, permite definir los nodos que van
a participar en dicho escenario y tienen algún tipo de relación con el
dispositivo que se quiere programar.

\section{Estructura del documento}
\label{sec:intro-icepick}

El presente documento se encuentra dividido en capítulos donde se
aborda el contexto en el que se ha enmarcado el proyecto IcePick, así
como sus principales aportaciones. Para una mejor compresión, el autor
recomienda al lector realizar una lectura secuencial.

A modo de resumen, se muestra una breve descripción de cada uno de los
capítulos:


\begin{variablelist}

\item [Capítulo~\ref{chap:antecedentes}: Antecedentes] El proyecto
  IcePick forma parte de Hesperia, un proyecto de investigación para
  el desarrollo de una sistema de seguridad inteligente. En este
  sistema se utilizan middlewares de comunicaciones por lo que la
  integración de redes de sensores y actuadores puede realizarse
  utilizando el compilador.

  Este capítulo se hace un análisis del contexto y las herramientas
  existentes antes de la realización de este trabajo.

\item [Capítulo~\ref{chap:objetivos}: Objetivos] El objetivo
  fundamental del proyecto es proporcionar las herramientas necesarias
  para agilizar el proceso de desarrollo de picoObjetos.

  En este capítulo se listan los objetivos generales y específicos del
  proyecto, quedando bien definido su alcance.

\item [Capítulo~\ref{chap:metodo}: Metodología y herramientas] Debido,
  fundamentalmente, a que IcePick forma parte de un proyecto de
  investigación, se ha escogido una metodología ágil de forma que sea
  posible obtener prototipos funcionales en poco tiempo.

  En este capítulo se describe dicha metodología, así como las
  herramientas que se han utilizado a lo largo del proyecto.

\item [Capítulo~\ref{chap:lenguajes}: Lenguajes] Una de las
  aportaciones principales de este trabajo son los lenguajes diseñados
  para modelar escenarios distribuidos.

  En este capítulo se describen los requisitos que deben cumplir estos
  lenguajes así como las decisiones de diseño y aspectos de
  implementación.

\item [Capítulo~\ref{chap:ipkc}: El compilador \acs{ipkc}] El
  compilador es la herramienta fundamental creada en el proyecto. A
  partir de los distintos lenguajes de entrada, se genera código
  ejecutable para una plataforma concreta. Además, se proporcionan
  las herramientas necesarias para su carga en el dispositivo
  final.

  En este capítulo se exponen los requisitos que debe cumplir el
  compilador en base a los lenguajes de entrada y se describe su
  diseño modular y extensible.

\item[Capítulo~\ref{chap:planificacion}: Desarrollo del proyecto] El
  proyecto se ha dividido en varias iteraciones. Al final de cada una
  de ellas se ha obtenido un prototipo funcional que el cliente
  evaluaba. En base a las deficiencias encontradas por éste y nuevas
  necesidades que surgían en el proyecto Hesperia, se desarrollaban
  nuevas iteraciones.

  Debido a que los requisitos varían con frecuencia, a lo largo de las
  iteraciones se han construido pruebas que han respaldado el
  cumplimiento de los requisitos.

  En este capítulo, se describe la evolución del proyecto, la
  estrategia seguida para diseñar las pruebas y una evaluación de los
  costes.

\item[Capítulo~\ref{chap:conclusiones}: Conclusiones y trabajo futuro]
  El proyecto IcePick no ha finalizado y hay otros proyectos donde se
  prevé usar sus resultados. A corto plazo están planificadas
  necesidades para los lenguajes y el compilador.

  En este capítulo se describen las conclusiones y algunas propuestas
  para trabajo futuro.
\end{variablelist}

Este documento cumple con la normativa para la elaboración de
Proyectos Fin de Carrera~\cite{esi} indicada por la Escuela Superior
de Informática de la Universidad de Castilla-La Mancha.

% Local variables:
%   ispell-local-dictionary: "castellano8"
% End:
