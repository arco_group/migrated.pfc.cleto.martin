# +CMD: python $file

import fsmutils as utils

# data = {'DUO_IByte_W': '\x0f::DUO::IByte::W',
#         'DUO_IBool_R': '\x0f::DUO::IBool::R',
#         'DUO_IByte_R': '\x0f::DUO::IByte::R',
#         'DUO_IBool_W': '\x0f::DUO::IBool::W',
#         'DUO_IByte_RW': '\x10::DUO::IByte::RW',
#         'DUO_IBool_RW': '\x10::DUO::IBool::RW'}

def check_values(data, result, chunks):
    for k,v in data.items():
        cad = ""
        for i in result[k]:
            cad += chunks[i]

        assert cad == v



data = []

data.append({1:'::ASD::iByte::R',
             2:'::ASD::iByte::W'})

data.append({1:'Esto es una prueba',
            2:'y esto es otra',
            3:'y otra mas',
            4:'y la ultima prueba'})

f = open("ipsum.txt", 'r')
d = {}
for i,line in enumerate(f.read().split('\n')):
    d[i] = line
f.close()
data.append(d)

data.append({'DUO_IBool_W': '\x0f::DUO::IBool::W',
             'DUO_Active_R': '\x10::DUO::Active::R',
             'DUO_IByte_W': '\x0f::DUO::IByte::W',
             '__TPC1': '\x04TPC1\x00\x00',
             'DUO_IByte_R': '\x0f::DUO::IByte::R',
             '__TPC2': '\x04TPC2\x00\x00',
             'OIDEx': '\x00\x00\x00\x00',
             'DUO_IBool_RWActiveR__ids_num': '\x06',
             'DUO_IBool_W__ids_num': '\x02',
             'id_277': '\x04LOC3\x00',
             'id_276': '\x04LOC2\x00',
             'id_275': '\x04LOC1\x00',
             'DUO_IBool_R': '\x0f::DUO::IBool::R',
             'Param_Map': '\x10\x12',
             'DUO_IBool_RW': '\x10::DUO::IBool::RW',
             'DUO_IBool_RWActiveR': '\x17::DUO::IBool::RWActiveR',
             'DUO_IByte_RW__ids_num': '\x04',
             'Ice_Object': '\r::Ice::Object',
             'DUO_IByte_RW': '\x10::DUO::IByte::RW',
             'op_set': '\x03set\x00'})

opt = utils.DataOptimizer()
for d in data:
    opt.initialize(d)
    result, chunks = opt.gen_data()
    check_values(d, result, chunks)
    opt.show_result()

