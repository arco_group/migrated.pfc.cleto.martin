# -*- coding: iso-8859-1 -*-

import scapy.all as scapy

def userProc(self):
    i = self.next()

    if i == 0: # init
        self.saved_value = 0

    if i == 2:
        ip = [x[4] for x in scapy.conf.route.routes if x[2] != '0.0.0.0'][0].ljust(15)
        for i in range(1,16):
            self.F[i] = ord(ip[i-1])

    elif i == 6: # get
        addr = self.fsmData[1] + 1
        self.R[addr] = self.saved_value

    elif i == 5: # set
        self.saved_value = self.R[self.fsmData[1] + 1]
