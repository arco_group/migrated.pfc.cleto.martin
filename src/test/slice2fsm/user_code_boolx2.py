# -*- coding: iso-8859-1 -*-

import scapy.all as scapy

IDENTITY       = 11

def digest(val):
    retval = 0
    for v in [ord(x) for x in val]:
        retval ^= ((v & 0xF)<<4) | ((v>>4) & 0xF)
    return retval

def userProc(self):
    i = self.next()    # nº de procedimiento

    if i == 0: # init
        self.saved_value = [digest("PIR1"): True,
                            digest("PIR2"): True]

    if i == 2:
        ip = [x[4] for x in scapy.conf.route.routes if x[2] != '0.0.0.0'][0].ljust(15)
        for i in range(1,16):
            self.F[i] = ord(ip[i-1])

    elif i == 6: # get BOOL
        addr = self.fsmData[1] + 1
        self.R[addr] = self.saved_value[self.R[IDENTITY]]

    elif i == 5: # set BOOL
        self.saved_value[self.R[IDENTITY]] = self.R[self.fsmData[1] + 1]
