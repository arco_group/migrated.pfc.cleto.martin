# -*- coding: iso-8859-1 -*-

# :ICEPICK:PROC:DUO.IBool.W:set:5
# :ICEPICK:PROC:DUO.IBool.R:get:6
# :ICEPICK:EVENT:SENSOR_EVENT:10

#El intérprete picoObject.py carga este fichero con execfile().  Este
#fichero no se debe llamar 'user.py' porque 'user' es un módulo
#estándar de Python

import struct

import scapy.all as scapy

def userProc(self):
    i = self.next()

    if i == 0: # init
        self.saved_value = 0

    if i == 2:
        ip = [x[4] for x in scapy.conf.route.routes if x[2] != '0.0.0.0'][0].ljust(15)
        for i in range(1,16):
            self.F[i] = ord(ip[i-1])

    elif i == 6: # get
        addr = self.fsmData[1] + 1
        val  = struct.pack('i', self.saved_value)

        for i,v in enumerate(val):
            self.R[addr+i] = ord(v)

    elif i == 11: # status
        addr = self.fsmData[1] + 1
        self.R[addr] = self.saved_value

    elif i == 5: # set
        val = ""
        for i in range(1,5):
            val += chr(self.R[self.fsmData[1] + i])

        self.saved_value = struct.unpack('i', val)[0]
