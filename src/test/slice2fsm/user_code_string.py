# -*- coding:utf-8;mode:python -*-

import struct

import scapy.all as scapy

def userProc(self):
    i = self.next()

    if i == 0: # init
        self.saved_value = ""
        self.value_size  = len(self.saved_value)

    if i == 2:
        ip = [x[4] for x in scapy.conf.route.routes if x[2] != '0.0.0.0'][0].ljust(15)
        for i in range(1,16):
            self.F[i] = ord(ip[i-1])

    elif i == 5: # set
        pass

    elif i == 6: # get
        self.R[16] = self.value_size + 1

    elif i == 11: # get_size
        self.value_size = self.fd.read()

    elif i == 12: # get_data
        self.saved_value = ""
        for i in range(self.value_size):
            self.saved_value += chr(self.fd.read())
        pass

    elif i == 13: # put_size
        self.fd.write(self.value_size)

    elif i == 14: # put_data
        if self.saved_value:
            for i in self.saved_value:
                self.fd.write(ord(i))


    elif i == 15: # get_oid
        print "Hola"
