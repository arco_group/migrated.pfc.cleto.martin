#!/usr/bin/python
# -*- mode:python -*-

import time
import sys
import Ice
import os
Ice.loadSlice('-I/usr/share/slice -I%s --all %s'
              % (os.path.join(os.environ["PICO"], "test/slice"),
                 os.path.join(os.environ["PICO"], "test/slice/picos.ice")))
import DUO
import IceStorm

class DUOObject:

    def __init__(self, argv):
        self.oid, self.iface, self.endpoint, self.initial, self.log, self.mode \
            = argv[1:]

        self.prx = None

        self.types = {"Bool":[True, False],
                      "Byte":[0, 128, 255, 10],
                      "Int" :[0, (2**31)/2, 2**31-1],
                      "Float":[0.0, 2000.0, 1234567.1234],
                      "String":["","Hola", "Hola Pico!"]}

    def run(self):
        self.ic = Ice.initialize()
        base = self.ic.stringToProxy(self.oid + " -%s:" % self.mode + self.endpoint)

        if self.mode == 't':
            exec("self.prx = %sPrx.checkedCast(base)" % self.iface)
            self.types_ids = self.prx.ice_ids()
            for i in self.types_ids:
                self.__test_twoway(i)
        else:
            exec("self.prx = %sPrx.uncheckedCast(base)" % self.iface)
            type = self.iface.split('.')[1].strip('I')
            self.prx.set(self.types[type][0], self.ic.stringToIdentity(""))

            time.sleep(1)
            sets = 0
            f = file(self.log, "r")
            for l in f.readlines():
                if "FSM_DEBUG: set" in l:
                    sets += 1

            assert sets == 1

    def __test_twoway(self, t):
        for i in self.types.keys():
            readable = False
            writeable = False

            if "::DUO::Active::W" in self.types_ids:
                topic = "TOPIC00001 -t:tcp -h 127.000.000.001 -p 15000"
                cb = "CALLBACK01 -t:tcp -h 127.000.000.001 -p 15000"
                cb = self.ic.stringToProxy(cb)
                topic = self.ic.stringToProxy(topic)
                topic = IceStorm.TopicPrx.uncheckedCast(topic)

                self.prx.setCbTopic(cb, topic)

            if "::DUO::I%s::R" % i  == t:
                readable = True
                retval = self.prx.get()
                assert type(self.types[i][0])(self.initial) == retval

            if "::DUO::I%s::W" % i  == t:
                writeable = True
                for v in self.types[i]:
                    self.prx.set(v, self.ic.stringToIdentity(""))

                time.sleep(1)

                matches = 0
                f = file(self.log, "r")
                for l in f.readlines():
                    if "FSM_DEBUG: set" in l:
                        matches += 1

                assert matches == len(self.types[i])

            if "::DUO::Pulse::W" == t:
                self.prx.set(self.ic.stringToIdentity(""))
                time.sleep(1)

                matches = 0
                f = file(self.log, "r")
                for l in f.readlines():
                    if "FSM_DEBUG: set" in l:
                        matches += 1
                assert matches == 1
                break


            if readable and writeable:
                retval = self.prx.get()
                self.prx.set(self.types[i][1])
                assert self.prx.get() == self.types[i][1]
                self.prx.set(retval)
                assert self.prx.get() == retval

ob = DUOObject(sys.argv)
ob.run()
