# -*- coding: iso-8859-1 -*-

import scapy.all as scapy

IDENTITY       = 11

def digest(val):
    retval = 0
    for v in [ord(x) for x in val]:
        retval ^= ((v & 0xF)<<4) | ((v>>4) & 0xF)
    return retval

def userProc(self):
    i = self.next()    # nº de procedimiento

    if i == 0: # init
        self.saved_value = [0, True]
        self.objects = [digest("PIR1"), digest("PIR2")]

    if i == 2:
        ip = [x[4] for x in scapy.conf.route.routes if x[2] != '0.0.0.0'][0].ljust(15)
        for i in range(1,16):
            self.F[i] = ord(ip[i-1])

    elif i == 6: # get BOOL
        if self.R[IDENTITY] == self.objects[1]:
            addr = self.fsmData[1] + 1
            self.R[addr] = self.saved_value[1]

    elif i == 5: # set BOOL
        if self.R[IDENTITY] == self.objects[1]:
            self.saved_value[1] = self.R[self.fsmData[1] + 1]

    elif i == 25: # set BYTE
        if self.R[IDENTITY] == self.objects[0]:
            self.saved_value[0] = self.R[self.fsmData[1] + 1]

    elif i == 26: # get BYTE
        if self.R[IDENTITY] == self.objects[0]:
            addr = self.fsmData[1] + 1
            self.R[addr] = self.saved_value[0]

