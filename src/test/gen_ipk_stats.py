#!/usr/bin/python
# -*- coding:utf-8 -*-

import sys
import os
from string import Template
import commands as cmd

picodir = os.environ['PICO']

if len(sys.argv) < 2 or len(sys.argv) > 3:
    print "Usage: ./gen_ipk_stats.py <ipk_dir> [output.stat]"
    sys.exit(1)

dirname = sys.argv[1]

output = "output.stat"
try:
    output = sys.argv[2]
except IndexError:
    pass

if not os.path.exists(dirname):
    print "Directory '%s' doesn't exist" % dirname
    sys.exit(1)


fout = file(output, "w")

# Crear cabecera
thead = Template("""
INFORME DE PRUEBAS
==================

- computadora: $machine
- usuario:     $user
- fecha:       $date
- revisión:    $rev


FICHEROS
========

""")

head = thead.substitute(machine=os.uname()[1],
                        user=os.environ["USER"],
                        date=cmd.getoutput("date"),
                        rev=cmd.getoutput("hg tip| head -1 | awk '{print $2}'")
                        )
fout.write(head)


tfiles = Template("""
[$index]  $path
$content

""")


lstfiles = {}
for root, dirs, files in os.walk(dirname):
    count = 1
    for f in files:
        if not os.path.splitext(f)[1] == ".stat":
            continue

        basename = os.path.splitext(f)[0]
        basename = os.path.splitext(basename)[0]
        lst = os.path.join(root, basename + ".fsm.lst")
        if not os.path.exists(lst):
            print "Warning: file '%s' has not lst definition. Ignored!" % f
            continue

        path = os.path.join(root, f)
        stat = file(path, "r")

        filename = stat.readline()
        if not filename.startswith("file:"):
            print "WARNING: Stat file malformed '%s'" % path
            stat.close()
            continue

        filename = filename[6:].replace(picodir, "$PICO")
        body = stat.read()
        fout.write(tfiles.substitute(index=count, path=filename, content=body))
        lstfiles[count] = lst
        count += 1

fout.write("RESULTADOS\n")
fout.write("==========\n\n")
fout.write(" # | data  # | flash  # | ram  # | code inst block goto |  tot | CT\n")
fout.write("---+---------+----------+--------+----------------------+------+-------\n")


for i, f in lstfiles.items():
    line = cmd.getoutput("tail -1 %s" % f)
    line = line.replace("lst[INFO]:", "").strip()
    fout.write("%2d %s\n"%(i,line))

fout.flush()
fout.close()


