// -*- mode:c++ -*-

module Test {

  interface A {
    void a1();
    void a2(byte a);
    void a3(bool a, int b);
  };

  interface B {
    int b1();
    byte b2();
    bool b3();
  };

  module SubTest {
    interface C extends Test::A,Test::B {};
  };
};

