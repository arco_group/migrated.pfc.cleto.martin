// -*- mode:c++ -*-

module Test {

  interface A {
    void a1();
    void a2(byte a);
    void a3(bool a, int b);
  };

  interface B {
    int b1();
    byte b2();
    bool b3();
  };

  interface C extends A,B {};

  module SubTest {
    interface D extends A,B,C {
      void d1(A objA);
    };
  };
};
