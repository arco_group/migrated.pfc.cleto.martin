//-*- mode: c++; coding: utf-8 -*-

//FIXME: Las interfaces DUO.*.RW no son interfaces DUO legales

#include <Ice/Identity.ice>

module DUO {
  module iBool {
    interface R { ["opID:5"] idempotent bool get(); }; 
    interface W { ["opID:6"] void set(bool v, Ice::Identity oid); };
    interface RW extends R, W {};
  };

  module iByte {
    interface R { idempotent byte get(); }; 
    interface W { void set(byte v, Ice::Identity oid); };
    interface RW extends R, W {};
  };
};

