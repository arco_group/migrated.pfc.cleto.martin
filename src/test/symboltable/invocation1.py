# -*- coding:utf-8 -*-

import icepick_utils as ipk

frontend = ipk.Frontend("invocation.ipk", sis_file = "methods.sis",
                        includes = ["."])


st = frontend.parse()


params = {"a1":["objD",0],
          "a2":["objD",1],
          "a3":["objA",2]}

timer = st.getTimerTriggers()[0]

invs = timer.getInvocations()
assert len(invs) == 3

for i in invs:
    name = i.getMethod().getName()
    assert i.getObject().getName() == params[name][0]
    assert i.getMethod() != None
    assert len(i.getParameters()) == params[name][1]
    assert len(i.getCondition().getParams()) == 0
    if name == "a3":
        for p in i.getParameters():
            inv = p.getValueAsInvocation()
            assert inv.getObject().getName() == "objB"
            assert inv.getMethod().getName() in ["b3", "b1"]
            assert len(inv.getParameters()) == 0
