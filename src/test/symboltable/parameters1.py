# -*- coding:utf-8 -*-

import icepick_utils as util
import IcePyck as ipk

frontend = util.Frontend("invocation.ipk", sis_file = "methods.sis",
                         includes = ["."])


st = frontend.parse()


params = {"a1":[0, []],
          "a2":[1, [ipk.NUMERIC]],
          "a3":[2, [ipk.INVOCATION, ipk.INVOCATION]]
          }

timer = st.getTimerTriggers()[0]

invs = timer.getInvocations()
assert len(invs) == 3

for i in invs:
    name = i.getMethod().getName()
    assert len(i.getParameters()) == params[name][0]
    parms = i.getParameters()
    for j,p in enumerate(parms):
        assert params[name][1][j] == p.getType()

    if name == "a2":
        param = i.getParameters()[0]
        assert 12 == param.getValueAsInteger()
    elif name == "a3":
        for p in i.getParameters():
            inv = p.getValueAsInvocation()
            assert inv.getMethod().getName() in ["b3", "b1"]
            assert inv.getObject().getName() in "objB"
            assert len(inv.getParameters()) == 0








