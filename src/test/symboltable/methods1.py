# -*- coding:utf-8 -*-

import icepick_utils as ipk

frontend = ipk.Frontend("methods.ipk", sis_file = "methods.sis",
                        includes = ["."])
st = frontend.parse()

### allInvocations
assert len(st.getAllInvocations()) == 9


### objetByName
obj_names = ["objA", "objB", "objD"]
for i in obj_names:
    st.getObjectByName(i)

try:
    st.getObjectByName("NOEXISTE");
    assert False
except Exception:
    pass


### getAdapters
assert len(st.getAdapters()) == 2

### methodsByInterface
assert len(st.getMethodsByInterface("::Test::A")) == 3
assert len(st.getMethodsByInterface("::Test::B")) == 3
assert len(st.getMethodsByInterface("::Test::C")) == 0
assert len(st.getMethodsByInterface("::Test::SubTest::D")) == 1

### getSIS
assert st.getSIS() != None
sis = st.getSIS()
assert len(sis.getEvents()) == 3

### boot
assert len(st.getBootTriggers()) == 1

### when
assert len(st.getWhenTriggers()) == 3

### function
assert len(st.getFunctionTriggers()) == 0

### timer
assert len(st.getTimerTriggers()) == 1

### event
assert len(st.getEventTriggers()) == 3

### remoteObjects
assert len(st.getRemoteObjects()) == 1

### adapterByObject
assert st.getAdapterByObject("objA") != None
try:
    st.getAdapterByObject("LuCasElDeleznable")
    assert False
except Exception, e:
    pass

### isParent
assert not st.isParent("::Test::A", "::Test::A")
assert st.isParent("::Test::A", "::Test::C")
assert not st.isParent("::Test::A", "::Test::B")
assert not st.isParent("::Test::B", "::Test::A")
assert st.isParent("::Test::B", "::Test::C")
assert st.isParent("::Test::C", "::Test::SubTest::D")
assert st.isParent("::Test::A", "::Test::SubTest::D")
assert st.isParent("::Test::B", "::Test::SubTest::D")

### localObjects
assert len(st.getLocalObjects()) == 1
