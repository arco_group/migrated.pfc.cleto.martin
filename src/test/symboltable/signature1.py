# -*- coding:utf-8 -*-

import icepick_utils as util
import IcePyck as ipk

frontend = util.Frontend("objects.ipk", sis_file = "methods.sis",
                         includes = ["."])
st = frontend.parse()

objects = ["A", "B", "C", "D"]

methods = {"ice_isA":["::Ice::Object", ipk.IDEMPOTENT],
           "ice_ping":["::Ice::Object", ipk.IDEMPOTENT],
           "ice_id":["::Ice::Object", ipk.IDEMPOTENT],
           "ice_ids":["::Ice::Object", ipk.IDEMPOTENT],
           "a1":["::Test::A", ipk.NORMAL],
           "a2":["::Test::A", ipk.NORMAL],
           "a3":["::Test::A", ipk.NORMAL],
           "b1":["::Test::B", ipk.NORMAL],
           "b2":["::Test::B", ipk.NORMAL],
           "b3":["::Test::B", ipk.NORMAL],
           "d1":["::Test::SubTest::D", ipk.NORMAL]
           }

paramsIn = {"ice_isA": [],
            "ice_id": [],
            "ice_ids": [],
            "ice_ping": [],
            "a1": [],
            "a2": [("a", "Byte")],
            "a3": [("a", "bool"), ("b", "Int")],
            "b1": [],
            "b2": [],
            "b3": [],
            "d1": [("objA", "const ::Test::APtr")],
            }

paramsOut = {"ice_isA": [("","bool")],
             "ice_id": [("","string")],
             "ice_ids": [("","::Ice::StrSeq")],
             "ice_ping": [("", "void")],
             "a1": [("", "void")],
             "a2": [("", "void")],
             "a3": [("", "void")],
             "b1": [("", "Int")],
             "b2": [("", "Byte")],
             "b3": [("", "bool")],
             "d1": [("", "void")],
             }

for i in objects:
    obj = st.getObjectByName("obj"+i)
    all_methods = obj.getMethods()
    for m in all_methods:
        assert methods[m.getName()] == [m.getIface(), m.getMode()]
        inParams = m.getInParams()
        params = []
        for p in inParams:
            params.append(tuple(p))
        assert paramsIn[m.getName()] == params

        outParams = m.getOutParams()
        params = []

        for p in outParams:
            params.append(tuple(p))

        assert paramsOut[m.getName()] == params


