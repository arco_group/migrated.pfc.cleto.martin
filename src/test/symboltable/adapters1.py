# -*- coding:utf-8 -*-

import icepick_utils as util
import IcePyck as ipk

frontend = util.Frontend("adapters.ipk", sis_file = "methods.sis",
                         includes = ["."])
st = frontend.parse()

assert len(st.getAdapters()) == 2

adapters = st.getAdapters()

local = adapters[0]
remote = adapters[1]

if local.getType() != ipk.LOCAL:
    local, remote = remote, local

assert remote.getType() == ipk.REMOTE
assert local.getType() == ipk.LOCAL

### Local
assert local.getName() == "theAdapter"
assert local.getEndpoint() == "theAdapter endpoint"
assert len(local.getObjects()) == 2
obj = local.getObjects()[0]
assert obj.getIdentity() == "OBJA"
obj = local.getObjects()[1]
assert obj.getIdentity() == "OBJD"

### Remote
assert remote.getName() == "other"
assert remote.getEndpoint() == "other endpoint"
assert len(remote.getObjects()) == 1
obj = remote.getObjects()[0]
assert obj.getIdentity() == "OBJB"
