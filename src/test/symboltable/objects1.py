# -*- coding:utf-8 -*-

import icepick_utils as util
import IcePyck as ipk

frontend = util.Frontend("objects.ipk", sis_file = "methods.sis",
                         includes = ["."])
st = frontend.parse()

names = [("A", ipk.ONEWAY), ("B", ipk.TWOWAY), ("C", ipk.TWOWAY),
         ("D", ipk.DATAGRAM)]

types = {"A":"::Test::A",
         "B":"::Test::B",
         "C":"::Test::C",
         "D":"::Test::SubTest::D"}

ifaces = {"A":["::Ice::Object", "::Test::A"],
         "B":["::Ice::Object", "::Test::B"],
         "C":["::Ice::Object", "::Test::A", "::Test::B", "::Test::C"],
         "D":["::Ice::Object", "::Test::A",
              "::Test::B", "::Test::C","::Test::SubTest::D"]}

nmethods = {"A":4+3, # 4 = metodos de Ice
            "B":4+3,
            "C":4+6,
            "D":4+7}

attr = {"A":0,
        "B":0,
        "C":0,
        "D":0}

objects = []
for i in names:
    obj = st.getObjectByName("obj"+i[0])
    assert obj.getMode() == i[1]
    objects.append(obj)
    assert types[i[0]] == obj.getInterface()
    assert ifaces[i[0]] ==  obj.getAllInterfaces()
    assert attr[i[0]] == len(obj.getAttributes())
    assert nmethods[i[0]] ==  len(obj.getMethods())

assert len(objects) == 4
assert ["objA", "objB", "objC", "objD"] == [x.getName() for x in objects]
