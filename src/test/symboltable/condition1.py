# -*- coding:utf-8 -*-

import icepick_utils as util
import IcePyck as ipk

frontend = util.Frontend("condition.ipk", sis_file = "methods.sis",
                         includes = ["."])


st = frontend.parse()

timer = st.getTimerTriggers()[0]

timer_if = {"a1":[0],
            "a2":[2,ipk.GT]}

for i in timer.getInvocations():
    name = i.getMethod().getName()
    cond = i.getCondition()
    assert timer_if[name][0] == len(cond.getParams())
    if len(cond.getParams()) > 0:
        assert timer_if[name][1] == cond.getType()


when = st.getWhenTriggers()[0]

types = [ipk.LTEQ, ipk.EQ, ipk.LT, ipk. GTEQ, ipk.NEQ]

for i,inv in enumerate(when.getInvocations()):
    cond = inv.getCondition()
    assert cond.getType() == types[i]
