					;ipk-mode

;;;; Minimal setup

(defvar ipk-mode-hook nil)

(defvar ipk-mode-map
  (let ((ipk-mode-map (make-keymap)))
    (define-key ipk-mode-map "\C-c \C-c" 'compile)
    ipk-mode-map)
  "Keymap for ipk major mode")


(defconst ipk-default-tab-width 3)

(add-to-list 'auto-mode-alist '("\\.ipk\\'" . ipk-mode))

;;;;; Syntax
(defconst ipk-font-lock-keywords-2
  (list
   '("\\<\\(uses\\|local\\|remote\\)" . font-lock-keyword-face)
   '("\\<\\(true\\|false\\|objects\\|endpoint\\|topic\\|publisher\\)" . font-lock-constant-face)
   '("\\<\\(object\\|adapter\\)" . font-lock-keyword-face)
   '("\\<\\(boot\\|timer\\|when\\|do\\|event\\|function\\)" . font-lock-keyword-face)
   "Minimal highlighting expressions for ipk mode"))

(defconst ipk-font-lock-keywords-1
  (append ipk-font-lock-keywords-2
	  (list
	   '("\\<\\(FIXME\\)" . font-lock-warning-face)
	   '("[a-zA-Z_][a-zA-Z0-9.]*:\\" . font-lock-constant-face)
	   '("[a-zA-Z_][a-zA-Z0-9.]*[ \t]+\\<\\([a-zA-Z_][a-zA-Z0-9]*\\)" 1 font-lock-variable-name-face)
	   '("\\<\\([a-zA-Z_][a-zA-Z0-9.]*\\)[ \t]+[a-zA-Z_][a-zA-Z0-9.]*" 1 font-lock-type-face)
	   "Second level of highlighting expressions for ipk mode")))

(defvar ipk-font-lock-keywords ipk-font-lock-keywords-2
  "Default highlighting expressions for ipk mode")

;;;;;Ident

(defun ipk-indent-line ()
  "Indent current line as ipk code"
  (interactive)
  (beginning-of-line)

					;Start buffer
  (if (bobp)
      (indent-line-to 0)

    (let ((not-indented t) cur-indent (counter -1))

      (if (looking-at "^[ \t]*\\(}\\|};\\)")
	  (progn
	    (save-excursion
	      (while not-indented
		(forward-line -1)
		(if (looking-at "^[ \t]*\\([a-zA-Z0-9 \-=/:;,.()\"]*}\\|[a-zA-Z0-9 =:;,.()\"{]*}\\)")
		    (setq counter (- counter 1)))
		(if (looking-at "^[ \t]*\\([a-zA-Z0-9 \-=/:;,.()\"]*}\\|[a-zA-Z0-9 =:;,.()\"{]*};\\)")
		    (setq counter (- counter 1)))

		(if (looking-at "^[ \t]*\\([a-zA-Z0-9 =/\-:;,.()\"]*{\\)")
		    (setq counter (+ counter 1)))
		(if (bobp)
		    (progn
		      (setq not-indented nil)
		      (setq cur-indent 0))
		  )
		(if (= counter 0)
		    (progn
		      (setq cur-indent (current-indentation))
		      (setq not-indented nil))))))

	(save-excursion
	  (while not-indented
	    (forward-line -1)
	    (if (looking-at "^[ \t]*\\(}\\|};\\|[a-zA-Z \-/()\".,=0-9]*}\\)")
		(progn
		  (setq cur-indent (current-indentation))
		  (setq not-indented nil))
	      (if (looking-at "^[ \t]*\\([a-zA-Z0-9 \-=:;,.()/\"]*{\\)")
		  (progn
		    (setq cur-indent (+ (current-indentation) ipk-default-tab-width))
		    (setq not-indented nil))
		(if (bobp)
		    (progn
		      (setq not-indented nil))
		  )
		)
	      )
	    )
	  )
	)
      (if cur-indent
	  (indent-line-to cur-indent)
	(indent-line-to 0)))))

;;;;;Syntax Table

					;Create a new Syntax Table
(defvar ipk-mode-syntax-table
  (let ((ipk-mode-syntax-table (make-syntax-table)))
					;Adding rules to table
    (modify-syntax-entry ?/ ". 124b" ipk-mode-syntax-table)
    (modify-syntax-entry ?* ". 23" ipk-mode-syntax-table)
    (modify-syntax-entry ?\n "> b" ipk-mode-syntax-table)
    ipk-mode-syntax-table)
  "Syntax table for ipk-mode")

;;;;;;Main


(defun ipk-mode ()
  "Major mode for editing ipk specification"
  (interactive)
  (kill-all-local-variables)
  (set-syntax-table ipk-mode-syntax-table)
  (use-local-map ipk-mode-map)
  (set (make-local-variable 'indent-line-function) 'ipk-indent-line)
  (set (make-local-variable 'font-lock-defaults) '(ipk-font-lock-keywords))
  (setq major-mode 'ipk-mode)
  (setq mode-name "IPK")
  (run-hooks 'ipk-mode-hook)
  )

(provide 'ipk-mode)
