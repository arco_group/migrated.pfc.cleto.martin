#!/usr/bin/python
# -*- coding: utf-8 -*-
'''\
%s [options] <mode> <bytecode> <userProc_file>"

The program supports three operation modes:
 console: to use with netcat.
 tcp:     default is a TCP server at port 9000.
 udp:     default is a UDP server at port 9000.
 xport:   to drive a xport device.

A picoObject.log file will be generated in working directory.'''


import sys, os, getopt, optparse
import struct
import select
import string, StringIO
import logging as log
import imp
import time
import socket
import signal
import gobject
import serial  # http://pyserial.sourceforge.net/


# FIXME: Los manejadores de eventos temporizados sólo se ejecutan una
# vez, si se necesitan eventos periódicos el handler es responsable de
# reinstalarse a sí mismo


# Arreglo para connect_flash:
### - Necesita como parámetro el tipo del endpoint ya que no es posible
###   escribir en flash ningún valor inicial.

# Añadiduras en la versión 3.4:
### - cmp
### - call y ret
### - put_val (manda un byte proporcionado)


FSM_VERSION = 3.4

info = log.info
log.basicConfig(level=log.DEBUG,
                    format='',
                    filename='picoObject.log',
                    filemode='w')

console = log.StreamHandler()
console.setLevel(log.ERROR)
#console.setFormatter(log.Formatter('%(levelname)-8s: %(message)s'))
log.getLogger().addHandler(console)


cmdstr = [
    'Nop',
    'Reset',      'Goto',       'GotoNZ',    'GotoEq',   'GotoNEq',
    'setEvent',   'Call',       'Ret',
    'Skip',       'SkipMod4',   'GetW',      'GetReg',   'GetStr', 'GetFlash',
    'Assure',     'TestDigest',
    'PutReg',     'PutStr',     'PutData',   'Template', 'PutVal', 'PutFlash',
    'SetW',       'Move',       'SetVal',    'Add',      'SetTimer',
    'Cmp',
    'ConnectData', 'ConnectFlash'   'Close',
    'raiseEvent',
    'UserProc',
    'Debug']

class Cmd:
    (NOP,
     RESET,      GOTO,        GOTO_NZ,     GOTO_EQ,  GOTO_NEQ,
     SET_EVENT,  CALL,        RET,
     SKIP,       SKIP_MOD_4,  GET_W,       GET_REG,  GET_STR, GET_FLASH,
     ASSURE,     TEST_DIGEST,
     PUT_REG,    PUT_STR,     PUT_DATA,    TEMPLATE, PUTVAL, PUT_FLASH,
     SET_W,      MOVE,        SET_VAL,     ADD,      SET_TIMER,
     CMP,
     CONNECTDATA,CONNECTFLASH,    CLOSE,
     RAISE_EVENT,
     USER_PROC,
     DEBUG) = range(35)


# Debugging
def funcname():
    try: raise SystemError
    except SystemError:
        exc, val, tb = sys.exc_info()
        caller = tb.tb_frame.f_back
        del exc, val, tb
        return "%s(): " % caller.f_code.co_name


def caller_name():
    try: raise SystemError
    except SystemError:
        exc, val, tb = sys.exc_info()
        retval = tb.tb_frame.f_back.f_back.f_code.co_name
        del exc, val, tb
        return retval

def fwarning(msg=''):
    log.warning("- %-22s%s" % (caller_name() + '():', msg))

def fdebug(msg=''):
    log.debug("- %-22s%s" % (caller_name() + '():', msg))

def finfo(msg=''):
    log.info("- %-22s%s" % (caller_name() + '():', msg))

def print_bank(pico):
    retval = ''
    values = [str(x).rjust(4) for x in pico.R]
    for i,v in enumerate(values):
        retval += v
        if (i+1) % 16 == 0: retval += '\n'
        elif (i+1) % 8 == 0: retval += ' - '
    return retval



# Utilities
def digest1(sum_, c):
    return sum_ ^ (((c & 0xF) << 4) | ((c>>4) & 0xF))

def to_char(c):
    if c < 0 or c > 255: return ' '
    if isinstance(c, int): c = chr(c)
    if c in string.printable: return repr(c)
    return ' '

def pretty(L):
    return '[' + ','.join([str(x).rjust(3) for x in L]) + ']'


DISCONNECTED, ACTIVE, PASSIVE = (0, 1, 2)
TIMEOUT   = -1
EMPTY     = -2
TRY_AGAIN = -3
IN_EVENT  = -4

RESET_EVENT    = 1
ERROR_EVENT    = 2
INCOMING_EVENT = 3
TIMER_EVENT    = 4

SENSOR_EVENT   = 10

# registros conocidos
RET_POINTER    = 9
IDENTITY       = 11


def cmd_need_read(op):
    if op == None: return False
    return Cmd.SKIP <= op <= Cmd.TEST_DIGEST

def cmd_need_write(op):
    return Cmd.PUT_REG <= op <= Cmd.TEMPLATE


class Pico:

    def __init__(self, code, data, userproc, debug, bank, flash):

        self.cmd = [
            self.cmd_fsmNop,
            self.cmd_fsmReset,
            self.cmd_fsmGoto,
            self.cmd_fsmGotoNZ,
            self.cmd_fsmGotoEq,
            self.cmd_fsmGotoNEq,
            self.cmd_fsmSetEvent,
            self.cmd_fsmCall,
            self.cmd_fsmRet,
            self.cmd_fsmSkip,
            self.cmd_fsmSkipMod4,
            self.cmd_fsmGetW,
            self.cmd_fsmGetReg,
            self.cmd_fsmGetStr,
            self.cmd_fsmGetFlash,
            self.cmd_fsmAssure,
            self.cmd_fsmTestDigest,
            self.cmd_fsmPutReg,
            self.cmd_fsmPutStr,
            self.cmd_fsmPutData,
            self.cmd_fsmTemplate,
            self.cmd_fsmPutVal,
            self.cmd_fsmPutFlash,
            self.cmd_fsmSetW,
            self.cmd_fsmMove,
            self.cmd_fsmSetVal,
            self.cmd_fsmAdd,
            self.cmd_fsmSetTimer,
            self.cmd_fsmCmp,
            self.cmd_fsmConnectData,
            self.cmd_fsmConnectFlash,
            self.cmd_fsmClose,
            self.cmd_fsmRaiseEvent,
            self.cmd_fsmUserProc,
            self.cmd_fsmDebug,
            ]

        self.len_cmd = [
            self.len_cmdSkip,
            self.len_cmdSkipMod4,
            self.len_cmdGetW,
            self.len_cmdGetReg,
            self.len_cmdGetStr,
            self.len_cmdGetFlash,
            self.len_cmdAssure,
            self.len_cmdTestDigest
            ]


        self.fd = None
        self.fsmCode = code
        self.fsmData = data
        self.fsmFlash = flash

        self.R = bank
        self.F = flash

        self.userproc = userproc
        if debug == None:
            self.fsmDebug = self.cmd_fsmNop
        else:
            self.fsmDebug = debug

        self.sensorSrc = 0
        self.restartSrc = 0

        self.page = 0
        self.p = 0

        self.handlers_raw = [None]*255
        self.inEvent = False
        self.unresolvedEvents = []
        self.handlers_raw[RESET_EVENT] = [0,0]
        self.handlers_raw[ERROR_EVENT] = [0,0]
        self.op = 0
        self.received = 0
        self.timeout_id = None
        self.str_timeout = None
        self.running = False
        self.gettingStr = False

        self.input_required = 0
        self.input_buffer = []
        self.saved_value = False

        self.status = DISCONNECTED
        self.source_id = None
        self.new_client = False


    def abs(self):
        return 256 * self.page + self.p

    def addr(self):
        return [self.page, self.p]

    def strstatus(self):
        return ['DISCONNECTED', 'ACTIVE', 'PASSIVE'][self.status]

    def goto(self, addr):  # salto explícito
        self.page, self.p = addr
        self.gotoF = True

    def goto_handler(self, handler):
        if self.handlers_raw[handler] == None:
            info ("goto_handler(): handler %s not set. Skipping" %handler)
            return False # Manejador de evento no instalado.

        self.goto(self.handlers_raw[handler])
        if not (handler == ERROR_EVENT or handler == RESET_EVENT):
            self.inEvent = True

        info("gotoHandler (%d) -> %s " % (handler, self.handlers_raw[handler]))
        return True # Manejador de evento instalado.

    def fireEvent(self):
        if self.events_allowed() and self.events_pending():
            event = self.unresolvedEvents.pop()
            if self.goto_handler(event):
                if event == INCOMING_EVENT:
                    self.status = PASSIVE
                info("Dispatching event %s" %event)

    def signal_event(self, event):
        info("Signaling event %d" %event)
        if event in self.unresolvedEvents:
            info("event %d yet preproccessed. Skipping" %event)
            return
        if ((not self.running) and self.events_allowed()) or \
                event == RESET_EVENT or event == ERROR_EVENT:
            if event == INCOMING_EVENT:
                self.status = PASSIVE
            if self.goto_handler(event):
                info("signal_event(): Resuming FSM :-S")
                self.run_while_has_data()
        else:
            self.unresolvedEvents.append(event)
            self.unresolvedEvents.sort()
            self.unresolvedEvents.reverse()
            info("signal_event(): Append event %d. Total: %d" %(event, len(self.unresolvedEvents)))


    def events_allowed(self):
        return not self.inEvent and self.status == DISCONNECTED

    def events_pending(self):
        return len(self.unresolvedEvents) > 0

    def enough_data(self):
        fdebug('There are %s, needs %s' % (len(self.input_buffer), self.input_required))
        return len(self.input_buffer) >= self.input_required

    def set_required_data(self, op):
        n = 0
        if cmd_need_read(op):
            n = self.len_cmd[self.op-Cmd.SKIP]()
            info('%s() needs %s bytes' % (cmdstr[self.op], n))

        self.input_required = n

    def fsm_getchar(self):
        "Extrae un byte del buffer de entrada"
#        assert len(self.input_buffer) > 0 or not self.events_pending()
        assert len(self.input_buffer) > 0

#         if self.inEvent:
#             log.error("Reading isn't posible in an event handler")
#             c = self.input_buffer.pop() # Rompe la cadena de entrada.
#             self.goto_handler(ERROR_EVENT)
#             return IN_EVENT

        c =  self.input_buffer.pop()
        info("-> %02x (%3d) %s" % (c, c, to_char(c)))
        return c

    def fsm_putchar(self, c):
        if not self.fd:
            log.warning("Disconnected, skipping write")
            return

        self.fd.write(c)

    def sync(self):
        pass

    def close(self):
        if self.fd == None: return

        self.status = DISCONNECTED

        self.fd.close()
        del self.fd
        self.fd = None


    def next(self, inc=True):
        if self.p > 255:
            self.page += 1
            self.p = 0

        retval = self.fsmCode[self.abs()]
        if inc: self.p += 1
        return retval


    def fetch_pointer(self):
        page = self.page
        offset = self.next()
        if offset < 4:
            page = offset
            offset = self.next()

        return [page, offset]


    ### implementación de comandos ###

    def cmd_fsmNop(self):
        info("Nop")


    def cmd_fsmReset(self):

        if self.fd:
            try:
                self.fd.flush()
            except WriteErrorEx:
                self.status = DISCONNECTED

        # FIXME: el handler por defecto debería ser Reset en lugar del
        # bloque main, para que se inicializen los contadores, pero eso
        # implicaría un bloque Reset obligatorio y de ubicación conocida

        # FIXME: desconectar realmente [socket.close()] ?
        if self.status == ACTIVE: self.status = DISCONNECTED
        self.goto_handler(RESET_EVENT)

        self.inEvent = False

        self.R[1] = 256     # nº de caracteres
        self.received = 0
        self.input_required = 0

        info('Reset() '+'#'*50+'\n\n')


    def cmd_fsmGoto(self):
        self.goto(self.fetch_pointer())
        info("Goto (%s)" % self.abs())


    def cmd_fsmGotoNZ(self):
        dest = self.fetch_pointer()

        if self.R[0] != 0:
            self.goto(dest)

        info("GotoNZ (%s) [%d] %s" %
              (dest, self.R[0], self.R[0] != 0))


    def cmd_fsmGotoEq(self):
        dest = self.fetch_pointer()
        v = self.next()

        if self.R[0] == v:
            self.goto(dest)

        info("GotoEq (%s, %d) [%d(%s)] %s" %
            (dest, v, self.R[0], to_char(self.R[0]), self.R[0] == v))


    def cmd_fsmGotoNEq(self):
        dest = self.fetch_pointer()
        v = self.next()

        if self.R[0] != v:
            self.goto(dest)

        info("GotoNEq (%s, %d) [%d(%s)] %s" %
            (dest, v, self.R[0], to_char(self.R[0]), self.R[0] != v))

    # se puede hacer con inc_reg(0, -1); gotonz()
    #def cmd_fsmDecAndGotoNZ():
    #    dest = fetch_pointer()
    #    i = self.next()
    #
    #    info("DecAndGotoNZ (%d, %d)" % (dest, i))
    #
    #    R[i] -= 1
    #    if R[i]:
    #        self.goto(dest)
    #
    #    info("DecAndGotoNZ: [%d -> %d]" % (R[i], self.abs()))


    def cmd_fsmSetEvent(self):
        pointer = self.fetch_pointer();
        event = self.next()
        self.handlers_raw[event] = pointer


        info("SetEvent (%d) -> %s" %(event, pointer))

    def len_cmdSkip(self):
        return 1;

    def cmd_fsmSkip(self):
        info("Skip () %d" % self.fsm_getchar())

    # FIXME: SIN PROBAR
    def len_cmdSkipMod4(self):
        i = self.next()
        lon = self.R[i]
        lon += (self.received + lon) % 4

        info("SkipMod4 (%d) [%d -> %d]" % (i, self.received, lon))
        return lon

    def cmd_fsmSkipMod4(self):
        raise NotImplementedError

    # def cmd_fsmSkipMod4():
    #     i = self.next()
    #     lon = R[i]
    #
    #     lon += (self.received + lon) % 4
    #
    #     info("SkipMod4 (%d) [%d -> %d]" % (i, self.received, lon))
    #
    #     for i in range(lon):
    #         getchar()
    #

    def len_cmdGetW(self):
        return 1

    def cmd_fsmGetW(self):
        ch = self.fsm_getchar()
        if ch == IN_EVENT:
            return
        if ch > 255: ch -= 256

        self.R[0] = int(ch)
        info("GetW: %d (%s)" % (self.R[0], to_char(ch)))

    def len_cmdGetReg(self):
        return 1

    def cmd_fsmGetReg(self): # GetCounter
        i = self.next()
        offset = self.next()
        ch = self.fsm_getchar()
        if ch == IN_EVENT:
            return
        ch += offset
        if ch > 255: ch -= 256

        self.R[i] = int(ch)
        info("GetReg (%s, %s): %d" % (i, offset, self.R[i]))

    def len_cmdGetStr(self):
        return self.R[0]

    def cmd_fsmGetStr(self): # Store
        dest = self.next()
        maxLon = self.next()
        lon = self.R[0]
        if lon > maxLon:
            info("GetStr (%d): %B ---> ERROR. Max lon: %B exceded" %(dest, lon, maxLon))
            self.goto_handler(ERROR_EVENT)
            return

        self.R[dest] = self.R[0]

        for i in range(lon):
            val = self.fsm_getchar()
            if val == IN_EVENT:
                self.gettingStr = False
                return
            self.R[dest+i+1] = val
            self.gettingStr = True

        self.gettingStr = False
        info("GetStr (%d): %dB" % (dest, lon))

    def len_cmdGetFlash(self):
        return self.R[0]

    def cmd_fsmGetFlash(self): # Store
        dest = self.next()
        maxLon = self.next()
        lon = self.R[0]
        if lon > maxLon:
            info("GetFlash (%d): %B ---> ERROR. Max lon: %B exceded" %(dest, lon, maxLon))
            self.goto_handler(ERROR_EVENT)
            return

        self.F[dest] = self.R[0]

        for i in range(lon):
            val = self.fsm_getchar()
            if val == IN_EVENT:
                self.gettingStr = False
                return
            self.F[dest+i+1] = val
            self.gettingStr = True

        self.gettingStr = False
        info("GetFlash (%d): %dB" % (dest, lon))


    def len_cmdAssure(self):
        return self.next()

    def cmd_fsmAssure(self):
        lon = self.input_required
        expected_sum = self.next()
        val_sum = 0

        info("Assure (%d, %d)" % (lon, expected_sum))

        for i in range(lon):
            ch = self.fsm_getchar()
            if ch == IN_EVENT:
                self.gettingStr = False
                return
            self.gettingStr = True
            val_sum = digest1(val_sum, ch)

        result = '+'
        self.gettingStr = False
        if val_sum != expected_sum:
            result = '!'
            self.goto_handler(ERROR_EVENT)

        info("Assure: %d%c " % (val_sum, result))



    def len_cmdTestDigest(self):
        return self.R[0]

    def cmd_fsmTestDigest(self):
        n = self.R[0]
        val_sum = 0
        for i in range(n):
            val = self.fsm_getchar()
            if val == IN_EVENT:
                return
            val_sum = digest1(val_sum, val)

        self.R[0] = val_sum

        info("TestDigest() [W:%d] : %s" % (n, val_sum))

    def cmd_fsmPutReg(self):
        i = self.next()
        value = self.R[i]
        self.fsm_putchar(value)

        info("PutReg (R[%d]) %d" % (i,value))


    def cmd_fsmPutStr(self):  # EchoStored
        i = self.next()
        lon = self.R[i]

        for j in range(lon):
            self.fsm_putchar(self.R[i+1+j])

        info("PutStr (%d) %dB" % (i, lon))


    def cmd_fsmPutData(self): # EchoStr
        i = self.next()
        lon = self.fsmData[i]

        for j in range(lon):
            self.fsm_putchar(self.fsmData[i+1+j])

        info("PutData (%d) %dB" % (i, lon))


    def cmd_fsmTemplate(self): # EchoTmpl
        i = self.next()
        pos = self.next()
        lon = self.fsmData[i]
        offset = self.R[0]

        #FIXME: aquí habrá problemas para offset negativos, habrá que hacer
        #lo mismo que en GetReg


        info("Template (%d, %d)  [lon:%d, W:%d]" % (i, pos, lon, offset))

        for j in range(lon):
            ch = self.fsmData[i+1+j]
            if j == pos:
                ch += offset
                if ch > 255: ch -= 256

            self.fsm_putchar(ch)


    def cmd_fsmPutVal(self):
        v = self.next()
        self.fsm_putchar(v)
        info("PutVal (%d)" % v)


    def cmd_fsmPutFlash(self): # EchoStr
        i = self.next()
        lon = self.F[i]

        for j in range(lon):
            self.fsm_putchar(self.F[i+1+j])

        info("PutFlash (%d) %dB" % (i, lon))


    def cmd_fsmSetW(self):
        self.R[0] = self.next()
        info("SetW (%d)" % self.R[0])


    def cmd_fsmMove(self):
        dest = self.next()
        source = self.next()
        self.R[dest] = self.R[source]
        info("Move (%d) -> (%d) [val:%d]" % (source, dest, self.R[source]))


    def cmd_fsmSetVal(self):
        i = self.next()
        val = self.next()
        self.R[i] = val

        info("SetVal (%d, %d)" % (i, val))


    def cmd_fsmAdd(self):
        i = self.next()
        val = self.next()
        if val > 255: val -= 256
        self.R[i] += val

        if self.R[i] > 255: self.R[i] -= 256

        info("Add(%d, %d) : %d" % (i, val, self.R[i]))

    def cmd_fsmSetTimer(self):

        def invoke_handler(*args):
            info("Timeout ocurred!!")

##            if self.status != DISCONNECTED:
##                info('Timeout skipped, status is %s' % self.strstatus())
##                #FIXME: el evento se pierde para siempre, es necesario
##                #que en algún punto se vuelva a intalar el manejador
##                return True

#             self.timeout_id = None
            self.signal_event(TIMER_EVENT)
            return True

        t = self.next()
        #self.timeout_id = None

        if t > 0:
            self.timeout_id = gobject.timeout_add(2*t*1000, invoke_handler)
            info("SetTimer (%d): %d. Timer restarted." %(t,2*t))
        else:
            gobject.source_remove(self.timeout_id)
            info("SetTimer (%d): %d. Timer deactivated." %(t,2*t))

    def cmd_fsmCmp(self):
        reg = self.next()

        debug = "Cmp (%d) [%d,%d]: " % (reg, self.R[0], self.R[reg])

        result = 1
        if self.R[0] < self.R[reg]:
            result = 2
        elif self.R[0] == self.R[reg]:
            result = 0

        self.R[0] = result

        debug += "%d" % result

        info(debug)

    def cmd_fsmConnectData(self):
        #proto_start = self.next() + 1
        #ip_start    = proto_start + 1
        #port_start = ip_start + 15
        #proto = chr(self.fsmData[proto_start])
        #ip    = ''.join(map(chr, self.fsmData[ip_start:ip_start+15])).strip()
        #port  = ''.join(map(chr, self.fsmData[port_start:port_start+2])).strip()
        assert self.status == DISCONNECTED, ("Already connected '%s'" % self.strstatus())
        self.eventsEnabled = False
        pos = self.next()
        size = self.fsmData[pos]
        proto = chr(self.fsmData[pos+1])
        ip    = ''.join(map(chr, self.fsmData[pos+2:pos+17])).strip()
        port  = ''.join(map(chr, self.fsmData[pos+17:pos+19])).strip()


        #print "%s:%s:%s" % (proto, ip, port)
        #for i in port: print ord(i)


        port   = int(struct.unpack('H', port)[0])
        info("Connect_Data (%s, %s, %s) [%s]" % (proto, ip, port, size))
        self.perform_connection(proto, ip, port)

    def cmd_fsmConnectFlash(self):
        #proto_start = self.next() + 1
        #ip_start    = proto_start + 1
        #port_start = ip_start + 15
        #proto = chr(self.fsmData[proto_start])
        #ip    = ''.join(map(chr, self.fsmData[ip_start:ip_start+15])).strip()
        #port  = ''.join(map(chr, self.fsmData[port_start:port_start+2])).strip()
        assert self.status == DISCONNECTED, ("Already connected '%s'" % self.strstatus())
        pos = self.next()
        size = self.F[pos]
        t = self.next()
        if t in [ord(x) for x in ['U', 'T']]:
            self.eventsEnabled = False

            ip    = ''.join(map(chr, self.F[pos+1:pos+16])).strip()
            port  = ''.join(map(chr, self.F[pos+16:pos+18])).strip()

            port   = int(struct.unpack('H', port)[0])
            info("Connect (%s, %s, %s) [%s]" % ('T', ip, port, size))

            self.perform_connection('T', ip, port) # FIXME
        else:
            dst   = struct.unpack('b', self.F[pos+1:pos+3])
            debug = self.F[pos+4] == 1
            self.perform_connection('X', dst, debug)

    def perform_connection(self, proto, ip, port):
        raise NotImplementedError

    def cmd_fsmEndEvent(self):
        self.inEvent = False;
        info ("EndEvent()")

    def cmd_fsmRaiseEvent(self):
        self.signal_event(self.next())

    def cmd_fsmClose(self):
        info("Close()")
        self.close()


    def cmd_fsmUserProc(self):
        self.userproc(self)
        info("UserProc()")
    def cmd_fsmDebug(self):
        info(fsmDebug[self.next()])

    def cmd_fsmCall(self):
        salto = self.fetch_pointer()
        info("Call to " + str(salto) + " from " + str(self.addr()))
        self.R[RET_POINTER], self.R[RET_POINTER + 1] = self.addr()

        self.goto(salto)

    def cmd_fsmRet(self):

        saved = (self.R[RET_POINTER], self.R[RET_POINTER+1])
        if saved != (0,0):
            info("Ret to " + str(saved) + " from " + str((self.page,self.p)))
            self.page = self.R[RET_POINTER]
            self.p    = self.R[RET_POINTER+1]

        self.R[RET_POINTER],self.R[RET_POINTER + 1] = 0,0

    # Operation #

    def getchar(self):
        if self.status == ACTIVE:
            log.error("Reading isn't posible in active connections")
            return None

        if self.status == DISCONNECTED: return EMPTY

        c = self.fd.read()

        if c == EMPTY:
            fwarning("Nothing to read -> DISCONNECTED")
            self.status = DISCONNECTED
            gobject.source_remove(self.source_id)
            return EMPTY

        return c


    def input_handler(self, sock, condition):

        finfo(self.strstatus())

        while 1:

            c = self.getchar()

            if c == EMPTY:
#                self.goto_handler(RESET_EVENT)
#                self.run_while_has_data()
                return False

            self.input_buffer.insert(0, c)

            if self.op == Cmd.RESET: break

            if self.enough_data():
                self.fireEvent()
                self.cmd[self.op]()
                break

        self.run_while_has_data()
        return True



    def run_until_reset(self):
        '''
        Para ejecutar bytecode al arranque. No debería incluir ninguna
        instrucción de lectura.
        '''
        def sensorEvent(*args):
            info(" ------ Generating SENSOR_EVENT -----")
            self.signal_event(SENSOR_EVENT)
            return True

        def restartEvent(*args):
            info (" ----- Initiating FSM code -----")
            self.signal_event(RESET_EVENT)
            return False

        if self.str_timeout:
            gobject.source_remove(self.str_timeout)

        self.running = True
        while 1:
            log.debug(":: P = %3d, onError = %3s, R: %s" %
                          (self.abs(), self.handlers_raw[ERROR_EVENT], pretty(self.R[:8])))


            self.fireEvent()
            self.op = self.next()
            ## assert not cmd_need_read(self.op)

            self.cmd[self.op]()

            if self.op == Cmd.RESET:
                self.running = False
                fdebug('QUIT')
                if self.sensorSrc > -1:
                    gobject.source_remove(self.sensorSrc)
                if self.restartSrc > -1:
                    gobject.source_remove(self.restartSrc)

                self.restartSrc = gobject.timeout_add(1000, restartEvent)
                self.sensorSrc = gobject.timeout_add(5000, sensorEvent)
                return

            #assert not cmd_need_read(self.op)


    def run_while_has_data(self):
        def str_timeout_handler(*args):
            info("Input Timeout ocurred!!")
            self.goto_handler(ERROR_EVENT)
            self.gettingStr = False
            self.run_while_has_data()
            return False

        if self.str_timeout:
            gobject.source_remove(self.str_timeout)

        self.running = True
        while 1:
            log.debug(":: P: %3d, onError: %3s, R: %s" %
                          (self.abs(), self.handlers_raw[ERROR_EVENT], pretty(self.R[:8])))

            self.fireEvent()
            self.op = self.next()
            self.set_required_data(self.op)

            if not self.enough_data():
                self.running = False
                fdebug('execution requires new data. Stopping')
                if self.gettingStr:
                    fdebug('Activating assure timeout')
                    self.str_timeout = gobject.timeout_add(5000, str_timeout_handler)
                break

            self.cmd[self.op]()


class PicoTCP(Pico):

    def __init__(self, *args):
        Pico.__init__(self, *args)

        log.getLogger().addHandler(log.StreamHandler())
        info('- TCP server mode. Port: %s' % opts.port)
        info("- FSM size: %d bytes\n" % len(self.fsmCode))

        master = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        master.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        master.bind(('', opts.port))
        master.listen(1)

        gobject.io_add_watch(master, gobject.IO_IN, self.master_tcp_handler)


    def master_tcp_handler(self, sock, condition):
        info("in master_tcp_handler")
        finfo(self.strstatus())
        child, client = sock.accept()

        if self.status != DISCONNECTED:
            log.warning('Connection attempt skipped: %s' % str(client))
            child.close()
            return True

#        self.status = PASSIVE
        info("\n\nIncoming TCP client from " + str(client))
        #self.cmd_fsmReset()
        self.fd = DecoFile(child.makefile('rw', 0))
        self.source_id = gobject.io_add_watch(child, gobject.IO_IN,
                                              self.input_handler)
        #for c in opts.preload:
        #    self.input_buffer.insert(0, ord(c))
        self.signal_event(INCOMING_EVENT)
        return True


    def perform_connection(self, proto, ip, port):
        protos = {'T':socket.SOCK_STREAM, 'U':socket.SOCK_DGRAM}
        s = socket.socket(socket.AF_INET, protos[proto])

        log.info("IP: %s type: %s" %(ip, type(ip)))
        try:
            s.connect((ip, port))
        except socket.error, e:
            s.close()
            log.warning("At connect %s:%s: '%s'" % (ip, port, e))
            self.goto_handler(ERROR_EVENT)
            return

        self.status = ACTIVE
        self.fd = DecoFile(s.makefile('rw', 128))

# class PicoUDP(Pico):

#     def __init__(self, *args):
#         Pico.__init__(self, *args)
#         log.getLogger().addHandler(log.StreamHandler())
#         info('*UDP* Server mode. Port: %s' % opts.port)
#         info("Total FSM size: %d bytes\n" % len(fsm_file.fsmCode))
#         sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
#         sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
#         sock.bind(('', opts.port))

#         gobject.io_add_watch(sock, gobject.IO_IN, self.input_handler)

#     def input_handler(self, sock, condition):
#         if self.status == PASSIVE:
#             log.debug("new input data: %s" % condition)
#             return True

#         data, peer = sock.recvfrom(0, socket.MSG_PEEK)
#         info("\n\nIncoming UDP data from " + str(peer))
#         sock.connect(peer)
#         self.fd = DecoFile(sock.makefile('rw', 0), opts.preload)
#         #self.new_client = True
#         self.signal_event(INCOMING_EVENT)
#         self.status = PASSIVE
#         return True


class PicoXPort(Pico):

    def __init__(self, *args):
        Pico.__init__(self, *args)

        log.getLogger().addHandler(log.StreamHandler())
        info('- XPort mode. Port: %s' % opts.device)
        info("- Total FSM size: %d bytes\n" % len(fsm_file.fsmCode))

        log.info("'%s'" % opts.preload)
        ser = serial.Serial()
        ser.port = 0
        ser.baudrate = 9600
        ser.rtscts=1
        ser.open()
        self.fd = DecoFile(ser, blocking=False)
        self.source_id = gobject.io_add_watch(ser.fileno(),
                                              gobject.IO_IN,
                                              self.input_handler)

        self.overread = []


    def assure(self, seq):
        '''No tiene nada que ver con el fsm_assure.
        Si lo leido corresponde con la secuencia devuelve '', sino devuelve
        la secuencia leida'''
        retval = []
        for i in range(len(seq)):
            retval.insert(0, self.fd.read())
            if retval[-1] == EMPTY: return retval

        log.info('%s %s' %(retval, seq))
        if seq != retval: return retval
        return []



    def getchar(self):
        if self.status == ACTIVE:
            log.error("Reading is NOT posible in active connections")
            return None

        if self.overread:
            c = self.overread.pop()
        else:
            c = self.fd.read()

        if c == EMPTY:
            fwarning("Nothing to read")
            #self.status = DISCONNECTED
            #gobject.source_remove(self.source_id)
            return EMPTY


        #if self.status == DISCONNECTED: self.status = PASSIVE

        if self.status != DISCONNECTED and c == 51: # '3' closed
            self.overread = self.assure([EMPTY, 13])
            if not self.overread:
                log.info('End of connection')
                self.status = DISCONNECTED
                return EMPTY

        if self.status == DISCONNECTED and c == 50: # '2' incoming
            #self.overread = self.assure([13, 49, 13, 32])
            self.overread = self.assure([32])
            if not self.overread:
                log.info('Incoming connection')

                self.goto(self.incoming_handler)
                self.run_until_reset()
                self.status = PASSIVE
                return EMPTY

        if self.status == DISCONNECTED: return EMPTY

        return c


    def perform_connection(self, proto, ip, port):
        self.fd.puts("ATDT%s,%s\r" % (ip, port))

        answer = self.fd.read()
        if answer in [48, 51, 52]:
            self.fd.read()
            self.goto_handler(ERROR_EVENT)
            return

        #self.input_buffer.insert(0, answer)
        self.status = ACTIVE

class PicoConsole(Pico):

    def __init__(self, *args):
        Pico.__init__(self, *args)

        log.getLogger().addHandler(log.StreamHandler())
        info('- Console mode.')
        info("- Total FSM size: %d bytes\n" % len(fsm_file.fsmCode))

        log.info("'%s'" % opts.preload)
        self.fd = DecoFile(sys.stdin, blocking=False)
        self.source_id = gobject.io_add_watch(sys.stdin.fileno(),
                                              gobject.IO_IN,
                                              self.input_handler)

        self.overread = []


    def getchar(self):
        if self.fd == None:
            self.fd = DecoFile(sys.stdin, blocking=False)
            self.source_id = gobject.io_add_watch(sys.stdin.fileno(),
                                                  gobject.IO_IN,
                                                  self.input_handler)

        if self.status == ACTIVE:
            log.error("Reading is NOT posible in active connections")
            return None

        if self.overread:
            c = self.overread.pop()
        else:
            c = self.fd.read()

        if c == EMPTY:
            fwarning("Nothing to read")
            return EMPTY

        if self.status == DISCONNECTED:
            log.info('Incoming connection')

            self.goto_handler(INCOMING_EVENT)
            self.run_until_reset()
            self.status = PASSIVE

        return c


    def perform_connection(self, proto, ip, port):
        log.info('Performing a connection')
        self.status = ACTIVE

class ReadErrorEx(Exception): pass
class WriteErrorEx(Exception): pass

class DecoFile:

    class stdFile:

        def close(self): pass

        def write(self, str):
            sys.stdout.write(str)

        def read(self, size=0):
            if size==0:
                return sys.stdin.read()
            return sys.stdin.read(size)

        def flush(self):
            sys.stdout.flush()

        def fileno(self):
            return sys.stdin.fileno()

    def __init__(self, handler, blocking=False):

        if handler.fileno() == 0:
            self.handler = self.stdFile()
        else:
            self.handler = handler

        self.fileno = self.handler.fileno

        self.out = ''
        if blocking:
            self.read_func = self.blocking_read
        else:
            self.read_func = self.non_blocking_read

    def blocking_read(self):
        return self.handler.read(1)

    def non_blocking_read(self):
        return os.read(self.handler.fileno(), 1)

    def read(self, n=1):
        try:
            if self.fileno() == 0:
                has_data = select.select([sys.stdin],[],[],2)[0]
            else:
                has_data = select.select([self.handler],[],[],2)[0]

            if not has_data: return EMPTY
            c = self.read_func()
        except OSError, e:
            log.warning('READ ERROR: %s' % e)
            self.handler.close()
            return EMPTY

        if not c:
            return EMPTY

        retval = ord(c)
        finfo("%02x (%3d) %s" % (retval, retval, to_char(retval)))
        return retval

    def write(self, c):
        self.out += chr(c)
        info( "<- %02x (%3d) %s" % (c, c, to_char(c)))


    def gets(self, n):
        retval = ''
        for i in range(n):
            retval += chr(self.read())
        return retval

    def puts(self, s):
        for c in s:
            self.write(ord(c))

        self.flush()


    def flush(self):
        try:
            self.handler.write(self.out)
            self.out = ''
            self.handler.flush()
        except socket.error, e:
            log.warning('WRITE ERROR: %s' % e)
            raise WriteErrorEx


    def close(self):
        self.flush()
        self.handler.close()
        self.handler = None



#class Stdio:
#    def write(self, data):
#        sys.stdout.write(data)
#
#    def flush(self):
#        sys.stdout.flush()
#
#    def fileno(self):
#        return sys.stdin.fileno()


class DisconnectedEx(Exception): pass
class TimeoutEx(Exception): pass




# --- Funciones del intérprete --- #

def load_module(fullpath):
    path, fname = os.path.split(fullpath)
    sys.path.append(path)
    module = os.path.splitext(fname)[0]
    return __import__(module)


class Mode:
    CONSOLE = 'console'
    TCP_SERVER = 'tcp'
    UDP_SERVER = 'udp'
    XPORT   = 'xport'


def leave(*args):
    os.kill(os.getpid(), signal.SIGKILL)

signal.signal(signal.SIGINT, leave)


if __name__ == '__main__':
    parser = optparse.OptionParser(usage=__doc__ % os.path.basename(__file__))
    parser.add_option('-i', dest='preload', type='string', default='',
                      help='consume this string at connect')
    parser.add_option('-x', dest='preload_hex', type='string', default='',
                      help='consume this bytes at connect (skip -i)')
    parser.add_option('-p', dest='port', type="int", default=9000,
                      help='use this port for server mode')
    parser.add_option('-d', dest='device', type='string',
                      help='use this device for xport mode')


    (opts, args) = parser.parse_args()

    if len(args) < 3:
        print "Pico VM for FSM v", FSM_VERSION
        print parser.format_help()
        sys.exit(1)

    opts.mode = args[0]

    if opts.preload_hex:
        data = opts.preload_hex.split(',')
        opts.preload = ''.join([chr(int(x)) for x in data])

    for f in args[1:]:
        if not os.path.exists(f):
            log.error("%s: Error: '%s' doesn't exist" % \
                      (os.path.basename(__file__), f))
            sys.exit(1)

    fsm_file = load_module(args[1])
    user_file = load_module(args[2])
    execfile(args[2])

    fsmDebug = None

    try:
        fsmDebug = fsm_file.fsmDebug
        log.info("- The FSM includes DEBUG info")
    except AttributeError:
        pass

    # Mode.UDP_SERVER: PicoUDP,
    classes = {Mode.TCP_SERVER: PicoTCP,
               Mode.XPORT: PicoXPort,
               Mode.CONSOLE: PicoConsole}

    pico = classes[opts.mode](fsm_file.fsmCode,
                              fsm_file.fsmData,
                              user_file.userProc,
                              fsmDebug,
                              fsm_file.fsmBank[:],
                              fsm_file.fsmFlash[:])


    pico.run_until_reset()
    info("--- Pico initialization finish ---\n")

    gobject.MainLoop().run()


#- conexión mauchly
#- envio anuncio
#- desconecta
#- consume validate de icestorm fuera de conexión
#- conexión pasiva
#- consume IP de cliente
