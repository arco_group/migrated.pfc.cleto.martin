// -*- mode: c++ -*-
#ifndef __ICEPYCK_H__
#define __ICEPYCK_H__

#include "SymbolTable.h"
#include <boost/python.hpp>

class PySymbolTable {
 private:
  const Semantic::SymbolTable& _st;
 public:
  PySymbolTable(const Semantic::SymbolTable& st) : _st(st) {}
  boost::python::list getInputParameters(const std::string& iface, const std::string& method);
  boost::python::list getAllObjectNames();



};


#endif
