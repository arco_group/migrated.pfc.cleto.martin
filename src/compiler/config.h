// -*- mode:c++ -*-
#ifndef __CONFIG_H__
#define __CONFIG_H__

#include <vector>
#include <string>
#include <stdexcept>

class ConfigException : public std::runtime_error {
public:
  ConfigException(const std::string& msg = "") : std::runtime_error(msg) {}
};

class Config {
public:
  bool debug;
  bool wAll;
  bool parse_only;

  std::string input;
  std::string output;
  std::string sis;
  std::string progname;

  std::vector<std::string> paths;

  Config(void);
  void parse(int argc, char* const argv[]);
  void parse(std::vector<std::string> argv);
  void add_path(const std::string& path);
};

extern Config config;

#endif
