// -*- coding:utf-8 mode:c++ -*-
#ifndef __PLUGINMANAGER__H__
#define __PLUGINMANAGER__H__

#include <map>
#include <vector>
#include <SemanticAnalyzer.h>

namespace Semantic {
  
  class IPKPlugin {
  public:
    virtual void initialize(const slc::SymbolTable& sst,
			    const ipk::SymbolTable& ist,
			    const sis::SymbolTable& mst) = 0;
    
    virtual void check(const std::string& key,
		       const std::string& value) = 0;
  };
  
  class PluginManager {
  public:
    static void initialize(const slc::SymbolTable& sst,
			   const ipk::SymbolTable& ist,
			   const sis::SymbolTable& mst);
    
    static void check(const std::string& name, const std::string& value);
    static void reg(const std::string& name, IPKPlugin*);
    static void unreg(const std::string& name);
    
  private:
    static std::map<std::string, IPKPlugin*>* _plugins;
    static std::map<std::string, IPKPlugin*>& getPlugins();
    
    static const slc::SymbolTable* _sst;
    static const ipk::SymbolTable* _ist;
    static const sis::SymbolTable* _mst;
  };
};

#endif
