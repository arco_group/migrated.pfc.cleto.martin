#!/usr/bin/python
# -*- coding:utf-8 -*-

from slice2fsm_utils import plain

from utils import Singleton

class Plugin:
    def __init__(self, name, ifaces, param_types):
        self.name   = name
        self.ifaces = ifaces
        self.param_types = param_types

class PluginManager:

    __metaclass__ = Singleton
    __st = None
    __plugins = []

    def initialize(self, st, fsm):
        for p in self.__plugins:
            p.initialize(st, fsm)

    def reg(self, plugin):
        self.__plugins.append(plugin)

    def getPlugin(self, iface):
        for p in self.__plugins:
            if plain(iface) in p.ifaces:
                return p

        return None

    def getSerializer(self, param_type):
        for p in self.__plugins:
            if plain(param_type) in p.param_types:
                return p
        return None

