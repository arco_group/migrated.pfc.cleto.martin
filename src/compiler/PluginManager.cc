// -*- coding:utf-8 mode:c++ -*-

#include <PluginManager.h>

using namespace std;
using namespace Semantic;


map<string, IPKPlugin*>* PluginManager::_plugins;

const slc::SymbolTable* PluginManager::_sst = 0;
const ipk::SymbolTable* PluginManager::_ist = 0;
const sis::SymbolTable* PluginManager::_mst = 0;

void
PluginManager::initialize(const slc::SymbolTable& sst,
 const ipk::SymbolTable& ist,
  const sis::SymbolTable& mst) {
 _sst = &sst;
_ist = &ist;
                   _mst = &mst;

  map<string, IPKPlugin*> plugins = getPlugins();
  for (map<string, IPKPlugin*>::const_iterator p = plugins.begin();
       p != plugins.end(); p++)
    p->second->initialize(*_sst, *_ist, *_mst);
}

void
PluginManager::check(const string& name, const string& value) {
  if (getPlugins().find(name) == getPlugins().end())
    throw Exception("Attribute '" + name
		    + "' is not managed by any pluging");

  IPKPlugin* aux = getPlugins()[name];
  aux->check(name, value);
}

void
PluginManager::reg(const string& name, IPKPlugin* plug)
{
  getPlugins()[name] = plug;
}

void
PluginManager::unreg(const string& name)
{
  getPlugins().erase(name);
}

map<std::string, IPKPlugin*>&
PluginManager::getPlugins() {
  return (_plugins ? *_plugins : *(_plugins = new map<string, IPKPlugin*>));
}
