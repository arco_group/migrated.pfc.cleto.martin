/* -*- coding:utf-8;mode:c++ -*- */

%{ /*** C/C++ Declarations ***/
  
#include <string>
  
#include "Scanner.hh"

/* import the parser's token type into a local typedef */
typedef IPK::Parser::token token;
typedef IPK::Parser::token_type token_type;

/* By default yylex returns int, we use token_type. Unfortunately yyterminate
 * by default returns 0, which is not of token_type. */
#define yyterminate() return token::END

%}

/*** Flex Declarations and Options ***/

/* enable c++ scanner class generation */
%option c++

/* change the name of the scanner class. results in "ExampleFlexLexer" */
%option prefix="IPK"

/* the manual says "somewhat more optimized" */
%option batch

/* enable scanner to generate debug output. disable this for release
 * versions. */
%option debug

/* no support for include files is planned */
%option yywrap nounput 

/* enables the use of start condition stacks */
%option stack

/* The following paragraph suffices to track locations accurately. Each time
 * yylex is invoked, the begin position is moved onto the end position. */
%{
#define YY_USER_ACTION  yylloc->columns(yyleng);
%}





START_COMMENT     "/*"
START_BLOCK       "{"
END_BLOCK         "}"
START_PAR         "("
END_P             ")"
DOT               "."
SEMICOLON         ";"
EQUAL             "="
COMMA             ","
DOLLAR            "$"

TRUE              "true"  | "True"
FALSE             "false" | "False"

COMMENT           "//"[^\n]*
FIXME             "FIXME"[^\n]*

DIGIT1            [0-9]*
DIGIT2            [1-9]*
VINT              (\+|-)?((0[0-7]+)|(0x[[:xdigit:]]+)|([[:digit:]]+))
VREAL             ({VINT}"."|{VINT}"."{VINT})

OP                ("+"|"-"|"*"|"/")

LITERAL           (\"[^\n"]+\")|(\"\")
ID                \\?[[:alpha:]_][[:alnum:]_]*

%x COMMENT_STATE

%% /*** Regular Expressions Part ***/

 /* code to place at the beginning of yylex() */
%{
  // reset location
  yylloc->step();
%}

%%

// {LITERAL}           {yylval->sval = new std::string(yytext, yyleng);
//                      return token::LITERAL;}

// ("+"|"-"|""){VREAL} {return token::VREAL;}
// ("+"|"-"|""){VINT}  {yylval->sval = new std::string(yytext, yyleng);
//  return token::VINT;}

// "{"     {return token::START_BLOCK;}
// "}"     {return token::END_BLOCK;}
// "("     {return token::START_P;}
// ")"     {return token::END_P;}

// {COMMENT}        {/*Bye, comments!*/}
// {FIXME}          {/*Bye, comments!*/}




// ";" {return token::SEMICOLON;}
// "." {return token::DOT;}
// "," {return token::COMMA;}
// "=" {return token::EQUAL;}

// [ \t\r]+ {
//     yylloc->step();
// }

// \n {
//     yylloc->lines(yyleng); 
//     yylloc->step();
// }

// {ID} {
//     map<string,int>::const_iterator pos = 
//        reservadas.find(new std::string(yytext, yyleng));

//     if(pos != reservadas.end())
//        return pos->second;

//     yylval->sval = new std::string(yytext, yyleng);
//     return token::ID;}


// "\n" {}
// .    {}


%%

void
initScanner()
{
    reservadas["uses"]     =   token::IPK_USES;
    reservadas["datagram"] =   token::IPK_DATAGRAM;
    reservadas["oneway"]   =   token::IPK_ONEWAY;
    reservadas["twoway"]   =   token::IPK_TWOWAY;
    reservadas["boot"]     =   token::IPK_BOOT;
    reservadas["repeat"]   =   token::IPK_REPEAT;
    reservadas["when"]     =   token::IPK_WHEN;
    reservadas["mode"]     =   token::IPK_MODE;
    reservadas["do"]       =   token::IPK_DO;
    reservadas["event"]    =   token::IPK_EVENT;
    reservadas["remote"]   =   token::IPK_REMOTE;
    reservadas["local"]    =   token::IPK_LOCAL;
}


namespace IPK {

Scanner::Scanner(std::istream* in,
		 std::ostream* out)
    : IPKFlexLexer(in, out)
{
}

Scanner::~Scanner()
{
}

void Scanner::set_debug(bool b)
{
    yy_flex_debug = b;
}

}

/* This implementation of ExampleFlexLexer::yylex() is required to fill the
 * vtable of the class ExampleFlexLexer. We define the scanner's main yylex
 * function via YY_DECL to reside in the Scanner class instead. */

#ifdef yylex
#undef yylex
#endif

int IPKFlexLexer::yylex()
{
    std::cerr << "in IPKFlexLexer::yylex() !" << std::endl;
    return 0;
}

/* When the scanner receives an end-of-file indication from YY_INPUT, it then
 * checks the yywrap() function. If yywrap() returns false (zero), then it is
 * assumed that the function has gone ahead and set up `yyin' to point to
 * another input file, and scanning continues. If it returns true (non-zero),
 * then the scanner terminates, returning 0 to its caller. */

int IPKFlexLexer::yywrap()
{
    return 1;
}
