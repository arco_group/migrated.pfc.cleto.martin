// -*- mode: c++ -*-
#ifndef __SYMBOLTABLE_H__
#define __SYMBOLTABLE_H__

#include "SliceVisitor.h"
#include "ipk/Driver.h"
#include "sis/Driver.h"

#include <string>
#include <map>
#include <vector>

class SymbolTableException: public std::runtime_error {
public:
  SymbolTableException(const std::string& msg = ""): std::runtime_error(msg){};
};

namespace Semantic {

  class Signature {
  public:
    Signature();
    Signature(const slc::Operation& op);
    Signature(const sis::Method& op);
    ipk::MethodType getMode() const;
    std::vector<std::pair<std::string, std::string> > getInputParams() const;
    std::vector<std::pair<std::string, std::string> > getOutputParams() const;
    std::map <std::string, std::string> getMetadata() const;

    std::string getName() const;
    std::string getInterface() const;

    void setName(const std::string& name);
    void setInterface(const std::string& iface);

    bool operator==(const Signature& other) const;

  private:
    std::vector<std::pair<std::string, std::string> > _inputParams;
    std::vector<std::pair<std::string, std::string> > _outputParams;
    std::map<std::string, std::string> _metadata;
    ipk::MethodType _mode;
    std::string _name;
    std::string _iface;
  };

  class Object {
  public:
    Object();
    Object(const ipk::Object& object, const std::string& oid);

    std::string getInterface() const;
    std::string getIdentity() const;
    std::string getName() const;

    std::vector<std::string> getAllInterfaces() const;
    ipk::ObjectMode getMode() const;

    std::vector<Signature> getMethods() const;
    std::map<std::string, std::string> getAttributes() const;

    bool operator==(const Object& other) const;

  protected:
    std::string _interface;
    std::string _oid;
    std::string _name;

    std::vector<std::string> _allInterfaces;
    std::map<std::string, std::string> _attr;

    ipk::ObjectMode _mode;

    std::vector<Signature> _methods;

    std::map<std::string, ipk::ObjectMode> _modes;

    void generateMethods();
  };

  class Adapter: public Object{
  public:
    Adapter(const ipk::Adapter& adapter, const ipk::SymbolTable& ist);
    //std::string getName() const;
    std::string getEndpoint() const;
    ipk::AdapterType getType() const;
    std::vector<Object> getObjects() const;

  private:
    ipk::AdapterType _type;
    //std::string _name;
    std::string _endpoint;
    std::vector<Object> _objects;
  };


  class Parameter;

  class Condition {
  public:
    Condition();
    Condition(const ipk::Condition& cond);

    std::vector<Parameter> getParameters() const;
    ipk::OperatorType getType() const;

    bool operator==(const Condition& other) const;
  private:
    std::vector<Parameter> _params;
    ipk::OperatorType _type;
  };

  class Invocation {
  public:
    Invocation();
    Invocation(const ipk::Invocation& invocation);

    Object getObject() const;
    Signature getMethod() const;
    std::vector<Parameter> getParameters() const;

    Condition getCondition() const;

    void setObject(const Object& object);
    void setMethod(const Signature& method);


  private:
    Object _object;
    Signature _method;
    std::vector<Parameter> _paramsValue;
    Condition _cond;

  };

  class Dictionary {
  };

  class Parameter {

    union {
      int ival;
      std::string* sval;
      bool bval;
      Invocation* inv;
      Dictionary* dict;
    };

    ipk::Type _varType;

  public:


    Parameter();
    Parameter(const ipk::Parameter& param);
    int getValueAsInteger() const;
    bool getValueAsBoolean() const;
    std::string getValueAsString() const;
    Invocation getValueAsInvocation() const;
    Dictionary getValueAsDictionary() const;
    ipk::Type getType() const;
  };

  class Trigger {
  public:
    Trigger(const ipk::Trigger& trigger);
    ipk::TriggerType getType() const;
    std::string getLabel() const;
    std::vector<Invocation> getInvocations() const;

  private:
    ipk::TriggerType _type;
    std::string _label;
    std::vector<Invocation> _invocations;
  };

  class Event: public Trigger {
  public:
    Event(ipk::Trigger* event);
    std::string getEvent() const;

  private:
    std::string _event;
  };

  class Timer: public Trigger {
  public:
    Timer(ipk::Trigger* repeat);
    int getTimeout() const;

  private:
    int _timeout;
  };

  struct LocalVariable {
  public:
    std::string varType;
    std::string value;
  };

  class When: public Trigger {
  public:
    When(ipk::Trigger* trigger);
    Invocation getMethod() const;
    std::vector<LocalVariable> getLocalVariables() const;

  private:
    Invocation _invocation;
    std::vector<LocalVariable> _localVariables;
  };

  class Boot: public Trigger {
  public:
    Boot(ipk::Trigger* boot);
  };

  class Function: public Trigger {
  public:
    Function(ipk::Trigger* function);
  };

  class SymbolTable {

  public:

    static SymbolTable
    createSymbolTable(slc::SymbolTable sst,
		      ipk::SymbolTable ist,
		      sis::SymbolTable mst);

    sis::SymbolTable getSIS();

    std::vector<Signature> getMethodsByInterface(const std::string& interface) const;
    ipk::SymbolTable getIcePickSymbolTable() const;
    std::vector<Adapter>  getAdapters() const;
    std::vector<Boot>     getBootTriggers() const;
    std::vector<Function> getFunctionTriggers() const;
    std::vector<Event>    getEventTriggers() const;
    std::vector<Timer>    getTimerTriggers() const;
    std::vector<When>     getWhenTriggers() const;

    std::vector<std::string> getParents(const std::string& interface) const;
    Object getObjectByName(const std::string& name) const;

    sis::Method getPrivateMethod(const std::string& iface,
				 const std::string& method) const;

    std::vector<Object> getLocalObjects() const;
    std::vector<Object> getRemoteObjects() const;

    bool isA(const Object& obj, const std::string& iface);
    bool isParent(const std::string& parent, const std::string& child);

    Semantic::Adapter getAdapterByObject(const std::string& name);

    std::vector<Invocation> getAllInvocations();

    static SymbolTable instance();

  private:

    SymbolTable(slc::SymbolTable sst, ipk::SymbolTable ist,
		sis::SymbolTable mst);

    static SymbolTable* _instance;
    slc::SymbolTable _sst;
    ipk::SymbolTable  _ist;
    sis::SymbolTable  _mst;
    std::vector<Adapter>  _adapters;
    std::vector<Boot>     _boots;
    std::vector<Function> _functions;
    std::vector<Event>    _events;
    std::vector<When>     _whens;
    std::vector<Timer>   _repeats;
    std::vector<Object> _objects;

    //    std::vector<Trigger> _triggers;
  };
}

#endif
