#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys, os

try:
    filename = os.path.join(sys.argv[1], 'ADDRESS.conf')
    fn = open(filename, "r")
    address = int(fn.read()) + 1
    fn.close()

except IOError:
    address = 1

except IndexError:
    print 'Usage: %s dirname' %(sys.argv[0])
    sys.exit(1)

fn = open(filename, "w")
fn.write(str(address))
fn.close()

print address
sys.exit(0)



