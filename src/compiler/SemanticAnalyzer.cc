#include <iostream>

#include <fstream>
#include <vector>
#include <sstream>
#include <string>
#include <algorithm>
#include <easyc++/logger.h>
#include <easyc++/os.h>


#include "SliceVisitor.h"

#include "SliceVisitor.h"
#include "SemanticAnalyzer.h"
#include "config.h"

//using namespace Slice;
using namespace std;
using namespace Semantic;


Analyzer* Analyzer::_instance = 0;

Analyzer
Analyzer::createAnalyzer(slc::SymbolTable sst,
				   ipk::SymbolTable& ist,
				   sis::SymbolTable& mst) {

  if (_instance)
    delete(_instance);

  PluginManager::initialize(sst, ist, mst);
  _instance = new Analyzer(sst, ist, mst);
  return *_instance;
}

Analyzer::Analyzer
Analyzer::getAnalyzer() {
  return *_instance;
}

Analyzer::Analyzer(slc::SymbolTable sst,
		   ipk::SymbolTable& ist,
		   sis::SymbolTable& mst)
  :  _sst(sst), _ist(ist), _mst(mst) {

  _modes["twoway"] = ipk::TWOWAY;
  _modes["oneway"] = ipk::ONEWAY;
  _modes["datagram"] = ipk::DATAGRAM;

  // FIXME - MAL!!! Lo debe hacer el backend
  _mode2str[ipk::TWOWAY] = "-t";
  _mode2str[ipk::ONEWAY] = "-o";
  _mode2str[ipk::DATAGRAM] = "-d";

  // Supported types
  _types.push_back("int");
  _types.push_back("byte");
  _types.push_back("short");
  _types.push_back("long");
  _types.push_back("char");
  _types.push_back("bool");
  _types.push_back("double");
}

void
Analyzer::parse() {
  // //Add global objects
  //   vector<ipk::Object> obs = _ist.getObjects();
  //   for (vector<ipk::Object>::const_iterator it = obs.begin();
  //        it != obs.end(); it++)
  //     addObject(it->getName(), "");


  //Checking manifest
  validateSIS();

  validateObjects();

  //Checking top-level adapters
  vector<ipk::Adapter> adapters = _ist.getAdapters();

  //   if (adapters.size() == 0)
  //     throw Exception("No local adapter declared");


  for (vector<ipk::Adapter>::const_iterator it = adapters.begin();
       it != adapters.end(); ++it)
    validateAdapter(*it);


  //Checking top-level sections
  vector<ipk::Trigger*> sections = _ist.getTriggers();
  for (vector<ipk::Trigger*>::const_iterator it = sections.begin();
       it != sections.end(); ++it) {
    validateTrigger(*it);
  }
}

void
Analyzer::validateSIS() {

  // All public method is in the slice definition
  vector<sis::Method> publics  = _mst.getPublicMethods();
  vector<sis::Method> privates = _mst.getPrivateMethods();

  bool found;
  for (vector<sis::Method>::const_iterator it = publics.begin();
       it != publics.end(); it++) {

    found = false;

    // Get all parents
    vector<string> inh;
    try {
      inh = _sst.getInheritance(it->interface);
    }catch (slc::NotFoundException e) {
      throw e;
    }

    inh.push_back(it->interface);

    slc::Operation aux(it->interface + "::", it->name);
    slc::Operation theOp;

    for (vector<string>::const_iterator i = inh.begin();
	 i != inh.end(); i++) {

      // Check the Interface
      slc::Iface iface;
      try {
	iface = _sst.searchIface(*i);
      } catch (slc::NotFoundException) {
	throw Exception("The method '" + it->name
			+ "' has an invalid interface: "
			+ it->interface);
      }

      // Check the method name
      vector<slc::Operation> ops = iface.getOperations();
      for (vector<slc::Operation>::const_iterator o = ops.begin();
	   o != ops.end(); o++) {
	if (o->getName() == it->name) {
	  found = true;
	  theOp = *o;
	  break;
	}
      }
    }

    if (not found)
      throw Exception("The method '" + it->name
		      + "' is not declared in '" + it->interface
		      + "' interface");

    // If the public method has not description it's assumed that
    // the slice definition is the same
    if (it->input.size() == 0 and it->output.size() == 0) {
      return;
    }


    // Cheking the params
    vector<slc::Param> theInputParams  = theOp.getInputParameters();
    vector<slc::Param> theOutputParams = theOp.getOutputParameters();

    if (theInputParams.size() != it->input.size())
      throw Exception("The method '" + it->interface
		      + "::" + it->name
		      + "' has '" + int2str(it->input.size())
		      + "' input parameters. Expected '"
		      + int2str(theInputParams.size())
		      + "' exactly");

    if (theOutputParams.size() != it->output.size())
      throw Exception("The method '" + it->interface
		      + "::" + it->name
		      + "' has '" + int2str(it->output.size())
		      + "' output parameters. Expected '"
		      + int2str(theOutputParams.size())
		      + "' exactly");


    for (unsigned int i = 0; i < it->input.size(); i++) {
      slc::Param p = theInputParams.at(i);
      vector<string>::const_iterator t = find(_types.begin(),
					      _types.end(),
					      ec::string::lower(p.getType()));

      // If the type of the input param is ok -> pass
      if (t != _types.end())
	continue;


      // The type can be a private method

      bool found = false;
      for (vector<sis::Method>::const_iterator privIt = privates.begin();
	   privIt != privates.end(); privIt++) {
	if (it->input.at(i) == privIt->name) {
	  found = true;
	  break;
	}
      }

      if (found)
	continue;

      vector<string> parts = ec::string::split(it->input.at(i), ":");
      if (parts.size() != 2)
	// The type is invalid and identifier is not a private method.
	// Then, fails.
	throw Exception("Argument '" + it->input.at(i)
			+ "' has a incompatible type. Required '"
			+ p.getType() + "' value or a private method");
    }
  }

  for (vector<sis::Method>::const_iterator it = privates.begin();
       it != privates.end(); it++) {

    found = false;

    // Get all parents
    vector<string> inh;
    try {
      inh = _sst.getInheritance(it->interface);
    }catch (slc::NotFoundException e) {
      throw e;
    }
  }
}

void
Analyzer::validateObjects() {
  vector<ipk::Object> objs = _ist.getObjects();

  if (objs.size() == 0)
    throw Exception("No objects declared");

  // Check interfaces
  for (vector<ipk::Object>::const_iterator it = objs.begin();
       it != objs.end(); it++) {

    string iface = it->getModule() + "::" + it->getIface();
    try {
      _sst.searchIface(iface);
    }catch (Exception e) {
      throw Exception(it->getFilename(),
			      it->getLine(),
			      "Interface '" + iface + "' does not exist");
    }
  }

  // Check duplicates
  for (vector<ipk::Object>::const_iterator it = objs.begin();
       it != objs.end(); it++) {
    try {
      addObject(it->getName(), "");
    } catch(Exception e) {
      throw Exception(it->getFilename(),
			      it->getLine(),
			      "Duplicated object name: '" + it->getName()
			      + "'");
    }
  }

  // Check for attributes
  for (vector<ipk::Object>::iterator it = objs.begin();
       it != objs.end(); it++) {

    AttributeList attrs = it->getAttributes();

    if (attrs.size() == 0) {
      assert(it->getMode() == ipk::TWOWAY);
      continue;
    }

    for (AttributeList::const_iterator a=attrs.begin(); a!=attrs.end(); a++) {
      if (a->first == "mode") {
	if (a->second.type != ipk::identifier)
	  throw Exception(a->second.getFilename(),
				  a->second.getLine(),
				  "Invalid type data of 'mode' attribute");

	if (_modes.find(*(a->second.value.sval)) == _modes.end())
	  throw Exception(a->second.getFilename(),
				  a->second.getLine(),
				  "Invalid value '" + *(a->second.value.sval)
				  + "' data of 'mode' attribute");
      }
    }
  }
}

void
Analyzer::validateAdapter(const ipk::Adapter& adapter) {

  //Adapter type must be 'remote' or 'local'
  if (adapter.getType() != ipk::LOCAL &&
      adapter.getType() != ipk::REMOTE) {
    throw Exception(adapter.getFilename(),
		    adapter.getLine(),
		    "Invalid adapter type for '" + adapter.getName()
		    + "'\n. Use 'remote' or 'local' type.");
  }

  AttributeList attrs = adapter.getAttributes();
  bool epDeclared = false;
  bool objDeclared = false;

  for (AttributeList::const_iterator it = attrs.begin();
       it != attrs.end(); it++) {
    if (it->first == "endpoint") {
      epDeclared = true;
      if (it->second.type != ipk::literal)
	throw Exception(it->second.getFilename(),
			it->second.getLine(),
			"Invalid type value for 'endpoint' property");

      if (*(it->second.value.sval) == "") {
	throw Exception(it->second.getFilename(),
			it->second.getLine(),
			string("Empty endpoint for '") +
			adapter.getName() + "' adapter. Needed protocol at least.");
      }
      continue;
    }

    if (it->first == "objects") {
      objDeclared = true;
      if (it->second.type != ipk::dictionary)
	throw  Exception(it->second.getFilename(),
			 it->second.getLine(),
			 "Invalid type value of 'objects' property");

      AttributeList list = *(it->second.value.dict);

      for (AttributeList::const_iterator l = list.begin();
	   l != list.end(); l++) {
	if (l->second.type != ipk::identifier)
	  throw Exception(it->second.getFilename(),
			  it->second.getLine(),
			  "Invalid value in 'objects' dictionary. Missing object name");

	addObject("", l->first);

	try {
	  ipk::Object obj = _ist.getObjectByName(*(l->second.value.sval));
	  obj.setIdentity(l->first);
	} catch (ipk::ObjectNotFoundException e) {
	  throw Exception(it->second.getFilename(),
			  it->second.getLine(),
			  "Object '" + *(l->second.value.sval)
			  + "' not declared");
	}
      }
      continue;
    }

    throw Exception(it->second.getFilename(),
		    it->second.getLine(),
		    "Attribute '" + it->first + "' not recognized");
  }

  if (not epDeclared)
    throw Exception(adapter.getFilename(),
		    adapter.getLine(),
		    "Adapter definition without 'endpoint' attribute");

  if (not objDeclared)
    throw Exception(adapter.getFilename(),
		    adapter.getLine(),
		    "Adapter definition without 'objects' attribute");

}


void
Analyzer::validateTrigger(ipk::Trigger* section) {

  ec::debug() << "Validating '"
	      << section->getType()
	      << "' section" << endl;

  try {
    addObject(section->getName(), section->getIdentity());
  } catch(Exception e) {
    throw Exception(section->getFilename(),
			    section->getLine(),
			    "Name conflict with label '" + section->getName()
			    + "'");
  }
  //  string label = section->getName();

  //   if (label != "") {

  //     vector<string>::iterator it;

  //     it = find(_labels.begin(), _labels.end(), label);
  //     if (it != _labels.end())
  //       throw Exception("Trigger label '"
  // 				  + label + "' used by other trigger");

  //     it = find(_obs.begin(), _obs.end(), label);
  //     if (it == _obs.end())
  //       throw Exception("Trigger label '"
  // 			      + label + "' has not been registered"
  // 			      + " as global object");

  //     _labels.push_back(label);
  //   }

  if (section->getType() == ipk::TRIGGER_WHEN)
    validateWhenInvocation(dynamic_cast<ipk::When*>(section));
  else if (section->getType() == ipk::TRIGGER_TIMER)
    validateTimer(dynamic_cast<ipk::Timer*>(section));
  else if (section->getType() == ipk::TRIGGER_BOOT and section->getName() != "")
    throw Exception(section->getFilename(),
			    section->getLine(),
			    "Boot trigger can not be labeled");
  else if (section->getType() == ipk::TRIGGER_EVENT)
    validateEvent(dynamic_cast<ipk::Event*>(section));


  vector<ipk::Invocation> invocations = section->getInvocations();

  for (vector<ipk::Invocation>::const_iterator inv = invocations.begin();
       inv != invocations.end(); inv++) {
    validateInvocation(*inv, section, true);
  }
}

void
Analyzer::validateEvent(const ipk::Event* event) {
  string e = event->getEvent();
  bool found = false;
  vector<sis::Event> events = _mst.getEvents();
  for (vector<sis::Event>::const_iterator it = events.begin();
       it != events.end(); it++) {
    if (it->name == e) {
      found = true;
      break;
    }
  }

  if (not found)
    throw Exception(event->getFilename(),
			    event->getLine(),
			    "Event '" + e + "' not declared at SIS file");
}

void
Analyzer::validateTimer(const ipk::Timer* timer) {
  if (timer->getTimeout() <= 0)
    throw Exception(timer->getFilename(),
			    timer->getLine(),
			    "Timer block with invalid timeout");
}

void
Analyzer::validateWhenInvocation(const ipk::When* when) {
  ipk::Invocation inv = when->getMethod();
  string name = inv.getObject();
  string method = inv.getMethod();

  ec::debug() << "Validating invocation '"
	      << name << "." << method << "' of a when trigger"
	      << endl;

  //Is an object
  ipk::Object ob;
  ob = _ist.getObjectByName(name);


  //Checking if the object implements that method in any its interfaces
  vector<slc::Iface> ifaces;
  ifaces.push_back(_sst.searchIface(ob.getModule() + "::" + ob.getIface()));
  //  vector<string> inh = ifaces[0].inheritance(); //Get all ifaces

  vector<string> inh = _sst.getInheritance(ifaces[0].scope()
					   + ifaces[0].name()); //Get all ifaces

  for (vector<string>::const_iterator j=inh.begin();
       j!=inh.end(); j++) {
    if (ob.getModule() + "::" + ob.getIface() != *j) {
      slc::Iface iface = _sst.searchIface(*j);
      ifaces.push_back(iface);
    }
  }

  bool found = false;
  slc::Operation op;

  //For each iface, look for the method
  for (vector<slc::Iface>::const_iterator j=ifaces.begin();
       j != ifaces.end(); j++) {
    vector<slc::Operation> ops = j->getOperations();

    for (vector<slc::Operation>::const_iterator k=ops.begin();
	 k != ops.end(); k++) {
      if (k->getName() == method) {
	op = *k;
	found = true;
	break;
      }
    }
  }

  if (not found)
    throw Exception("Object '" + name
			    + "' has not '" + method
			    + "' as public method.");


  //Checking if the params of the invocations are right
  vector<ipk::Parameter> givenParams  = inv.getParameters();
  if (givenParams.size() == 0)
    return;

  vector<slc::Param> expectedParams  = op.getInputParameters();

  // More parameters given than expected
  if (givenParams.size() > expectedParams.size())
    throw Exception("When invocation malformed: '" + method
			    + "' of object '" + name
			    + "' takes exactly "
			    + int2str(expectedParams.size())
			    + " arguments ("
			    + int2str(givenParams.size()) + " given)");

  // Same number of both parameters
  if (expectedParams.size() == givenParams.size()) {
    for (unsigned int i = 0; i<givenParams.size(); i++) {
      if (givenParams[i].type != ipk::identifier)
	throw Exception("When invocation malformed: parameter '"
				+ int2str(i) + "' is not an identifier.");

      string value = *(givenParams[i].value.sval);

      if (expectedParams[i].getName() != value)
	throw Exception("When invocation malformed: parameter '"
				+ value
				+ "' is not valid. Expected '"
				+ expectedParams[i].getName() + "'");
    }
    return;
  }


  // Less parameters given than expected
  for (unsigned int i = 0; i<givenParams.size(); i++) {
    if (givenParams[i].type != ipk::identifier)
      throw Exception("When invocation malformed: parameter '"
  			      + int2str(i) + "' is not an identifier.");

    found = false;

    for (unsigned int j = 0; j<expectedParams.size(); j++)
      if (expectedParams[j].getName() == *(givenParams[i].value.sval)) {
	found = true;
	break;
      }

    if (not found)
      throw Exception("When invocation malformed: parameter '"
			      + *(givenParams[i].value.sval)
			      + "' not incluyed at method's signature");
  }
}

void
Analyzer::validateInvocation(const ipk::Invocation& inv,
				   ipk::Trigger* trigger,
				   bool topLevel)
{
  string name = inv.getObject();
  string method = inv.getMethod();

  ec::debug() << "Validating invocation '"
	      << name << "." << method << "'"
	      << endl;

  //Is a valid object?
  ipk::Object ob;
  try {
    ob = _ist.getObjectByName(name);
  } catch(ipk::ObjectNotFoundException e) {
    throw Exception(inv.getFilename(),
			    inv.getLine(),
			    "Object '" + name + "' has not been declared");
  }


  //Checking if the object implements that method in any its interfaces
  vector<slc::Iface> ifaces;
  ifaces.push_back(_sst.searchIface(ob.getModule() + "::" + ob.getIface()));
  //  vector<string> inh = ifaces[0].inheritance(); //Get all ifaces

  vector<string> inh = _sst.getInheritance(ifaces[0].scope()
					   + ifaces[0].name()); //Get all ifaces

  for (vector<string>::const_iterator j=inh.begin();
       j!=inh.end(); j++) {
    if (ob.getModule() + "::" + ob.getIface() != *j) {
      slc::Iface iface = _sst.searchIface(*j);
      ifaces.push_back(iface);
    }
  }

  bool found = false;
  slc::Operation op;

  //For each iface, look for the method
  for (vector<slc::Iface>::const_iterator j=ifaces.begin();
       j != ifaces.end(); j++) {
    vector<slc::Operation> ops = j->getOperations();

    for (vector<slc::Operation>::const_iterator k=ops.begin(); k != ops.end(); k++) {
      if (k->getName() == method) {
	op = *k;
	found = true;
	break;
      }
    }

    if (found)
      break;
  }


  if (found == true) {
    // -* Slice interface *-
    //Checking if the params of the invocations are right
    vector<slc::Param> in  = op.getInputParameters();
    vector<slc::Param> out = op.getOutputParameters();
    vector<ipk::Parameter> params  = inv.getParameters();

    if (in.size() != params.size())
      throw Exception(inv.getFilename(),
			      inv.getLine(),
			      "'" + method
			      + "' of object '" + name
			      + "' takes exactly " + int2str(in.size())
			      + " arguments ("
			      + int2str(params.size()) + " given)");

    //It's not possible to invoke a method with output params
    if (out.size() > 1 and topLevel)
      ec::warn() << "The output params of invocation '" << method
		 << "." << name << "' will be ignored"  << endl;

    if (out[0].getType() != "void" and topLevel)
      ec::warn() << "The output params of invocation '" << method
		 << "." << name << "' will be ignored"  << endl;

    //Replace values for parametres ".oid"
    replaceParamValues(&params);

    //Check the parameters
    for (unsigned int j=0; j<in.size(); j++) {
      string expected = in[j].getType();
      checkParamTypes(params[j], trigger, expected);
    }
    validateCondition(inv.getCondition());
    return;
  }

  ec::debug() << "Searching at SIS file to locate '"
	      << method << "' as private method"
	      << endl;

  vector<sis::Method> privates = _mst.getPrivateMethods();
  vector<sis::Method>::const_iterator it;

  string iface = ob.getModule() + "::" + ob.getIface();

  //if (iface[0] == '_')
  //  iface = iface.substr(1, iface.size());

  found = false;

  for(it = privates.begin(); it != privates.end(); it++) {
    if (iface != it->interface or method != it->name)
      continue;

    // Check parameters
    vector<ipk::Parameter> params  = inv.getParameters();
    vector<string> types = it->input;

    if (types.size() != params.size())
      throw Exception("'" + method
		      + "' of object '" + name
		      + "' takes exactly " + int2str(types.size())
		      + " arguments ("
		      + int2str(params.size()) + " given)");

    for (unsigned int j=0; j<types.size(); j++)
      checkParamTypes(params[j], trigger, types[j]);

    found = true;
    break;
  }

  if (not found)
    throw Exception("Object '" + name
			    + "' has not '" + method
			    + "' method.");

  // Check the condition if exists
  validateCondition(inv.getCondition());


}

void
Analyzer::validateCondition(const ipk::Condition& cond) {

  if (cond.getParameters().size() == 0)
    return;

  vector<ipk::Type> allowed;
  allowed.push_back(ipk::numeric);
  allowed.push_back(ipk::boolean);
  allowed.push_back(ipk::invocation);
  allowed.push_back(ipk::identifier);


  vector<ipk::Parameter> pars = cond.getParameters();

  for (vector<ipk::Parameter>::const_iterator p=pars.begin(); p!=pars.end(); p++) {
    bool ok = false;

    if (p->type == ipk::identifier)
      if (isObjRegistered(*(p->value.sval)))
	throw Exception(cond.getFilename(),
			cond.getLine(),
			string("Object identifier is not valid as condition operand: '")+
			*(p->value.sval) + "'");


    for (vector<ipk::Type>::const_iterator t=allowed.begin(); t!=allowed.end(); t++)
      if (p->type == *t) {
	ok = true;
	break;
      }

    if (not ok)
      throw Exception(cond.getFilename(),
		      cond.getLine(),
		      string("Only numeric, when-variable, boolean or ") +
		      "invocation are allowed as condition operand");
  }

}

void
Analyzer::addObject(const string& name, const string& oid) {

  if (name != "") {
    if (find(_obs.begin(), _obs.end(), name) != _obs.end()) {
      throw Exception("Double definition of '"
		      + name + "' object");
    }
    _obs.push_back(name);
  }

  if (oid != "") {
    if (find(_oids.begin(), _oids.end(), oid) != _oids.end())
      throw Exception("Double use of '" + oid + "' identity");

    _oids.push_back(oid);
  }
}


bool
Analyzer::isObjRegistered(const string& name) {
  for (unsigned int i = 0; i<_obs.size(); i++) {
    if (name == _obs[i])
      return true;
  }

  return false;
}


bool
Analyzer::isOidRegistered(const string& oid) {
  for (unsigned int i = 0; i<_oids.size(); i++) {
    if (oid == _oids[i])
      return true;
  }

  return false;
}

void
Analyzer::checkParamTypes(const ipk::Parameter& p,
				ipk::Trigger* trigger,
				const string& type) {

  ipk::Type t = p.type;
  string v;

  if (t == ipk::boolean) {

    if (type == "bool")
      return;

    bool val = p.value.bval;
    v = (val)?("true"):("false");
  }

  if (t == ipk::identifier) {

    if (type == "string" ||
	type.find("Prx") != string::npos)
      return;

    if (trigger->getType() == ipk::TRIGGER_WHEN){
      ec::debug() << "WARNING - No se hace comprobación de los tipos"
		  << " en la invocacion del when" << endl;

      //       ipk::When* w = dynamic_cast<ipk::When*>(trigger);
      //       vector<slc::Param> inv = w->getMethod().getInputParameters();
      //       for (
      return;
    }

    v = *p.value.sval;
  }

  if (t == ipk::literal) {
    if (type == "string" ||
	type.find("Ice::Identity") != string::npos ||
	type.find("Prx") != string::npos)
      return;

    v = *p.value.sval;
  }

  if (t == ipk::attribute) {
    vector<string> parts = ec::string::split(*p.value.sval, ".");
    if (isObjRegistered(parts[0])) {
      if (parts[1] == "oid" and type == "const Ice::Identity")
	return;
    }
    v = *p.value.sval;
  }


  if (t == ipk::numeric) {
    int val = p.value.ival;
    if (type == "Int" || type == "Byte" || type == "Short") {
      return;
    }

    v = int2str(val);
  }

  if (t == ipk::invocation) {
    ipk::Invocation val = *p.value.inv;
    validateInvocation(val, trigger, false);

    ipk::Object ob = _ist.getObjectByName(val.getObject());
    slc::Iface iface = _sst.searchIface(ob.getModule() + "::" + ob.getIface());

    vector<string> ifaces = iface.inheritance();
    ifaces.push_back(ob.getModule() + "::" + ob.getIface());
    for (vector<string>::const_iterator it=ifaces.begin(); it!=ifaces.end(); it++) {

      slc::Iface i = _sst.searchIface(*it);
      vector<slc::Operation> ops = i.getOperations();

      for (vector<slc::Operation>::const_iterator op=ops.begin(); op!=ops.end(); op++) {
	if (op->getName() == val.getMethod()) {
	  if (op->getOutputParameters().size() == 1) {
	    slc::Param p = op->getOutputParameters()[0];
	    if (p.getType() == type)
	      return;
	  }
	}
      }
    }

    // Maybe using a local method
    vector<sis::Method> privates = _mst.getPrivateMethods();
    for (vector<sis::Method>::const_iterator it=privates.begin(); it!= privates.end();
	 it ++) {
      if (find(ifaces.begin(), ifaces.end(), it->interface) == ifaces.end())
	continue;

      if (it->name != val.getMethod())
	continue;

      if (it->output.size() != 1)
	continue;

      if (it->output[0] == type)
	return;
    }

    // FIXME
    if (type == "string")
      return;


    v = val.getObject() + "." + val.getMethod() + "()";
  }
  if (t == ipk::dictionary) {
    if (type == "const ::IceStorm::QoS" ||
	type == "::IceStorm::QoS")
      return;
  }

  throw Exception("Invalid type of argument '"
		  + v +"'. Expected '" + type +"'");
}

void
Analyzer::replaceParamValues(vector<ipk::Parameter>* params)
{
  vector<ipk::Parameter>::iterator it;
  for(it = params->begin(); it != params->end(); it++) {


    if (it->type != ipk::attribute)
      continue;


    string attr = *(it->value.sval);
    size_t pos = attr.find(".oid");
    if (pos != string::npos) {
      ipk::Object obj;
      string objName = attr.substr(0, pos);

      try {
	obj = _ist.getObjectByName(objName);
      }
      catch (ipk::ObjectNotFoundException e) {
	throw Exception("The object '" + objName
				+ "' is used but not declared");
      }

      vector<ipk::Adapter> adapters = _ist.getAdapters();
      for (vector<ipk::Adapter>::const_iterator ad = adapters.begin();
	   ad != adapters.end(); ad++) {

	AttributeList attrs = ad->getAttributes();
	for (AttributeList::const_iterator at = attrs.begin();
	     at != attrs.end(); at++) {

	  if (at->first != "objects")
	    continue;

	  AttributeList list = *(at->second.value.dict);
	  for (AttributeList::const_iterator l = list.begin();
	       l != list.end(); l++) {
	    if (*(l->second.value.sval) != objName)
	      continue;

	    string msg = "Changed '" + objName + "'";
	    *(it->value.sval) = "\"" + l->first + "\"";
	    it->type = ipk::literal;
	    ec::debug() << msg << " to '" << *(it->value.sval) << "'" << endl;
	    return;
	  }
	}
      }
    }
  }
}

    // El proxy no es bueno reemplazarlo porque necesito saber qué
    // proxy objeto de objeto está como parámetro. Que lo reemplace el backend.

//     else if (it->type == ipk::identifier) {
//       ipk::Object obj;
//       ipk::Adapter adp;

//       string ident = *(it->value.sval);


//       try {
// 	adp = _ist.getAdapterByObject(ident);
// 	obj = _ist.getObjectByName(ident);
// 	}catch(ipk::ObjectNotFoundException e) {
// 	  throw Exception("The object '" + ident
// 				  + "' is used but not declared");
//       }

//       *(it->value.sval) = "\"" + obj.getIdentity() + " "
// 	+ _mode2str[obj.getMode()]
// 	+ ":" + adp.getEndpoint() + "\"";

//       string msg = "Changed '" + ident + "'";
//       ec::debug() << msg << " to '" << *(it->value.sval) << "'" << endl;
//       it->type = ipk::literal;

//     }

// void
// Analyzer::populateObject(ipk::Object& ob, ::SymbolTable st) {

//   slc::Iface iface = _sst.searchIface(ob.getModule() + "::" + ob.getIface());

//   vector<string> a = iface.inheritance();
//   vector<string>::iterator it;

//   for (it = a.begin(); it!=a.end(); it++)
//     ob.setIfaceParents(iface.inheritance());
// }


slc::SymbolTable
Analyzer::getSliceSymbolTable() {
  return _instance->_sst;
}

ipk::SymbolTable
Analyzer::getIPKSymbolTable() {
  return _instance->_ist;
}

sis::SymbolTable
Analyzer::getSISSymbolTable() {
  return _instance->_mst;

}


