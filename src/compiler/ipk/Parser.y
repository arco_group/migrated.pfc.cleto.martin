/* -*- C++ -*- */

%code top {
#include "Driver.h"
}

%code requires {
#include "Driver.h"
}

%skeleton "lalr1.cc"
%require "2.4"
%debug

%defines
%define "parser_class_name" "Parser"
%define "namespace" "ipk"

// The parsing context.
%parse-param { class Driver& driver }

%locations
%initial-action
{
  // Initialize the initial location.
  @$.begin.filename = @$.end.filename = &driver.file;
};


%debug
%error-verbose
%name-prefix="ipk"

// Symbols.
%union
{
  int          ival;
  std::string* sval;
  bool         bval;
  Invocation* inv;
  Parameter* par;
  std::vector<Parameter>* vpar;
  Trigger* bck;
  Condition* cnd;
  OperatorType* op;
  Attribute* attr;
  std::pair<std::string, Attribute>* pattr;
  std::vector<std::pair<std::string, Attribute> >* vect;
  AdapterType adapt;
};

%type <inv> invocation

%type <op>  comp
%type <par> parameter
%type <vpar> parameters
%type <sval> label
%type <sval> key
%type <sval> interface
%type <attr> val
%type <bck> when_trigger
%type <bck> boot_trigger
%type <bck> function_trigger
%type <cnd> condition
%type <bck> event_trigger
%type <bck> timer_trigger
%type <bck> single_trigger
%type <bck> labeled_trigger
%type <vect> dict
%type <vect> keyval
%type <vect> attributes
%type <pattr> attr
%type <adapt> adapter_type

%token COLON     "':'"
%token SEMICOLON "';'"
%token DOT       "'.'"
%token COMMA     "','"
%token EQUAL     "'='"
%token START_BLOCK  "'{'"
%token END_BLOCK    "'}'"
%token START_P      "'('"
%token END_P        "')'"

%token EQ       "'=='"
%token NEQ      "'!='"
%token GT       "'>'"
%token LT       "'<'"
%token GTEQ     "'>='"
%token LTEQ     "'<='"

%token IF       "'if'"

%token <sval> LITERAL  "LITERAL"
%token <ival> VINT     "INTVALUE"
%token <sval> ID       "IDENTIFIER"
%token <bval> TRUE
%token <bval> FALSE

%token IPK_USES        "USES"
%token IPK_BOOT        "BOOT"
%token IPK_TIMER       "TIMER"
%token IPK_WHEN        "WHEN"
%token IPK_DO          "DO"
%token IPK_EVENT       "EVENT"
%token IPK_REMOTE      "REMOTE"
%token IPK_LOCAL       "LOCAL"
%token IPK_FUNCTION    "FUNCTION"
%token IPK_OBJECT      "OBJECT"
%token IPK_ADAPTER     "ADAPTER"

%token END 0 "end of file"

%start program

%{

#include "Driver.h"
#include "Scanner.hh"
#include <easyc++/string.h>

  /* this "connects" the bison parser in the driver to the flex scanner class
   * object. it defines the yylex() function call to pull the next token from the
   * current lexer object of the driver context. */
#undef yylex
#define yylex driver.lexer->lex

  using namespace std;
  using namespace ipk;

  Adapter currentAdapter;
  Object  currentObject;
  vector<Invocation> currentInvocations;
  vector<Parameter> currentParameters;
  Invocation* currentInvocation;
  string currentModule;
  string currentIface;
%}

%%

program
: uses declarations triggers END {}
| uses declarations END {}
| declarations triggers END {}
| declarations END {}
;

declarations: declaration {}
| declaration declarations {}
;

declaration: object_decl SEMICOLON
| adapter_decl SEMICOLON
;

uses
: use
| uses use
;

use
: IPK_USES LITERAL SEMICOLON
{
  string use = *$2;
  use = use.substr(1, use.size()-2);
  driver.visitor->visitUse(use);
}
;

object_decl
: IPK_OBJECT interface ID {
  Object obj;
  string lex = *$2;
  size_t npos = lex.rfind(':');
  string iface = lex.substr(npos+1, lex.size()-2 );
  string module = lex.substr(0, npos-1);

  obj.setModule(module);
  obj.setIface(iface);
  obj.setName(*$3);
  obj.setMode(TWOWAY);

  obj.setLine(@1.begin.line);
  obj.setFilename(*(@1.begin.filename));
  driver.visitor->visitObject(obj);

  delete($2);

}
| IPK_OBJECT interface ID START_BLOCK attributes END_BLOCK
{
  Object obj;
  string lex = *$2;
  size_t npos = lex.rfind(':');
  string iface = lex.substr(npos+1, lex.size()-2 );
  string module = lex.substr(0, npos-1);

  obj.setModule(module);
  obj.setIface(iface);
  obj.setName(*$3);
  obj.setAttributes(*$5);

  obj.setLine(@1.begin.line);
  obj.setFilename(*(@1.begin.filename));
  driver.visitor->visitObject(obj);

  delete($2);
}
;

interface
: ID DOT ID
{
  $$ = new string("::" + *$1 + "::" + *$3);
}
| interface DOT ID
{
  $$ = new string(*$1 + "::" + *$3);
}
;

attributes
: attr SEMICOLON
{
  $$ = new vector<pair<string, Attribute> >();
  $$->push_back(*$1);
}
| attributes attr SEMICOLON
{
  $1->push_back(*$2);
  $$ = $1;
}

;

attr
: ID EQUAL ID
{
  Attribute a;
  a.type = identifier;
  a.value.sval = $3;
  a.setLine(@1.begin.line);
  a.setFilename(*(@1.begin.filename));

  $$ = new pair<string, Attribute>(*$1, a);
}
| ID EQUAL LITERAL {

  Attribute a;
  a.type = literal;
  a.value.sval = new string($3->substr(1, $3->size()-2));
  a.setLine(@1.begin.line);
  a.setFilename(*(@1.begin.filename));


  $$ = new pair<string, Attribute>(*$1, a);
 }
| ID EQUAL dict {

  Attribute a;
  a.type = dictionary;
  a.value.dict = $3;
  a.setLine(@1.begin.line);
  a.setFilename(*(@1.begin.filename));

  $$ = new pair<string, Attribute>(*$1, a);
 }
;

dict
: START_BLOCK keyval END_BLOCK
{
  $$ = $2;
}
| START_BLOCK END_BLOCK
{
  $$ = new AttributeList();
}
;

keyval
: key COLON val
{
  $$ = new AttributeList();
  $$->push_back(pair<string, Attribute>(*$1, *$3));
}
| keyval COMMA key COLON val
{
  $1->push_back(pair<string, Attribute>(*$3, *$5));
  $$ = $1;
}
;

key
: LITERAL {$$ = new string($1->substr(1, $1->size()-2));}
| ID {$$ = $1;}
;

val
: LITERAL
{
  $$ = new Attribute();
  $$->type = literal;
  $$->value.sval = new string($1->substr(1, $1->size()-2));
}
| ID
{
  $$ = new Attribute();
  $$->type = identifier;
  $$->value.sval = $1;
}
| VINT
{
  $$ = new Attribute();
  $$->type = numeric;
  $$->value.ival = $1;
}
;

adapter_decl
: adapter_type IPK_ADAPTER ID START_BLOCK attributes END_BLOCK
{
  Adapter adapt;
  adapt.setType($1);
  adapt.setName(*$3);
  adapt.setAttributes(*$5);

  adapt.setLine(@1.begin.line);
  adapt.setFilename(*(@1.begin.filename));

  driver.visitor->visitAdapter(adapt);
}
;

adapter_type
: IPK_LOCAL
{
  $$ = LOCAL;
}
| IPK_REMOTE
{
  $$ = REMOTE;
}
;

//****************

triggers: trigger
| triggers trigger
;

trigger: single_trigger {
  driver.visitor->visitTrigger($1);
}
| labeled_trigger {
  driver.visitor->visitTrigger($1);
  }
;


labeled_trigger: label single_trigger {
  Trigger* b = $2;
  b->setName(*$1);
  b->setLine(@1.begin.line);
  b->setFilename(*(@1.begin.filename));
  $$ = b;
 }
;

label: ID COLON {
  $$ = $1;
}
;

single_trigger: when_trigger
{
  $$ = $1;
  $$->setLine(@1.begin.line);
  $$->setFilename(*(@1.begin.filename));
}
| boot_trigger
{
  $$ = $1;
  $$->setLine(@1.begin.line);
  $$->setFilename(*(@1.begin.filename));
}
| function_trigger
{
  $$ = $1;
  $$->setLine(@1.begin.line);
  $$->setFilename(*(@1.begin.filename));
}
| timer_trigger
{
  $$ = $1;
  $$->setLine(@1.begin.line);
  $$->setFilename(*(@1.begin.filename));
}
| event_trigger
{
  $$ = $1;
  $$->setLine(@1.begin.line);
  $$->setFilename(*(@1.begin.filename));
}
;


when_trigger
: IPK_WHEN invocation IPK_DO START_BLOCK invocations END_BLOCK
{
  When* w = new When;
  w->setMethod(*$2);
  w->setInvocations(currentInvocations);
  currentInvocations.clear();
  $$ = w;
}
| IPK_WHEN ID DOT ID IPK_DO START_BLOCK invocations END_BLOCK
{
  When* w = new When;
  w->setMethod(Invocation(*$2, *$4));
  w->setInvocations(currentInvocations);
  currentInvocations.clear();
  $$ = w;
}
;

boot_trigger: IPK_BOOT START_BLOCK invocations END_BLOCK
{
  Boot* section = new Boot;
  section->setInvocations(currentInvocations);
  currentInvocations.clear();
  $$ = section;
}
;

function_trigger: IPK_FUNCTION START_BLOCK invocations END_BLOCK
{
  Function* section = new Function;
  section->setInvocations(currentInvocations);
  currentInvocations.clear();
  $$ = section;
}
;

timer_trigger: IPK_TIMER START_P VINT END_P START_BLOCK invocations END_BLOCK
{
  Timer* section = new Timer;
  section->setTimeout($3);
  section->setInvocations(currentInvocations);
  currentInvocations.clear();
  $$ = section;
}
;

event_trigger: IPK_EVENT ID IPK_DO START_BLOCK invocations END_BLOCK
{
  Event* section = new Event;
  section->setEvent(*$2);

  section->setInvocations(currentInvocations);
  currentInvocations.clear();
  $$ = section;
}
;

invocations
: invocation SEMICOLON
{
  $1->setLine(@1.begin.line);
  $1->setFilename(*(@1.begin.filename));

  currentInvocations.push_back(*$1);
}
| invocations invocation SEMICOLON
{
  $2->setLine(@1.begin.line);
  $2->setFilename(*(@1.begin.filename));

  currentInvocations.push_back(*$2);
}
;

invocation
: ID DOT ID START_P parameters END_P
{
  $$ = new Invocation(*$1, *$3, *$5);
}
| ID DOT ID START_P END_P
{
  $$ = new ipk::Invocation(*$1, *$3);
}
| invocation IF START_P condition END_P
{
  $1->setCondition(*$4);
  $$ = $1;
}
;

condition
: parameter comp parameter
{
  Condition *cond = new Condition();
  cond->setLine(@1.begin.line);
  cond->setFilename(*(@1.begin.filename));

  vector<Parameter> p;
  p.push_back(*$1);
  p.push_back(*$3);
  cond->setParameters(p);
  cond->setType(*$2);
  $$ = cond;
}
;

// FIXME - Considerar que puede haber condiciones
comp
: EQ
{
  $$ = new OperatorType(OP_EQ);
}
| NEQ
{
  $$ = new OperatorType(OP_NEQ);
}
| GT
{
  $$ = new OperatorType(OP_GT);
}
| LT
{
  $$ = new OperatorType(OP_LT);
}
| GTEQ
{
  $$ = new OperatorType(OP_GTEQ);
}
| LTEQ
{
  $$ = new OperatorType(OP_LTEQ);
}
;


parameters
: parameter
{
  $$ = new std::vector<Parameter>();
  $$->push_back(*$1);
}
| parameters COMMA parameter
{
  $$ = $1;
  $$->push_back(*$3);
}
;

parameter
: TRUE
{
  Parameter* p = new Parameter();;
  p->type = boolean;
  p->value.bval = true;
  $$ = p;
}
| FALSE
{
  Parameter* p = new Parameter();;
  p->type = boolean;
  p->value.bval = false;
  $$ = p;
}
| ID DOT ID
{
  Parameter* p = new Parameter();
  p->type = attribute;
  p->value.sval = new std::string(*$1 + "." + *$3);
  $$ = p;
}
| ID
{
  Parameter* p = new Parameter();
  p->type = identifier;
  p->value.sval = new std::string(*$1);
  $$ = p;
}
| LITERAL
{
  Parameter* p = new Parameter();
  p->type = literal;
  p->value.sval = new std::string(*$1);

  $$ = p;
}
| VINT
{
  Parameter* p = new Parameter();
  p->type = numeric;
  p->value.ival = $1;
  $$ = p;
}
| invocation {

  Parameter* p = new Parameter();
  p->type = invocation;
  p->value.inv = new Invocation(*$1);
  $$ = p;
  }
| START_BLOCK END_BLOCK {
  Parameter* p = new Parameter();
  p->type = dictionary;
  p->value.dict = new AttributeList();
  $$ = p;
  }
;



%%

void
ipk::Parser::error (const Parser::location_type& l,
		    const std::string& m)
{
  driver.error(l, m);
}
