/* -*- coding:utf-8;mode:c++ -*- */

%{ /*** C/C++ Declarations ***/

#include <string>
#include <map>
#include <sstream>

#include "Scanner.hh"

/* import the parser's token type into a local typedef */
typedef ipk::Parser::token token;
typedef ipk::Parser::token_type token_type;

/* By default yylex returns int, we use token_type. Unfortunately yyterminate
 * by default returns 0, which is not of token_type. */
#define yyterminate() return token::END

std::map<std::string, token_type> reservadas;

#define YY_NO_UNISTD_H
%}

/*** Flex Declarations and Options ***/

/* enable c++ scanner class generation */
%option c++

/* change the name of the scanner class. results in "ExampleFlexLexer" */
%option prefix="ipk"

/* the manual says "somewhat more optimized" */
%option batch

/* enable scanner to generate debug output. disable this for release
 * versions. */
%option debug

/* no support for include files is planned */
%option yywrap nounput

/* enables the use of start condition stacks */
%option stack

/* The following paragraph suffices to track locations accurately. Each time
 * yylex is invoked, the begin position is moved onto the end position. */
%{
#define YY_USER_ACTION  yylloc->columns(yyleng);
%}

START_COMMENT     "/*"
START_BLOCK       "{"
END_BLOCK         "}"
START_PAR         "("
END_P             ")"
DOT               "."
COLON             ":"
SEMICOLON         ";"
EQUAL             "="
COMMA             ","
DOLLAR            "$"

TRUE              "true"
FALSE             "false"

IF                "if"
EQ                "=="
NEQ               "!="
GT                ">"
LT                "<"
GTEQ              ">="
LTEQ              "<="

COMMENT           "//"[^\n]*
FIXME             "FIXME"[^\n]*

DIGIT1            [0-9]*
DIGIT2            [1-9]*
VINT              (\+|-)?((0[0-7]+)|(0x[[:xdigit:]]+)|([[:digit:]]+))
VREAL             ({VINT}"."|{VINT}"."{VINT})

OP                ("+"|"-"|"*"|"/")

LITERAL           (\"[^\n"]+\")|(\"\")
ID                \\?[[:alpha:]_][[:alnum:]_]*


%x COMMENT_STATE


%%

 /* code to place at the beginning of yylex() */

%{
  // reset location
  yylloc->step();
%}

{TRUE}       {return token::TRUE;}
{FALSE}      {return token::FALSE;}

{IF}         {return token::IF;}
{EQ}         {return token::EQ;}
{NEQ}        {return token::NEQ;}
{GT}         {return token::GT;}
{LT}         {return token::LT;}
{GTEQ}       {return token::GTEQ;}
{LTEQ}       {return token::LTEQ;}

{LITERAL}           {yylval->sval = new std::string(yytext, yyleng);
                     return token::LITERAL;}

("+"|"-"|""){VINT}  {yylval->ival = atoi(yytext);
 return token::VINT;}

"{"     {return token::START_BLOCK;}
"}"     {return token::END_BLOCK;}
"("     {return token::START_P;}
")"     {return token::END_P;}

{COMMENT}        {/*Bye, comments!*/}
{FIXME}          {/*Bye, comments!*/}




";" {return token::SEMICOLON;}
":" {return token::COLON;}
"." {return token::DOT;}
"," {return token::COMMA;}
"=" {return token::EQUAL;}

[ \t\r]+ {
    yylloc->step();
}

\n {
    yylloc->lines(yyleng);
    yylloc->step();
}

{ID} {
    std::map<std::string, token_type>::const_iterator pos =
       reservadas.find(std::string(yytext, yyleng));

    if(pos != reservadas.end())
       return pos->second;

    yylval->sval = new std::string(yytext, yyleng);
    return token::ID;}

.    {
       std::string msg = *(yylloc->begin.filename);
       std::stringstream line;
       line << yylloc->begin.line;
       msg += ":" + line.str() + ": Ilegal character:'";
       msg += std::string(yytext) + "'";

       YY_FATAL_ERROR(msg.c_str());
}


%%

namespace ipk {

Scanner::Scanner(std::istream* in,
		 std::ostream* out)
    : ipkFlexLexer(in, out)
{
    reservadas["uses"]     =   token::IPK_USES;
    reservadas["boot"]     =   token::IPK_BOOT;
    reservadas["timer"]    =   token::IPK_TIMER;
    reservadas["when"]     =   token::IPK_WHEN;
    reservadas["do"]       =   token::IPK_DO;
    reservadas["event"]    =   token::IPK_EVENT;
    reservadas["remote"]   =   token::IPK_REMOTE;
    reservadas["local"]    =   token::IPK_LOCAL;
    reservadas["function"] =   token::IPK_FUNCTION;
    reservadas["object"]   =   token::IPK_OBJECT;
    reservadas["adapter"]  =   token::IPK_ADAPTER;

}

Scanner::~Scanner()
{
}

void Scanner::set_debug(bool b)
{
    yy_flex_debug = b;
}

}

/* This implementation of ExampleFlexLexer::yylex() is required to fill the
 * vtable of the class ExampleFlexLexer. We define the scanner's main yylex
 * function via YY_DECL to reside in the Scanner class instead. */

#ifdef yylex
#undef yylex
#endif

int ipkFlexLexer::yylex()
{
    std::cerr << "in IPKFlexLexer::yylex() !" << std::endl;
    return 0;
}

/* When the scanner receives an end-of-file indication from YY_INPUT, it then
 * checks the yywrap() function. If yywrap() returns false (zero), then it is
 * assumed that the function has gone ahead and set up `yyin' to point to
 * another input file, and scanning continues. If it returns true (non-zero),
 * then the scanner terminates, returning 0 to its caller. */

int ipkFlexLexer::yywrap()
{
    return 1;
}
