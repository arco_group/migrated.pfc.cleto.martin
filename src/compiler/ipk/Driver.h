// -*- mode:c++ -*-
#ifndef __IPKDRIVER_H__
#define __IPKDRIVER_H__

#include <list>
#include <string>
#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <stdexcept>

#define AttributeList std::vector<std::pair<std::string, ipk::Attribute> >

namespace ipk {

  class ParsingException : public std::runtime_error {
  public:
    ParsingException(const std::string& msg = "") : std::runtime_error(msg) {}
  };

  class ObjectNotFoundException : public std::runtime_error {
  public:
    ObjectNotFoundException(const std::string& msg = "") :
      std::runtime_error(msg) {}
  };


  // For SliceVisitor
  struct Method {
    std::string name;
    std::string returnType;
    std::vector<std::string> paramTypes;
  };

  struct Iface {
    std::string name;
    std::vector<Method> methods;
  };

  struct Module {
    std::string name;
    std::vector<Iface> interfaces;
  };
  // *******************



  class Entity {
  public:
    std::string getFilename() const {return _filename;}
    int getLine() const {return _line;}

    void setFilename(const std::string& f) {_filename=f;}
    void setLine(const int& l) {_line = l;}

  private:
    std::string _filename;
    int _line;
  };

  typedef enum {
    ONEWAY = 10,
    TWOWAY = 20,
    DATAGRAM = 30
  } ObjectMode;


  typedef enum {
    LOCAL  = 11,
    REMOTE = 22
  } AdapterType;


  typedef enum {
    IDEMPOTENT = 0,
    NORMAL = 1
  } MethodType;

  typedef enum {
    TRIGGER_WHEN,
    TRIGGER_TIMER,
    TRIGGER_BOOT,
    TRIGGER_FUNCTION,
    TRIGGER_EVENT,
  } TriggerType;

  typedef enum {
    OP_EQ,
    OP_NEQ,
    OP_GT,
    OP_GTEQ,
    OP_LT,
    OP_LTEQ,
  } OperatorType;

  typedef enum{
    literal,
    identifier,
    attribute,
    numeric,
    boolean,
    invocation,
    dictionary
  }Type;



  class Parameter;

  class Condition: public Entity {
  public:
    Condition();
    Condition(std::vector<Parameter> params,
	      OperatorType type);

    OperatorType getType() const;
    std::vector<Parameter> getParameters() const;

    void setParameters(const std::vector<Parameter>& params);
    void setType(const OperatorType& type);

  private:
    std::vector<Parameter> _params;
    OperatorType _type;
  };


  class Invocation: public Entity{
  public:
    Invocation();

    Invocation(const std::string& object,
	       const std::string& method,
	       const std::vector<Parameter>& parameters);

    Invocation(const std::string& object,
	       const std::string& method,
	       const std::vector<Parameter>& parameters,
	       const Condition& cond);

    Invocation(const std::string& object,
	       const std::string& method);

    Invocation(const std::string& object,
	       const std::string& method,
	       const Condition& cond);

    std::string getObject() const;
    std::string getMethod() const;
    std::vector<Parameter> getParameters() const;
    Condition getCondition() const;
    void addParameter(const Parameter& par);
    void setCondition(const Condition& cond);

  private:
    std::string _object;
    std::string _method;
    std::vector<Parameter> _parameters;
    Condition _cond;
  };


  //  class Dictionary;
  class Attribute;

  union Value {
    int ival;
    std::string* sval;
    bool bval;
    Invocation* inv;
    AttributeList* dict;
  };

  struct Parameter: public Entity {
    Type type;
    Value value;
  };

  struct Attribute: public Entity {
    Type type;
    Value value;
  };

  class Dictionary: public Entity {
  public:
    Dictionary();
    void add(const std::string& key, const Attribute& val);

  private:
    std::map<std::string, Attribute> _map;
  };


  class Object: public Entity{
  public:
    Object();

    void setAttributes(AttributeList attrs);
    void setModule(const std::string& );
    void setIface(const std::string&);
    void setIfaceParents(const std::vector<std::string>& parents);
    void setName(const std::string& name);
    void setIdentity(const std::string& oid);
    void setMode(const ObjectMode& mode);

    std::string getName() const;
    std::string getIdentity() const;
    std::string getIface() const;
    std::string getModule() const;

    std::vector<std::string> getIfaceParents() const;
    AttributeList getAttributes() const;

    ObjectMode getMode() const;

  protected:
    std::string _module;
    std::string _iface;
    std::vector<std::string> _ifaceParents;

    AttributeList _attributes;
    std::string _oid;
    std::string _name;
    ObjectMode _mode;

  };

  class Trigger: public Object {
  public:
    Trigger();
    virtual std::vector<Invocation> getInvocations() const;
    virtual TriggerType getType() const;

    void setInvocations(const std::vector<Invocation>& invocations);
    void setType(const TriggerType& type);

  protected:
    std::vector<Invocation> _invocations;
    TriggerType _type;
  };

  class Boot: public Trigger {
  public:
    Boot();
  };

  class Function: public Trigger {
  public:
    Function();
  };

  class Timer: public Trigger {
  public:
    Timer();
    void setTimeout(int t);
    int  getTimeout() const;

  private:
    int _timeout;
  };

  class Event: public Trigger {
  public:
    Event();
    void setEvent(const std::string& event);
    std::string getEvent() const;

  private:
    std::string _event;
  };

  class When: public Trigger {
  public:
    When();
    void setMethod(const Invocation& inv);
    Invocation getMethod() const;
  private:
    Invocation _method;
  };


  class Adapter: public Entity {
  public:
    Adapter();
    AdapterType getType() const;
    std::string getName() const;
    std::string getEndpoint() const;
    //std::vector<Object> getObjects() const;

    void setType(const AdapterType& type);
    void setName(const std::string& name);
    void setAttributes(const AttributeList& attrs);
    void setEndpoint(const std::string& endpoint);
    AttributeList getAttributes() const;

  private:
    AdapterType _type;
    std::string _name;
    std::string _endpoint;
    //std::vector<Object> _objects;
    AttributeList _attributes;
  };

  class SymbolTable {

  public:
    void addUse(const std::string& use);
    void addAdapter(const Adapter& adapter);
    void addObject(const Object& adapter);
    void addTrigger(Trigger* section);

    std::vector<std::string> getUses() const;
    std::vector<Adapter> getAdapters() const;
    std::vector<Trigger*> getTriggers() const;
    std::vector<Object> getObjects() const;
    Trigger* getTriggerByLabel(const std::string& label) const;
    Object getObjectByName(const std::string& name) const;
    //    Adapter getAdapterByObject(const std::string& name) const;

  private:
    std::vector<std::string> _uses;
    std::vector<Adapter> _adapters;
    std::vector<Object> _objects;
    std::vector<Trigger*> _triggers;
  };

  class Visitor {
  public:
    Visitor(SymbolTable* t);
    ~Visitor();

    void visitUse(const std::string& use);
    void visitAdapter(const Adapter& adapter);
    void visitTrigger(Trigger* section);
    void visitObject(const Object& obj);
    void visitInvocation(const Invocation& inv);
    void visitParameter(const Parameter& p);

  private:
    SymbolTable* _t;
  };

  class Visitable {
  public:
    virtual void accept(Visitor* v) = 0;
  };

  class Driver: public Visitable {
  public:
    Driver();
    ~Driver();

    void warning(const char*);
    void warning(const std::string&);

    void accept(Visitor* v);
    void nextLine();
    int getLine() const;
    std::string getStrLine() const;
    std::string getFilename() const;

    // Handling the scanner.
    void scanBegin ();
    void scanEnd ();
    bool trace_scanning;

    // Handling the parser.
    bool parse(const std::string& f);
    std::string file;
    bool trace_parsing;

    Object currentObject;

    // Error handling.
    void error(const class location& l, const std::string& m);
    void error(const std::string& m);

    Visitor* visitor;
    class Scanner* lexer;

  private:
    bool _continue;
    int _errors;
    int _line;
    std::string _input;

  };
}

#endif
