// -*- coding: utf-8 -*-

#include <cstdlib>
#include <fstream>
#include <sstream>

#include <easyc++/logger.h>
#include <config.h>

#include "Driver.h"
#include "Scanner.hh"

using namespace std;


namespace ipk {

  Driver::Driver():
    trace_scanning (false), trace_parsing (false) {
  }

  Driver::~Driver() {
  }

  void
  Driver::nextLine() {
    _line++;
  }

  int
  Driver::getLine() const {
    return _line;
  }

  string
  Driver::getStrLine() const {
    stringstream ss;
    ss<<_line;
    return ss.str();
  }

  string
  Driver::getFilename() const {
    return _input;
  }

  void
  Driver::accept(Visitor* v) {
    visitor = v;
  }

  bool
  Driver::parse(const string& f)
  {
    file = f;

    ifstream in(file.c_str());
    if (!in.good()) return false;

    Scanner scanner(&in, NULL);
    scanner.set_debug(false); // FIXME: cambiar al nivel correspondiente
    this->lexer = &scanner;

    Parser parser(*this);
    parser.set_debug_level(false);  // FIXME: cambiar al nivel correspondiente
    return (parser.parse() == 0);
  }

  void
  Driver::error (const class location& l, const string& m)
  {
    cerr << l << ": " << m << endl;
    exit(1);
  }

  void
  Driver::error (const string& m)
  {
    cerr << m << endl;
    exit(1);
  }

  void
  Driver::scanBegin()
  {
  }

  void
  Driver::scanEnd()
  {
  }

  //***********
  // Dictionary
  //***********

  Dictionary::Dictionary() {}

  void
  Dictionary::add(const string& key, const Attribute& val) {
    _map[key] = val;
  }


  //***********
  // Condition
  //***********

  Condition::Condition(){ };

  Condition::Condition(vector<Parameter> params, OperatorType type)
    :_params(params), _type(type) { };

  OperatorType
  Condition::getType() const {
    return _type;
  }

  vector<Parameter>
  Condition::getParameters() const {
    return _params;
  }

  void
  Condition::setParameters(const vector<Parameter>& params) {
    _params = params;
  }

  void
  Condition::setType(const OperatorType& type) {
    _type = type;
  }

  //***********
  // Invocation
  //***********


  Invocation::Invocation(){
  }

  Invocation::Invocation(const string& object,
			 const string& method,
			 const vector<Parameter>& parameters):
    _object(object), _method(method), _parameters(parameters){
  }


  Invocation::Invocation(const string& object,
			 const string& method):
    _object(object), _method(method){
  }


  void
  Invocation::addParameter(const Parameter& p) {
    _parameters.push_back(p);
  }

  string Invocation::getObject() const{
    return _object;
  }

  string Invocation::getMethod() const {
    return _method;
  }

  vector<Parameter> Invocation::getParameters() const {
    return _parameters;
  }

  Condition Invocation::getCondition() const{
    return _cond;
  }


  void Invocation::setCondition(const Condition& cond) {
    _cond = cond;
  }


  //********
  //Trigger
  //********
  Trigger::Trigger()
  {
    _module = "IPK";
    _iface = "Trigger";
  }

  vector<Invocation>
  Trigger::getInvocations() const {
    return _invocations;
  }

  TriggerType
  Trigger::getType() const {
    return _type;
  }

  // string
//   Trigger::getLabel() const {
//     return _label;
//   }

  void
  Trigger::setInvocations(const vector<Invocation>& invocations) {
    _invocations = invocations;
  }

  void
  Trigger::setType(const TriggerType& type) {
    _type = type;
  }

  // void
  //   Trigger::setLabel(const string& label) {
  //     _label = label;
  //   };


  //********
  //Timer
  //********


  Timer::Timer() {
    _type = TRIGGER_TIMER;
    _module = "IPK";
    _iface  = "Timer";
  }

  void
  Timer::setTimeout(int t) {
    _timeout = t;
  }

  int
  Timer::getTimeout() const {
    return _timeout;
  }

  //********
  //When
  //********

  When::When() {
    _type = TRIGGER_WHEN;
  }

  void
  When::setMethod(const Invocation& method) {
    _method = method;
  }

  Invocation
  When::getMethod() const {
    return _method;
  }

  //******
  //Boot
  //******

  Boot::Boot() {
    _type = TRIGGER_BOOT;
  }

  //******
  //Function
  //******

  Function::Function() {
    _type = TRIGGER_FUNCTION;
    _module = "IPK";
    _iface  = "Function";
  }

  //*****
  //Event
  //*****

  Event::Event() {
    _type = TRIGGER_EVENT;
  }

  void
  Event::setEvent(const string& event) {
    _event = event;
  }

  string
  Event::getEvent() const {
    return _event;
  }


  //****************************
  // ::Object
  //****************************


  Object::Object() : _mode(TWOWAY) {
  }


  void
  Object::setAttributes(const AttributeList attrs) {
    vector<string> aux;

    if (not config.parse_only) {

      for (AttributeList::const_iterator it=attrs.begin(); it!=attrs.end(); it++) {
	if (find(aux.begin(), aux.end(), it->first) == aux.end())
	  aux.push_back(it->first);
	else
	  throw ParsingException("Duplicated attribute definition: '" + it->first + "'");
      }
    }

    _attributes = attrs;
  }

  void
  Object::setModule(const string& module) {
    _module = module;
  }

  void
  Object::setIface(const string& iface) {
    _iface = iface;
  }

  void
  Object::setIfaceParents(const vector<string>& ifaces)
  {
    _ifaceParents = ifaces;
  }

  void
  Object::setName(const string& name) {
    _name = name;
  }

  void
  Object::setIdentity(const string& oid) {
    _oid = oid;
  }

  void
  Object::setMode(const ObjectMode& m) {
    _mode = m;
  }

  string
  Object::getModule() const {
    if (_module.find_first_of("::", 0) != 0) {
      return string("::") + _module;
    }

    return _module;
  }

  string
  Object::getIface() const {
    return _iface;
  }

  vector<string>
  Object::getIfaceParents() const {
    return _ifaceParents;
  }

  string
  Object::getName() const {
    return _name;
  }

  string
  Object::getIdentity() const {
    return _oid;
  }

  ObjectMode
  Object::getMode() const{
    return _mode;
  }

  AttributeList
  Object::getAttributes() const {
    return _attributes;
  }

  //*******
  //Adapter
  //*******


  Adapter::Adapter() {}

  AdapterType
  Adapter::getType() const {
    return _type;
  }

  string
  Adapter::getName() const {
    return _name;
  }

  string
  Adapter::getEndpoint() const {
    return _endpoint;
  }

//   vector<Object>
//   Adapter::getObjects() const {
//     return _objects;
//   }

  void
  Adapter::setType(const AdapterType& type) {
    _type = type;
  }

  void
  Adapter::setName(const string& name) {
    _name = name;
  }

  void
  Adapter::setEndpoint(const string& endpoint) {
    _endpoint = endpoint;
  }

//   void
//   Adapter::addObject(const Object& ob) {
//     _objects.push_back(ob);
//   }

  void
  Adapter::setAttributes(const AttributeList& attrs) {
    _attributes = attrs;
  }

  AttributeList
  Adapter::getAttributes() const {
    return _attributes;
  }

  //*********************
  //Visitor
  //*********************

  Visitor::Visitor(SymbolTable* t) {
    _t = t;
  }

  Visitor::~Visitor(){}

  void
  Visitor::visitUse(const string& use) {

    if (not config.parse_only) {
      if (use == "")
	throw ParsingException("an uses literal can not be empty");

      vector<string> uses = _t->getUses();
      if (find(uses.begin(), uses.end(), use) != uses.end())
	throw ParsingException("Duplicated uses: " + use);
    }

    _t->addUse(use);
  }

  void
  Visitor::visitAdapter(const Adapter& adapter) {
    _t->addAdapter(adapter);
  }

  void
  Visitor::visitTrigger(Trigger* section) {
    //FIXME - Comprobar que es una sección válida
    _t->addTrigger(section);
  }

  void
  Visitor::visitObject(const Object& obj) {
    _t->addObject(obj);
  }

  //**************
  //SymbolTable
  //**************

  void
  SymbolTable::addUse(const string& use) {
    _uses.push_back(use);
    ec::debug() << "SymbolTable: added '" << use
		<< "' as used file" << endl;
  }

  void
  SymbolTable::addAdapter(const Adapter& adapter) {
    _adapters.push_back(adapter);
    ec::debug() << "SymbolTable: added '" << adapter.getName()
		<< "' as new adapter" << endl;
  }

  void
  SymbolTable::addTrigger(Trigger* section) {
    ec::debug() << "SymbolTable: added '" << section->getType()
		<< "' as new trigger labeled: '" << section->getName() << "'"
		<< endl;
    _triggers.push_back(section);
  }

  void
  SymbolTable::addObject(const Object& obj) {
    _objects.push_back(obj);
    ec::debug() << "SymbolTable: added '" << obj.getName()
		<< "' as new global object of type: '"
		<< obj.getModule() << "::"<< obj.getIface() << "'" << endl;

  }

  vector<string>
  SymbolTable::getUses() const {
    return _uses;
  }

  vector<Adapter>
  SymbolTable::getAdapters() const {
    return _adapters;
  }

  vector<Trigger*>
  SymbolTable::getTriggers() const {
    return _triggers;
  }

  vector<Object>
  SymbolTable::getObjects() const {
    return _objects;
  }

  Object
  SymbolTable::getObjectByName(const string& name) const {

    for (vector<Object>::const_iterator j = _objects.begin();
	 j != _objects.end(); j++) {
      if (j->getName() == name)
	return *j;
    }

    for (vector<Trigger*>::const_iterator it = _triggers.begin();
	 it != _triggers.end(); it++) {
      if ((*it)->getName() == name)
	return *(*it);
    }

    throw ObjectNotFoundException();
  }

//   Adapter
//   SymbolTable::getAdapterByObject(const string& name) const {
//     for (vector<Adapter>::const_iterator i = _adapters.begin();
// 	 i != _adapters.end(); i++) {

//       vector<Object> objs = i->getObjects();
//       for (vector<Object>::const_iterator j = objs.begin();
// 	   j != objs.end(); j++) {
// 	if (j->getName() == name)
// 	  return *i;
//       }
//     }

//     throw ObjectNotFoundException();
//   }

  Trigger*
  SymbolTable::getTriggerByLabel(const string& name) const {

    for(vector<Trigger*>::const_iterator it = _triggers.begin();
	it != _triggers.end(); it++) {

      if ((*it)->getName() == name)
	return *it;
    }

    throw ObjectNotFoundException();
  }

}
