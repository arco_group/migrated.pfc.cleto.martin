// -*- mode:c++ -*-
#ifndef __SEMANTICANALYZER__H__
#define __SEMANTICANALYZER__H__

#include <SymbolTable.h>
#include <PluginManager.h>

namespace Semantic {

  class Analyzer {

  public:
    static Analyzer createAnalyzer(slc::SymbolTable sst,
				   ipk::SymbolTable& ist,
				   sis::SymbolTable& mst);

  static Analyzer getAnalyzer();

  void parse();

private:
  static Analyzer* _instance;

  std::map<std::string, ipk::ObjectMode> _modes;

  Analyzer(slc::SymbolTable sst,
	   ipk::SymbolTable& ist,
	   sis::SymbolTable& mst);

  friend class IPKPlugin;

  slc::SymbolTable _sst;
  ipk::SymbolTable _ist;
  sis::SymbolTable _mst;

  std::vector<std::string> _obs;       //List of declared objects
  std::vector<std::string> _labels;    //List of declared triggers
  std::vector<std::string> _adapters;  //List of endpoints used
  std::vector<std::string> _oids;      //List of UUID's used
  std::vector<std::string> _endpoints; //List of endpoints used

  std::map<ipk::ObjectMode, std::string> _mode2str;

  std::vector<std::string> _types;

  void validateSIS();
  void validateAdapter(const ipk::Adapter& adapter);
  void validateObject (const ipk::Object& object);
  void validateObjects ();
  void validateTrigger(ipk::Trigger* section);
  void validateInvocation(const ipk::Invocation& inv,
			  ipk::Trigger* trigger,
			  bool topLevel);

  void validateCondition(const ipk::Condition& cond);


  void validateTimer(const ipk::Timer* timer);
  void validateEvent(const ipk::Event* event);
  void validateWhenInvocation(const ipk::When* when);

  void checkParamTypes(const ipk::Parameter& value,
		       ipk::Trigger* trigger,
		       const std::string& type);

  void replaceParamValues(std::vector<ipk::Parameter>* params);

  void addObject(const std::string& name, const std::string& oid);

  bool isObjRegistered(const std::string& name);
  bool isOidRegistered(const std::string& oid);

  static slc::SymbolTable getSliceSymbolTable();
  static ipk::SymbolTable getIPKSymbolTable();
  static sis::SymbolTable getSISSymbolTable();

};

  inline std::string int2str(int v) {
    std::ostringstream oss;
    oss << v;
    return oss.str();
  }

  class Exception: public std::runtime_error {
  public:
    Exception(const std::string& msg = ""):
      std::runtime_error(msg) { };

    Exception(const std::string& filename,
	      const int& line,
	      const std::string& msg = ""):
      std::runtime_error(filename + ":" + int2str(line) + " " + msg){};

  };
};


#endif
