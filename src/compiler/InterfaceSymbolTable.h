// -*- coding:utf-8; mode:c++ -*-
#ifndef __INTERFACEST__H__
#define __INTERFACEST__H__

#include <ipk/Driver.h>

class InterfaceSymbolTable {
  virtual void addIPKModule(const ipk::Module& module) = 0;
};

#endif
