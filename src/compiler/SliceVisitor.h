// -*- coding:utf-8; mode:c++ -*-

#ifndef __SLICEVISITOR__H__
#define __SLICEVISITOR__H__

#include <stack>
#include <stdexcept>

#include <Slice/Parser.h>
#include <InterfaceSymbolTable.h>

namespace slc {
  class NotFoundException: public std::runtime_error {
  public:
    NotFoundException(const std::string& msg = ""): std::runtime_error(msg){};
  };

  class Param {

  public:
    Param(const std::string& type, const std::string& name);

    std::string getName() const;
    std::string getType() const;

  private:
    std::string _type;
    std::string _name;
  };

  class Operation {

  public:
    Operation(const std::string& rtype,
	      const std::string& name,
	      const std::string& iface );

    Operation(const std::string& iface,
	      const std::string& name);

    Operation();
    bool operator==(const Operation& other) const;

    void addMetadata(const std::string&, const std::string&);
    void addInputParam(const Param&);
    void addOutputParam(const Param&);

    std::vector<Param> getInputParameters() const ;
    std::vector<Param> getOutputParameters() const;
    std::map<std::string,std::string> getMetadata() const;
    std::string getName() const;
    std::string getType() const;
    std::string getReturnedType() const;
    std::string getInterface() const;



  private:
    std::string _name;
    std::string _type;
    std::string _iface;

    std::map<std::string, std::string> _metadata;
    std::vector<Param> _inputParams;
    std::vector<Param> _outputParams;
  };

  class SymbolTable;

  class Iface {

  public:
    Iface(const std::string&, const std::string&);
    Iface();

    bool operator==(const Iface& other) const;

    void addOperation(const Operation&);
    void addInheritance(const std::string& scope,
			const std::string& name);
    std::vector<std::string> inheritance();
    std::vector<Operation> getOperations() const;
    std::string name() const;
    std::string scope() const;

  private:
    std::string _name;
    std::string _scope;
    std::vector<Operation> _operations;
    std::vector<std::string> _inheritance;
  };


  class Module {
  public:
    Module();
    Module(const std::string& name, const std::string& scope = "::");

    void addModule(const Module&);
    void addIface(const Iface&);

    std::vector<Module> modules() const;
    std::vector<Module*> getModules() const;

    std::vector<Iface> ifaces() const;
    Module searchModule(const std::string&) const;
    std::string name() const;
    std::string scope() const;

  private:
    std::vector<Module> _modules;
    std::vector<Iface> _ifaces;
    std::string _name;
    std::string _scope;
  };


  class SymbolTable : virtual InterfaceSymbolTable {

  public:
    SymbolTable();
    void addModule(const Module& );
    void addClass();
    std::vector<Module> modules();
    std::vector<std::string> getInheritance(const std::string& base) const;
    Module searchModule(const std::string&) const;
    Iface searchIface(const std::string&) const;

    virtual void addIPKModule(const ipk::Module& module);

  private:
    std::vector<Module> _modules;
    void updateModule(Module* old, const Module* actual);
  };


  class SliceParserException: public std::runtime_error {
  public:
    SliceParserException(const std::string& msg) : std::runtime_error(msg) {}
  };

  class Visitor: public Slice::ParserVisitor {

  public:
    Visitor(SymbolTable*);
    Visitor();

    virtual bool visitModuleStart   (const Slice::ModulePtr&);
    virtual void visitModuleEnd     (const Slice::ModulePtr&);
    virtual void visitClassDecl     (const Slice::ClassDeclPtr&);
    virtual bool visitClassDefStart (const Slice::ClassDefPtr&);
    virtual void visitClassDefEnd   (const Slice::ClassDefPtr&);
    virtual bool visitExceptionStart(const Slice::ExceptionPtr&);
    virtual void visitExceptionEnd  (const Slice::ExceptionPtr&);
    virtual bool visitStructStart   (const Slice::StructPtr&);
    virtual void visitStructEnd     (const Slice::StructPtr&);
    virtual void visitOperation     (const Slice::OperationPtr&);
    virtual void visitParamDecl     (const Slice::ParamDeclPtr&);
    virtual void visitDataMember    (const Slice::DataMemberPtr&);
    virtual void visitSequence      (const Slice::SequencePtr&);
    virtual void visitDictionary	  (const Slice::DictionaryPtr&);
    virtual void visitEnum	      (const Slice::EnumPtr&);
    virtual void visitConst	      (const Slice::ConstPtr&);

  private:
    SymbolTable* _symbolTable;
    Module _module;
    Module _parent;
    Iface _iface;
    int _moduleDepth;
    std::stack<Module> _recursiveModules;
  };


  class Parser {
  public:
    Parser(const std::vector<std::string>& uses);
    void accept(Visitor* v);

  private:
    void parse(const std::string& filename);
    std::vector<std::string> _files;
    Visitor* _visitor;
  };

}
#endif
