#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <iterator>
#include <easyc++/logger.h>

#include "config.h"

using namespace std;


Config::Config(void) {
  // valores por defecto
  output = "a.out";
  sis = "";
  debug = false;
  wAll = false;
  parse_only = false;
}

void
Config::add_path(const string& val) {
  paths.push_back(val);
}

void
Config::parse(int argc, char* const argv[]) {

  char c;

  progname = argv[0];

  while ((c = getopt(argc, argv, "po:I:m:vw")) != -1)
    switch(c) {
    case 'p':
      parse_only = true;
    case 'o':
      output = optarg;
      break;
    case 'I':
      paths.push_back(optarg);
      break;
    case 'v':
      ec::Logger::root.set_level(ec::Logger::DEBUG);
      break;
    case 'w': // Si es true, si hay warning se retorna
      // un 1 al final... esto haria que 'make'
      // fallase... por eso mejor opcional.

      // FIXME: Qué es esto?
      ec::info() << "->Populating warnings as errors (warnings return 1 - real errors return 2)" << endl;
      wAll = true;
      break;
    case 'S':
      sis = optarg;
      break;
    case '?':
      if (optopt == 'o' || optopt == 'I') {
	cerr << "Option -" << (char)optopt <<" requires an argument." << endl;
	throw ConfigException();
      }
    default:
      ec::warn() << "Unknown option " << optopt << endl;
    }


  // Postconditions

  if (optind >= argc) {
    //ec::critical() << "No input file" << endl;
    throw ConfigException("No input file");
  }

  input = argv[optind];

  FILE *fp = fopen(input.c_str(), "r");
  if (!fp) {
    //ec::critical() << "Input file does not exist" << endl;
    throw ConfigException("Input file does not exist");
  }
  fclose(fp);

  //copy(paths.begin(), paths.end(),
  //     ostream_iterator<string>(Logger::root.debug(), "\n"));
}


void
Config::parse(vector<string> args) {

  int argc = args.size();
  char** argv = (char**)malloc(argc * sizeof(char*));
  for (int i= 0; i<argc; i++)
    argv[i] = strdup(args[i].c_str());

  parse(argc, argv);
}

extern Config config;
