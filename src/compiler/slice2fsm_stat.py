#!/usr/bin/python
# -*- coding:utf-8 -*-

import sys, os
from backend_base import BackendBase

import slice2fsm
import ipk2stats

class Backend(BackendBase):

    def __init__(self, optparser):
        self.name = "slice2fsm_stat"

        self.slice2fsm = slice2fsm.Backend(optparser)
        self.ipk2stats = ipk2stats.Backend(optparser)
        BackendBase.__init__(self, optparser, "slice2fsm_stat")


    def build(self, parser_config, st, options, args):

        self.slice2fsm.build(parser_config, st, options, args)
        self.ipk2stats.build(parser_config, st, options, args)
