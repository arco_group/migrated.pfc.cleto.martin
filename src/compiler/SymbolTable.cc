#include <stdlib.h>
#include <easyc++/logger.h>

#include "SymbolTable.h"

using namespace std;

Semantic::Signature::Signature() {
}

Semantic::Signature::Signature(const sis::Method& op) {

  _name  = op.name;
  _iface = op.interface;

  for (vector<string>::const_iterator it = op.output.begin();
       it != op.output.end(); it++)
    _outputParams.push_back(make_pair("", (*it) ));


  for (vector<string>::const_iterator it = op.input.begin();
       it != op.input.end(); it++)
    _inputParams.push_back(make_pair("", (*it) ));
}

Semantic::Signature::Signature(const slc::Operation& op) {

  if (op.getType() != "")
    _mode = ipk::IDEMPOTENT;
  else
    _mode = ipk::NORMAL;

  vector<slc::Param> in = op.getInputParameters();
  for (vector<slc::Param>::const_iterator it = in.begin();
       it != in.end(); it++)
    _inputParams.push_back(make_pair(it->getName(), it->getType()));

  vector<slc::Param> out = op.getOutputParameters();
  for (vector<slc::Param>::const_iterator it = out.begin();
       it != out.end(); it++)
    _outputParams.push_back(make_pair(it->getName(), it->getType()));

  _metadata = op.getMetadata();
  _name     = op.getName();
  _iface    = op.getInterface();
}

ipk::MethodType
Semantic::Signature::getMode() const {
  return _mode;
}

string
Semantic::Signature::getName() const {
  return _name;
}

string
Semantic::Signature::getInterface() const {
  return _iface;
}

void
Semantic::Signature::setInterface(const string& iface) {
  _iface = iface;
}

void
Semantic::Signature::setName(const string& name) {
  _name = name;
}

vector<pair<string, string> >
Semantic::Signature::getInputParams() const {
  return _inputParams;
}

vector<pair<string, string> >
Semantic::Signature::getOutputParams() const {
  return _outputParams;
}

map <string, string>
Semantic::Signature::getMetadata() const {
  return _metadata;
}

bool
Semantic::Signature::operator==(const Signature& other) const {

  if (_name != other.getName())
    return false;

  if (_inputParams.size() != other.getInputParams().size())
    return false;

  if (_outputParams.size() != other.getOutputParams().size())
    return false;

  vector<pair<string, string> > otherInput = other.getInputParams();
  for (unsigned int i = 0; i<_inputParams.size(); i++)
    if (_inputParams[i].second != otherInput[i].second)
      return false;

  vector<pair<string, string> > otherOutput = other.getOutputParams();
  for (unsigned int i = 0; i<_outputParams.size(); i++)
    if (_outputParams[i].second != otherOutput[i].second)
      return false;

  return true;
}


//********************************
//********************************

Semantic::Object::Object() {
}

Semantic::Object::Object(const ipk::Object& object, const string& oid) {
  //Sanity check for values
  assert(object.getIface() != "");
  assert(object.getName()  != "");
  assert(object.getModule() != "");

  // FIXME - LLevarlo a algun .h porque mas de uno lo necesita
  _modes["twoway"] = ipk::TWOWAY;
  _modes["oneway"] = ipk::ONEWAY;
  _modes["datagram"] = ipk::DATAGRAM;

  _mode = ipk::TWOWAY; // By default - TWOWAY mode

  AttributeList attrs = object.getAttributes();
  for (AttributeList::const_iterator it = attrs.begin();
       it!=attrs.end(); it++) {
    if (it->first == "mode")
      _mode = _modes[*(it->second.value.sval)];
    else
      _attr[it->first] = *(it->second.value.sval);
  }

  _interface = object.getModule() + "::" + object.getIface();

  _oid = oid;
  _name = object.getName();

  Semantic::SymbolTable::SymbolTable st = Semantic::SymbolTable::instance();
  _allInterfaces = st.getParents(_interface);

  _allInterfaces.push_back(_interface);


  //Sanity check for values
  for (vector<string>::const_iterator it=_allInterfaces.begin();
       it != _allInterfaces.end();
       it++)
    assert(*it != "");


  vector<Semantic::Signature> methods;
  for (vector<string>::const_iterator i = _allInterfaces.begin();
       i != _allInterfaces.end(); i++) {
    methods = st.getMethodsByInterface(*i);
    _methods.insert(_methods.end(), methods.begin(), methods.end());
  }

//   methods = st.getMethodsByInterface(_interface);
//   _methods.insert(_methods.end(), methods.begin(), methods.end());

  //  ec::debug() << "Object " << _name << " created " << endl;
}

string
Semantic::Object::getInterface() const {
  return _interface;
}

vector<Semantic::Signature>
Semantic::Object::getMethods() const {
  return _methods;
}

map<string, string>
Semantic::Object::getAttributes() const {
  return _attr;
}

ipk::ObjectMode
Semantic::Object::getMode() const {
  return _mode;
}

string
Semantic::Object::getIdentity() const {
  return _oid;
}

string
Semantic::Object::getName() const {
  return _name;
}

vector<string>
Semantic::Object::getAllInterfaces() const {
  return _allInterfaces;
}

bool
Semantic::Object::operator==(const Semantic::Object& other) const {
  return _name == other.getName();
}

//********************************
//********************************


Semantic::Adapter::Adapter(const ipk::Adapter& adapter,
			   const ipk::SymbolTable& ist) {

  assert(adapter.getName() != "");
  //  assert(adapter.getEndpoint() != "");
  assert(adapter.getType() == ipk::LOCAL ||
	 adapter.getType() == ipk::REMOTE);

  _name = adapter.getName();
  _type = adapter.getType();


  AttributeList attrs = adapter.getAttributes();
  for (AttributeList::const_iterator it = attrs.begin();
       it != attrs.end(); it++) {
    if (it->first == "endpoint") {
      _endpoint = *(it->second.value.sval);
    }
    else if (it->first == "objects") {
      AttributeList list = *(it->second.value.dict);
      for (AttributeList::const_iterator l = list.begin();
	   l != list.end(); l++) {
	Semantic::Object obj(ist.getObjectByName(*(l->second.value.sval)),
			     l->first);
	_objects.push_back(obj);
      }
    }
  }
  ec::debug() << "Adapter " << _name << " added" << endl;
}

// string
// Semantic::Adapter::getName() const {
//   return _name;
// }

string
Semantic::Adapter::getEndpoint() const {
  return _endpoint;
}

ipk::AdapterType
Semantic::Adapter::getType() const {
  return _type;
}

vector<Semantic::Object>
Semantic::Adapter::getObjects() const {
  return _objects;
}

//********************************
//********************************

Semantic::Condition::Condition() {};

Semantic::Condition::Condition(const ipk::Condition& cond) {

  vector<ipk::Parameter> parameters = cond.getParameters();

  for (vector<ipk::Parameter>::const_iterator p = parameters.begin();
       p != parameters.end(); p++) {
    _params.push_back(Semantic::Parameter(*p));
  }


  _type   = cond.getType();
}


vector<Semantic::Parameter>
Semantic::Condition::getParameters() const {
  return _params;
}

ipk::OperatorType
Semantic::Condition::getType() const {
  return _type;
}

bool
Semantic::Condition::operator==(const Semantic::Condition& other) const {
  if (_params.size() != other.getParameters().size())
    return false;

//   vector<Semantic::Parameter> otherParams = other.getParameters();
//   for (unsigned int i=0; i<_params.size(); i++) {
//     if (otherParams[i] != _params[i])
//       return false;

  return (other.getType() == _type);
}

//********************************
//********************************

Semantic::Invocation::Invocation() {}

Semantic::Invocation::Invocation(const ipk::Invocation& invocation) {
  assert (invocation.getObject() != "");
  assert (invocation.getMethod() != "");

  string ob     = invocation.getObject();
  string method = invocation.getMethod();

  Semantic::SymbolTable::SymbolTable st = Semantic::SymbolTable::instance();

  _object = st.getObjectByName(ob);

  vector<Signature> m = _object.getMethods();


  bool found = false;
  for (vector<Signature>::const_iterator it = m.begin(); it != m.end(); it++) {
    if (it->getName() == method) {
      _method = *it;
      found = true;
      break;
    }
  }
  if (not found) { // Se supone que esta en el manifest
    // Se debe construir una signature para el metodo privado
    // FIXME - Buscar en el manifest directamente, no usando st

    string iface = _object.getInterface();
    sis::Method pm = st.getPrivateMethod(iface, method);
    Signature s(pm);
    _method = s;
  }

  vector<ipk::Parameter> params = invocation.getParameters();

  for (vector<ipk::Parameter>::const_iterator it = params.begin();
       it != params.end(); it++)
    _paramsValue.push_back(Parameter(*it));


  _cond = Semantic::Condition(invocation.getCondition());

  ec::debug() << "Invocation '" << ob << "." << method
	      << "' added" << endl;
}

void
Semantic::Invocation::setObject(const Object& object) {
  _object = object;
}

void
Semantic::Invocation::setMethod(const Signature& method) {
  _method = method;
}


Semantic::Object
Semantic::Invocation::getObject() const {
  return _object;

}

Semantic::Signature
Semantic::Invocation::getMethod() const {
  return _method;
}

vector<Semantic::Parameter>
Semantic::Invocation::getParameters() const {
  return _paramsValue;
}

Semantic::Condition
Semantic::Invocation::getCondition() const {
  return _cond;
}


//********************************
//********************************

Semantic::Parameter::Parameter() {}

Semantic::Parameter::Parameter(const ipk::Parameter& param) :
  _varType(param.type) {

  if (_varType == ipk::identifier)
    sval = new string(*(param.value.sval));

  else if (_varType == ipk::numeric)
    ival = param.value.ival;

  else if (_varType == ipk::literal) {
    ival = param.value.ival;
  }
  else if (_varType == ipk::attribute) {
    ival = param.value.ival;
  }
  else if (_varType == ipk::boolean)
    bval = param.value.bval;
  else if (_varType == ipk::invocation) {
    inv = new Semantic::Invocation(*(param.value.inv));
  }
  else if (_varType == ipk::dictionary) {
    dict = new Semantic::Dictionary(); // FIXME - De momento es vacio
  }
}

int
Semantic::Parameter::getValueAsInteger() const {
  return ival;
}

string
Semantic::Parameter::getValueAsString() const {
  return *sval;
}

bool
Semantic::Parameter::getValueAsBoolean() const {
  return bval;
}

Semantic::Invocation
Semantic::Parameter::getValueAsInvocation() const {
  return *inv;
}

Semantic::Dictionary
Semantic::Parameter::getValueAsDictionary() const {
  return *dict;
}

ipk::Type
Semantic::Parameter::getType() const {
  return _varType;
}


//********************************
//********************************

Semantic::Trigger::Trigger(const ipk::Trigger& trigger) {

  _type = trigger.getType();
  assert (_type == ipk::TRIGGER_WHEN  ||
	  _type == ipk::TRIGGER_EVENT ||
	  _type == ipk::TRIGGER_BOOT  ||
	  _type == ipk::TRIGGER_FUNCTION ||
	  _type == ipk::TRIGGER_TIMER );

  vector<ipk::Invocation> invs = trigger.getInvocations();
  _label = trigger.getName();
  for (vector<ipk::Invocation>::const_iterator it = invs.begin();
       it != invs.end(); it++) {
    _invocations.push_back(Semantic::Invocation(*it));
  }
}

ipk::TriggerType
Semantic::Trigger::getType() const {
  return _type;
}

string
Semantic::Trigger::getLabel() const {
  return _label;
}


vector<Semantic::Invocation>
Semantic::Trigger::getInvocations() const {
  return _invocations;
}

//********************************
//********************************

Semantic::Event::Event(ipk::Trigger* event):
  Semantic::Trigger::Trigger(*event) {

  ipk::Event* e = dynamic_cast<ipk::Event*>(event);
  _event = e->getEvent();
}

string
Semantic::Event::getEvent() const {
  return _event;
}

//********************************
//********************************

Semantic::Timer::Timer(ipk::Trigger* repeat):
  Semantic::Trigger(*repeat) {

  ipk::Timer * w = dynamic_cast<ipk::Timer*>(repeat);
  _timeout = w->getTimeout();
}

int
Semantic::Timer::getTimeout() const {
  return _timeout;
}

//********************************
//********************************

Semantic::When::When(ipk::Trigger* when):
  Semantic::Trigger(*when) {

  ipk::When* w = dynamic_cast<ipk::When*>(when);
  _invocation = Semantic::Invocation(w->getMethod());

  vector<Semantic::Parameter> params = _invocation.getParameters();

  Semantic::Signature sig = _invocation.getMethod();

  vector<pair<string, string> > inParams = sig.getInputParams();

  for (vector<Semantic::Parameter>::const_iterator it = params.begin();
       it != params.end(); it++) {

    for (vector<pair<string, string> >::const_iterator par=inParams.begin();
	 par != inParams.end(); par++) {

      if (par->first == it->getValueAsString()) {
	LocalVariable l;
	l.varType = par->second;
	l.value = par->first;
	_localVariables.push_back(l);
	ec::debug() << "Added '" << l.varType << " " << l.value
		    << "' as new local variable of a when trigger"
		    << endl;
      }
    }
  }
}

Semantic::Invocation
Semantic::When::getMethod() const {
  return _invocation;
}

vector<Semantic::LocalVariable>
Semantic::When::getLocalVariables() const {
  return _localVariables;
}

//********************************
//********************************

Semantic::Boot::Boot(ipk::Trigger* boot):
  Semantic::Trigger(*boot) {}

//********************************
//********************************

Semantic::Function::Function(ipk::Trigger* function):
  Semantic::Trigger(*function) {}

//********************************
//********************************


Semantic::SymbolTable* Semantic::SymbolTable::_instance = 0;

Semantic::SymbolTable::SymbolTable(slc::SymbolTable sst,
				   ipk::SymbolTable ist,
				   sis::SymbolTable mst):

  _sst(sst), _ist(ist), _mst(mst) {

  _instance = this;

  // vector<ipk::Object> objs = ist.getObjects();
//   for (vector<ipk::Object>::const_iterator it = objs.begin();
//        it != objs.end(); it++) {
//     _objects.push_back(Semantic::Object(*it));
//     cout << "añadido" << endl;
//   }

  vector<ipk::Adapter> adapters = _ist.getAdapters();

  ec::debug() << "-- Start adapter information -- " << endl;

  for (vector<ipk::Adapter>::const_iterator it = adapters.begin();
       it != adapters.end(); it++) {
    _adapters.push_back(Semantic::Adapter(*it, _ist));
  }

  ec::debug() << "-- End of adapter information -- " << endl;
  ec::debug() << "-- Start Triggers information -- " << endl;
  vector<ipk::Trigger*> triggers = _ist.getTriggers();

  for (vector<ipk::Trigger*>::const_iterator it = triggers.begin();
       it != triggers.end(); it++) {

    if ((*it)->getType() == ipk::TRIGGER_BOOT)
      _boots.push_back(Semantic::Boot(*it));

    else if ((*it)->getType() == ipk::TRIGGER_WHEN)
      _whens.push_back(Semantic::When(*it));

    else if (((*it)->getType() == ipk::TRIGGER_TIMER))
      _repeats.push_back(Semantic::Timer(*it));

    else if (((*it)->getType() == ipk::TRIGGER_EVENT))
      _events.push_back(Semantic::Event(*it));

    else if (((*it)->getType() == ipk::TRIGGER_FUNCTION)) {
      _functions.push_back(Semantic::Function(*it));
    }
  }
  ec::debug() << "-- End of Triggers information -- " << endl;

}

Semantic::SymbolTable::SymbolTable
Semantic::SymbolTable::createSymbolTable(slc::SymbolTable sst,
					 ipk::SymbolTable ist,
					 sis::SymbolTable mst) {

  if (!_instance)
    _instance = new Semantic::SymbolTable(sst, ist, mst);

  return *_instance;
}

sis::SymbolTable
Semantic::SymbolTable::getSIS() {
  return _mst;
}

sis::Method
Semantic::SymbolTable::getPrivateMethod(const string& iface,
					const string& method) const{

  vector<sis::Method> v = _mst.getPrivateMethods();
  for (vector<sis::Method>::const_iterator it= v.begin();
       it != v.end(); it++)
    {
      if (it->name == method and
	  it->interface == iface)
	return (*it);
    }

  // FIXME
  throw ("Not found");
}

vector<Semantic::Signature>
Semantic::SymbolTable::getMethodsByInterface(const string& interface) const {


  vector<Semantic::Signature> retval;

  slc::Iface iface;
  try {
    iface = _sst.searchIface(interface);
  }catch(...){

    throw SymbolTableException("Interfaz '" + interface +
  			       "' no encontrada");
  }
  vector<slc::Operation> ops = iface.getOperations();

  for (vector<slc::Operation>::const_iterator it=ops.begin();
       it != ops.end(); it++) {

    Semantic::Signature s(*it);
    retval.push_back(s);
  }

  return retval;
}

Semantic::Object
Semantic::SymbolTable::getObjectByName(const string& name) const {

  for (vector<Semantic::Adapter>::const_iterator it = _adapters.begin();
       it != _adapters.end(); it++) {

    vector<Semantic::Object> objs = it->getObjects();

    for (vector<Semantic::Object>::const_iterator j = objs.begin();
	 j != objs.end(); j++) {
      if (j->getName() == name)
	return *j;
    }
  }

  vector<ipk::Object> objs = _ist.getObjects();

  for (vector<ipk::Object>::const_iterator it = objs.begin();
       it != objs.end(); it++) {
    if (name == it->getName())
      return Semantic::Object(*it, "");
  }

  vector<ipk::Trigger*> triggers = _ist.getTriggers();

  for (vector<ipk::Trigger*>::const_iterator it = triggers.begin();
       it != triggers.end(); it++) {
    if ((*it)->getName() == name)
      return Semantic::Object(*(*it), "");
  }
  //FIXME
  throw ("Object Not Found!!");
}

vector<string>
Semantic::SymbolTable::getParents(const string& interface) const {
  return _sst.getInheritance(interface);
}


Semantic::SymbolTable::SymbolTable
Semantic::SymbolTable::instance() {
  return *_instance;
}

vector<Semantic::Adapter>
Semantic::SymbolTable::getAdapters() const {
  return _adapters;
}

vector<Semantic::Boot>
Semantic::SymbolTable::getBootTriggers() const {
  return _boots;
}

vector<Semantic::Function>
Semantic::SymbolTable::getFunctionTriggers() const {
  return _functions;
}

vector<Semantic::Event>
Semantic::SymbolTable::getEventTriggers() const {
  return _events;
}

vector<Semantic::Timer>
Semantic::SymbolTable::getTimerTriggers() const {
  return _repeats;
}

vector<Semantic::When>
Semantic::SymbolTable::getWhenTriggers() const {
  return _whens;
}

vector<Semantic::Object>
Semantic::SymbolTable::getLocalObjects() const {
  vector<Semantic::Object> retval;

  for (vector<Semantic::Adapter>::const_iterator it = _adapters.begin();
       it != _adapters.end(); it++) {

    if (it->getType() == ipk::LOCAL) {
      vector<Semantic::Object> obs = it->getObjects();
      retval.insert(retval.begin(), obs.begin(), obs.end());
    }
  }

  return retval;
}
vector<Semantic::Object>
Semantic::SymbolTable::getRemoteObjects() const {
  vector<Semantic::Object> retval;

  for (vector<Semantic::Adapter>::const_iterator it = _adapters.begin();
       it != _adapters.end(); it++) {

    if (it->getType() == ipk::REMOTE) {
      vector<Semantic::Object> obs = it->getObjects();
      retval.insert(retval.begin(), obs.begin(), obs.end());
    }
  }

  return retval;

}

bool
Semantic::SymbolTable::isA(const Semantic::Object& obj,
			   const string& iface) {

  if (obj.getInterface() == iface)
    return true;

  vector<string> ifaces = obj.getAllInterfaces();

  for (vector<string>::const_iterator it = ifaces.begin();
       it != ifaces.end(); it++)
    if (iface == *it)
      return true;

  return false;
}


Semantic::Adapter
Semantic::SymbolTable::getAdapterByObject(const string& name) {
  for(vector<Semantic::Adapter>::const_iterator it = _adapters.begin();
      it != _adapters.end(); it++) {

    vector<Semantic::Object> obs = it->getObjects();
    for (vector<Semantic::Object>::const_iterator ob = obs.begin();
	 ob != obs.end(); ob++) {
      if (ob->getName() == name)
	return *it;
    }
  }
  throw SymbolTableException("Object '" + name
			     + "' not found in any adapter");
}


vector<Semantic::Invocation>
Semantic::SymbolTable::getAllInvocations() {
  vector<Semantic::Invocation> retval;

  for (vector<Semantic::Boot>::const_iterator it = _boots.begin();
       it != _boots.end(); it++) {

    vector<Semantic::Invocation> invs = it->getInvocations();
    retval.insert(retval.end(), invs.begin(), invs.end());
  }

  for (vector<Semantic::Timer>::const_iterator it = _repeats.begin();
       it != _repeats.end(); it++) {

    vector<Semantic::Invocation> invs = it->getInvocations();
    retval.insert(retval.end(), invs.begin(), invs.end());
  }

  for (vector<Semantic::When>::const_iterator it = _whens.begin();
       it != _whens.end(); it++) {

    vector<Semantic::Invocation> invs = it->getInvocations();
    retval.insert(retval.end(), invs.begin(), invs.end());
  }

  for (vector<Semantic::Event>::const_iterator it = _events.begin();
       it != _events.end(); it++) {

    vector<Semantic::Invocation> invs = it->getInvocations();
    retval.insert(retval.end(), invs.begin(), invs.end());
  }

  return retval;
}

ipk::SymbolTable
Semantic::SymbolTable::getIcePickSymbolTable() const {
  return _ist;
}

bool
Semantic::SymbolTable::isParent(const string& parent,
				const string& child) {

  slc::Iface interface;
  try {
    interface = _sst.searchIface(child);
  } catch (...) {
    return false;
  }

  stack<string, vector<string> > ifaces(interface.inheritance());
  string i;
  vector<string> parents;
  while (not ifaces.empty()) {

    i = ifaces.top();
    if (i == parent)
      return true;

    ifaces.pop();

    try {
    interface = _sst.searchIface(i);
    } catch (...) {
      return false;
    }

    parents = interface.inheritance();

    for (vector<string>::const_iterator it = parents.begin();
	 it != parents.end(); it++) {
      ifaces.push(*it);
    }
  }

  return false;
}
