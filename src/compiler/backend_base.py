#!/usr/bin/python
# -*- coding:utf-8 -*-

import optparse
import logging

class BackendBase:

    def __init__(self, optparser, name):
        self.name = name

        # Esto es buena idea
        self.log = logging.getLogger(name)

        optparser.usage = "usage %s [options] <input_file>" % self.name

        try:
            optparser.add_option("-o",  dest="output",
                             help="The output filename")

            optparser.add_option("-d", dest="outdir", default='.',
                             help="Set output directory")

        except optparse.OptionConflictError:
            return
