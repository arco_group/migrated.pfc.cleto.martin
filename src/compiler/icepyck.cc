// -*- coding: utf-8 -*-

#include <boost/python.hpp>
#include <boost/python/stl_iterator.hpp>
#include <boost/python/enum.hpp>

#include <iterator>
#include <algorithm>

#include <easyc++/string.h>
#include <easyc++/logger.h>


#include "SymbolTable.h"
#include "icepyck.h"
#include "config.h"

#include "SemanticAnalyzer.h"


using namespace boost::python;

Config config;

Config make_config(list sys_argv) {
  stl_input_iterator<std::string> begin(sys_argv), end;
  std::vector<std::string> argv(begin, end);
  config.parse(argv);
  return config;
}

Config get_config() {
  return config;
}

void set_config(Config c) {
  config = c;

  if (config.debug)
    ec::Logger::root.set_level(ec::Logger::DEBUG);
}

Semantic::SymbolTable parse() {

  bool ok;

  ipk::SymbolTable ist;
  ipk::Visitor iV(&ist);
  ipk::Driver iParser;
  iParser.accept(&iV);

  ec::debug() << "++ ICEPICK SINTACTIC CHECKING ++" << std::endl;
  ok = iParser.parse(config.input);
  ec::debug() << "-- ICEPICK SINTACTIC CHECKING --" << std::endl;

  sis::SymbolTable mst;
  sis::Visitor mV(&mst);
  sis::Driver sis;
  sis.accept(&mV);

  if (config.sis != "") {
    ec::debug() << "++ SIS SINTACTIC CHECKING ++" << std::endl;
    ok = sis.parse(config.sis);
    ec::debug() << "-- SIS SINTACTIC CHECKING --" << std::endl;
  }

  if (config.parse_only) {
    if (!ok)
      exit(-1);
    exit(0);
  }

  slc::SymbolTable sst;
  slc::Visitor sV(&sst);
  slc::Parser sParser(ist.getUses());
  ec::debug() << "++ SLICE SINTACTIC CHECKING ++" << std::endl;
  sParser.accept(&sV);
  ec::debug() << "-- SLICE SINTACTIC CHECKING --" << std::endl;

  Semantic::Analyzer semanticP = Semantic::Analyzer::createAnalyzer(sst, ist, mst);
  ec::debug() << "++ SEMANTIC CHECKING ++" << std::endl;
  semanticP.parse();
  ec::debug() << "-- SEMANTIC CHECKING --" << std::endl;

  ec::debug() << "++ CREATING SYMBOLTABLE ++" << std::endl;
  Semantic::SymbolTable retval = Semantic::SymbolTable::createSymbolTable(sst,
									  ist,
									  mst);
  ec::debug() << "-- CREATING SYMBOLTABLE --" << std::endl;
  return retval;
}


Semantic::SymbolTable parse_from_args(list sys_argv) {

  stl_input_iterator<std::string> begin(sys_argv), end;
  std::vector<std::string> argv(begin, end);

  config.parse(argv);
  return parse();
}



list
test_vector_pair() {

  list retval;
  retval.append(make_tuple("HOLA","ADIOS"));
  return retval;
}


template<class T>
struct pair_item {
  typedef std::pair<T,T> Pair;

  static T& get(Pair& self, unsigned int i) {
    if (i > 1) {
      PyErr_SetString(PyExc_IndexError,"Pair index error");
      throw_error_already_set();
    }

    if (i == 0) return self.first;
    return self.second;
  }

  static std::string str(Pair& self) {
    std::string retval("{");
    retval += self.first + ", " + self.second + ")";
    return retval;
  }
};

template<class Key, class Val>
struct map_item {
  typedef std::map<Key,Val> Map;

  static Val& get(Map& self, Key const& idx) {
    if (self.find(idx) == self.end()) {
      PyErr_SetString(PyExc_KeyError,"Map key not found");
      throw_error_already_set();
    }
    return self[idx];
  }

  static void set(Map& self, const Key idx, const Val val) { self[idx]=val; }

  static void del(Map& self, const Key n) { self.erase(n); }

  static bool in(Map const& self, const Key n) { return self.find(n) != self.end(); }

  static list keys(Map const& self) {
	list t;
	for (typename Map::const_iterator it=self.begin(); it!=self.end(); ++it)
	  t.append(it->first);
	return t;
  }
  static list values(Map const& self) {
	list t;
	for (typename Map::const_iterator it=self.begin(); it!=self.end(); ++it)
	  t.append(it->second);
	return t;
  }
  static list items(Map const& self) {
	list t;
	for (typename Map::const_iterator it=self.begin(); it!=self.end(); ++it)
	  t.append(make_tuple(it->first, it->second));
	return t;
  }
  static std::string str(Map& self) {
    std::string retval("{");

    for (typename Map::const_iterator it=self.begin(); it!=self.end(); ++it) {
      retval = retval + (*it).first + ":" + (*it).second + ", ";
    }

    retval = retval + "}";
    return retval;
  }

};


void IndexError() {
  PyErr_SetString(PyExc_IndexError, "Index out of range");
  throw_error_already_set();
}

template<class T>
struct vector_item {

  typedef std::vector<T> Vector;

  static T& get(Vector& self, int i) {
    if (i<0) i+=self.size();
    if (i>=0 && (unsigned int)i<self.size())
      return self[i];
    IndexError();
    return self[i]; // dead code
  }
  static void set(Vector& self, unsigned int i, T const& v) {
    if( i<0 ) i+=self.size();
    if( i>=0 && i<self.size() ) {
      //x[i]=v;
      typename Vector::iterator it = self.begin() + i;
      self.erase(it);
      self.insert(it, v);
    }
    else IndexError();
  }
  static void del(Vector& self, unsigned int i) {
    if( i<0 ) i+=self.size();
    if( i>=0 && i<self.size() ) {
      typename Vector::iterator it = self.begin() + i;
      self.erase(it);
    }
    else IndexError();
  }
  static void add(Vector& self, T const& v) {
    self.push_back(v);
  }
  static bool is_equal(Vector& self, list the_other) {
    stl_input_iterator<std::string> begin(the_other), end;
    std::vector<std::string> other(begin, end);

    sort(self.begin(), self.end());
    sort(other.begin(), other.end());

    return equal(self.begin(), self.end(), other.begin());
  }
  static std::string str(Vector& self) {

    std::ostringstream oss;
    oss << "[";
    std::copy(self.begin(), self.end(), std::ostream_iterator<T>(oss, ", "));
    oss << "]";
    return oss.str();

//    std::string retval("[");
//    for (unsigned int i = 0; i<self.size(); i++) {
//      retval = retval + "\"" + self[i] +  "\"";
//      if (i != self.size() - 1)
//	retval += ", ";
//    }
//
//    retval = retval + "]";
//    return retval;

  }
  //    static bool in(Vector const& self, T const& v)
  //    {
  //        return find_eq(self.begin, self.end, v) != self.end();
  //    }
};


typedef std::vector<std::string>           VectorStr;
typedef std::pair<std::string,std::string> PairStr;
typedef std::vector<PairStr>               VectorPairStr;
typedef std::vector<Semantic::Adapter>     VectorAdapters;
typedef std::vector<Semantic::Object>      VectorObjects;
typedef std::vector<Semantic::Signature>   VectorSignatures;
typedef std::vector<Semantic::Boot>        VectorBootTriggers;
typedef std::vector<Semantic::Function>    VectorFunctionTriggers;
typedef std::vector<Semantic::Event>       VectorEventTriggers;
typedef std::vector<Semantic::When>        VectorWhenTriggers;
typedef std::vector<Semantic::Timer>      VectorTimerTriggers;
typedef std::vector<Semantic::Invocation>  VectorInvocations;
typedef std::vector<Semantic::Parameter>   VectorParameters;
typedef std::vector<Semantic::LocalVariable>  VectorLocalVariable;

typedef std::vector<sis::Event>         VectorEvent;
typedef std::vector<sis::Method>        VectorMethod;
typedef std::vector<int> VectorSize;

typedef std::map<std::string,std::string>  MapDict;



list
PySymbolTable::getAllObjectNames() {

// list retval;

//   VectorStr source = _st.getAllObjectNames();
//   for(VectorStr::iterator it = source.begin(); it != source.end(); ++it) {
//     retval.append(*it);
//     std::cout << *it << std::endl;
//   }

  return list();
}


void exportSIS()
{
  // map the SIS namespace to a sub-module
  // make "from mypackage.SIS import <whatever>" work
  object sisModule(handle<>(borrowed(PyImport_AddModule("IcePyck.sis"))));
  // make "from mypackage import Util" work
  scope().attr("sis") = sisModule;
  // set the current scope to the new sub-module
  scope sis_scope = sisModule;
  // export stuff in the SIS namespace


  class_<sis::Driver>("Driver")
    .def("parse",  &sis::Driver::parse)
    ;

  class_<sis::SymbolTable>("SymbolTable")
    .def("getPublicMethods",  &sis::SymbolTable::getPublicMethods)
    .def("getPrivateMethods",  &sis::SymbolTable::getPrivateMethods)
    .def("getEvents",  &sis::SymbolTable::getEvents)
    ;

  class_<sis::Event>("Event")
    .def_readonly("name", &sis::Event::name)
    .def_readonly("code", &sis::Event::code)
    ;

  class_<sis::Method>("Method")
    .def_readonly("name"   , &sis::Method::name)
    .def_readonly("iface"  , &sis::Method::interface)
    .def_readonly("code"   , &sis::Method::code)
    .def_readonly("input"  , &sis::Method::input)
    .def_readonly("output" , &sis::Method::output)
    ;

  class_<VectorEvent>("VectorEvent")
    .def("__len__",     &VectorEvent::size)
    .def("clear",       &VectorEvent::clear)
    .def("append",      &vector_item<sis::Event>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<sis::Event>::get, return_value_policy<copy_non_const_reference>())
    ;

  class_<VectorMethod>("VectorMethod")
    .def("__len__",     &VectorMethod::size)
    .def("clear",       &VectorMethod::clear)
    .def("append",      &vector_item<sis::Method>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<sis::Method>::get, return_value_policy<copy_non_const_reference>())
    ;

  class_<VectorSize>("VectorSize")
    .def("__len__",     &VectorSize::size)
    .def("clear",       &VectorSize::clear)
    .def("append",      &vector_item<int>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<int>::get, return_value_policy<copy_non_const_reference>())
    ;
}

BOOST_PYTHON_MODULE(IcePyck)
{
  class_<VectorStr>("VectorStr")
    .def("__len__",     &VectorStr::size)
    .def("clear",       &VectorStr::clear)
    .def("append",      &vector_item<std::string>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<std::string>::get, return_value_policy<copy_non_const_reference>())
    .def("__setitem__", &vector_item<std::string>::set, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__delitem__", &vector_item<std::string>::del)
    .def("__iter__", 	iterator<VectorStr>())
    .def("__eq__",      &vector_item<std::string>::is_equal)
    .def("__str__", 	&vector_item<std::string>::str)
    ;

  class_<VectorAdapters>("VectorAdapters")
    .def("__len__",     &VectorAdapters::size)
    .def("clear",       &VectorAdapters::clear)
    .def("append",      &vector_item<Semantic::Adapter>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<Semantic::Adapter>::get, return_value_policy<copy_non_const_reference>())
    ;

  class_<VectorSignatures>("VectorSignatures")
    .def("__len__",     &VectorSignatures::size)
    .def("clear",       &VectorSignatures::clear)
    .def("append",      &vector_item<Semantic::Signature>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<Semantic::Signature>::get, return_value_policy<copy_non_const_reference>())
    ;

  class_<VectorObjects>("VectorObjects")
    .def("__len__",     &VectorObjects::size)
    .def("clear",       &VectorObjects::clear)
    .def("append",      &vector_item<Semantic::Object>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<Semantic::Object>::get, return_value_policy<copy_non_const_reference>())
    ;

  class_<VectorInvocations>("VectorInvocations")
    .def("__len__",     &VectorInvocations::size)
    .def("clear",       &VectorInvocations::clear)
    .def("append",      &vector_item<Semantic::Invocation>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<Semantic::Invocation>::get, return_value_policy<copy_non_const_reference>())
    ;

  class_<VectorBootTriggers>("VectorBootTriggers")
    .def("__len__",     &VectorBootTriggers::size)
    .def("clear",       &VectorBootTriggers::clear)
    .def("append",      &vector_item<Semantic::Boot>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<Semantic::Boot>::get, return_value_policy<copy_non_const_reference>())
    ;

  class_<VectorFunctionTriggers>("VectorFunctionTriggers")
    .def("__len__",     &VectorFunctionTriggers::size)
    .def("clear",       &VectorFunctionTriggers::clear)
    .def("append",      &vector_item<Semantic::Function>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<Semantic::Function>::get, return_value_policy<copy_non_const_reference>())
    ;

  class_<VectorTimerTriggers>("VectorTimerTriggers")
    .def("__len__",     &VectorTimerTriggers::size)
    .def("clear",       &VectorTimerTriggers::clear)
    .def("append",      &vector_item<Semantic::Timer>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<Semantic::Timer>::get, return_value_policy<copy_non_const_reference>())
    ;


  class_<VectorWhenTriggers>("VectorWhenTriggers")
    .def("__len__",     &VectorWhenTriggers::size)
    .def("clear",       &VectorWhenTriggers::clear)
    .def("append",      &vector_item<Semantic::When>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<Semantic::When>::get, return_value_policy<copy_non_const_reference>())
    ;

  class_<VectorLocalVariable>("VectorLocalVariable")
    .def("__len__",     &VectorLocalVariable::size)
    .def("clear",       &VectorLocalVariable::clear)
    .def("append",      &vector_item<Semantic::LocalVariable>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<Semantic::LocalVariable>::get, return_value_policy<copy_non_const_reference>())
    ;

  class_<VectorEventTriggers>("VectorEventTriggers")
    .def("__len__",     &VectorEventTriggers::size)
    .def("clear",       &VectorEventTriggers::clear)
    .def("append",      &vector_item<Semantic::Event>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<Semantic::Event>::get, return_value_policy<copy_non_const_reference>())
    ;

  class_<VectorParameters>("VectorParameters")
    .def("__len__",     &VectorParameters::size)
    .def("clear",       &VectorParameters::clear)
    .def("append",      &vector_item<Semantic::Parameter>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<Semantic::Parameter>::get, return_value_policy<copy_non_const_reference>())
    ;

  class_<PairStr>("PairStr")
    .def("__getitem__", &pair_item<std::string>::get,  return_value_policy<copy_non_const_reference>())
    .def("__str__",     &pair_item<std::string>::str)
    ;

  class_<VectorPairStr>("VectorPairStr")
    .def("__len__",     &VectorPairStr::size)
    .def("clear",       &VectorPairStr::clear)
    .def("append",      &vector_item<PairStr>::add, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__getitem__", &vector_item<PairStr>::get, return_value_policy<copy_non_const_reference>())
    .def("__setitem__", &vector_item<PairStr>::set, with_custodian_and_ward<1,2>()) // to let container keep value
    .def("__delitem__", &vector_item<PairStr>::del)
    .def("__iter__", 	iterator<VectorPairStr>())
    .def("__str__", 	&vector_item<PairStr>::str)
    ;

  class_<MapDict>("MapDict")
    .def("__len__",      &MapDict::size)
    .def("__getitem__",  &map_item<std::string,std::string>::get, return_value_policy<copy_non_const_reference>())
    .def("__setitem__",  &map_item<std::string,std::string>::set)
    .def("__delitem__",  &map_item<std::string,std::string>::del)
    .def("clear",        &MapDict::clear)
    .def("__contains__", &map_item<std::string,std::string>::in)
    .def("has_key",      &map_item<std::string,std::string>::in)
    .def("keys",         &map_item<std::string,std::string>::keys)
    .def("values",       &map_item<std::string,std::string>::values)
    .def("items",        &map_item<std::string,std::string>::items)
    .def("__str__",      &map_item<std::string,std::string>::str)
    ;

  class_<Semantic::SymbolTable>("SymbolTable", no_init)
    .def("getMethodsByInterface",
	 &Semantic::SymbolTable::getMethodsByInterface)
    .def("getAdapters",        &Semantic::SymbolTable::getAdapters)
    .def("getBootTriggers",    &Semantic::SymbolTable::getBootTriggers)
    .def("getFunctionTriggers",&Semantic::SymbolTable::getFunctionTriggers)
    .def("getEventTriggers",   &Semantic::SymbolTable::getEventTriggers)
    .def("getTimerTriggers",   &Semantic::SymbolTable::getTimerTriggers)
    .def("getWhenTriggers",    &Semantic::SymbolTable::getWhenTriggers)
    .def("getLocalObjects",    &Semantic::SymbolTable::getLocalObjects)
    .def("getRemoteObjects",   &Semantic::SymbolTable::getRemoteObjects)
    .def("getObjectByName",    &Semantic::SymbolTable::getObjectByName)
    .def("getSIS",             &Semantic::SymbolTable::getSIS)
    .def("isA",                &Semantic::SymbolTable::isA)
    .def("isParent",           &Semantic::SymbolTable::isParent)
    .def("getAdapterByObject", &Semantic::SymbolTable::getAdapterByObject)
    .def("getAllInvocations",  &Semantic::SymbolTable::getAllInvocations)
    ;

  class_<Semantic::Adapter>("Adapter", no_init)
    .def("getName",            &Semantic::Adapter::getName)
    .def("getEndpoint",        &Semantic::Adapter::getEndpoint)
    .def("getType",            &Semantic::Adapter::getType)
    .def("getObjects",         &Semantic::Adapter::getObjects)
    ;

  class_<Semantic::Object>("Object", no_init)
    .def("getInterface",     &Semantic::Object::getInterface)
    .def("getIdentity",      &Semantic::Object::getIdentity)
    .def("getName",          &Semantic::Object::getName)
    .def("getMode",          &Semantic::Object::getMode)
    .def("getMethods",       &Semantic::Object::getMethods)
    .def("getAllInterfaces", &Semantic::Object::getAllInterfaces)
    .def("getAttributes",    &Semantic::Object::getAttributes)
    .def("__eq__",           &Semantic::Object::operator==)
    ;

  class_<Semantic::Signature>("Signature", no_init)
    .def("getMode",            &Semantic::Signature::getMode)
    .def("getName",            &Semantic::Signature::getName)
    .def("getIface",           &Semantic::Signature::getInterface)
    .def("getInParams",        &Semantic::Signature::getInputParams)
    .def("getOutParams",       &Semantic::Signature::getOutputParams)
    .def("__eq__",             &Semantic::Signature::operator==)
    ;

  class_<Semantic::Invocation>("Invocation", no_init)
    .def("getObject",             &Semantic::Invocation::getObject)
    .def("getMethod",             &Semantic::Invocation::getMethod)
    .def("getParameters",         &Semantic::Invocation::getParameters)
    .def("getCondition",          &Semantic::Invocation::getCondition)
    ;

  class_<Semantic::Condition>("Condition", no_init)
    .def("getParams",             &Semantic::Condition::getParameters)
    .def("getType",               &Semantic::Condition::getType)
    ;

  class_<Semantic::Trigger>("Trigger", no_init)
    .def("getInvocations",        &Semantic::Trigger::getInvocations)
    .def("getLabel",              &Semantic::Trigger::getLabel)
    ;

  class_<Semantic::Event, bases<Semantic::Trigger> >("Event", no_init)
    .def("getEvent",        &Semantic::Event::getEvent)
    ;

  class_<Semantic::When, bases<Semantic::Trigger> >("When", no_init)
    .def("getMethod",         &Semantic::When::getMethod)
    .def("getLocalVariables", &Semantic::When::getLocalVariables)
    ;

  class_<Semantic::Boot, bases<Semantic::Trigger> >("Boot", no_init)
    ;

  class_<Semantic::Function, bases<Semantic::Trigger> >("Function", no_init)
    ;

  class_<Semantic::Timer, bases<Semantic::Trigger> >("Timer", no_init)
    .def("getTimeout",        &Semantic::Timer::getTimeout)
    ;

  enum_<ipk::Type>("ParameterType")
    .value("LITERAL", ipk::literal)
    .value("IDENTIFIER", ipk::identifier)
    .value("ATTRIBUTE", ipk::attribute)
    .value("NUMERIC", ipk::numeric)
    .value("BOOLEAN", ipk::boolean)
    .value("INVOCATION", ipk::invocation)
    .value("DICTIONARY", ipk::dictionary)
    .export_values()
    ;

  enum_<ipk::OperatorType>("OperatorType")
    .value("EQ",   ipk::OP_EQ)
    .value("NEQ",  ipk::OP_NEQ)
    .value("GT",   ipk::OP_GT)
    .value("LT",   ipk::OP_LT)
    .value("GTEQ", ipk::OP_GTEQ)
    .value("LTEQ", ipk::OP_LTEQ)
    .export_values()
    ;


  class_<Semantic::Parameter>("Parameter")
    .def("getType", &Semantic::Parameter::getType)
    .def("getValueAsInteger", &Semantic::Parameter::getValueAsInteger)
    .def("getValueAsBoolean", &Semantic::Parameter::getValueAsBoolean)
    .def("getValueAsString", &Semantic::Parameter::getValueAsString)
    .def("getValueAsInvocation", &Semantic::Parameter::getValueAsInvocation)
    .def("getValueAsDictionary", &Semantic::Parameter::getValueAsDictionary)
    ;

  class_<PySymbolTable>("PySymbolTable",
			init<Semantic::SymbolTable>())
    //.def("getInputParameters",    &PySymbolTable::getInputParameters)
    //    .def("getAllObjectNames",     &PySymbolTable::getAllObjectNames)
    ;


  class_<Config>("Config")
    .def_readwrite("input",    &Config::input)
    .def_readwrite("output",   &Config::output)
    .def_readwrite("sis",      &Config::sis)
    .def_readwrite("paths",    &Config::paths)
    .def_readwrite("debug",    &Config::debug)
    .def_readwrite("parse_only",        &Config::parse_only)
    .def("add_path", &Config::add_path)
    ;

  class_<Semantic::LocalVariable>("LocalVariable")
    .def_readwrite("var_type",  &Semantic::LocalVariable::varType)
    .def_readwrite("value",   &Semantic::LocalVariable::value)
    ;

  enum_<ipk::ObjectMode>("ObjectMode")
    .value("ONEWAY", ipk::ONEWAY)
    .value("TWOWAY", ipk::TWOWAY)
    .value("DATAGRAM", ipk::DATAGRAM)
    .export_values()
    ;

  enum_<ipk::AdapterType>("AdapterType")
    .value("LOCAL", ipk::LOCAL)
    .value("REMOTE", ipk::REMOTE)
    .export_values()
    ;

  enum_<ipk::MethodType>("MethodType")
    .value("IDEMPOTENT", ipk::IDEMPOTENT)
    .value("NORMAL", ipk::NORMAL)
    .export_values()
    ;

  def("make_config", make_config);
  def("get_config", get_config);
  def("set_config", set_config);
  def("parse", parse);

  def("test_vector_pair", test_vector_pair);

  exportSIS();


}



//http://www.boost.org/doc/libs/1_35_0/libs/python/doc/tutorial/doc/html/python/exposing.html
