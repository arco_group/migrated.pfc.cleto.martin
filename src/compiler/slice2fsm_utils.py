#!/usr/bin/python
# -*- coding:utf-8 -*-

import re, struct
import logging


import sys, os
import optparse

BASEDIR = os.path.dirname(__file__)

sys.path.append(os.path.join(BASEDIR, 'lib'))


import IcePyck as ipk

log = logging.getLogger('ipk')
console = logging.StreamHandler()
console.setFormatter(logging.Formatter('%(levelname)s[%(name)s]:  %(message)s'))
log.addHandler(console)
log.setLevel(logging.ERROR)

from fsm2data import toByteSeq, Literal, run


mode2str = {ipk.ONEWAY:"-o",
           ipk.TWOWAY:"-t",
           ipk.DATAGRAM:"-d"
           }

class Ice_op_mode:
    NORMAL, NONMUTATING, IDEMPOTENT = range(3)


modes = {ipk.NORMAL:Ice_op_mode.NORMAL,
         ipk.IDEMPOTENT:Ice_op_mode.IDEMPOTENT,
         }

SIZES = {"TCP_IDENTITY_SIZE":10,
         "TCP_ENDPOINT_SIZE":17,
         "TCP_ADDRESS_SIZE": 15,
         "TCP_PORT_SIZE":4,
         "XBOW_IDENTITY_SIZE":4,
         "XBOW_ENDPOINT_SIZE":3,
         "XBOW_ADDRESS_SIZE":2,
         "XBOW_PORT_SIZE":2}

PROTO = {"tcp":1,
         "xbow":18,
         "udp":3}

def plain_list(data):
    if not isinstance(data, list):
        return [data]

    retval = []

    for i in data:
        if not isinstance(i, list):
            retval.extend([i])
        else:
            retval.extend(plain_list(i))

    return retval

def expand_values(data):
    retval = []
    for i in data:
        if isinstance(i, list):
            for j in i:
                retval.append(j)
        elif isinstance(i, str):
            for c in i:
                retval.append(ord(c))

        else:
            retval.append(i)

    return retval

def plain(tid):
    return tid.strip(':').replace('::', '_').replace('.', '_')

def unplain(tid):
    return "::" + tid.replace('_', '::')


def count(data):
    unplained = [list, str, tuple]
    retval = 0

    if not type(data) in unplained:
        return 1

    if isinstance(data, str):
        return len(data)

    for i in data:
        retval += count(i)

    return retval

def merge(*input):
    return reduce(list.__add__, input, list())

def merge_uniq(*input):
    return list(reduce(set.union, input, set()))


# está también en icepick
def value_get(value, fallback):
    return value if value else fallback


def digest(val):
    retval = 0
    for v in [ord(x) for x in val]:
        retval ^= ((v & 0xF)<<4) | ((v>>4) & 0xF)
    return retval


class IceProxy:

    def __init__(self, strproxy):

        if not strproxy.strip():
            self.oidName = 'Null'
            self.oidCat = ''
            self.proxyData = [0, 0]
            self.endpoint =  NullEndpoint()
            self.size = 2
            return

        marshaller = {'tcp':  TcpEndpoint,
                      'udp':  UdpEndpoint,
                      'xbow': XbowEndpoint,
                      }

        # Manual > Ice Protocol > Data Encoding > Proxies
        modes = {'-t':0, '-o': 1, '-d':3}

        match_obj = re.search(\
            "(.+)\s+(-\D)?\s*:"   # identity, mode
            "s*(\w+)\s+"            # protocol
            "(.*)",                 # endpoint params
            strproxy)

        oid, mode, proto, endpoint = match_obj.groups()

        try:
            self.oidCat, self.oidName = oid.split('/')
        except ValueError, e:
            log.warning("IceProxy: Empty category in: '%s'" % oid)
            self.oidCat = ''
            self.oidName = oid


        self.packed_oid = [count(self.oidName), self.oidName,
                           count(self.oidCat),  self.oidCat,]

        self.proxyData = \
            self.packed_oid + \
            [0,  # facet
             modes[mode],
             0,  # secure
             1,  # nº de endpoints FIXME: esto aquí.. puff
             ]


        # hay un parser de endpoint para cada tipo
        self.proto = proto
        self.endpoint = marshaller[proto](endpoint)

        self.size = count(self.proxyData) + self.endpoint.size

    def packed(self):
        return [self.proxyData] + self.endpoint.data


    def get_identity(self):
        if self.oidCat:
            return "%s/%s" % (self.oidCat, self.oidName)
        return self.oidName


def parse_ip_endpoint(endpoint):
    match_obj = re.search(\
        "-h\s+(\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3})\s+"  # host
        "-p\s+(\d{1,5})",       # port
        endpoint)
    try:
        host, port = match_obj.groups()
    except AttributeError:
        return  None

    return host, port

def parse_xbow_endpoint(endpoint):

    match_obj = re.search(\
        "-h\s+(0x[a-fA-F0-9]{1,4})(\s*-d)?", # endpoint destino
        endpoint)

    destination, debug = None, None

    try:
        destination, debug = match_obj.groups()
        if not debug:
            debug = False
        else:
            debug = True
    except AttributeError:
        raise Exception("Endpoint %s malformed" % endpoint)

    return destination, debug

class TcpEndpoint:

    def __init__(self, endpoint):
        self.host, self.port = parse_ip_endpoint(endpoint)
        self.dynamic = True

        val = [[1,0],    # encoding
               15,       # host len
               self.host.ljust(15),
               toByteSeq(struct.pack('<L', int(self.port))),
               [0xFF, 0xFF, 0xFF, 0xFF],  # timeout
               0]        # compress


        self.size = count(val) + 4
        self.data = [[1,0]] + [toByteSeq(struct.pack('<L', self.size))] + val
        self.size += 2

        if self.host != '000.000.000.000':
            self.dynamic = False
            return

        self.data = [self.data[:4], None, self.data[5:]]

    def get_socket(self):
        return ['T', self.host.ljust(15),
                [ord(x) for x in struct.pack('H', int(self.port))]]


    def hash(self):
        val = hash(self.get_socket)
        return '%s' % -1*val if val < 0 else val


class UdpEndpoint:
    pass


class XbowEndpoint:
    def __init__(self, endpoint):
        self.host, self.debug = parse_xbow_endpoint(endpoint)
        self.dynamic = True

        val = [1,0,1,0,'X',toByteSeq(struct.pack('H', int(self.host,16)))]

#         val.append(int(self.debug))
        self.size = count(val) + struct.calcsize('L')
        # Protocol [18,0] : xbow

        self.data = [[18,0]] + [toByteSeq(struct.pack('L', self.size))] + val
        self.size += 2
        if int(self.host, 16) > 0:
            self.dynamic = False
            return

        self.data = [self.data[:4], None, self.data[5:]]

    def get_socket(self):
        allocation = ['X', toByteSeq(struct.pack('H', int(self.host, 16)))]
        return allocation

    def hash(self):
        val = hash(self.get_socket)
        return '%s' % -1*val if val < 0 else val


class NullEndpoint:

    def __init__(self):
        self.size = 0
        self.data = []

    def get_socket(self):
        return None
