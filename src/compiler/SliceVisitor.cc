// -*- coding:utf-8 -*-

#include "SliceVisitor.h"
#include "config.h"

#include <Slice/CPlusPlusUtil.h>
#include <Slice/Preprocessor.h>
#include <easyc++/logger.h>
#include <easyc++/os.h>

#include <stack>
#include <list>
#include <cstring>

using namespace std;
using namespace Slice;
using namespace IceUtil;
using namespace IceUtilInternal;

namespace slc {

  Module::Module(const string& name, const string& scope)
    : _name(name), _scope(scope) {}

  Module::Module() {}

  void
  Module::addModule(const Module& module) {

    if (module.name() == "IPK")
      throw SliceParserException("Can not be declared a module named 'IPK'");

    for (unsigned int i=0; i<_modules.size(); i++) {
      if (_modules[i].name() == module.name()) {
	//FIXME
	return;
      }
    }

    ec::debug() << "SliceModule: added " << module.scope() << module.name() << endl;
    _modules.push_back(module);
  }

  void
  Module::addIface(const Iface& iface){
    _ifaces.push_back(iface);
  }

  vector<Module>
  Module::modules() const {
    return _modules;
  }

  vector<Module*>
  Module::getModules() const {

    vector<Module*> retval;

    for(unsigned int i=0; i<_modules.size(); i++)
      retval.push_back(const_cast<Module*>(&_modules[i]));

    return retval;
  }

  vector<Iface>
  Module::ifaces() const {
    return _ifaces;
  }

  string
  Module::name() const {
    return _name;
  }

  string
  Module::scope() const {
    return _scope;
  }

  Module
  Module::searchModule(const string& module) const {

    for(vector<Module>::const_iterator it = _modules.begin();
	it != _modules.end(); it++) {
      if (module == it->scope()+it->name()) {
	return *it;
      }
      try {
	return it->searchModule(module);
      }
      catch(NotFoundException e) {}
    }
    throw NotFoundException("Module " + module + " not found");
  }


  //
  // Iface
  //

  Iface::Iface(const string& name, const string& scope)
    : _name(name), _scope(scope) {}

   Iface::Iface() {
     //     _inheritance.push_back("::Ice::Object");
   }

  bool
  Iface::operator==(const Iface& other) const {
    return (this->name() == other.name()
	    and
	    this->scope() == other.scope());
  }

  void
  Iface::addInheritance(const string& scope, const string& name) {

    string iface = scope+name;
    if (iface == this->name())
      return;

    bool found = false;
    for (unsigned int i=0; i<_inheritance.size(); i++) {
      if (_inheritance[i] == iface) {
	found = true;
	break;
      }
    }
    if (!found) {
      _inheritance.push_back(iface);
    }
  }

  vector<string>
  Iface::inheritance() {
    return _inheritance;
  }

  void
  Iface::addOperation(const Operation& op) {
    _operations.push_back(op);

  }

  string
  Iface::scope() const {
    return _scope;
  }


  vector<Operation>
  Iface::getOperations() const{
    return _operations;
  }

  string
  Iface::name() const {
    return _name;
  }

  //
  // Param
  //

  Param::Param(const string& type, const string& name)
    : _type(type), _name(name) {}


  string
  Param::getName() const{
    return _name;
  }

  string
  Param::getType() const{
    return _type;
  }

  //
  // Operation
  //

  Operation::Operation(const string& type, const string& name,
		       const string& iface)
    : _name(name), _type(type), _iface(iface)   {
  }

  Operation::Operation(const string& iface, const string& name)
    : _name(name), _type("void"), _iface(iface) {}

  Operation::Operation() {}

  bool
  Operation::operator==(const Operation& other) const {
    return (this->getInterface() == other.getInterface()
	    and
	    this->getName() == other.getName());
  }

  void
  Operation::addMetadata(const string& name, const string& val) {
    //  _metadata.insert(name, val);
    _metadata[name] = val;
  }

  void
  Operation::addInputParam(const Param& param) {
    _inputParams.push_back(param);
  }

  void
  Operation::addOutputParam(const Param& param) {
    _outputParams.push_back(param);
  }

  vector<Param>
  Operation::getOutputParameters() const{
    return _outputParams;
  }

  vector<Param>
  Operation::getInputParameters() const{
    return _inputParams;
  }


  map<string, string>
  Operation::getMetadata() const{
    return _metadata;
  }

  string
  Operation::getName() const{
    return _name;
  }

  string
  Operation::getType() const{
    return _type;
  }

  string
  Operation::getInterface() const{
    return _iface;
  }
  //
  // SymbolTable
  //

  SymbolTable::SymbolTable() {
    //Add an initial definition of Ice::Object
    Module m("Ice", "::");
    Iface i("Object", "::Ice::");

    map<string,string> methods;
    methods["ice_ping"] = "void";
    methods["ice_isA"]  = "bool";
    methods["ice_id"]   = "string";
    methods["ice_ids"]  = "::Ice::StrSeq";

    map<string, string>::iterator it;

    for (it = methods.begin(); it != methods.end(); it++) {
      Operation op("idempotent", (*it).first, "::Ice::Object");
      op.addOutputParam(Param((*it).second, ""));
      i.addOperation(op);
    }

    m.addIface(i);
    addModule(m);

    // Add ::IPK::Trigger
    m = Module("IPK", "::");
    i = Iface("Trigger", "::IPK::");
    Operation op("::IPK::Trigger", "enable");
    op.addInputParam(Param("bool", "value"));
    op.addOutputParam(Param("void", ""));
    i.addOperation(op);
    m.addIface(i);
    addModule(m);

    i = Iface("Timer", "::IPK::");
    i.addInheritance(string(fixKwd("::IPK::")), string(fixKwd("Trigger")));
    op = Operation("::IPK::Timer", "reset");
    op.addOutputParam(Param("void", ""));
    i.addOperation(op);
    m.addIface(i);
    addModule(m);

    i = Iface("Function", "::IPK::");
    i.addInheritance(string(fixKwd("::IPK::")), string(fixKwd("Trigger")));
    op = Operation("::IPK::Function", "run");
    op.addOutputParam(Param("void", ""));
    i.addOperation(op);
    m.addIface(i);
    addModule(m);

  }

  void SymbolTable::addModule(const Module& module) {

    for (unsigned int i=0; i<_modules.size(); i++) {
      if (_modules[i].name() == module.name()) {
	ec::debug() << "SymbolTable: "
		    << _modules[i].scope() + _modules[i].name()
		    << " is registered. Need to be update." << endl;

	updateModule(&_modules[i], &module);
	return;
      }
    }

    ec::debug() << "SymbolTable: added "
		<< module.scope() + module.name()
		<< endl;

    _modules.push_back(module);
  }

  void SymbolTable::addClass() {}

  void SymbolTable::addIPKModule(const ipk::Module& module) {
    Module m(module.name);

    vector<ipk::Iface> ifaces = module.interfaces;
    for (vector<ipk::Iface>::const_iterator it = ifaces.begin();
	 it != ifaces.end(); it++) {

      Iface i(it->name, "::" + module.name + "::");
      vector<ipk::Method> methods = it->methods;
      for (vector<ipk::Method>::const_iterator j = methods.begin();
	   j != methods.end(); j++) {
	Operation op(j->returnType, j->name,
		     "::" + module.name + "::" + it->name);
	op.addOutputParam(Param(j->returnType, ""));
	vector<string> types = j->paramTypes;
	for (vector<string>::const_iterator k = types.begin();
	     k != types.end(); k++) {
	  op.addInputParam(Param(*k, ""));
	}
	i.addOperation(op);
      }
      m.addIface(i);
    }
    _modules.push_back(m);
  }

  void
  SymbolTable::updateModule(Module* old,
			    const Module* actual) {


    //Incluir las nuevas interfaces definidas
    vector<Iface> oldIfaces = old->ifaces();
    vector<Iface> ifaces = actual->ifaces();
    bool found = false;
    for (unsigned int i = 0; i < ifaces.size(); i++ ) {
      for (unsigned int j = 0; j < oldIfaces.size(); j++ ) {
	if (oldIfaces[j].name() != ifaces[i].name()) {
	  ec::debug() << "SymbolTable: adding " << ifaces[i].name()
		      << " as interface of module " << old->scope()
		      << old->name()
		      << endl;

	  old->addIface(ifaces[i]);
	  break;
	}
      }
    }

    //Incluir los nuevos modulos y modificar, si prodece, los existentes
    vector<Module*> oldModules = old->getModules();
    vector<Module*> newModules = actual->getModules();

    found = false;
    for (unsigned int i=0; i<newModules.size(); i++) {
      for (unsigned int j = 0; j < oldModules.size(); j++ ) {
	if (newModules[i]->name() == oldModules[j]->name()) {
	  updateModule(oldModules[j] , newModules[i]);
	  found = true;
	}

      }
      if (!found) {
	old->addModule(*newModules[i]);
      }
      found = false;
    }
  }

  Module
  SymbolTable::searchModule(const string& module) const {

    vector<Module>::const_iterator it;
    for(it = _modules.begin(); it != _modules.end(); it++) {
      if(0 == module.compare(it->scope()+it->name()))
	return *it;

      try {
	return it->searchModule(module);
      }
      catch(NotFoundException e){ }
    }
    throw NotFoundException("Module " + module + "  not found");
  }

  vector<Module>
  SymbolTable::modules() {
    return _modules;
  }

  Iface
  SymbolTable::searchIface(const string& iface) const {

    int pos = iface.rfind(':');
    string moduleName = iface.substr(0,pos - 1);
    string ifaceName = iface.substr(pos+1, iface.size()-1);

    Module module;
    try {
      module = searchModule(moduleName);
    } catch (NotFoundException e) {
      throw e;
    }

    vector<Iface> preIfaces = module.ifaces();
    vector<Iface>::iterator ifaceIt;
    Iface aux(ifaceName, moduleName + "::");
    ifaceIt = find(preIfaces.begin(), preIfaces.end(), aux);

    if (ifaceIt == preIfaces.end())
      throw NotFoundException("Interface " + aux.name() + " not found");
    return *ifaceIt;

    //    for(vector<Iface>::iterator ifaceIt = preIfaces.begin();
    //        ifaceIt != preIfaces.end(); ifaceIt++) {
    //      if (ifaceName == ifaceIt->name()) {
    //        cout << "Found " << moduleName << "::" <<ifaceName << endl;
    //        return *ifaceIt;
    //      }
    //    }
    //    throw "Not found";
  }

  vector<string>
  SymbolTable::getInheritance(const string& base) const {
    vector<string> retval;

    if (base == "::Ice::Object")
      return retval;

    if (base == "::IPK::Trigger")
      return retval;

    if (base.find("::IPK::") == base.npos)
      retval.push_back("::Ice::Object");

    Iface iface;
    try {
      iface = searchIface(base);
    }
    catch (NotFoundException e) {
      throw e;
    }

    vector<string> inh = iface.inheritance();
    for (vector<string>::const_iterator it = inh.begin();
	 it != inh.end(); it++) {

      if (find(retval.begin(), retval.end(), *it) != retval.end())
	continue;

      retval.push_back(*it);

      vector<string> uppers =getInheritance(*it);

      if (uppers.size() >= 1) {
	for (vector<string>::const_iterator i = uppers.begin() + 1;
	     i != uppers.end(); i++) {

	  if (find(retval.begin(), retval.end(), *i) == retval.end())
	    retval.push_back(*i);
	}
      }
    }

    return retval;
  }

  //
  //Visitor
  //

  Visitor::Visitor(SymbolTable* symbolTable):_symbolTable (symbolTable)
  {
    _moduleDepth = 0; // No modules defined yet
  }

  bool
  Visitor::visitModuleStart(const ModulePtr& p)
  {
    _recursiveModules.push(_module);
    _module = Module(fixKwd(p->name()), fixKwd(p->scope()));
    _moduleDepth++;

    return true;
  }

  void
  Visitor::visitModuleEnd(const ModulePtr& p)
  {
    _parent = _recursiveModules.top();
    _recursiveModules.pop();
    if (_recursiveModules.empty()) {
      // The actual module is the parent module
      // so _module has to be insert into symbol table's module list.
      _symbolTable->addModule(_module);
    }
    else {
      // _parent has the parent of _module, so _module has to be
      // insert into _parent's module list.
      _parent.addModule(_module);
      // and now _module becomes _parent;
      _module = _parent;
    }
    _moduleDepth--;
  }

  bool
  Visitor::visitClassDefStart(const ClassDefPtr& p)
  {
    if (_moduleDepth) {
      // Start of an Interface definition.
      _iface = Iface(fixKwd(p->name()), fixKwd(p->scope()));
      //    _iface.addInheritance(string(fixKwd("::Ice::")),
      //string(fixKwd("Object")));

      ClassList bases = p->bases();
      for (ClassList::iterator basesIt = bases.begin();
	   basesIt != bases.end();
	   basesIt++){
	_iface.addInheritance(string(fixKwd((*basesIt)->scope())),
			      string(fixKwd((*basesIt)->name())));
      }

    }
    else {
      // Start of a Class definition
    }
    return true;
  }

  void
  Visitor::visitClassDefEnd(const ClassDefPtr& p)
  {
    if (_moduleDepth) {
      // End of an Interface definition.
      _module.addIface(_iface);
    }
    else {
      // End of a Class definition
    }
  }

  void
  Visitor::visitClassDecl(const ClassDeclPtr& p) {}

  bool
  Visitor::visitExceptionStart(const ExceptionPtr&)
  {return true;}

  void
  Visitor::visitExceptionEnd(const ExceptionPtr&) {}

  bool
  Visitor::visitStructStart(const StructPtr&)
  {return true;}

  void
  Visitor::visitStructEnd(const StructPtr&) {}

  void
  Visitor::visitOperation(const OperationPtr& p) {

    size_t strIndex;
    string metadataName, metadataValue;

    StringList operationMetaData;

    string name = p->name();
    string operationType = "";

    if(p->mode() == Slice::Operation::Idempotent || p->mode() == Slice::Operation::Nonmutating){
      // Idempotent operation
      operationType = "Idempotent";
    }

    // Creates the new operation.
    string iface = p->scope();
    iface = iface.substr(0, iface.size() - 2);
    Operation operation(operationType, name, iface);
    TypePtr ret = p->returnType();
    string retS = returnTypeToString(ret, "", p->getMetaData());

    operation.addOutputParam(Param(retS, ""));

    // Looking for metadata.
    operationMetaData = p->getMetaData();
    for(StringList::iterator it = operationMetaData.begin(); it != operationMetaData.end(); it++) {
      strIndex = it->find(':',0);
      if (strIndex != string::npos) {
	metadataName.assign(*it,0,strIndex);
	metadataValue.assign(*it,strIndex+1,it->length()-(strIndex+1));
	// Adds the new metadata information to the operation.
	operation.addMetadata(metadataName, metadataValue);
      }
    }

    // Looking for parameters.
    ParamDeclList paramList = p->parameters();
    string typeString;
    for(ParamDeclList::const_iterator q = paramList.begin();
	q != paramList.end();
	++q) {

      string paramName = fixKwd((*q)->name());
      StringList metaData = (*q)->getMetaData();

      if (!(*q)->isOutParam())
	typeString = inputTypeToString((*q)->type(), false, metaData);
      else
	typeString = outputTypeToString((*q)->type(), false, metaData);

      // Create the new parameter
      Param param(typeString, paramName);

      // and add it to the operation.
      if ((*q)->isOutParam())
	operation.addOutputParam(param);
      else {
	operation.addInputParam(param);
      }
    }
    // Insert the new operation into the actual iface.
    _iface.addOperation(operation);
  }

  void
  Visitor::visitParamDecl(const ParamDeclPtr& p) {}

  void
  Visitor::visitDataMember(const DataMemberPtr& p) {}

  void
  Visitor::visitSequence(const SequencePtr& p) {}

  void
  Visitor::visitDictionary(const DictionaryPtr&) {}

  void
  Visitor::visitEnum(const EnumPtr& p) {}

  void
  Visitor::visitConst(const ConstPtr&) {}
}

string
Slice::inputTypeToString(const TypePtr& type, bool useWstring, const StringList& metaData) {
  static const char* inputBuiltinTable[] =
    {
      "Byte",
      "bool",
      "Short",
      "Int",
      "Long",
      "Float",
      "Double",
      "string",
      "ObjectPtr",
      "ObjectPrx",
      "LocalObjectPtr"
    };

  BuiltinPtr builtin = BuiltinPtr::dynamicCast(type);
  if(builtin) {
    if(builtin->kind() == Builtin::KindString) {
      string strType = findMetaData(metaData, true);
      if(strType != "string" && (useWstring || strType == "wstring")) {
	if(featureProfile == IceE) {
	  return "const Wstring";
	}
	else {
	  return "const wstring";
	}
      }
    }
    return inputBuiltinTable[builtin->kind()];
  }

  ClassDeclPtr cl = ClassDeclPtr::dynamicCast(type);
  if(cl) {
    return "const " + fixKwd(cl->scoped() + "Ptr");
  }

  StructPtr st = StructPtr::dynamicCast(type);
  if(st) {
    if(findMetaData(st->getMetaData(), false) == "class") {
      return "const " + fixKwd(st->scoped() + "Ptr");
    }
    return "const " + fixKwd(st->scoped());
  }

  ProxyPtr proxy = ProxyPtr::dynamicCast(type);
  if(proxy) {
    return "const " + fixKwd(proxy->_class()->scoped() + "Prx");
  }

  EnumPtr en = EnumPtr::dynamicCast(type);
  if(en) {
    return fixKwd(en->scoped());
  }

  SequencePtr seq = SequencePtr::dynamicCast(type);
  if(seq) {
    string seqType = findMetaData(metaData, true);
    if(!seqType.empty()) {
      if(seqType == "array" || seqType == "range:array") {
	TypePtr elemType = seq->type();
	string s = typeToString(elemType, inWstringModule(seq), seq->typeMetaData());
	return "const ::std::pair<const " + s + "*, const " + s + "*>";
      }
      else if(seqType.find("range") == 0) {
	string s;
	if(seqType.find("range:") == 0) {
	  s = seqType.substr(strlen("range:"));
	}
	else {
	  s = fixKwd(seq->scoped());
	}
	if(s[0] == ':') {
	  s = " " + s;
	}
	return "const pair<" + s + "::const_iterator, " + s + "::const_iterator>&";
      }
      else {
	return "const " + seqType;
      }
    }
    else {
      return "const " + fixKwd(seq->scoped());
    }
  }

  ContainedPtr contained = ContainedPtr::dynamicCast(type);
  if(contained) {
    return "const " + fixKwd(contained->scoped());
  }

  return "???";
}

string
Slice::outputTypeToString(const TypePtr& type, bool useWstring, const StringList& metaData)
{
  static const char* outputBuiltinTable[] =
    {
      "Byte",
      "bool",
      "Short",
      "Int",
      "Long",
      "Float",
      "Double",
      "string",
      "ObjectPtr",
      "ObjectPrx",
      "LocalObjectPtr"
    };

  BuiltinPtr builtin = BuiltinPtr::dynamicCast(type);
  if(builtin) {
    if(builtin->kind() == Builtin::KindString) {
      string strType = findMetaData(metaData, true);
      if(strType != "string" && (useWstring || strType == "wstring")) {
	if(featureProfile == IceE) {
	  return "Wstring&";
	}
	else {
	  return "wstring&";
	}
      }
    }
    return outputBuiltinTable[builtin->kind()];
  }

  ClassDeclPtr cl = ClassDeclPtr::dynamicCast(type);
  if (cl) {
    return fixKwd(cl->scoped() + "Ptr");
  }

  StructPtr st = StructPtr::dynamicCast(type);
  if (st) {
    if(findMetaData(st->getMetaData(), false) == "class") {
      return fixKwd(st->scoped() + "Ptr");
    }
    return fixKwd(st->scoped());
  }

  ProxyPtr proxy = ProxyPtr::dynamicCast(type);
  if (proxy) {
    return fixKwd(proxy->_class()->scoped() + "Prx");
  }

  SequencePtr seq = SequencePtr::dynamicCast(type);
  if(seq) {
    string seqType = findMetaData(metaData, false);
    if(!seqType.empty()) {
      return seqType;
    }
    else {
      return fixKwd(seq->scoped());
    }
  }
  ContainedPtr contained = ContainedPtr::dynamicCast(type);
  if(contained) {
    return fixKwd(contained->scoped());
  }

  return "???";
}

string
Slice::typeToString(const TypePtr& type, bool useWstring, const StringList& metaData, bool inParam)
{
  static const char* builtinTable[] =
    {
      "Byte",
      "bool",
      "Short",
      "Int",
      "Long",
      "Float",
      "Double",
      "string",
      "ObjectPtr",
      "ObjectPrx",
      "LocalObjectPtr"
    };

  BuiltinPtr builtin = BuiltinPtr::dynamicCast(type);
  if (builtin) {
    if (builtin->kind() == Builtin::KindString) {
      string strType = findMetaData(metaData, true);
      if (strType != "string" && (useWstring || strType == "wstring")) {
	if (featureProfile == IceE) {
	  return "::Ice::Wstring";
	}
	else {
	  return "::std::wstring";
	}
      }
    }
    return builtinTable[builtin->kind()];
  }

  ClassDeclPtr cl = ClassDeclPtr::dynamicCast(type);
  if (cl) {
    return fixKwd(cl->scoped() + "Ptr");
  }

  StructPtr st = StructPtr::dynamicCast(type);
  if(st) {
    if(findMetaData(st->getMetaData(), false) == "class") {
      return fixKwd(st->scoped() + "Ptr");
    }
    return fixKwd(st->scoped());
  }

  ProxyPtr proxy = ProxyPtr::dynamicCast(type);
  if (proxy) {
    return fixKwd(proxy->_class()->scoped() + "Prx");
  }

  SequencePtr seq = SequencePtr::dynamicCast(type);

  if(seq) {
    string seqType = findMetaData(metaData, true);

    if(seqType.empty()) return fixKwd(seq->scoped());

    if(seqType == "array" || seqType == "range:array") {
      if (not inParam) return fixKwd(seq->scoped());
      TypePtr elemType = seq->type();
      string s = typeToString(elemType, inWstringModule(seq), seq->typeMetaData());
      return "::std::pair<const " + s + "*, const " + s + "*>";
    }
    else {
      if(seqType.find("range") != 0) return seqType;

      if (not inParam) return fixKwd(seq->scoped());
      string s;

      if(seqType.find("range:") == 0) {
	s = seqType.substr(strlen("range:"));
      }
      else {
	s = fixKwd(seq->scoped());
      }

      if(s[0] == ':') {
	s = " " + s;
      }

      return "::std::pair<" + s + "::const_iterator, " + s + "::const_iterator>";
    }
  }

  ContainedPtr contained = ContainedPtr::dynamicCast(type);
  if (contained) {
    return fixKwd(contained->scoped());
  }

  EnumPtr en = EnumPtr::dynamicCast(type);
  if (en) {
    return fixKwd(en->scoped());
  }

  return "???";
}


namespace slc
{
  Parser::Parser(const std::vector<std::string>& uses) {

    string fullPath;
    string startPath;
    string msg;

    ec::info() << "::: Validating uses files :::"  << endl;

    for (vector<string>::const_iterator it = uses.begin();
	 it!=uses.end(); it++) {

      string file = ec::os::path::resolveFile(*it,
					      config.paths);

      if (file == "")
	throw ec::IOError("No such file: " + *it);

      _files.push_back(file);
    }
  }

  void
  Parser::accept(Visitor* v) {
    _visitor = v;
    for (unsigned int i=0; i<_files.size(); i++)
      parse(_files[i]);
  }


  void
  Parser::parse(const string& filename) {

    int estado;
    vector<string> paths = config.paths;
    vector<string> slicePaths;
    vector<string>::const_iterator it;

    for (it = paths.begin(); it != paths.end(); ++it) {

      //cout << ::Preprocessor::normalizeIncludePath(*it) << endl;

      slicePaths.push_back("-I" + ::Preprocessor::normalizeIncludePath(*it));
      ec::debug() << "Adding included path: " << *it <<  endl;
    }

    Slice::Preprocessor icecpp(config.progname, filename, slicePaths);

    ec::debug() << "--- OUTPUT FROM SLICE PREPROCESSOR" << endl;
    // true: mantener comentarios
    FILE* cppHandle = icecpp.preprocess(true);
    ec::debug() << "--- OUTPUT FROM SLICE PREPROCESSOR" << endl;

    if (cppHandle == 0)
      throw SliceParserException("Error at Slice preprocessing time");

    ec::debug() << "'" << filename << "' preprocessed successfully" << endl;

    /*
     * 1: ignRedefs
     * 2: true si el parser de Slice analice los ficheros incluidos
     * 3: true para permitir los prefijos 'ice_'
     * 4: caseSensitive
     */

    Slice::UnitPtr u = Slice::Unit::createUnit(false, true, true, true);
    ec::debug() << "--- OUTPUT FROM SLICE PARSER" << endl;
    //  estado = u->parse(filename, cppHandle, config.debug);

    // FIXME - Salida verbose (o no) del parser de Slice
    estado = u->parse(filename, cppHandle, false);
    ec::debug() << "--- OUTPUT FROM SLICE PARSER" << endl;

    if (estado == EXIT_FAILURE) {
      throw SliceParserException("Error at Slice parsing time");
    }

    if(!icecpp.close()) {
      u->destroy();
    }

    u->visit(_visitor, false);

    u->destroy();
  }
}
