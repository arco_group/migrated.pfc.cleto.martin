
# Las claves se almacenan en una lista para tenerlas en el mismo
# orden en que se insertaron
class SortedDict(dict):
    def __init__(self, base={}):
        self.__keylist = []
        for k,v in base.items():
            self[k] = v

    def __getitem__(self, key):
        if not key in dict.keys(self):
            self[key] = []
        return dict.__getitem__(self, key)

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        if key in self.__keylist:
            self.__keylist.remove(key)
        self.__keylist.append(key)

    def keys(self):
        return self.__keylist

    def values(self):
        return [self[k] for k in self.__keylist]

    def items(self):
        return [(k, self[k]) for k in self.__keylist]

    def __iter__(self):
        return self.__keylist.__iter__()

    def update(self, other):
        for k,v in other.items():
            self[k] = v

    def clear(self):
        self.__keylist = []
        dict.clear(self)



class Record:
    "Records ad hoc"

    def __init__(self, **entries):
        self.__dict__.update(entries)

    def __getattr__(self, attr):
        if not self.__dict__.has_key(attr):
            raise AttributeError, attr
        return self.__dict__.get(attr, None)

    def __setattr__(self, attr, value):
        self.__dict__[attr] = value

    def __repr__(self):
        retval = ''
        for i in self.__dict__.keys():
            retval += "%s:'%s', " % (i, self.__dict__[i])
        return retval


# Singleton pattern
class Singleton(type):
    def __init__(cls, name, bases, dct):
        cls.__instance = None
        type.__init__(cls, name, bases, dct)

    def __call__(cls, *args, **kw):
        if cls.__instance is None:
            cls.__instance = type.__call__(cls, *args,**kw)
        return cls.__instance

    def loaded(cls):
        return cls.__instance != None
