# -*- coding: utf-8 -*-


import slice2fsm_utils as utils

def tree_insert(root, cad):
    t = root
    for ch in cad:
        done = False

        # inserción
        for child in t:
            if child[0] == ch:  # valor
                t = child[1]    # hijos
                done = True

        if not done:
            child = [ch, []]
            t.append(child)
            t = child[1]  # la lista de hijos del recién añadido

    t.append([None, []])


def compact(T):
    for child in T:
        compact(child[1])
        if len(child[1]) == 1:
            next = child[1][0][0]
            if next == None: next = ''
            child[0] += next
            child[1] =  child[1][0][1]   # sus nietos son sus hijos

        child[1] = [x for x in child[1] if x[0] != None]


def get_chunks(T):
    retval = []
    for child in T:
        retval.append(child[0])
        retval.extend(get_chunks(child[1]))
    return list(set(retval))



def gen_tree(values):
    retval = []
    for i in values:
        tree_insert(retval, i)
#    compact(retval)
    return retval


# This function split a set of strings to avoid redundancy
def get_fragments(T, values, chunks):

    compact(T)

    retval = {}
    for i in values:
        items = []
        retval[i] = items
        while len(i):
            for c in chunks:
                if i.startswith(c):
                    i = i[len(c):]
                    items.append(c)
                    break

    return retval


def draw(A, conn=''):
    for i,v in enumerate(A):
        brother = '+'
        parent = '| '

        if len(A)-1 == i:
            brother = '`'
            parent = '  '

        print '%s%s-%s' % (conn, brother, v[0])

        if len(v) > 1:
            draw(v[1], conn + parent)



def LCSubstr_set(S, T):
    m = len(S); n = len(T)
    L = [[0] * (n+1) for i in xrange(m+1)]
    LCS = []
    longest = 0
    for i in xrange(m):
        for j in xrange(n):
            if S[i] == T[j]:
                v = L[i][j] + 1
                L[i+1][j+1] = v
                if v > longest:
                    longest = v
                    LCS = []
                if v == longest:
                    LCS.append(S[i-v+1:i+1])
    return LCS

#         for i in data:
#             self.nodes.append(Node(i, [i]))

#         for n in self.nodes:
#             for f in n.fragments:
#                 print f

class DataOptimizer:
    def __init__(self):
        pass

    def initialize(self, data, long_min = 3):
        self.data = data
        self.result = {}
        self.chunks = {}
        self.long_min = long_min


        for k,v in self.data.items():
            if not v:
                self.data.pop(k)
            else:
                self.result[k] = []
                self.data[k]   = utils.plain_list(v)[0]

#         print "+++",self.data
#         print

    def gen_data(self):

        for i, (id,val) in enumerate(self.data.items()):
#             print self.result, self.chunks
#             raw_input()

            if i == 0:
                chunk_id = self.new_chunk(val)
                self.result[id] = [chunk_id]
                continue

            parts = self.get_best_seq(self.try_to_build(val))

            if parts:
                self.result[id] = parts
                continue


            best = self.get_best_partition(val)
            if not best:
                chunk_id = self.new_chunk(val)
                self.result[id] = [chunk_id]
                continue


#            print self.result, self.chunks
#            raw_input()

            common_part = best[1]

            code = self.new_chunk(common_part)
            sp = val.split(common_part)

            for j,p in enumerate(sp):

                if not p and j == 0:
                    self.result[id].append(code)
                    continue
                if not p:
                    continue

                subparts = self.get_best_seq(self.try_to_build(p))
                if subparts:
                    self.result[id].extend(subparts)
                else:
                    p_code = self.new_chunk(p)
                    self.result[id].append(p_code)

                if len(sp) - 1 != j:
                    self.result[id].append(code)


#             print self.result, self.chunks
#             raw_input()


            self.update()

       #  for k,v in self.result.items():
#             print k,v

#         print "----"
#         for k,v in self.chunks.items():
#             print k,v
        return self.result, self.chunks


    def show_result(self):
        print self.result, self.chunks

        old_bytes = 0
        for k,v in self.data.items():
            print k,v
            old_bytes += 1 + len(v)
        print "Memoria antigua:", old_bytes, "bytes"
        print "--"

        new_bytes = 0
        for k,v in self.chunks.items():
            print k,v
            if len(v) > 1:
                new_bytes += 1 + len(v)
        print "Memoria nueva:", new_bytes, "bytes"

    def get_best_seq(self, lst):
        if not lst:
            return []
        best = lst.pop()
        for i in lst:
            if len(best) > len(i):
                best = i
        return best

    def get_word_use(self, code):
        retval = []
        for k,v in self.result.items():
            if code in v:
                retval.append(k)
        return retval

    def update(self):
        for k,v in self.chunks.items():
            if len(v) <= self.long_min:
                continue

            try:
                others = self.chunks.values()
                others.remove(v)
            except ValueError:
                continue

            for o in others:
                if v not in o:
                    continue

                o_id = self.get_chunk_id(o)

                self.chunks.pop(o_id)

                parts = o.split(v)
                subparts = []
                for j,p in enumerate(parts):
                    if not p and j == 0:
                        subparts.append(k)
                        continue

                    if not p:
                        continue

                    subp = self.try_to_build(p)
                    if subp:
                        subparts.extend(subp)
                    else:
                        p_code = self.new_chunk(p)
                        subparts.append(p_code)

                    if len(parts) - 1 != j:
                        subparts.append(k)

                for w in self.get_word_use(o_id):
                    self.result[w] = self.replace(self.result[w], o_id, subparts)

        # FIXME - Quizas no es necesario
        for k,v in self.result.items():
            self.result[k] = utils.plain_list(v)

    def replace(self, lst, old, new):
        retval = []
        for i in lst:
            if old == i:
                retval.extend(new)
            else:
                retval.append(i)
        return retval

    def get_best_partition(self, value):
        partitions = []
        for n,c in self.chunks.items():
            l = LCSubstr_set(value,c)
            for i in l:
                if len(i) <= self.long_min:
                    continue
                partitions.append(((n,c), i))

        if not partitions:
            return partitions

        longest = partitions.pop()
        for p in partitions:
            if len(longest[1]) < len(p[1]):
                longest = p

        return longest

    def try_to_build(self, value):
        if value in self.chunks.values():
            return [[self.get_chunk_id(value)]]

        retval = []


        for i,c in self.chunks.items():
            subsol = []
            parts = value.split(c)
            if parts == ['','']:
                continue
            if len(parts) == 1:
                continue

            for j,p in enumerate(parts):
                if not p and j == 0:
                    subsol.append(i)
                    continue

                if not p:
                    continue

                subp = self.try_to_build(p)
                if not subp:
                    subsol = []
                    break

                subsol.extend(subp)
                subsol = utils.plain_list(subsol)

                if len(parts) -1 != j:
                    subsol.append(i)

            if subsol and subsol not in retval:
                retval.append(subsol)
        return retval


    def new_chunk(self, value):
        if value in self.chunks.values():
            return self.get_chunk_id(value)

        values = self.chunks.keys()
        n = 0
        while 1:
            if n not in values:
                self.chunks[n] = value
                return n
            else:
                n += 1

    def get_chunk_id(self, chunk):
        for k,v in self.chunks.items():
            if v == chunk:
                return k
        return None

# tids = {1:'::ASD::iByte::R',
#         2:'::ASD::iByte::W',
#         3:'::DUO::iByte::RW',
#         4:'::DUO::iBool::R',
#         5:'::Ice::Object',
#         6:'leon',
#         }

# data = {'DUO_IBool_W': '\x0f::DUO::IBool::W',
#         'DUO_IByte_W': '\x0f::DUO::IByte::W',
#         'DUO_IBool_R': '\x0f::DUO::IBool::R',
#         'DUO_IByte_R': '\x0f::DUO::IByte::R',
#         'DUO_IBool_RW': '\x10::DUO::IBool::RW',
#         'DUO_IByte_RW': '\x10::DUO::IByte::RW'}

# opt = DataOptimizer()
# opt.initialize(data)
# a,b = opt.gen_data()
# print a
# print
# print b

# # data
# tree = gen_tree(tids)
# draw(tree)

# chunks = get_chunks(tree)
# print chunks

# for i in chunks:
#     print "data.tid_%-12s = '%s'" % (plain(i), i)
# print

# mapa = get_fragments(tree, tids, chunks)
# for k,v in mapa.items():
#     print "%-20s %s" % (plain(k), [plain(x) for x in v])


# ids = ['::Ice::Object',
#        'leon',
#        '::DUO::iByte::R',
#        '::DUO::iByte::W',
#        '::DUO::iByte::RW',]

# for i in ids:
#     for j in mapa[i]:
#         print '    put_data(data.tid_%s)' % plain(j)
