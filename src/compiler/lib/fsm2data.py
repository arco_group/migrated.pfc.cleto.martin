#!/usr/bin/python
# -*- coding: utf-8 -*-
#
# Compilador de

# Changelog
# 2009/06/02 - Versión 3.4
#            - Se añade cmp call y ret

# 2009/01/30 - Versión 3.4
#            - Se añade call y ret
#            - Se implementa put_val

# 2008/10/21 - Versión 3.3
#            - Se sustituye moveW(dest) por move(dest, source)
#
# 2008/10/01 - Versión 3.3
#            - Se añade la instrucción raiseEvent
#
# 2008/09/17 - Versión 3.3
#            - Se eliminan las instrucciones onIncoming, onReset y onTimeout
#            - Se añaden las instrucciones setEvent y endEvent

# Changelog
# 2008/03/03 - Versión 3.2
#            - Instrucción onIncoming

#
#  FEATURES:
#  - Se pueden crear variable globales que se pueden usar sólo en los consume()
#  - Se permiten slices para los data.*
#  - Detecta variables no definidas y saltos a bloques no definidos
#  - Tipos incongruentes en los parámetros (FIXME:no todos)
#  - Poder saltar a bloques por debajo del actual (no definidos aún)
#  - Generar los datos para varios lenguajes


# - fsmStr no puede empezar en la dirección 0x100. Para calcular la
#   dirección en la que ha de empezar ese bloque ha de sumarse a la
#   longitud del bloque fsmMain (incluida la instrucción addwf PCL,F) el
#   valor 0x05, que es la direccion a partir de la cual se inserta dicho
#   bloque. Esto se debe a que si el bloque fsmMain pasa la primera
#   página de código se produce el Error [118] Overwriting previous
#   address contents. [HECHO]


# - Una página de "microcódigo" ocupa 256 bytes, pero el bloque fsmMain
#   NO TIENE PORQUÉ EMPEZAR EN LA DIRECCIÓN 0 DE LA PÁGINA ASIGNADA. Eso
#   es porque puede haber instrucciones antes (inicialización del micro,
#   etc....). En concreto, fsmData.inc se incluye a partir de la dirección
#   0x05. Ésto también hay que tenerlo en cuenta a la hora de calcular los
#   saltos y los cambios de página. [HECHO]


# == Utilidades del compilador accesibles desde la FSM ==
#
# - lit(*args):          Devuelve un "Literal" con los parametros serializados
# - digest(*args):       Devuelve un digest para la lista de parámetros
# - util_get_address():  Devuelve la "IP pública" de este PC en este momento
# - ice_request()        Crea un mensaje de ICE Request
# - ice_proxy_tcp        Crea un proxy TCP de ICE
# - ice_proxy_udp        Crea un proxy UDP de ICE
# - ice_proxy_null       Crea un proxy ICE nulo
# - Ice_op_mode          Un enumerado con los modos de invocación



import sys, os, string, types, sets, time, re
import math
import traceback
import optparse
import struct


#- debugging facilities -#
import logging

ALL = 100

log = logging.getLogger('fsm')
log.setLevel(logging.DEBUG)



#log.basicConfig(level=log.DEBUG,
#                format='',
#                filename='fsm_compilation.log',
#                filemode='w')

console = logging.StreamHandler()
console.setLevel(logging.ERROR)
#console.setFormatter(log.Formatter('%(levelname)-8s: %(message)s'))
logging.getLogger().addHandler(console)



def require(version):
    FSM_COMPILER = "3.5"
    if version != FSM_COMPILER:
        logging.critical("Compiler version mismatch %s != %s" % \
                         (version, FSM_COMPILER))
        sys.exit(1)





PIC_PAGE0_OFFSET = 0x15


def decode(code):
    retval = "%-10s (%2d)  " % (cmdstr[code[0]], code[0])
    if len(code) > 1: retval += str(code[1:])
    return retval


def assert_type(obj, type_):
    if not isinstance(obj, type_):
        raise TypeError("Argument of type '%s' must be a '%s'" % \
                        (obj.__class__.__name__, type_))


#- funciones auxiliares -#

# crea una lista plana de bytes a partir de un iterable que puede contener
# tuplas, Literal's, enteros y cadenas.
def toByteSeq(*val):
    if len(val) == 1:
        val = val[0]
    #print 'toByte:', val, type(val)
    if isinstance(val, tuple) or isinstance(val, list):
        retval = []
        for i in val: retval.extend(toByteSeq(i))
        return retval
    elif isinstance(val, Literal):
        return list(val.data)
    elif isinstance(val, int):
        return [val]
    elif isinstance(val, str):
        return [ord(x) for x in list(val)]
    elif isinstance(val, IceProxy):
        return val.pack()
    else:
        raise TypeError('Invalid type argument for assure()')

def get_digest(val):
    retval = 0
    for v in val:
        retval ^= ((v & 0xF)<<4) | ((v>>4) & 0xF)
    return retval

def get_digest_better(val):
    retval = 0
    for i,v in enumerate(val):
        retval ^= ((v+i & 0xF)<<4) | ((v>>4) & 0xF)
    return retval

def digest(*args):
    val = toByteSeq(args)
    return get_digest(val)

def to_char(c):
    if c < 0 or c > 255: return ' '
    if isinstance(c, int): c = chr(c)
    if c in string.printable: return repr(c)
    return ' '

def util_get_address():
    try:
        log_loading = logging.getLogger("scapy.loading")
        log_loading.setLevel(logging.WARNING)
        import scapy.all as scapy
        return [x[4] for x in scapy.conf.route.routes if x[2] != '0.0.0.0'][0]
    except ImportError:
        log.warning("package 'scapy' not found")
        return '0.0.0.0'


##
# -vec debe ser una lista de bytes
# -sub puede ser una lista de bytes o una cadena
def util_list_find(vec, sub):
    if isinstance(sub, list):
        sub = ''.join([chr(x) for x in sub])

    org = ''.join([chr(x) for x in vec])
    return org.find(sub)


# encapsulación de variables
class Literal(object):
    def __init__(self, name, data, pos=None, has_len=False):
        self.name = name
        self.start = pos
        self.data = data
        self.has_len = has_len


    def __getitem__(self, key):
        stop = key.stop
        if stop > len(self): stop = ''
        return Literal(('%s[%s:%s]' % (self.name, key.start, stop)),
                       self.data[key], self.start)

    @property
    def wLen(self):
        if self.has_len:
            return self
        return Literal(self.name + '+len',
                       toByteSeq(len(self), self.data),
                       has_len = True)

    def __len__(self):
        lon = len(self.data)
        if self.has_len: lon -= 1
        return lon

    def __repr__(self):
        haslen = 'N'
        if self.has_len: haslen = 'Y'
        return "<Literal:'%s' start=%s len=%d[%s] data=%s >" % \
               (self.name, self.start, len(self), haslen, self.data)

    def toString(self):
        return ','.join([to_char(x) for x in self.data])




def lit(*value):
    return Literal('', toByteSeq(value))

# contenedores
#- class Var(dict):
#-     def __getattr__(self, name):
#-         try:
#-             return self.__getitem__(name)
#-         except KeyError:
#-             raise NameError("'var.%s' member not defined." % name)
#-
#-     def __setattr__(self, name, *value):
#-         value = Literal(name, toByteSeq(value))
#-         #value = toByteSeq(value)
#-         #field = [len(value)] + value
#-         self.__setitem__(name, value)


class Data(list):

    def __init__(self):
        #self['fsmData'] = []
        self.raw = []  # raw bytes

    def alloc(self, value, name=''):
        if isinstance(value, int):
            raise TypeError, "alloc an int is forbbiden (%s)" % value

        pos = len(self.raw)
        value = toByteSeq(value)
        buff = [len(value)] + value
        self.raw.extend(buff)

        retval = Literal(name if name else len(self), value, pos)
        self.append(retval)
        return retval


#    def keys(self):
#        return [x for x in dict.keys(self) if x != 'fsmData']


#    def __getattr__(self, name):
#        try:
#            return self.__getitem__(name)
#        except KeyError:
#            raise NameError("'data.%s' member not defined." % name)


#    def __setattr__(self, name, *value):
#        if isinstance(value, int):
#            raise TypeError, "El valor de '%s' no puede ser tipo entero (%s)" % (name, value)
#
#        pos = len(self.fsmData)
#
#        value = toByteSeq(value)
#        buff = [len(value)] + value
#
#        self.fsmData.extend(buff)
#        literal = Literal(name, value, pos)
#
#        if self.__dict__.has_key(name):
#            dict.__setattr__(self, name, literal)
#        else:
#            self.__setitem__(name, literal)


class WritableData(Data):

    def alloc(self, value, name=''):

        if isinstance(value, int):
            value = [0] * value

        return Data.alloc(self, value, name)


debug_msg = []


class Bank:
    def __init__(self):
        self.data = [0] # working
        self.allocs = 0
    def alloc(self, value):
        retval = len(self.data)

        if isinstance(value, list):
            seq = toByteSeq(value)
            self.data += [len(seq)] + seq
        elif isinstance(value, str):
            self.data += [len(value)] + [ord(x) for x in value]
        else:
            self.data += [0] * value

        self.allocs += 1
        return retval

    def init(self, reg, values):
        for i, val in enumerate(values):
            self.data[reg+i] = val

    def __len__(self):
        return len(self.data)


## Implementación de comandos

cmdstr = ['Nop',
          'Reset',      'Goto',       'GotoNZ',    'GotoEq', 'GotoNEq',
          'setEvent',   'Call',       'Ret',
          'Skip',       'SkipMod4',   'GetW',      'GetReg', 'GetStr',
          'GetFlash',
          'Assure',     'TestDigest',
          'PutReg',     'PutStr',     'PutData',   'Template', 'PutVal',
          'PutFlash',
          'SetW',       'Move',       'SetVal',    'Add',    'SetTimer',
          'Cmp',
          'ConnectData', 'ConnectFlash',    'Close',
          'raiseEvent',
          'UserProc',
          'Debug']

class Cmd(object):
    (NOP,
     RESET,      GOTO,        GOTO_NZ,     GOTO_EQ,  GOTO_NEQ,
     SET_EVENT,  CALL,        RET,
     SKIP,       SKIP_MOD_4,  GET_W,       GET_REG,  GET_STR,
     GET_FLASH,
     ASSURE,     TEST_DIGEST,
     PUT_REG,    PUT_STR,     PUT_DATA,    TEMPLATE, PUTVAL,
     PUT_FLASH,
     SET_W,      MOVE,        SET_VAL,     ADD,      SET_TIMER,
     CMP,
     CONNECTDATA, CONNECTFLASH,   CLOSE,
     RAISE_EVENT,
     USER_PROC,
     DEBUG) = range(35)

    gotos = [GOTO, GOTO_NZ, GOTO_EQ, GOTO_NEQ,
             SET_EVENT, CALL,]

    def __init__(self, bytecode): #, data, flash):
        self.code = bytecode
#        self.data = data
#        self.flash = flash


    def reset(self):
        self.code.append([Cmd.RESET])

    def goto(self, dest):
        assert_type(dest, str)
        self.code.append([Cmd.GOTO, dest])

    def goto_nz(self, dest):
        assert_type(dest, str)
        self.code.append([Cmd.GOTO_NZ, dest])

    def goto_eq(self, dest, value):
        if isinstance(value, str):
            assert len(value) == 1
            value = ord(value)
        #FIXME: tratar value negativo
        assert_type(dest, str)
        self.code.append([Cmd.GOTO_EQ, dest, value])

    def goto_neq(self, dest, value):
        if isinstance(value, str):
            assert len(value) == 1
            value = ord(value)
        #FIXME: tratar value negativo
        assert_type(dest, str)
        self.code.append([Cmd.GOTO_NEQ, dest, value])

    def set_event(self, callback, event):
        assert_type(callback, str)
        self.code.append([Cmd.SET_EVENT, callback, event])

    def skip(self):
        self.code.append([Cmd.SKIP])

    def skip_mod_4(self):
        raise NotImplemented

    def get_w(self):
        self.code.append([Cmd.GET_W])

    def get_reg(self, i, offset=0):
        # FIXME, 'i' está en el rango?
        # if offset < 0: offset += 256
        self.code.append([Cmd.GET_REG, i, offset])

    def get_str(self, i, max):
        assert_type(i, int)
        assert_type(max, int)
        self.code.append([Cmd.GET_STR, i, max])

    def get_flash(self, lit, max):
        assert_type(max, int)
        self.code.append([Cmd.GET_FLASH, lit.start, max])

    def assure(self, *args):
        val = toByteSeq(args)
        self.code.append([Cmd.ASSURE, len(val), get_digest(val)])
        #print "Assure:", args
        #print len(val), ':', val, get_digest(val)
        #raw_input()

    def test_digest(self):
        self.code.append([Cmd.TEST_DIGEST])

    def put_reg(self, i):
        self.code.append([Cmd.PUT_REG, i])

    def put_str(self, pos):
        assert_type(pos, int)
        self.code.append([Cmd.PUT_STR, pos])

    def put_data(self, lit):
        self.code.append([Cmd.PUT_DATA, lit.start])

    def put_flash(self, lit):
        self.code.append([Cmd.PUT_FLASH, lit.start])

    def template(self, lit, pos):
        self.code.append([Cmd.TEMPLATE, lit.start, pos])

    def put_val(self, val):
        self.code.append([Cmd.PUTVAL, val])

    def set_w(self, val):
        if isinstance(val, str):
            assert len(val) == 1
            val = ord(val)
        self.code.append([Cmd.SET_W, val])

    def move(self, dest, source):
        self.code.append([Cmd.MOVE, dest, source])

    def set_val(self, index, val):
        self.code.append([Cmd.SET_VAL, index, val])

    def add(self, index, val):
        #FIXME: negativos
        self.code.append([Cmd.ADD, index, val])

    def set_timer(self, val):
        self.code.append([Cmd.SET_TIMER, val])

    def cmp(self, reg):
        self.code.append([Cmd.CMP, reg])

    def connect_data(self, lit):
        self.code.append([Cmd.CONNECTDATA, lit.start])

    def connect_flash(self, lit, t):
        self.code.append([Cmd.CONNECTFLASH, lit.start, t])

    def close(self):
        self.code.append([Cmd.CLOSE])

    def raise_event(self, val):
        self.code.append([Cmd.RAISE_EVENT, val])

    def debug(self, cad):
        if not opts.debug: return
        self.code.append([Cmd.DEBUG, len(debug_msg)])
        debug_msg.append('FSM_DEBUG: ' + cad)

    def call(self, dest):
        assert_type(dest, str)
        self.code.append([Cmd.CALL, dest])

    def ret(self):
        self.code.append([Cmd.RET])

    def user_proc(self, n):
        self.code.append([Cmd.USER_PROC, n])



class Bytecode:

    class BlockDesc:
        def __init__(self, name, start):
            self.name = name
            self.start = start

        def __str__(self):
            return "%3d[%d:%3d] - %s" % \
                (self.start, self.start / 256, self.start % 256, self.name)

        def far_addr(self):
            return (self.start / 256), (self.start % 256)

    class CmdDesc:
        def __init__(self, code, start, bytecode):
            self.op = code[0]
            self.code = code
            self.size = len(code)
            self.bytecode = bytecode
            #self.fsmCode = bytecode.fsmCode
            self.start = start

        def __str__(self):
            return "%3d[%d:%3d] --- %-14s  %-14s" % \
                   (self.start, self.start / 256, self.start % 256,
                    cmdstr[self.op] + "(%s)" % self.op, self.code[1:])
                    #self.fsmCode[self.start+1:self.start+1+self.size-1])


    class GotoDesc(CmdDesc):
        def __init__(self, *args):
            Bytecode.CmdDesc.__init__(self, *args)
            self.target_pos = None
            self.mustFar = False
            self.isFar = False
            self.label = self.code[1]  # la etiqueta de salto (en texto)


        # obtiene la dirección absoluta de salto a partir de pagina y offset
        def calc_target_pos(self):
            if self.isFar:
                return self.code[1] * 256 + self.code[2]
            else:
                return (self.start / 256) * 256 + self.code[1]

        # return pointer absolute address
        def pointer_abs(self):
            return self.start + 1

        def modify_target(self, offset):
            self.target_pos += offset
            self.update()

        def update(self):
            pos = self.pointer_abs()
            self.page = pos / 256
            self.target_page = self.target_pos / 256
            self.target_offset = self.target_pos % 256

            if self.page != self.target_page  or \
               (self.page == self.target_page and self.target_offset % 256 < 4):
                self.mustFar = True

            if self.page == self.target_page and self.target_offset % 256 > 5:
                self.mustFar = False


            if self.isFar:
                self.code[1:3] = self.target_page, self.target_offset
            elif self.mustFar:
                self.code[1]   = self.target_pos
            else:
                self.code[1]   = self.target_offset

        def pointer_repr(self):
            return 'Pos: %4d [%d] -> %4d [%d:%3d] %s%s' %\
                   (self.pointer_abs(), self.page,
                    self.target_pos, self.target_page, self.target_offset,
                    ' F'[int(self.mustFar)], ' +'[int(self.isFar)])

        def convert2far(self):
            log.info("Converting '%s %s' to far" % (self.start,
                                                    cmdstr[self.op]))
            self.isFar = True
            self.size += 1

            self.code[1:1] = [None] # new byte for address
            self.update()

            self.bytecode.insert(self.pointer_abs())


        def convert2near(self):
            log.info("\nConverting '%s %s' to near" % (self.start,
                                                       cmdstr[self.op]))
            self.isFar = False
            self.size -= 1

            self.code[1:2] = []  # remove a byte in address
            self.update()

            self.bytecode.remove(self.pointer_abs())



    def __init__(self, arch):
        self.blocks = []    # block descriptions
        self.commands = []  # comand descriptions
        self.p = 0          # bytecode insertion point
        self.platform = arch # Arquitectura para la que se genera el bytecode.

    def add_block(self, blockname):
        self.blocks.append(Bytecode.BlockDesc(blockname, self.p))
        log.info('Block added: %s' % self.blocks[-1])

    # devuelve el bloque indicado, siendo f un nombre o una función
    def get_block(self, f):
        if isinstance(f, str):
            name = f
        elif isinstance(f, types.FunctionType):
            name = f.__name__
        else:
            log.error("Error: get_block() arg must be str o Function, not %s" %\
                           type(f))
            sys.exit(1)

        for i in self.blocks:
            if i.name == name:
                return i
        return None

    def get_gotos(self):
        return [x for x in self.commands
                if isinstance(x, Bytecode.GotoDesc)]

    # append a command to bytecode
    def append(self, code):
        if code[0] in Cmd.gotos:
            cmdclass = Bytecode.GotoDesc
        else:
            cmdclass = Bytecode.CmdDesc

        self.commands.append(cmdclass(code, self.p, self))
        log.info("[%3d] -    %s" % (self.p, decode(code)))

        self.p += len(code)


    def insert_nops(self, pos, n=1):
        index = self.get_cmd_start_in(pos)
        self.insert(pos, n)

        nops = [Bytecode.CmdDesc([Cmd.NOP], pos+i, self) for i in range(n)]
        self.commands[index:index] = nops



    def insert(self, pos, n=1):
        log.info("Inserting  %d bytes in pos %d" % (n, pos))

        # update block addresses
        for b in self.blocks:
            if b.start >= pos:
                b.start += n

        # update command addresses
        for c in self.commands:
            if c.start >= pos:
                c.start += n
            if isinstance(c, Bytecode.GotoDesc) and c.target_pos >= pos:
                c.modify_target(+n)



    # Remove bytes from bytecode at pos (absolute)
    def remove(self, pos, n=1):
        log.info("Removing %d bytes en pos %d" % (n, pos))

        # update block addresses
        for b in self.blocks:
            if b.start >= pos:
                b.start -= n

        # update command addresses
        for c in self.commands:
            if c.start >= pos:
                c.start -= n
            if isinstance(c, Bytecode.GotoDesc) and c.target_pos > pos:
                c.modify_target(-n)


    def resolve_labels(self):
        log.info("\n-- Resolving labels --")
        for c in self.get_gotos():
            block = self.get_block(c.label)
            if block == None:
                log.error("Block '%s' not defined" % c)
                sys.exit(1)

            c.target_pos = block.start
            log.info("Label: %-40s %4s" % (c.label, block.start))

            c.update()


    # return index (on self.command list) of the cmd over addr
    def get_cmd_over(self, addr):
        #print addr, self.get_size()
        assert(addr < self.get_size())
        for i,c in enumerate(self.commands):
            if c.start > addr:
                return i-1
        if c.start == addr:
            return i
        return None


    #returns index of cmd starts in addr
    def get_cmd_start_in(self, addr):
        assert(addr < self.get_size())
        for i,c in enumerate(self.commands):
            if c.start == addr:
                return i
        return None

    # bytecode size
    def get_size(self):
        return sum([c.size for c in self.commands])

    # returns raw bytecode
    def get_rawcode(self):
        retval = []
        for c in self.commands:
            retval.extend(c.code)

        # etiquetas no resueltas?
        for i in retval:
            assert isinstance(i, int), "%s is not a valid code" % i

        # ajuste de negativos
        for i in range(len(retval)):
            if retval[i] < 0:
                retval[i] += 256

        return retval


    # returns True if it does modifications
    def fix_broken_cmds(self):
        retval = False
        npages = int(math.ceil(float(self.get_size()) / 256))

        #print 'size:',self.get_size(), 'npages:',npages

        for p in range(1, npages):
            ## delete NOPs in the page except the located at end
            log.info('Checking broken commands in page: %s' % p)

            # skip NOPs at page end
            # FIXME: ¿por qué se suma 1 a 'p'?
            pos = p*256 - 1  # last byte of this page
            pos = min(pos, self.get_size()-1)

            first = self.get_cmd_over(pos)
            while self.commands[first].op == Cmd.NOP: first -= 1

            # delete others NOPs
            #print 'eliminar nops hasta', first, self.commands[first]
            if p == 1 and self.platform == 'pic':
                log.info("Page 1 in pic platform: Skipping deleting initial NOPs")
            else:
                log.info("Deleting initial NOPs")
                pos = (p-1)*256 # begin of this page
                index = self.get_cmd_over(pos)

                for i in range(index, first):
                    if self.commands[i].op == Cmd.NOP:
                        self.remove(self.commands[i].start)
                        self.commands[i:i+1] = []
                        retval = True

            ## insert NOPs to align hypothetical broken cmd to page
            pos = p*256
            index = self.get_cmd_over(pos)
            cmd = self.commands[index]
            assert(pos >= cmd.start)
            if pos > cmd.start: # broken cmd found
                n_nops = pos - cmd.start
                self.insert_nops(cmd.start, n_nops)
                retval = True

        for c in self.get_gotos():
             c.update()

        return retval


    def check_sanity(self):
        log.info('\n-- Checking sanity:')
        error = False

        # check for gaps
        p = 0
        for c in self.commands:
            if c.start != p:
                log.critical("Command aligment error at %s:\n %s" % (p,c))
                error = True
                break

            p += c.size


        # comprobar que las direcciones de bloques corresponden con comandos
        # TODO

        # Check goto targets match
        for g in self.get_gotos():

            block = self.get_block(g.label)
            #if block.start != g.target_pos:

            if g.target_pos != g.calc_target_pos():
                log.critical('Sanity error: target mismatch!\n%s --> %s ' % (g, block))
                error = True
                break

        # comprobar bloques no usados (no hay saltos hacia ellos)
        # TODO

        # comprobar que todos los nombres de bloque se han resuelto
        # TODO

        bytecode = self.get_rawcode()

        # comprobar que ningún valor está fuera del rango de byte
        for x in bytecode:
            if x > 255:
                log.critical("Sanity error: codification out of range: '%s'" % x)
                error = True

        assert len(bytecode) < 1024, "bytecode size excedeed"

        if error:
            log.critical('\nThere were fatal errors!')
            sys.exit(3)

        log.info('No errors detected.\n')


    def __str__(self):
        retval = '-- Decoded bytecode (%d bytes):\n' % self.get_size()
        bp = 0 # block pointer
        cp = 0 # instruction pointer
        for c in self.commands:
            for b in self.blocks:
                if b.start == c.start:
                    retval += str(b) + '\n'
            retval += str(c) + '\n'
        return retval


    def print_status(self, msg='', logger=log):
        if msg: logger.info("\n----- %s -----" % msg)
        logger.info(str(self))

        logger.info('- Gotos')
        for c in self.get_gotos():
            logger.info("%-41s %s" %(c, c.pointer_repr()))


# --- language backends  --- #


def log_dataset(logger, dataset):
    for i in dataset: logger.info("%-22s: %4s", *i)


class Backend: # builder pattern

    head_message = " Generated by fsm2data.py - DO NOT EDIT!"
    def __init__(self, code, data, bank, flash):
        self.code = code
        self.data = data
        self.bank = bank
        self.flash = flash

        self.product = ''
        self.extension = 'unknown'
        self.gen_all()

    def save(self, fname):
        fd = open(fname, 'w')
        fd.write(self.product)
        fd.close()


    def format_list(self, L, func=lambda x:x, width=8,
                    prefix='', suffix = ',', format=4):
        retval = ''
        format = "%%%ds" % format

        for i,v  in enumerate(L):
            if ((i%width) == 0):
                if i > 0: retval += suffix
                retval += '\n  ' + prefix + (format % func(v))
            else:
                retval += ',' + format % func(v)
        return retval

    @staticmethod
    def create(backend, code, data, bank, flash):
        if backend.lower() == 'c':
            return C_backend(code, data, bank, flash)
        if backend.lower() in ['python','py']:
            return Python_backend(code, data, bank, flash)
        if backend.lower() in ['pic']:
            return PIC_backend(code, data, bank, flash)
        if backend.lower() in ['java']:
            return Java_backend(code, data, bank, flash)
        return None

    def gen_all(self):
        self.gen_head()
        self.gen_code()
        self.gen_data()
        self.gen_bank()
        self.gen_flash()
        self.gen_cmd_list()
        self.gen_debug()
        self.gen_footer()

    def gen_head(self):
        pass

    def gen_code(self):
        raise NotImplementedError

    def gen_data(self):
        raise NotImplementedError

    def gen_bank(self):
        raise NotImplementedError

    def gen_debug(self):
        pass

    def gen_cmd_list(self):
        pass

    def gen_footer(self):
        pass


class C_backend(Backend):

    def __init__(self, *args):
        Backend.__init__(self, *args)
        self.extension = 'c'

    def gen_head(self):
        self.product += "/*\n" + self.head_message + "\n*/\n"

    def gen_code(self):
        self.product += 'unsigned char fsmCode[] PROGMEM = {%s\n};\n\n' % \
                        self.format_list(self.code.get_rawcode())

    def gen_data(self):
        self.product += 'unsigned char fsmData[] PROGMEM = {%s\n};\n\n' % \
                        self.format_list(self.data.raw)

    def gen_bank(self):
        self.product += '#define fsmBankSize  %d\n' % len(self.bank.data)
        self.product += 'unsigned char R[] = {%s\n};\n' % \
                        self.format_list(self.bank.data)

    def gen_flash(self):
        self.product += '\n#define fsmFlashSize %d\n' % len(self.flash.raw)
        self.product += 'unsigned char fsmFlash[] = {%s\n};\n\n' % \
                        self.format_list(self.flash.raw)

    def gen_cmd_list(self):
        self.product += '\n'
        for c in sets.Set([x.op for x in self.code.commands]):
            self.product += '#define %s\n' % cmdstr[c].upper()


class Python_backend(Backend):
    def __init__(self, *args):
        Backend.__init__(self, *args)
        self.extension = 'py'

    def gen_head(self):
        self.product += "#" + self.head_message + "\n\n"

    def gen_code(self):
        bytecode = self.code.get_rawcode()
#        self.adjust_neg(bytecode)

        self.product += 'fsmCode = [%s]\n\n'  % \
                        self.format_list(self.code.get_rawcode())
    def gen_data(self):
        self.product += 'fsmData = [%s]\n\n' % \
                        self.format_list(self.data.raw)

    def gen_bank(self):
        self.product += 'fsmBank = [%s]\n\n' % \
                        self.format_list(self.bank.data)

    def gen_flash(self):
        self.product += 'fsmFlash = [%s]\n\n' % \
                        self.format_list(self.flash.raw)

    def gen_debug(self):
        if not opts.debug: return
        self.product += '\nfsmDebug = [\n'
        for m in debug_msg:
            self.product += "'%s',\n" % m
        self.product += ']\n'


def int2byte(i):
    if i > 127:
        return -256 + i
    else:
        return i


class Java_backend(Backend):
    def __init__(self, *args):
        Backend.__init__(self, *args)
        self.extension = 'java'

    def gen_head(self):
        self.product += "//" + self.head_message + "\n\n"
        self.product += "public class fsmCode {\n\n"

    def gen_code(self):
        self.product += 'public final static byte code[] = {%s};\n\n'  % \
                        self.format_list(self.code.get_rawcode(), func=int2byte)
    def gen_data(self):
        self.product += 'public final static byte data[] = {%s};\n\n' % \
                        self.format_list(self.data.raw, func=int2byte)

    def gen_bank(self):
        self.product += 'public final static byte bank[] = {%s};\n\n' % \
                        self.format_list(self.bank.data, func=int2byte)

    def gen_flash(self):
        self.product += 'public final static byte flash[] = {%s};\n\n' % \
                        self.format_list(self.flash.raw, func=int2byte)

    def gen_footer(self):
        self.product += '} \n'


class PIC_backend(Backend):

    def __init__(self, *args):
        Backend.__init__(self, *args)
        self.extension = 'inc'

    def gen_code(self):
        code = self.code.get_rawcode()

        # delete the initial offset dummy NOPs
        n_nops = 0
        for c in code:
            if c != Cmd.NOP: break
            n_nops += 1
        code = code[n_nops:]

        self.product += 'fsmCode:\n   movwf PCL%s\n\n' % \
                        self.format_list(code, func=hex, format=5,
                                         prefix=' dt', suffix='')
    def gen_data(self):
        n_page = (self.code.get_size() + PIC_PAGE0_OFFSET) / 256
        self.product += '   org 0x%d00\n' % (n_page + 1)
        self.product += 'fsmDataPage equ %s\n' % (n_page + 1)
        self.product += 'fsmData:\n   addwf PCL,F' +  \
                        self.format_list(self.data.raw,
                                         func=hex, format=5,
                                         prefix=' dt', suffix='')
        self.product += '\n'


    def gen_flash(self):
        # FIXME: preguntar a javi que es lo que tiene ir aqui
        # JAVI: Aqui no tiene que venir NADA. La flash (eeprom)
        # no se inicializa, se supone que los datos son persistentes.
        # DAVID: ¿Y por qué no puede haber datos iniciales?

        pass
    #         self.product += 'fsmFlash = [%s]\n\n' % \
#                         self.format_list(self.flash.raw)


    def gen_bank(self):
        self.product += '\nfsmBankSize equ %d\n' % len(self.bank.data)
        self.product += 'fsmBank: \n    addwf PCL,F %s\n' % \
                        self.format_list(self.bank.data,
                                         func=hex, format=5,
                                         prefix='  dt', suffix='')

    def gen_cmd_list(self):
        self.product += '\n'
        for c in sets.Set([x.op for x in self.code.commands]):
            self.product += '#define %s\n' % cmdstr[c].upper()


def return_error(v):
    print 'ERROR: %s' %  v
    tb = traceback.extract_tb(sys.exc_traceback)
    for i in tb:
        if i[0] == '<string>':
            print "       At line %s of block '%s'" % tuple(i[1:3])
    sys.exit(1)





def usage():
      print '''\
Usage: fsm2data.py <fsm_file> <language> <target_file> [-s key=value]
       fsm2data.py --debug

with <language> in ["c", "py", "pic", "java"]
'''
      return 1


#--- MAIN --------------------------------------------------
#
# FIXME: determinar el backend por la extensión del fichero destino


def run_fsm(source, lang):

    log.addHandler(logging.FileHandler(source + '.log', 'w'))
    log.info("Begin of compilation log")

    bytecode = Bytecode(lang)
    data = Data()
    flash = WritableData()
    bank = Bank()

    isa = Cmd(bytecode)

    # FSM environment
    env = {}
    env['data'] = data
    env['bank'] = bank
    env['flash'] = flash

    import inspect

    # add ISA cmds to the FSM environment
    for m in inspect.getmembers(isa, inspect.ismethod):
        env[m[0]] = m[1]

    # add utils to FSM environment
    for f in [lit, digest, require]:
        env[f.__name__] = f

    # set FSM variable values
    fsm_variables = {}
    if opts.set_variables:
        for i in opts.set_variables:
            name, value = tuple(i.split('='))
            code = '%s = "%s"' % (name, value)
            fsm_variables[name] = code


    # From here, run the FSM source
    try:
        fd = open(source)
        content = fd.readlines()
        fd.close()
    except IOError, e:
        log.critical(e)
        sys.exit(1)


    log.info('\n-- Parsing fsm source:')

    buff = ''
    n = 0
    for line in content:
        n += 1
    #log('   line %s (%s)' % (line, state))
        if not line.strip(): continue

        if '\\ ' in line:
            line = line.split('\\')[0] + '\\\n'

        if '=' in line: # variable definition
            name = line.split()[0]
            try:
                code = fsm_variables[name]
                print "Overriden value: %s" % code
                exec(code) in env
                del fsm_variables[name]
                continue
            except KeyError:
                pass
            except SyntaxError, e:
                log.error("In bytecode '%s':\n%s \n\t%s" % (source, e, e.text))
                sys.exit(1)

        buff += line


    try:
        exec(buff) in env
    except SyntaxError, e:
        log.error("In bytecode '%s':\n%s \n\t%s" % (source, e, e.text))
        sys.exit(1)


    if fsm_variables:
        print '\nWARNING: Next variables does not exist in the source:', fsm_variables.keys()

#    print env

    for line in content:
        if line.startswith('def '):
            name = line[4:line.find('(')].strip()
            bytecode.add_block(name)
            env[name]()

    return bytecode, data, bank, flash


def run(source, lang, final=None, debug=False):

    if debug: opts.debug=True

    bytecode, data, bank, flash = run_fsm(source, lang)
    bytecode.resolve_labels()

    if lang == 'pic':
        bytecode.insert_nops(0, PIC_PAGE0_OFFSET)


    bytecode.print_status('after resolve pointers')


    has_changed = True
    iteration = 1
    while has_changed:
        has_changed = False

        for p in bytecode.get_gotos():
            if p.mustFar and not p.isFar:
                p.convert2far()
                has_changed = True

        bytecode.print_status('iter %d, after convert2far. Changes: %s' %\
                              (iteration, has_changed))

        has_changed |= bytecode.fix_broken_cmds()

        bytecode.print_status('iter %d, after fix broken. Changes: %s' %\
                              (iteration, has_changed))

        for p in bytecode.get_gotos():
            if p.isFar and not p.mustFar:
                p.convert2near()
                has_changed = True

        bytecode.print_status('iteration %d, after convert2near. Changes: %s' %\
                              (iteration, has_changed))

        iteration += 1


    bytecode.check_sanity()
    backend = Backend.create(lang, bytecode, data, bank, flash)

    if not backend:
        log.error("FSM Backend unavailable: '%s'" % lang)
        return 1

    if not final: final = os.path.splitext(source)[0] + backend.extension
    backend.save(final)


    # printing LST

    lstlog  = logging.getLogger('lst')
    lstlog.setLevel(logging.DEBUG)
    handler = logging.FileHandler(source + '.lst', 'w')
    handler.setFormatter(logging.Formatter('%(name)s[%(levelname)s]:    %(message)s'))
    lstlog.addHandler(handler)

    lstlog.info('Compilation results for %s', source)
    lstlog.info('%s\n', time.asctime())

    bytecode.print_status('', lstlog)

    lstlog.info('\n- Data:')
    log_dataset(lstlog,
                [(lit.name, "%2s" % (len(lit.data))) for lit in data])
                #[(i, "%2s" % (len(data[i].data))) for i in sorted(data.keys())])


    lstlog.info('\n- Code:')
    log_dataset(lstlog, [
            ('blocks',        len(bytecode.blocks)),
            ('commands',      len(bytecode.commands)),
            ('gotos',         len(bytecode.get_gotos())),
            ])

    lstlog.info('\n- Sizes:')
    log_dataset(lstlog, [
            ('Data',          len(data.raw)),
            ('Code',          bytecode.get_size()),
            ('Bank',          len(bank)),
            ('Flash',         len(flash.raw))
            ])

    lstlog.info('-'*30)
    log_dataset(lstlog,
                [('Total', len(data.raw) + bytecode.get_size() + len(bank))])



    line = []
    line.append("%3d %3d"         % (len(data.raw), len(data)))
    line.append("%5d %2d"         % (len(flash.raw), len(flash)))
    line.append("%3d %2d"         % (len(bank), bank.allocs))
    line.append("%4d %4d %5d %4d" % (bytecode.get_size(),
                                     len(bytecode.commands),
                                     len(bytecode.blocks),
                                     len(bytecode.get_gotos())))

    line.append("%4s" % (len(data.raw) + bytecode.get_size() + len(bank)))
    line.append('-')

    line = '| ' + str.join(' | ', line).replace(' 0 ', ' - ')
    lstlog.info(line)
    #exitmsg =  "\nfsm2data: '%s' generated. " % (final)
    #    exitmsg += '[It includes DEBUG info!]\n'

    log.log(ALL, "Generated succesfully")


class Config:
    set_variables = None
    debug = None

opts = Config()

if __name__ == "__main__":

    parser = optparse.OptionParser(usage=__doc__)
    parser.add_option('--debug',
                      action='store_true', dest='debug', default=False,
                      help='include debug messages')
    parser.add_option('-v', action='count', dest='verbosity',
                      help='Increase compiler verbosity')

    parser.add_option('-s', '--set_variables',
                      action='append', type="str",
                      help='set FSM variable values')


    (opts, args) = parser.parse_args()

    if len(args) != 3:
        sys.exit(usage())


    run(*args[:3])










