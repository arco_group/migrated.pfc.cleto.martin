/* -*- coding:utf-8;mode:c++ -*- */


%{ /*** C/C++ Declarations ***/

#include <string>
#include <map>
#include <sstream>
#include "Scanner.hh"

/* import the parser's token type into a local typedef */
typedef sis::Parser::token token;
typedef sis::Parser::token_type token_type;

/* By default yylex returns int, we use token_type. Unfortunately yyterminate
 * by default returns 0, which is not of token_type. */
#define yyterminate() return token::END

std::map<std::string, token_type> keywords;
%}


/*** Flex Declarations and Options ***/

/* enable c++ scanner class generation */
%option c++

/* change the name of the scanner class. results in "ExampleFlexLexer" */
%option prefix="sis"

/* the manual says "somewhat more optimized" */
%option batch

/* enable scanner to generate debug output. disable this for release
 * versions. */
%option debug

/* no support for include files is planned */
%option yywrap nounput

/* enables the use of start condition stacks */
%option stack

/* The following paragraph suffices to track locations accurately. Each time
 * yylex is invoked, the begin position is moved onto the end position. */
%{
#define YY_USER_ACTION  yylloc->columns(yyleng);
%}

START_COMMENT     "/*"
START_BLOCK       "{"
END_BLOCK         "}"
START_PAR         "("
END_P             ")"
DOT               "."
SEMICOLON         ";"
COLON             ":"
EQUAL             "="
COMMA             ","

COMMENT           "//"[^\n]*
FIXME             "FIXME"[^\n]*

BLANK             [ \t]
DIGIT1            [0-9]*
DIGIT2            [1-9]*
INT               (\+|-)?((0[0-7]+)|(0x[[:xdigit:]]+)|([[:digit:]]+))
ID                [[:alpha:]_][[:alnum:]_]*


%x COMMENT_STATE

%%

%{
  // reset location
  yylloc->step();
%}


{BLANK}+ yylloc->step ();
[\n]+    yylloc->lines (yyleng); yylloc->step ();

("+"|"-"|""){INT}   {

  yylval->ival = atoi(yytext);
  return token::INTEGER;
}

"{"     {return token::START_BLOCK;}
"}"     {return token::END_BLOCK;}
"("     {return token::START_P;}
")"     {return token::END_P;}



{COMMENT}        {/*Bye, comments!*/}
{FIXME}          {/*Bye, comments!*/}

{START_COMMENT}  {BEGIN(COMMENT_STATE);}

<COMMENT_STATE>[^*\n]*
<COMMENT_STATE>"*"+[^*/]*
<COMMENT_STATE>"\n" {}
<COMMENT_STATE>"*"+"/"        BEGIN(INITIAL);

";" {return token::SEMICOLON;}
"." {return token::DOT;}
"," {return token::COMMA;}
"=" {return token::EQUAL;}
":" {return token::COLON;}


{ID} {
  std::map<std::string, token_type>::const_iterator pos =
    keywords.find(std::string(yytext, yyleng));

  if(pos != keywords.end())
    return pos->second;

  yylval->sval = new std::string(yytext, yyleng);
  return token::ID;
}

. {
  std::string msg = *(yylloc->begin.filename);
  std::stringstream line;
  line << yylloc->begin.line;
  msg += ":" + line.str() + ": Ilegal character:'";
  msg += std::string(yytext) + "'";

  YY_FATAL_ERROR(msg.c_str());}

%%

namespace sis {

Scanner::Scanner(std::istream* in,
		 std::ostream* out)
    : sisFlexLexer(in, out)
{
  keywords["event"]    = token::EVENT;
  keywords["remote"]   = token::REMOTE;
  keywords["local"]    = token::LOCAL;
  keywords["input"]    = token::INPUT;
  keywords["output"]   = token::OUTPUT;
}

Scanner::~Scanner()
{
}

void Scanner::set_debug(bool b)
{
    yy_flex_debug = b;
}

}

/* This implementation of ExampleFlexLexer::yylex() is required to fill the
 * vtable of the class ExampleFlexLexer. We define the scanner's main yylex
 * function via YY_DECL to reside in the Scanner class instead. */

#ifdef yylex
#undef yylex
#endif

int sisFlexLexer::yylex()
{
    std::cerr << "in sisFlexLexer::yylex() !" << std::endl;
    return 0;
}

/* When the scanner receives an end-of-file indication from YY_INPUT, it then
 * checks the yywrap() function. If yywrap() returns false (zero), then it is
 * assumed that the function has gone ahead and set up `yyin' to point to
 * another input file, and scanning continues. If it returns true (non-zero),
 * then the scanner terminates, returning 0 to its caller. */

int sisFlexLexer::yywrap()
{
    return 1;
}

