// -*- mode:c++ -*-

#ifndef SISDRIVER_H
#define SISDRIVER_H

#include <string>
#include <map>
#include <vector>
#include <stdexcept>

namespace sis {

  class SISException : public std::runtime_error {
  public:
  SISException(const std::string& msg = "") : std::runtime_error(msg) {}
  };

  typedef enum {
    BOOL,
    BYTE,
    INT,
    SHORT,
    FLOAT,
    DOUBLE,
    LONG
  } Type;

  class SymbolTable;


  struct Event {
    std::string name;
    int code;
  };

  struct Method {
    std::string interface;
    std::string name;

    int code;

    std::vector<std::string> input;
    std::vector<std::string> output;

  };

  class SymbolTable {
  public:
    void addPrivateMethod(const Method& pr);
    void addPublicMethod(const Method& pu);
    void addEvent(const Event& e);

    std::vector<Method> getPublicMethods() const;
    std::vector<Method> getPrivateMethods() const;
    std::vector<Event> getEvents() const;

  private:
    std::vector<Method> _publics;
    std::vector<Method> _privates;
    std::vector<Event> _events;
  };

  class Visitor {
  public:
    Visitor(SymbolTable* t);
    ~Visitor();

    void visitPrivateMethod(const Method& priv);
    void visitPublicMethod(const Method& pub);
    void visitEvent(const Event& event);

  private:
    SymbolTable* _t;
  };

  class Visitable {
  public:
    virtual void accept(Visitor* v) = 0;
  };

  class Driver: public Visitable {
  public:
    Driver ();
    ~Driver ();

    void accept(Visitor* v);

    // Handling the scanner.
    void scanBegin ();
    void scanEnd ();
    bool trace_scanning;

    // Handling the parser.
    bool parse (const std::string& f);
    std::string file;
    bool trace_parsing;

    // Error handling.
    void error (const class location& l, const std::string& m);
    void error (const std::string& m);

    class Scanner* lexer;
    Visitor* visitor;
  };


}

#endif
