/* -*- C++ -*- */

%code top {
#include "Driver.h"
}

%code requires {
#include "Driver.h"
 }


%skeleton "lalr1.cc"
%require "2.4"
%defines
%define "parser_class_name" "Parser"

%define "namespace" "sis"

// The parsing context.
%parse-param { class Driver& driver }


%locations
%initial-action
{
  // Initialize the initial location.
  @$.begin.filename = @$.end.filename = &driver.file;
};

%debug
%error-verbose
%name-prefix="sis"

   // Symbols.
%union
{
  int          ival;
  std::string *sval;
};

%type <sval> value method_type attribute_type

%token START_BLOCK "'}'"
%token END_BLOCK "'{'"
%token START_P "')'"
%token END_P "'('"

%token SEMICOLON ';'
%token COLON     ':'
%token DOT  "'.'"
%token COMMA "','"
%token EQUAL "'='"

%token <ival> INTEGER "'integer value'"
%token <sval> ID  "'identifier'"

%token LOCAL "'local'"
%token REMOTE "'remote'"
%token EVENT "'event'"
%token INPUT "'input'"
%token OUTPUT "'output'"

%token END 0 "end of file"

%start manifest

%{

#include "Driver.h"
#include "Scanner.hh"

/* this "connects" the bison parser in the driver to the flex scanner class
 * object. it defines the yylex() function call to pull the next token from the
 * current lexer object of the driver context. */
#undef yylex
#define yylex driver.lexer->lex

using namespace sis;
using namespace std;

vector<string> params;
Method current;

string iface = "::";
%}


%%

manifest
: declaration {}
| manifest declaration{}
;

declaration
: event SEMICOLON {}
| method SEMICOLON {}
;

event
: EVENT ID START_P INTEGER END_P {
  Event e;
  e.name = *$2;
  e.code = $4;
  driver.visitor->visitEvent(e);
};

method_type
: REMOTE  { $$ = new string("remote");}
| LOCAL { $$ = new string("local");}
;


method
: method_type type ID START_P INTEGER END_P {

  current.interface = iface;
  current.name      = *$3;
  current.code      = $5;

  if (*$1 == "remote")
    driver.visitor->visitPublicMethod(current);
  else
    driver.visitor->visitPrivateMethod(current);

  iface = "::";
};

method
: method_type type ID START_P INTEGER END_P START_BLOCK definition END_BLOCK {

  current.interface = iface;
  current.name      = *$3;
  current.code      = $5;

  if (*$1 == "remote")
    driver.visitor->visitPublicMethod(current);
  else
    driver.visitor->visitPrivateMethod(current);
  iface = "::";
  current.input.clear();
  current.output.clear();
};


type:
| ID DOT ID { iface += *$1 + "::" + *$3; }
| type DOT ID { iface += "::" + *$3;}
;

definition
: attribute SEMICOLON
| definition attribute SEMICOLON
;

attribute_type
: INPUT { $$ = new string("input");}
| OUTPUT { $$ = new string("output"); }
;

attribute
: attribute_type EQUAL list {
  if (*$1 == "input")
    current.input  = params;
  else
    current.output = params;

  params.clear();
}
;

list
: value { params.push_back(*$1); }
|
| list COMMA value{params.push_back(*$3); }
;

value
: ID { *$$ = *$1;}
| ID COLON ID { *$$ = *$1 + ":" + *$3 ;}

%%

void
sis::Parser::error (const Parser::location_type& l,
		    const std::string& m)
{
  driver.error(l, m);
  throw SISException(m);
}
