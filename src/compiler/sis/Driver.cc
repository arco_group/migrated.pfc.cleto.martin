#include <cstdlib>
#include <fstream>
#include <sstream>

#include <easyc++/logger.h>

#include "Driver.h"
#include "Scanner.hh"
#include "config.h"


using namespace std;

namespace sis
{

  Driver::Driver():
    trace_scanning (false), trace_parsing (false) {
  }

  Driver::~Driver() {
    //if (env)
    //  delete(env);
  }

  void
  Driver::accept(Visitor* v) {
    visitor = v;
  }

  bool
  Driver::parse(const string& f)
  {
    file = f;

    ifstream in(file.c_str());
    if (!in.good())
      throw SISException("Can't open SIS file: '"
			      + file + "'");

    Scanner scanner(&in, NULL);
    scanner.set_debug(trace_scanning);
    this->lexer = &scanner;
    Parser parser(*this);
    parser.set_debug_level(trace_parsing);
    return (parser.parse() == 0);
  }

  void
  Driver::error (const class location& l, const string& m)
  {
    cerr << l << ": " << m << endl;
  }

  void
  Driver::error (const string& m)
  {
    cerr << m << endl;
  }

  void
  Driver::scanBegin ()
  {
  }

  void
  Driver::scanEnd ()
  {
  }

  //** VISITOR **

  Visitor::Visitor(SymbolTable* t) {
    _t = t;
  }

  Visitor::~Visitor() {}

  void
  Visitor::visitPrivateMethod(const Method& priv) {
    vector<Method> privates = _t->getPrivateMethods();

    if (not config.parse_only) {

      for (vector<Method>::const_iterator it = privates.begin();
	   it != privates.end(); it++) {
	if (it->code == priv.code) {
	  ec::error() << "Duplicated method code on SIS file: " << priv.code << endl;
	  exit(1);
	}

	if (it->interface == priv.interface and it->name == priv.name) {
	  ec::error() << "Duplicated method on SIS file: " << priv.name << endl;
	  exit(1);
	}
      }
    }

    _t->addPrivateMethod(priv);
  }

  void
  Visitor::visitPublicMethod(const Method& pub) {

    if (not config.parse_only) {

      vector<Method> publics = _t->getPublicMethods();

      for (vector<Method>::const_iterator it = publics.begin();
	   it != publics.end(); it++) {
	if (it->code == pub.code) {
	  ec::error() << "Duplicated method code on SIS file: " << pub.code << endl;
	  exit(1);
	}

	if (it->interface == pub.interface and it->name == pub.name) {
	  ec::error() << "Duplicated method on SIS file: " << pub.name << endl;
	  exit(1);
	}
      }
    }

    _t->addPublicMethod(pub);
  }

  void
  Visitor::visitEvent(const Event& event){
    if (not config.parse_only) {
      vector<Event> events = _t->getEvents();

      for (vector<Event>::const_iterator it = events.begin(); it != events.end(); it++) {
	if (it->name == event.name) {
	  ec::error() << "Duplicated event name on SIS file: " << event.name << endl;
	  exit(1);
	}
	if (it->code == event.code) {
	  ec::error() << "Duplicated event code on SIS file: " << event.code << endl;
	  exit(1);
	}
      }
    }
    _t->addEvent(event);
  }


  //** SYMBOLTABLE **

  void
  SymbolTable::addPrivateMethod(const Method& pr)
  {
    ec::debug() << "Added '" << pr.name
		<< "' as new PrivateMethod "
		<< "'" << pr.input.size() << ","
		<< pr.output.size() << "'"<< endl;


    _privates.push_back(pr);
  }

  void
  SymbolTable::addPublicMethod(const Method& pu)
  {
    ec::debug() << "Added '" << pu.name
		<< "' as new PublicMethod "
		<< endl;

    _publics.push_back(pu);
  }

  void
  SymbolTable::addEvent(const Event& e)
  {
    ec::debug() << "Added '" << e.name
		<< "' as new Event "
		<< endl;

    _events.push_back(e);
  }


  vector<Method>
  SymbolTable::getPublicMethods() const
  {
    return _publics;
  }

  vector<Method>
  SymbolTable::getPrivateMethods() const
  {
    return _privates;
  }

  vector<Event>
  SymbolTable::getEvents() const
  {
    return _events;
  }
}
