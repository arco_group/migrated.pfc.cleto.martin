#!/usr/bin/python
# -*- coding:utf-8 -*-


from backend_base import BackendBase
import sys
import os

class Backend(BackendBase):

    def __init__(self, optparser):
        BackendBase.__init__(self, optparser, "slice2fsm")


    def build(self, parser_config, st, options, args):

        foutput = os.path.basename(args[0]) + ".stat"

        # if options.output:
        # foutput = options.output

        sys.stdout = file(foutput, "w")


        print "file: " + args[0]

        print "Objetos locales:"

        for obj in st.getLocalObjects():
            print "- '%s'    %s" % (obj.getName(), obj.getInterface())

        print "Objetos remotos:"
        for obj in st.getRemoteObjects():
            line  = "- '%s'    %s" % (obj.getName(), obj.getInterface())
            adapter = st.getAdapterByObject(obj.getName())
            if not adapter.getEndpoint():
                line += '    Adapter: NO ESPECIFICADO'
            else:
                line += '    Adapter: "%s"' % adapter.getEndpoint()

            print line

        print "Eventos:"

        triggers = {"boot":st.getBootTriggers(),
                    "event":st.getEventTriggers(),
                    "when":st.getWhenTriggers(),
                    "timer":st.getTimerTriggers()}

        for name, lst in triggers.items():
            print "- %s: %s" % (name, len(lst))

        print "Invocaciones:"

        local = 0
        remote = 0

        for inv in st.getAllInvocations():

            obj = inv.getObject()
            if obj in st.getLocalObjects():
                local += 1
            else:
                remote += 1

        print "- locales: %s" % local
        print "- remotas: %s" % remote

        sys.stdout.flush()
        sys.stdout.close()

        return 0
