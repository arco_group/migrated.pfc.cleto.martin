// -*- coding:utf-8 mode:c++

#include <PluginManager.h>
#include <SemanticAnalyzer.h>
#include <easyc++/logger.h>
#include <easyc++/string.h>

using namespace std;
using namespace Semantic;

class DUO_Active: virtual IPKPlugin {

private:
  class dummy {};

  DUO_Active(const dummy&);
  static DUO_Active instance;

  void check_topic(const string& value);
  void check_publisher(const string& value);

  slc::SymbolTable _sst;
  ipk::SymbolTable _ist;
  sis::SymbolTable _mst;

public:
  void initialize(const slc::SymbolTable& sst,
		  const ipk::SymbolTable& ist,
		  const sis::SymbolTable& mst);

  void check (const string& key, const string& value);
};


DUO_Active DUO_Active::instance(*new dummy());

DUO_Active::DUO_Active(const dummy&) {
  ec::debug() <<  "DUO_Active plugin registered" << endl;

  PluginManager::reg("topic", this);
  PluginManager::reg("publisher", this);
}

void
DUO_Active::initialize(const slc::SymbolTable& sst,
		       const ipk::SymbolTable& ist,
		       const sis::SymbolTable& mst)
{
  _sst = sst;
  _ist = ist;
  _mst = mst;
}

void
DUO_Active::check(const string& key, const string& value) {

  try {
    if (key == "topic")
      check_topic(value);
    else
      check_publisher(value);
  }catch (Semantic::Exception e) {
    throw e;
  }
}

void
DUO_Active::check_topic(const string& value) {

  // FIXME : Comprobar que el objeto topic es IceStorm::TopicPrx
  //  if (ec::string::startswith(value, string("\"")))
  //     return;
  
  //   ipk::Object ob;
  //   try {
  //     ob = _ist.getObjectByName(value);
  //   } catch (ipk::ObjectNotFoundException e) {
  //     throw new SemanticException("Object '" + value + "' used as topic "
  // 				+ "not declared");
  //   }
  
  //   cout << ob.getIface() << endl;
  //   if (ob.getIface() == "::IceStorm::TopicPrx")
  //     return;
  
  //   vector<string> ifaces = ob.getIfaceParents();
  //   for (vector<string>::const_iterator it = ifaces.begin();
  //        it != ifaces.end(); it++) {
  //     if ((*it) == "::IceStorm::TopicPrx")
  //       return;
  //   }
  
  //   throw new SemanticException("Object '" + value + "' usead as 'topic' "
  // 			      + "does not implement ::IceStorm::TopicPrx");
  
}

void
DUO_Active::check_publisher(const string& value) {
  // FIXME : Comprobar que el objeto topic es escribible
  //  cout << "comprobaciones de publisher" << endl;
}


