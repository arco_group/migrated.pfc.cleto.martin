#!/usr/bin/python
# -*- coding:utf-8 -*-

import PluginManager as pm
from slice2fsm_utils import *
from fsm2data import toByteSeq

class IceStorm_Plugin(pm.Plugin):

    def __init__(self):
        pm.Plugin.__init__(self, "IceStorm", [plain("::IceStorm::Topic"),],
                           [plain("const ::IceStorm::QoS")])
        pm.PluginManager().reg(self)

        self.__st = None
        self.__fsm = None
        self.__ifaces = ["const ::IceStorm::Topic"]

    def initialize(self, st, fsm):
        self.__st    = st
        self.__fsm   = fsm
        self.__data  = self.__fsm.DATA
        self.__bank  = self.__fsm.BANK
        self.__flash = self.__fsm.FLASH

    def build_metho(self, block, iface, method):
        pass

    def getParamSize(self, name, type_):
        return 0

    def getSizeOf(self, type_):
        if plain(type_) in plain("const ::IceStorm::QoS"):
            return 1  # FIXME - Size of an empty QoS

    def serialize(self, t, p):
        assert p.getType() == ipk.DICTIONARY
        # FIXME - Solo vale para diccionarios vacios
        return [0]

    def build_param(self, type_, val, block):
        block.put_val(0)

IceStorm_Plugin()
