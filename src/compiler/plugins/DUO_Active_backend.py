#!/usr/bin/python
# -*- coding:utf-8 -*-

import PluginManager as pm
import slice2fsm_utils as utils
from fsm2data import toByteSeq
from slice2fsm_utils import SIZES, PROTO

class DUO_Active(pm.Plugin):

    def __init__(self):
        pm.Plugin.__init__(self, "DUO_Active",
                           [utils.plain("::DUO::Active::R"),
                            utils.plain("::DUO::Active::W")], [])

        pm.PluginManager().reg(self)
        self.__st = None
        self.__fsm = None
        self.__ifaces = ["::DUO::Active::R", "::DUO::Active::W"]

    def initialize(self, st, fsm):
        self.__st    = st
        self.__fsm   = fsm
        self.__data  = self.__fsm.DATA
        self.__bank  = self.__fsm.BANK
        self.__flash = self.__fsm.FLASH

    def build_method(self, block, iface, method):
        if utils.plain(iface) not in ["DUO_Active_R", "DUO_Active_W"]:
            raise Exception("Called Active plugin with an invalid iface: %s" %
                            iface)

        self.activesR = []
        self.activesW = []
        self.topics = {}    # key = topic / value =object oid
        self.publishers ={} # key = publisher / value = object oid

        for o in self.__st.getLocalObjects():
            if self.__st.isA(o, self.__ifaces[0]):
                self.activesR.append(o)
            if self.__st.isA(o, self.__ifaces[1]):
                self.activesW.append(o)

        if method.getName() == "getTopic":
            self.buildGetTopic(block)
        elif method.getName() == "getCb":
            self.buildGetPublisher(block)
        elif method.getName() == "setCbTopic":
            self.buildSetCbTopic(block)
        else:
            raise Exception("Method '%s' not valid" % method.getName())


#         # Get all topics and publishers per object

    def serializeData(self, data):
        retval = data
        if not data.startswith('"'):
            # topic especificado como objeto ipk
            obj  = self.__st.getObjectByName(data)
            adap = self.__st.getAdapterByObject(obj.getName())
            if (obj.getIdentity() and adap.getEndpoint()):
                retval = '"'+obj.getIdentity() + " " \
                    + utils.mode2str[obj.getMode()]   \
                    + ":" + adap.getEndpoint()  + '"'
            else:
                retval = obj.getName()

        return retval

    def buildGetTopic(self, block):
        if len(self.activesR) == 0:
            return

        for o in self.activesR:

            t = o.getAttributes()["topic"]
            val = self.serializeData(t)

            oid = "OID_%s" % o.getName()
            self.__fsm.constants[oid] = utils.digest(o.getIdentity())
            try:
                self.topics[val]
                block.goto_eq("'send_topic_%s'" % self.topics[val],
                              oid)
                continue
            except KeyError:
                self.topics[val] = o.getIdentity()

                block.goto_eq("'send_topic_%s'" % o.getName(), oid)
                b = self.__fsm.make_block("send_topic_%s" % o.getName())
                self.genGetCode("topic_%s" % o.getName(), val, b)

    def buildGetPublisher(self, block):
        if len(self.activesR) == 0:
            return

        for o in self.activesR:

            t = o.getAttributes()["publisher"]
            val = self.serializeData(t)

            oid = "OID_%s" % o.getName()
            self.__fsm.constants[oid] = utils.digest(o.getIdentity())

            try:
                self.publishers[val]
                block.goto_eq("'send_publisher_%s'" % self.publishers[val], oid)
                continue
            except KeyError:
                self.publishers[val] = o.getName()
                block.goto_eq("'send_publisher_%s'" % o.getName(), oid)
                b = self.__fsm.make_block("send_publisher_%s" % o.getName())
                self.genGetCode("publisher_%s" %o.getName(), val, b)

    def genGetCode(self, name, val, b):
        if val.startswith('"'):
            val = utils.IceProxy(val.strip())
            b.set_val("param1_len", val.size)
            b.call("'reply'")
            self.__flash[name] = val.packed()
            b.put_val(len(val.packed()))
            b.put_flash("flash_%s" % name)
        else:
            obj  = self.__st.getObjectByName(val)
            adap = self.__st.getAdapterByObject(val)
            ep   = adap.getEndpoint()

            assert ep in ["tcp", "udp", "xbow"], \
                "Active::R endpoint must be 'tcp', 'udp' or 'xbow'"

            ep_type = ep[0].upper()

            total_size = 6 + 1 + SIZES["%s_IDENTITY_SIZE" % ep.upper()] \
                + 7 + 1 + SIZES["%s_ADDRESS_SIZE" % ep.upper()] \
                + SIZES["%s_PORT_SIZE" % ep.upper()]

            if ep == "tcp":
                total_size += 5

            b.set_val("param1_len", total_size)
            b.call("'reply'")

            b.put_val(SIZES["%s_IDENTITY_SIZE" % ep.upper()])
            b.put_flash("flash_%s_identity" % obj.getName())

            data = [0,0,0,0] + [PROTO[ep]] + [1]
            key = self.__data.alloc("%s_twoway" % ep, data)
            b.mark_put_data("data_%s" % key)

            b.set_w(total_size - SIZES["%s_IDENTITY_SIZE" % ep.upper()] \
                        - 7 - 7)
            b.template("data_IceRetSeq", 0)
            b.put_val(SIZES["%s_ADDRESS_SIZE" % ep.upper()])

            b.set_w(SIZES["%s_ENDPOINT_SIZE" % ep.upper()])
            b.put_flash("flash_%s_socket_%s" % (adap.getName(), ep_type))

            if ep == "tcp":
                key = self.__data.alloc("tcp_tail", [0,0,255,255,255,255,0])
                b.mark_put_data("data_%s" % key)

        b.goto("'ice_close'")

# #     p = o.getAttributes()["publisher"]

# #                 if not p.startswith('"'):
# #                     obj  = self.__st.getObjectByName(p)

# #                     adap = self.__st.getAdapterByObject(obj.getName())

# #                     if (obj.getIdentity() and adap.getEndpoint()):
# #                         p = '"'+obj.getIdentity() + " " \
# #                             + mode2str[obj.getMode()]   \
# #                             + ":" + adap.getEndpoint()  + '"'
# #                     else:
# #                         p = obj.getName()

# #                 if p in self.publishers.keys():
# #                     if o.getIdentity() not in self.publishers[p]:
# #                         self.publishers[p].append(o.getIdentity())
# #                 else:
# #                     self.publishers[p] = [o.getIdentity()]


#         data = None
#         name = ""

#         if method.getName() == "getTopic":
#             data = self.topics
#             name = "topic"
#         elif method.getName() == "getCb":
#             data = self.publishers
#             name = "publisher"

#         elif method.getName() == "setCbTopic":
#             self.buildSetCbTopic(block)
#             return
#         else:
#             raise NotImplementedError

#         # Build the get's methods
#         for i, (val, objs) in enumerate(data.items()):
#             for o in objs:
#                 block.goto_eq("'send_%s_%s'" % (name, i),
#                               digest(o))

# #            param_block = self.__fsm.make_block("send_%s_%s" % (name, i))
#             b = self.__fsm.make_block("send_%s_%s" % (name, i))
#             if val.startswith('"'):
#                 val = IceProxy(val.strip())
#                 key = "%s_%s" % (name, i)

#                 b.set_val("param1_len", val.size)
#                 b.call("'reply'")
#                 self.__flash[key] = val.packed()
#                 b.put_val(len(val.packed()))
#                 b.put_flash("flash_%s" % key)
#                 b.goto("'ice_close'")
#             else:
#                 obj  = self.__st.getObjectByName(val)
#                 adap = self.__st.getAdapterByObject(val)
#                 ep   = adap.getEndpoint()

#                 assert ep in ["tcp", "udp", "xbow"], \
#                     "Active::R endpoint must be 'tcp', 'udp' or 'xbow'"

#                 total_size = 6 + 1 + SIZES["%s_IDENTITY_SIZE" % ep.upper()] \
#                     + 7 + 1 + SIZES["%s_ADDRESS_SIZE" % ep.upper()] \
#                     + SIZES["%s_PORT_SIZE" % ep.upper()]

#                 if ep == "tcp":
#                     total_size += 5

#                 b.set_val("param1_len", total_size)
#                 b.call("'reply'")

#                 b.put_val(SIZES["%s_IDENTITY_SIZE"] % ep.upper())
#                 b.put_flash("flash_%s_identity" % obj.getName())

#                 data = [0,0,0,0].extend(PROTO[ep]).append(1)
#                 key = self.__data.alloc("%s_twoway" % ep, data)
#                 b.mark_put_data("data_%s" % key)

#                 b.set_w(total_size - SIZES["%s_IDENTITY_SIZE" % ep.upper()] \
#                             - 7 - 7)
#                 b.template("data_IceRetSeq", 0)
#                 b.put_val(SIZES["%s_ADDRESS_SIZE" % ep.upper()])

#                 b.set_w(SIZES["%s_ENDPOINT_SIZE" % ep.upper()])
#                 b.put_flash("flash_%s_socket" % adap.getName())

#                 if ep == "tcp":
#                     key = self.__data.alloc("tcp_tail", [0,0,255,255,255,255,0])
#                     b.mark_put_data("data_%s" % key)

#                 b.goto("'ice_close'")

    def buildSetCbTopic(self, block):
        objs = self.__st.getLocalObjects()

        block.move(0, "identity")
        block.assure([0, 0])
        block.skip()
        block.assure([0, 0, 0, 1, 0])

        blocks = {} # local_object:block
        for o in self.activesW:
            self.__fsm.constants["OID_%s" % o.getName()] = \
                utils.digest(o.getIdentity())
            block.goto_eq("'SetCbTopic_%s'" % o.getName(),
                          utils.digest(o.getIdentity()))
            blocks[o] = self.__fsm.make_block("SetCbTopic_%s" % o.getName())

        # Implementar los nuevos bloques
        for o, b in blocks.items():

            for key in ["publisher", "topic"]:
                name = o.getAttributes()[key]
                other   = self.__st.getObjectByName(name)
                adapter = self.__st.getAdapterByObject(name)

                b.call("'Get_%s_%s'" % (key,o.getName()))
                the_block = self.__fsm.get_block("Get_%s_%s" % (key,
                                                                o.getName()))

                if the_block:
                    continue

                the_block = self.__fsm.make_block("Get_%s_%s" % (key,
                                                                 o.getName()))
                the_block.epilogo.ret()

                # pub_block.assure([0, 0, (6+IDENTITY_SIZE+12+TCP_ENDPOINT_SIZE+7)*2,
                # 0, 0, 0, 1, 0])

                the_block.get_w()

                assert len(other.getIdentity()) == 0, \
                    "%s attribute need an empty identity" % key

                ep = adapter.getEndpoint()
                assert ep in ["tcp", "udp", "xbow"], \
                    "Active::W endpoint must be 'tcp', 'udp' or 'xbow'"

                ep_type = ep[0].upper()

                self.__flash["%s_identity" % name] = SIZES["%s_IDENTITY_SIZE"
                                                           % ep.upper()]
                the_block.get_flash("flash_%s_identity" % name,
                                    SIZES["%s_IDENTITY_SIZE" % ep.upper()])

                the_block.assure([0,0])
                the_block.skip()

                assures = [0, 1] # Secure, numero de endpoints
                if ep == "tcp":
                    # initial_size + ip_size + ip + port + tcp_tail
                    assures.extend([1,0, 6 + 1
                                    + SIZES["TCP_ADDRESS_SIZE"]
                                    + SIZES["TCP_PORT_SIZE"] + 5])
                elif ep == "udp":
                    assures.append([3,0,31]) # FIXME - poner los de UDP
                elif ep == "xbow":
                    assures.append([18,0, 4 + 2 + 2 + SIZES["XBOW_ENDPOINT_SIZE"]])

                assures.extend([0,0,0,1,0])
                if ep == "xbow":
                    assures.extend([1,0])

                the_block.assure(assures)
                s = SIZES["%s_ENDPOINT_SIZE" % ep.upper()]
                if not ep == "xbow":
                    the_block.skip()

                the_block.set_w(s)
                self.__flash["%s_socket_%s" % (adapter.getName(),
                                               ep_type)] = s
                the_block.get_flash("flash_%s_socket_%s" % (adapter.getName(),
                                                            ep_type),
                                    s)

                if ep == "tcp":
                    the_block.assure([0,0,255,255,255,255,0])

            b.set_val("param1_len", 0)
            b.goto("'reply'")

    def getParamSize(self, name, type_):

#         if utils.plain(type_) == "IceStorm_TopicPrx":
#             # buscar el topic mayor de los objetos locales
#             for obj in st.getLocalObjects():
#                 if len(obj.getAttributes()) == 0:
#                     raise Exception ("'topic' attibute is needed for '"
#                                      + obj.getName()+ "' object")

#                 for k,v in obj.getAttributes().items():
#                     if k != "topic":
#                         continue

#                 v = v.strip('"')
#                 retval = max(retval, len(v))


#         elif utils.plain(type_) == "ObjectPrx":
#             # buscar el proxy del publisher mas largo
#             for obj in st.getLocalObjects():
#                 if len(obj.getAttributes()) == 0:
#                     raise Exception ("'publisher' attibute is needed for '"
#                                      + obj.getName()+ "' object")

#                 for k,v in obj.getAttributes().items():
#                     if k != "publisher":
#                         continue

#                 v = v.strip('"')
#                 retval = max(retval, len(v))
        return 0

DUO_Active()
